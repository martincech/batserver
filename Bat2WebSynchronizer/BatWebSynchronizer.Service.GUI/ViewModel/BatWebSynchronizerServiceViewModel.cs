using System.Collections.Generic;
using System.Linq;
using Bat2WebSynchronizer.Tray.ViewModel;
using BatWebSynchronizer.Device;
using BatWebSynchronizer.PermanentStorage;
using Desktop.Wpf.Applications;

namespace BatWebSynchronizer.Service.GUI.ViewModel
{
   public class BatWebSynchronizerServiceViewModel : ServiceViewModel
   {
      #region Private fields

      private readonly IStoredData storage;
      private readonly WebConnectivityViewModel connectivityVm;
      private readonly EloSettingsViewModel eloSettingsVm;

      #endregion

      #region Constructor

      public BatWebSynchronizerServiceViewModel(
         IStoredData storage,
         WebConnectivityViewModel connectivityVm,
         EloSettingsViewModel eloSettingsVm) 
         : base(SynchronizerService.NameOfService)
      {
         this.storage = storage;
         this.connectivityVm = connectivityVm;
         this.connectivityVm.SaveLoginInfoCommand = new RelayCommand(SaveLogin);

         this.eloSettingsVm = eloSettingsVm;
         eloSettingsVm.SaveEloSettingsCommand = new RelayCommand(SaveEloInfo);
         eloSettingsVm.SynchronizeNowCommand = new RelayCommand(SynchronizeNow);
         eloSettingsVm.ScanNowCommand = new RelayCommand(ScanScalesNow);
      }

      #endregion

      public void SaveLogin(LoginInfo loginInfo)
      {
         storage.SaveLoginInfo(loginInfo);
         SendCommandToService((int) SynchronizerService.ServiceCommands.ReloadSynchronizationInfo);
      }

      public void SaveEloInfo(IEnumerable<Elo> elos)
      {
         storage.SaveEloSettings(elos);
         SendCommandToService((int) SynchronizerService.ServiceCommands.ReloadElosInfo);
      }

      public void SynchronizeNow()
      {
         SendCommandToService((int)SynchronizerService.ServiceCommands.SynchronizeNow);
      }

      public void ScanScalesNow()
      {
         SendCommandToService((int)SynchronizerService.ServiceCommands.ScanForScalesNow);
      }

      #region Private helpers

      private void SaveEloInfo()
      {
         SaveEloInfo(eloSettingsVm.Devices);
      }

      private void SaveLogin()
      {
         SaveLogin(new LoginInfo
         {
            ServerUrl = connectivityVm.ServerUrl,
            Password = connectivityVm.Password,
            UserName = connectivityVm.UserName
         });
      }

      protected override void UpdateServiceInfo()
      {
         var synced = storage.LoadSyncedData().Count();
         var unsynced = storage.LoadUnsyncedData().Count();
         connectivityVm.SyncedCount = synced;
         connectivityVm.ReceivedCount = unsynced + synced;
      }

      #endregion
   }
}