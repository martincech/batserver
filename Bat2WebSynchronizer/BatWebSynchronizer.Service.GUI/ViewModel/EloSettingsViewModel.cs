﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Timers;
using System.Windows;
using System.Windows.Input;
using BatWebSynchronizer.Device;
using BatWebSynchronizer.PermanentStorage;
using MvvmFramework.Observable;

namespace BatWebSynchronizer.Service.GUI.ViewModel
{
   public class EloSettingsViewModel : ObservableObject
   {
      #region Private fields

      private readonly IStoredData storage;
      private ICommand saveEloSettingsCommand;
      private ICommand scanNowCommand;
      private ICommand synchronizeNowCommand;
      private ObservableCollection<Elo> devices;
      private const int TIMER_INTERVAL_IN_MS = 5000;
      private readonly Timer timer;

      #endregion

      public EloSettingsViewModel(IStoredData storageData)
      {
         storage = storageData;
         Devices = new ObservableCollection<Elo>(storage.LoadEloSettings());

         timer = new Timer(TIMER_INTERVAL_IN_MS);
         timer.Elapsed += ReloadSettingsFromFile;
         timer.Enabled = true;
      }

      public ICommand SaveEloSettingsCommand
      {
         get { return saveEloSettingsCommand; }
         set { SetProperty(ref saveEloSettingsCommand, value); }
      }

      public ObservableCollection<Elo> Devices { get { return devices; } private set { SetProperty(ref devices, value); } }

      public ICommand ScanNowCommand
      {
         get { return scanNowCommand; }
         set { SetProperty(ref scanNowCommand, value); }
      }

      public ICommand SynchronizeNowCommand
      {
         get { return synchronizeNowCommand; }
         set { SetProperty(ref synchronizeNowCommand, value); }
      }

      private void ReloadSettingsFromFile(object sender, ElapsedEventArgs elapsedEventArgs)
      {
         timer.Enabled = false;
         var newDevices = storage.LoadEloSettings();

         Application.Current.Dispatcher.Invoke(() =>
         {
            foreach (var dev in newDevices)
            {
               var item = Devices.FirstOrDefault(x => x.SerialNumber == dev.SerialNumber);
               if (item == null)
               {  // new device
                  Devices.Add(dev);
               }
               else
               {  // update device
                  dev.SynchronizationPeriod = item.SynchronizationPeriod;

                  Devices.Remove(item);
                  Devices.Add(dev);
               }
            }
         });
         timer.Enabled = true;
      }
   }
}
