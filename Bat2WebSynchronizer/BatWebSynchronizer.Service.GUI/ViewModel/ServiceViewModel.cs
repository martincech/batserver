using System;
using System.Linq;
using System.ServiceProcess;
using System.Threading;
using Desktop.Wpf.Applications;
using MvvmFramework.Observable;
using Timer = System.Timers.Timer;

namespace BatWebSynchronizer.Service.GUI.ViewModel
{
   public class ServiceViewModel : ObservableObject, IDisposable
   {
      #region Private fields

      private const int SERVICE_CHECK_PERIOD = 1000;
      private string name;
      private RelayCommand start;
      private RelayCommand stop;
      private ServiceController serviceController;
      private readonly Timer healthCheckTimer;

      #endregion

      #region Public interfaces

      public ServiceViewModel(string name)
      {
         Name = name;
         if (ServiceExists)
         {
            InitServiceController();
         }
         healthCheckTimer = new Timer
         {
            AutoReset = true,
            Interval = SERVICE_CHECK_PERIOD
         };
         healthCheckTimer.Elapsed += (sender, args) => ServiceHealthCheck();
         healthCheckTimer.Start();
      }

      #region Commands

      public RelayCommand Start
      {
         get
         {
            return start ?? (start = new RelayCommand(
               () => { ChangeServiceState(c => c.Start()); },
               () =>
                  IsServiceHealthy &&
                  State != SynchronizerService.ServiceState.ServiceRunning &&
                  State != SynchronizerService.ServiceState.ServiceStartPending
               ));
         }
      }

      public RelayCommand Stop
      {
         get
         {
            return stop ?? (stop = new RelayCommand(
               () => { ChangeServiceState(c => c.Stop()); },
               () =>
                  IsServiceHealthy &&
                  State != SynchronizerService.ServiceState.ServiceStopped &&
                  State != SynchronizerService.ServiceState.ServiceStopPending &&
                  serviceController.CanStop
               ));
         }
      }

      #endregion

      #region Public properties

      public string Name
      {
         get { return name; }
         private set { SetProperty(ref name, value); }
      }

      public SynchronizerService.ServiceState State
      {
         get
         {
            return
               serviceController == null
                  ? SynchronizerService.ServiceState.ServiceShutdown
                  : (SynchronizerService.ServiceState) serviceController.Status;
         }
      }

      public bool Running
      {
         get { return State == SynchronizerService.ServiceState.ServiceRunning; }
      }

      public void Dispose()
      {
         healthCheckTimer.Stop();
         using (healthCheckTimer)
         {
         }
      }

      #endregion

      #endregion

      #region Private helpers

      private void InitServiceController()
      {
         try
         {
            serviceController = new ServiceController(Name);
         }
         catch
         {
         }
      }

      private bool ServiceExists
      {
         get { return ServiceController.GetServices().Any(s => s.ServiceName == Name); }
      }

      private void ServiceHealthCheck()
      {
         if (!Monitor.TryEnter(healthCheckTimer)) return;

         try
         {
            var exists = ServiceExists;
            if (exists && serviceController == null)
            {
               InitServiceController();
            }
            else if (!exists && serviceController != null)
            {
               using (serviceController)
               {
               }
               serviceController = null;
            }
            if (IsServiceHealthy)
            {
               UpdateServiceInfo();
            }
            ServiceConditionChanged();
         }
         finally
         {
            Monitor.Exit(healthCheckTimer);
         }
      }

      protected virtual void UpdateServiceInfo()
      {
      }

      private void ServiceConditionChanged()
      {
         if (serviceController != null)
         {
            serviceController.Refresh();
         }
         RaisePropertyChanged(() => State);
         RaisePropertyChanged(() => Running);
         try
         {
            Start.RaiseCanExecuteChanged();
            Stop.RaiseCanExecuteChanged();
         }
         catch (Exception e)
         {
            Console.WriteLine(e);
         }
      }

      private void ChangeServiceState(Action<ServiceController> action)
      {
         try
         {
            if (IsServiceHealthy)
            {
               action(serviceController);
            }
         }
         catch
         {
         }
      }

      private bool IsServiceHealthy
      {
         get
         {
            return serviceController != null &&
                   (State != SynchronizerService.ServiceState.ServiceShutdown ||
                    State != SynchronizerService.ServiceState.ServiceShutdownPending);
         }
      }

      protected void SendCommandToService(int command)
      {
         ChangeServiceState(c => c.ExecuteCommand(command));
      }

      #endregion
   }
}