using System.Collections.ObjectModel;
using Bat2WebSynchronizer.Tray.ViewModel;

namespace BatWebSynchronizer.Service.GUI.ViewModel
{
   public class ServiceGuiMainWindowVm : MainVm
   {
      #region Private fields

      private ObservableCollection<ServiceViewModel> services;
      private ServiceViewModel selectedService;
      private EloSettingsViewModel eloSettingsViewModel;

      #endregion

      public ServiceGuiMainWindowVm()
      {
         Services = new ObservableCollection<ServiceViewModel>();
      }

      #region Public properties

      public ObservableCollection<ServiceViewModel> Services
      {
         get { return services; }
         private set { SetProperty(ref services, value); }
      }

      public ServiceViewModel SelectedService
      {
         get { return selectedService; }
         set { SetProperty(ref selectedService, value); }
      }

      public EloSettingsViewModel EloSettings
      {
         get { return eloSettingsViewModel; }
         set { SetProperty(ref eloSettingsViewModel, value); }
      }

      #endregion
   }
}