using Bat2WebSynchronizer.Tray.ViewModel;
using BatWebSynchronizer.PermanentStorage;
using BatWebSynchronizer.Service.GUI.ViewModel;
using BatWebSynchronizer.Synchronizer;

namespace BatWebSynchronizer.Service.GUI
{
   public class Backend
   {
      private WebConnectivityViewModel connectivityVm;
      private readonly FileStorage storage;
      private readonly IWebApi api;
      private BatWebSynchronizerServiceViewModel service;
      private EloSettingsViewModel eloSettings;

      /// <summary>
      /// Initializes a new instance of the <see cref="T:System.Object"/> class.
      /// </summary>
      public Backend()
      {
         storage = new FileStorage();
         api = new FakeApi();
         ServiceGuiMainVm = new ServiceGuiMainWindowVm();
         InitConnectivityVm();
         InitSettings();
         InitServices();
      }

      public ServiceGuiMainWindowVm ServiceGuiMainVm { get; private set; }

      private void InitServices()
      {
         service = new BatWebSynchronizerServiceViewModel(storage, connectivityVm, eloSettings);
         ServiceGuiMainVm.Services.Add(service);
      }

      private void InitConnectivityVm()
      {
         var loginInfo = storage.LoadLoginInfo();
         connectivityVm = new WebConnectivityViewModel(loginInfo, api);
         ServiceGuiMainVm.ConnectivityViewModel = connectivityVm;
      }

      private void InitSettings()
      {
         eloSettings = new EloSettingsViewModel(storage);
         ServiceGuiMainVm.EloSettings = eloSettings;
      }
   }
}