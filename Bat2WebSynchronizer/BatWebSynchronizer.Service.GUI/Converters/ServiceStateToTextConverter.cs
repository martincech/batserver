﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace BatWebSynchronizer.Service.GUI.Converters
{
   public class ServiceStateToTextConverter : IValueConverter
   {
      public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
      {
         var state = value as SynchronizerService.ServiceState? ?? 0;
         switch (state)
         {
            case SynchronizerService.ServiceState.ServiceStopped:
               return Service.Properties.Resources.Service_stopped;
            case SynchronizerService.ServiceState.ServiceStartPending:
               return Service.Properties.Resources.Service_start_pending;
            case SynchronizerService.ServiceState.ServiceStopPending:
               return Service.Properties.Resources.Service_stop_pending;
            case SynchronizerService.ServiceState.ServiceRunning:
               return Service.Properties.Resources.Service_running;
            case SynchronizerService.ServiceState.ServiceContinuePending:
               return Service.Properties.Resources.Service_continue_pending;
            case SynchronizerService.ServiceState.ServicePausePending:
               return Service.Properties.Resources.Service_pause_pending;
            case SynchronizerService.ServiceState.ServicePaused:
               return Service.Properties.Resources.Service_paused;
            case SynchronizerService.ServiceState.ServiceShutdownPending:
               return Service.Properties.Resources.Service_shutdown_pending;
            default:
               return Service.Properties.Resources.Service_Shutdown;
         }
      }

      public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
      {
         throw new NotImplementedException();
      }
   }
}
