﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace BatWebSynchronizer.Service.GUI.Converters
{
   public class MinutesToMsConverter : IValueConverter
   {
      public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
      {
         return (value is int ? (int)value / (60 * 1000) : 0);
      }

      public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
      {
         return (value is int ? (int)value * 60 * 1000 : 0);         
      }
   }
}
