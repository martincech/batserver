﻿using System;
using System.Diagnostics;
using System.Windows;
using Windows;
using BatWebSynchronizer.Service.GUI.View;
using Desktop.Wpf;
using Desktop.Wpf.Presentation;

namespace BatWebSynchronizer.Service.GUI
{
   public partial class App
   {

      public App()
      {
         if (SingleApplication.IsAlreadyRunning())
         {
            Environment.Exit(0);
         }
         InitializeComponent();
         DispatcherHelper.Initialize();
      }

      [STAThread]
      [DebuggerNonUserCode]
      public static void Main()
      {
         AppDomain.CurrentDomain.UnhandledException += CurrentDomainOnUnhandledException;

         // init frontend
         var mainWindow = new MainWindow();
         // init backend         
         var backend = new Backend();
         mainWindow.DataContext = backend.ServiceGuiMainVm;
         //run
         var application = new App { MainWindow = mainWindow };
         mainWindow.Show();
         application.Run();
      }     

      private static void CurrentDomainOnUnhandledException(object sender, UnhandledExceptionEventArgs e)
      {
         var ex = (Exception)e.ExceptionObject;
         Logger.Write("Unhandled Error\n\t\t" + ex.Message + "\n\t\t" + ex.StackTrace + "\n");

         MessageBox.Show(ex.Message + ex.StackTrace,
            "Error", MessageBoxButton.OK, MessageBoxImage.Stop);
      }
   }
}
