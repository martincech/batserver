﻿using System;
using System.Threading;
using System.Threading.Tasks;
using BatWebSynchronizer.PermanentStorage;
using BatWebSynchronizer.Synchronizer;

namespace BatWebSynchronizer.Service.GUI
{
   public class FakeApi : IWebApi
   {
      #region Implementation of IWebApi

      public Task<bool> Login(LoginInfo loginInfo, CancellationToken cancelToken = new CancellationToken())
      {
         throw new NotImplementedException();
      }

      public Task<bool> SyncStatistic(StatisticData message, CancellationToken cancelToken = new CancellationToken())
      {
         throw new NotImplementedException();
      }

      public bool IsLogged { get; private set; }
      public bool IsCommunicating { get; private set; }
      public event EventHandler ConnectivityStatusChanged;

      #endregion

      protected virtual void OnConnectivityStatusChanged()
      {
         var handler = ConnectivityStatusChanged;
         if (handler != null) handler(this, EventArgs.Empty);
      }
   }
}