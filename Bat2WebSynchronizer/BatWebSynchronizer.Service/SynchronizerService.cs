﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.ServiceProcess;
using BatWebSynchronizer.Device;
using BatWebSynchronizer.PermanentStorage;
using BatWebSynchronizer.Service.Properties;
using BatWebSynchronizer.Synchronizer;

namespace BatWebSynchronizer.Service
{
   public partial class SynchronizerService : ServiceBase, ILogger
   {

      public static string NameOfService = "BatWebSynchronizer";
      public enum ServiceState
      {
         ServiceStopped = 0x00000001,
         ServiceStartPending = 0x00000002,
         ServiceStopPending = 0x00000003,
         ServiceRunning = 0x00000004,
         ServiceContinuePending = 0x00000005,
         ServicePausePending = 0x00000006,
         ServicePaused = 0x00000007,
         ServiceShutdownPending = 0x00000008,
         ServiceShutdown = 0x00000009
      }
      [StructLayout(LayoutKind.Sequential)]
      public struct ServiceStatus
      {
         public long dwServiceType;
         public ServiceState dwCurrentState;
         public long dwControlsAccepted;
         public long dwWin32ExitCode;
         public long dwServiceSpecificExitCode;
         public long dwCheckPoint;
         public long dwWaitHint;
      }

      public enum ServiceCommands
      {
         ReloadSynchronizationInfo = 128,
         ReloadElosInfo,
         SynchronizeNow,
         ScanForScalesNow
      }
      
      [DllImport("advapi32.dll", SetLastError = true)]
      private static extern bool SetServiceStatus(IntPtr handle, ref SynchronizerService.ServiceStatus serviceStatus);

      private readonly CollectorsToWebSynchronizer collectorsToWebSynchronizer;

      public SynchronizerService()
      {
         InitializeComponent();
         ServiceName = NameOfService;
         eventLog = new EventLog();
         try
         {
            if (!EventLog.SourceExists(NameOfService))
            {
               EventLog.CreateEventSource(
                  NameOfService, NameOfService);
            }
            eventLog.Source = NameOfService;
            eventLog.Log = NameOfService;
            var storage = new FileStorage();
            var api = new WebApi();
            collectorsToWebSynchronizer = new CollectorsToWebSynchronizer(storage, api, this);
         }
         catch (Exception e)
         {
            Console.WriteLine(e);
         }
      }


      protected override void OnStart(string[] args)
      {
         UpdateState(ServiceState.ServiceStartPending);
         collectorsToWebSynchronizer.Start();
         base.OnStart(args);
         UpdateState(ServiceState.ServiceRunning);
      }

      protected override void OnStop()
      {
         UpdateState(ServiceState.ServiceStopPending);
         collectorsToWebSynchronizer.Stop();
         base.OnStop();
         UpdateState(ServiceState.ServiceStopped);
      }


      protected override void OnContinue()
      {
         UpdateState(ServiceState.ServiceContinuePending);
         collectorsToWebSynchronizer.Start();
         base.OnContinue();
         UpdateState(ServiceState.ServiceRunning);
      }

      /// <summary>
      /// When implemented in a derived class, executes when a Pause command is sent to the service by the Service Control Manager (SCM). Specifies actions to take when a service pauses.
      /// </summary>
      protected override void OnPause()
      {
         UpdateState(ServiceState.ServicePausePending);
         collectorsToWebSynchronizer.Stop();
         base.OnPause();
         UpdateState(ServiceState.ServicePaused);
      }

      /// <summary>
      /// When implemented in a derived class, executes when the system is shutting down. Specifies what should occur immediately prior to the system shutting down.
      /// </summary>
      protected override void OnShutdown()
      {
         UpdateState(ServiceState.ServiceShutdownPending);
         collectorsToWebSynchronizer.Stop();
         base.OnShutdown();
         UpdateState(ServiceState.ServiceShutdown);
      }

      public void Log(string message)
      {
         Log(DateTime.Now, message);
      }

      public void Log(DateTime time, string message)
      {
         WriteInfo(message);
      }

      protected override void OnCustomCommand(int command)
      {
         switch ((ServiceCommands)command)
         {
            case ServiceCommands.ReloadSynchronizationInfo:
               collectorsToWebSynchronizer.ReloadSynchronizer();
               break;
            case ServiceCommands.ReloadElosInfo:
               collectorsToWebSynchronizer.ReloadElos();
               break;
            case ServiceCommands.ScanForScalesNow:
               foreach (var elo in collectorsToWebSynchronizer.DeviceList.Where(d => d is Elo).Cast<Elo>())
               {
                  elo.ScanForNewScales();
               }
               break;
            case ServiceCommands.SynchronizeNow:
               foreach (var elo in collectorsToWebSynchronizer.DeviceList.Where(d => d is Elo).Cast<Elo>())
               {
                  elo.ReadDataFromScales();
               }
               break;
         }
         base.OnCustomCommand(command);
      }


      private void UpdateState(ServiceState state)
      {
         var serviceStatus = new ServiceStatus
         {
            dwCurrentState = state, dwWaitHint = 100000
         };
         SetServiceStatus(ServiceHandle, ref serviceStatus);
         switch (state)
         {
            case ServiceState.ServiceStopped:
               WriteInfo(Resources.Service_stopped);
               break;
            case ServiceState.ServiceStartPending:
               WriteInfo(Resources.Service_start_pending);
               break;
            case ServiceState.ServiceStopPending:
               WriteInfo(Resources.Service_stop_pending);
               break;
            case ServiceState.ServiceRunning:
               WriteInfo(Resources.Service_running);
               break;
            case ServiceState.ServiceContinuePending:
               WriteInfo(Resources.Service_continue_pending);
               break;
            case ServiceState.ServicePausePending:
               WriteInfo(Resources.Service_pause_pending);
               break;
            case ServiceState.ServicePaused:
               WriteInfo(Resources.Service_paused);
               break;
            case ServiceState.ServiceShutdownPending:
               WriteInfo(Resources.Service_shutdown_pending);
               break;
            case ServiceState.ServiceShutdown:
               WriteInfo(Resources.Service_Shutdown);
               break;
            default:
               throw new ArgumentOutOfRangeException("state", state, null);
         }
      }

      private void WriteInfo(string message)
      {
         EventLog.WriteEntry(message, EventLogEntryType.Information);
      }
   }
}
