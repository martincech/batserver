﻿using System.ServiceProcess;

namespace BatWebSynchronizer.Service
{
   public static class Program
   {
      /// <summary>
      /// The main entry point for the application.
      /// </summary>
      public static void Main()
      {
         var servicesToRun = new ServiceBase[]
         {
            new SynchronizerService()
         };
         ServiceBase.Run(servicesToRun);
      }
   }
}
