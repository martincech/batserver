﻿using System.ComponentModel;

namespace BatWebSynchronizer.Service
{
   [RunInstaller(true)]
   public partial class ProjectInstaller : System.Configuration.Install.Installer
   {
      public ProjectInstaller()
      {
         InitializeComponent();
      }

      private void BatWebSynchronizerProcessInstallaer_AfterInstall(object sender, System.Configuration.Install.InstallEventArgs e)
      {

      }
   }
}
