﻿namespace BatWebSynchronizer.Service
{
   partial class ProjectInstaller
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary> 
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Component Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
         this.BatWebSynchronizerInstaller = new System.ServiceProcess.ServiceInstaller();
         this.BatWebSynchronizerProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
         // 
         // BatWebSynchronizerInstaller
         // 
         this.BatWebSynchronizerInstaller.Description = "Synchronization service to get data from BAT2 to web.";
         this.BatWebSynchronizerInstaller.DisplayName = "BatWebSynchronizer";
         this.BatWebSynchronizerInstaller.ServiceName = "BatWebSynchronizer";
         this.BatWebSynchronizerInstaller.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
         // 
         // BatWebSynchronizerProcessInstaller
         // 
         this.BatWebSynchronizerProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
         this.BatWebSynchronizerProcessInstaller.Password = null;
         this.BatWebSynchronizerProcessInstaller.Username = null;
         this.BatWebSynchronizerProcessInstaller.AfterInstall += new System.Configuration.Install.InstallEventHandler(this.BatWebSynchronizerProcessInstallaer_AfterInstall);
         // 
         // ProjectInstaller
         // 
         this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.BatWebSynchronizerInstaller,
            this.BatWebSynchronizerProcessInstaller});

      }

      #endregion
      private System.ServiceProcess.ServiceInstaller BatWebSynchronizerInstaller;
      public System.ServiceProcess.ServiceProcessInstaller BatWebSynchronizerProcessInstaller;
   }
}