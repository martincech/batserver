﻿using System;
using System.Linq;
using Bat2Library.Sms;
using BatWebSynchronizer.Collector;
using BatWebSynchronizer.Synchronizer;
using Usb.MODEM;

namespace BatWebSynchronizer.Device
{
   public class Modem : DataCollector<Scale>, IDevice, IDisposable
   {
#if DEBUG
      private int ReadyCounter = 0;
#endif
      public ModemDevice UnderlyingDevice { get; private set; }
      private SmsStatus settings;

      internal StatisticData UpdateStatus(SmsStatus status)
      {
         lock (this)
         {
            if (status == null)
            {
               return null;
            }
#if DEBUG
            if (status.Event == SmsEvent.READY)
            {
               ReadyCounter++;
               if (ReadyCounter%2== 0)
               {
                  status.SmsText =
                     "SCALE 1 DAY 83 10.12.2005 FEMALES: Cnt 75 Avg 3.789 Gain 0.012 Sig 0.286 Cv 8 Uni 80 MALES: Cnt 21 Avg 4.566 Gain 0.005 Sig 0.169 Cv 4 Uni 100";
                  status.SmsNumber = "+420777123456";
                  status.Event = SmsEvent.SMS_READ_OK;
               }
            }
#endif
            settings = status;
            StatisticData data = null;

            if (Event == SmsEvent.SMS_READ_OK)
            {
               SmsDecode.Decode(settings.SmsText, out data);
               data.PhoneNumber = settings.SmsNumber;
            }
            if (data != null)
            {
               if (DeviceList.All(s => s.Name != data.ScaleName))
               {
                  DeviceList.Add(new Scale {Address = data.PhoneNumber, Connected = true, Name = data.ScaleName});
               }
            }
            OnStatusChanged();
            return data;
         }
      }

      public event EventHandler StatusChanged; 
      public Modem(ModemDevice underlyingDevice)
      {
         UnderlyingDevice = underlyingDevice;
         settings = new SmsStatus();
      }

      public string Description
      {
         get { return ProductName; }
      }

      /// <summary>
      ///    The product name.
      /// </summary>
      public string ProductName
      {
         get { return UnderlyingDevice.ProductName; }
      }

      /// <summary>
      ///    The device serial number.
      /// </summary>
      public string SerialNumber
      {
         get { return UnderlyingDevice.SerialNumber; }
      }

      public string PortName
      {
         get { return UnderlyingDevice.PortName; }
      }

      public int SignalStrength
      {
         get { return settings.SignalStrength; }
      }

      public SmsEvent Event
      {
         get { return settings.Event; }
      }

      public string ModemName
      {
         get { return settings.ModemName; }
      }

      protected virtual void OnStatusChanged()
      {
         var handler = StatusChanged;
         if (handler != null) handler(this, EventArgs.Empty);
      }

      #region Implementation of IDisposable

      /// <summary>
      /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
      /// </summary>
      public void Dispose()
      {
         
      }

      #endregion
   }
}