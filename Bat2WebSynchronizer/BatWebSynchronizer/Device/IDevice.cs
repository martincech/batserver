﻿using System.Collections.ObjectModel;

namespace BatWebSynchronizer.Device
{
   public interface IDevice
   {
      string Description { get; }
      string ProductName { get; }
      string SerialNumber { get; }
      string PortName { get;}

      ObservableCollection<Scale> DeviceList { get; }
   }
}
