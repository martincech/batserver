﻿namespace BatWebSynchronizer.Device
{
   public class Scale
   {
      public string Address { get; set; }
      public string Name { get; set; }
      public bool Connected { get; set; }

   
      public override string ToString()
      {
         return string.Format("Scale {0} [Address {1}]", Name, Address);
      }

   }
}