using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.Xml.Serialization;
using Bat2Library.ModbusWorker;
using Bat2Library.ModbusWorker.Bat2;
using Bat2Library.ModbusWorker.Worker;
using BatWebSynchronizer.Collector;
using BatWebSynchronizer.PermanentStorage;
using BatWebSynchronizer.Synchronizer;
using Modbus;
using SerialPort;
using Usb.ELO;
using ModbusManager = Bat2Library.ModbusWorker.Bat2.ModbusManager;
using Timer = System.Timers.Timer;

namespace BatWebSynchronizer.Device
{
   public class Elo : DataCollector<Scale>, IDevice, IDisposable
   {
      #region Private fields and properties

      private const int ELO_CHECK_TIME = 60*1000; //60s
      private const int MODBUS_REPLY_TIMEOUT = 2000;
      private readonly Setup setup;
      private ModbusManager manager;
      private EloDevice underlyingDevice;
      private IStoredData storage;
      private ILogger log;
      private readonly List<Timer> timers;
      private Timer readTimer;
      private Timer scanNewTimer;
      private CancellationTokenSource tokenSource;
      private Task worker;
      private readonly BlockingCollection<Action> scaleActions;

      private int synchronizationPeriod;
      private readonly Action checkAliveness;
      private readonly Action scanForNew;
      private readonly Action readAll;
      private int scanNewPeriod;
       private bool isConnected;

       private void InitManager(ModbusManager modbusManager)
      {
         manager = modbusManager;
         manager.ScaleConnected += ScaleConnected;
         manager.ScaleDisConnected += ScaleDisconnected;
         RestartWorker();
      }

      private void InitCheckForScalesConnectivityTimer()
      {
         timers.Add(CreateTimer(ELO_CHECK_TIME, (sender, args) => ScheduleActionForExecution(checkAliveness)));
      }

      private void  ScheduleActionForExecution(Action action)
      {
         if (!scaleActions.Contains(action))
         {
            scaleActions.Add(action);
         }
      }

      private void InitScanNewTimer()
      {
         scanNewTimer = CreateTimer(ScanNewPeriod, (sender, args) => ScanForNewScales());
         timers.Add(scanNewTimer);
      }

      public void ScanForNewScales()
      {
         ScheduleActionForExecution(scanForNew);
      }
      public void ReadDataFromScales()
      {
         ScheduleActionForExecution(readAll);
      }


      private void InitReadTimer()
      {
         readTimer = CreateTimer(SynchronizationPeriod, (sender, args) => ReadDataFromScales());
         timers.Add(readTimer);
      }



      private static Timer CreateTimer(int time, ElapsedEventHandler elapsedRoutine)
      {
         var connectedTimer = new Timer(time);
         connectedTimer.Elapsed += elapsedRoutine;
         connectedTimer.Start();
         elapsedRoutine(null, null);
         return connectedTimer;
      }

      private void ScaleConnected(object sender, Bat2Modbus bat2Modbus)
      {
         var device = DeviceList.FirstOrDefault(s => s.Address == bat2Modbus.SlaveAddress.ToString());
         if (device == null)
         {
            device = new Scale();
         }
         device.Address = bat2Modbus.SlaveAddress.ToString();
         device.Connected = true;
         device.Name = bat2Modbus.CurrentState.ScaleName;
         if (!DeviceList.Contains(device))
         {
            DeviceList.Add(device);
            ScheduleActionForExecution(readAll);
         }
      }

      private void ScaleDisconnected(object sender, Bat2Modbus bat2Modbus)
      {
         var device = DeviceList.FirstOrDefault(s => s.Address == bat2Modbus.SlaveAddress.ToString());
         if (device != null)
         {
            DeviceList.Remove(device);
         }
      }

      private EloDevice UnderlyingDevice
      {
         get { return underlyingDevice; }
         set
         {
            underlyingDevice = value;
            if (underlyingDevice != null)
            {
               setup.ModbusScales.PortNumber = int.Parse(underlyingDevice.PortName.ToLower().Substring(3));
               SerialNumber = UnderlyingDevice.SerialNumber;
               ManagerChangeProtocolConfig();
            }
         }
      }

      #endregion

      private Elo()
      {
         setup = new Setup {ModbusProtocol = {ReplyTimeout = MODBUS_REPLY_TIMEOUT, NumberOfAttempts = 5}};
         AddressFrom = 1;
         AddressTo = 255;
         ScanNewPeriod = 30*60*1000;
         SynchronizationPeriod = 5*60*1000;
         scaleActions = new BlockingCollection<Action>();
         timers = new List<Timer>();
         checkAliveness = CheckConnectedScalesAlivenessRoutine;
         readAll = ReadDataRoutine;
         scanForNew = ScanNewScalesRoutine;
      }

      public static Elo LoadFromFileOrCreate(IStoredData storage, EloDevice eloDevice, ILogger log = null)
      {
         var eloList = storage.LoadEloSettings();
         var elo = eloList.FirstOrDefault(f => f.SerialNumber == eloDevice.SerialNumber) ?? new Elo();
         elo.UnderlyingDevice = eloDevice;
         elo.IsConnected = true;
         elo.storage = storage;
         elo.log = log;
         elo.SaveSettings();
         elo.InitManager(new ModbusManager(eloDevice, elo.setup.ModbusProtocol));
         elo.Log("Elo inited.");
         return elo;
      }

      private void RestartWorker()
      {
         StopWorker();
         worker = new Task((() =>
         {
            while (!tokenSource.IsCancellationRequested)
            {
               Action action;
               try
               {
                  var flag = scaleActions.TryTake(out action, -1, tokenSource.Token);
                  if (!flag) continue;
                  action();
               }
               catch (Exception e)
               {
                  Log(e.Message);
               }
            }
         }), tokenSource.Token);
         //InitCheckForScalesConnectivityTimer();
         InitScanNewTimer();
         InitReadTimer();
         worker.Start();
      }

      private void StopWorker()
      {
         if (worker != null)
         {
            tokenSource.Cancel();
            worker.Wait();
         }
         tokenSource = new CancellationTokenSource();
         foreach (var timer in timers)
         {
            timer.Stop();
         }
         timers.Clear();
      }

      private void ManagerChangeProtocolConfig()
      {
         if (manager != null)
         {
            manager.ModbusConfig = setup.ModbusProtocol;
         }
      }

      public byte AddressFrom
      {
         get { return (byte)setup.ModbusScales.FirstAddress; }
         set { setup.ModbusScales.FirstAddress = value; }
      }

      public byte AddressTo
      {
         get { return (byte)setup.ModbusScales.LastAddress; }
         set { setup.ModbusScales.LastAddress = value; }
      }

      public void SaveSettings()
      {
         storage.SaveEloSettings(this);
      }

      [XmlIgnore]
      public string Description
      {
         get { return "Wire connection(ELO E216)"; }
      }

      [XmlIgnore]
      public string ProductName
      {
         get { return UnderlyingDevice == null? "unknown" : UnderlyingDevice.ProductName; }
      }

      //TODO here the setter should be private, but for deserialization needed public - change the xml serializer way
      public string SerialNumber { get; set; }

      [XmlIgnore]
      public string PortName
      {
         get { return UnderlyingDevice == null ? "none" : UnderlyingDevice.PortName; }
      }

       public bool IsConnected
       {
           get { return UnderlyingDevice == null? isConnected : true; }
           set { isConnected = value; }
       }

       public int Mode
      {
         get { return (int) setup.ModbusProtocol.Protocol; }
         set
         {
            setup.ModbusProtocol.Protocol = (ModbusPacket.PacketMode) value;
            ManagerChangeProtocolConfig();
         }
      }

      public int Parity
      {
         get { return (int) setup.ModbusProtocol.Parity; }
         set
         {
            setup.ModbusProtocol.Parity = (Parity) value;
            ManagerChangeProtocolConfig();
         }
      }

      public int BaudRate
      {
         get { return setup.ModbusProtocol.Speed; }
         set
         {
            setup.ModbusProtocol.Speed = value;
            ManagerChangeProtocolConfig();
         }
      }

      public int ReplyTimeout
      {
         get { return setup.ModbusProtocol.ReplyTimeout; }
         set
         {
            setup.ModbusProtocol.ReplyTimeout = value;
            ManagerChangeProtocolConfig();
         }
      }

      public int SilentInterval
      {
         get { return setup.ModbusProtocol.SilentInterval; }
         set
         {
            setup.ModbusProtocol.SilentInterval = value;
            ManagerChangeProtocolConfig();
         }
      }

      public int PortNumber
      {
         get { return setup.ModbusScales.PortNumber; }
      }

      public int ScanNewPeriod
      {
         get { return scanNewPeriod; }
         set
         {
            scanNewPeriod = value;
            if (scanNewTimer != null)
            {
               scanNewTimer.Interval = value;
            }
         }
      }

      public int SynchronizationPeriod
      {
         get { return synchronizationPeriod; }
         set
         {
            synchronizationPeriod = value;
            if (readTimer != null)
            {
               readTimer.Interval = value;
            }
         }
      }

      #region Actions to be executed by executive thread

      private void ScanNewScalesRoutine()
      {
         Log(string.Format("Scanning for new scales in rage {0} - {1}", AddressFrom, AddressTo));
         manager.NewScalesOnRange(AddressFrom, AddressTo);
         Log(string.Format("Scan complete in rage {0} - {1}", AddressFrom, AddressTo));
      }

      private void CheckConnectedScalesAlivenessRoutine()
      {
         Log("CheckingConnectedScales");
         manager.CheckConnectivityOfExistingScales();
         Log("Scale connectivity checked");
      }

      private void ReadDataRoutine()
      {
         Log("Reading data from scales.");
         manager.ReadAll();
         foreach (var connectedScale in manager.ConnectedScales.ToList())
         {
            OnDataReaded(ToSmsData(connectedScale.CurrentState));
         }
         Log("Data read from all connected scales.");
      }

      #endregion

      private void Log(string readingDataFromScales)
      {
         if (log != null)
         {
            log.Log(readingDataFromScales);
         }
      }

      private static StatisticData ToSmsData(CurrentState state)
      {
         var smsData = new StatisticData
         {
            ScaleName = state.ScaleName,
            DayNumber = state.StatisticsBuilderF.Statistics.DayNumber,
            Date = state.DateTimeBuilder.DateTime,
            FemaleData = Stats(state.StatisticsBuilderF.Statistics, state.DailyGainF)
         };
         if (state.UseBothGenders)
         {
            smsData.MaleData = Stats(state.StatisticsBuilderM.Statistics, state.DailyGainM);
         }
         return smsData;
      }

      private static StatisticData.StatData Stats(Statistics stats, float gain)
      {
         return new StatisticData.StatData
         {
            Count = stats.Count,
            Average = stats.Average,
            Gain = gain,
            Sigma = stats.Sigma,
            Cv = stats.Cv,
            Uniformity = stats.Uniformity
         };
      }

      public void Dispose()
      {
         Log("Disposing elo");
         StopWorker();
         if (UnderlyingDevice != null)
         { 
            UnderlyingDevice = null;
            IsConnected = false;
            SaveSettings();
         }
      }
   }
}