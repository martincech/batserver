using System;
using System.Collections.ObjectModel;
using System.Xml.Serialization;
using BatWebSynchronizer.Synchronizer;

namespace BatWebSynchronizer.Collector
{
   public abstract class DataCollector
   {
      public event EventHandler<StatisticData> DataReaded;
      protected void OnDataReaded(StatisticData e)
      {
         var handler = DataReaded;
         if (handler != null) handler(this, e);
      }
   }
   public abstract class DataCollector<T> : DataCollector
   {
      protected DataCollector()
      {
         DeviceList = new ObservableCollection<T>();
      }

      [XmlIgnore]
      public ObservableCollection<T> DeviceList { get; private set; }
   }
}