﻿using System.Linq;
using BatWebSynchronizer.Device;
using BatWebSynchronizer.PermanentStorage;
using Usb;
using Usb.ELO;

namespace BatWebSynchronizer.Collector
{

   public class ModbusCollector : DeviceDataCollector<Elo>
   {
      private readonly IStoredData storage;

      public ModbusCollector(IStoredData storage, ILogger log) : base(log)
      {
         this.storage = storage;
         UsbDeviceManager.Instance.DeviceConnected += UsbDeviceConnected;
         UsbDeviceManager.Instance.DeviceDisconnected += UsbDeviceDisconnected;
      }

      private void UsbDeviceConnected(object sender, UsbDevice usbDevice)
      {
         var eloDevice = usbDevice as EloDevice;
         if (eloDevice == null || DeviceList.Any(e => e.SerialNumber == eloDevice.SerialNumber)) return;
         Log.Log("Detected new ELO, scanning for connected scales");
         var elo = Elo.LoadFromFileOrCreate(storage, eloDevice, Log);
         AddToList(elo);
      }

      private void UsbDeviceDisconnected(object sender, UsbDevice usbDevice)
      {
         var eloDevice = usbDevice as EloDevice;
         if (eloDevice == null || DeviceList.All(e => e.SerialNumber != eloDevice.SerialNumber)) return;

         Log.Log("Detected device disconnection");
         var elo = DeviceList.First(e => e.SerialNumber == eloDevice.SerialNumber);
         RemoveFromList(elo);
      }
   }
}
