using System;
using System.Globalization;
using System.Linq;
using System.Text;
using BatWebSynchronizer.Synchronizer;
using Utilities;

namespace BatWebSynchronizer.Collector
{
   public class SmsDecode
   {
      private const int SHORT_MESSAGE_LENGHT = 18;
      private const int SHORT_MESSAGE_LENGHT_EXTENDED = 13;
      private const int LONG_MESSAGE_LENGHT_EXTENDED = 19;

      private static void DeleteWord(StringBuilder text, out string word)
      {
         var spaceIndex = text.ToString().IndexOf(' ');
         if (spaceIndex == 0)
         {
            word = text.ToString();
            throw new ArgumentException();
         }
         word = text.ToString().Substring(0, spaceIndex);
         text.Remove(0, spaceIndex + 1);
      }

      /// <summary>
      /// Decode SMS text to a struct
      /// </summary>
      /// <param name="text">SMS text</param>
      /// <param name="statisticData">DecodedSms instance</param>
      /// <returns>True if successful</returns>
      public static bool Decode(string text, out StatisticData statisticData)
      {
         int sn;
         char[] delimeters = { ' ', '\t', '\n' };
         if (Int32.TryParse(text.Split(delimeters).First(), NumberStyles.HexNumber, CultureInfo.InvariantCulture, out sn))
         {
            return DecodeExtended(text, out statisticData);
         }
         return DecodeBasic(text, out statisticData);
      }

      private static bool DecodeExtended(string text, out StatisticData statisticData)
      {
         // 7FFFFFFF 999 12/31/9999 99999 99.999 -99.999 9.999 99.9 99.9 -999.99 9999999 9999999 99.999
         // 7FFFFFFF 999 12/31/9999 99999 99.999 -99.999 9.999 99.9 99.9 99999 99.999 -99.999 9.999 99.9 99.9 -999.99 9999999 9999999 99.999
         statisticData = new StatisticData();
         char[] delimeters = { ' ', '\t', '\n' };
         var lenght = text.ToLower().Split(delimeters).Length;

         if (!(lenght == SHORT_MESSAGE_LENGHT_EXTENDED || lenght == LONG_MESSAGE_LENGHT_EXTENDED))
         {
            return false;
         }

         var textBuilder = new StringBuilder(text);
         try
         {
            //Scale number
            string str;
            DeleteWord(textBuilder, out str);
            statisticData.ScaleNumber = int.Parse(str, NumberStyles.HexNumber);
            //Day
            DeleteWord(textBuilder, out str);
            statisticData.DayNumber = int.Parse(str);

            //Date
            DeleteWord(textBuilder, out str);
            statisticData.Date = DateTime.ParseExact(str, "MM/dd/yyyy", CultureInfo.InvariantCulture);

            //Count
            statisticData.FemaleData = GetStatData(textBuilder);

            if (lenght > SHORT_MESSAGE_LENGHT_EXTENDED)
            {
               statisticData.MaleData = GetStatData(textBuilder);
            }

            //Temperature
            double temperature;
            DeleteWord(textBuilder, out str);
            statisticData.FemaleData.Temperature = double.TryParse(str, NumberStyles.Float,
               CultureInfo.InvariantCulture, out temperature)
               ? (double?)temperature
               : null;

            //Carbon dioxide
            int carbonDioxide;
            DeleteWord(textBuilder, out str);
            statisticData.FemaleData.CarbonDioxide = int.TryParse(str, out carbonDioxide) ? (int?)carbonDioxide : null;

            //Ammonia
            int ammonia;
            DeleteWord(textBuilder, out str);
            statisticData.FemaleData.Ammonia = int.TryParse(str, out ammonia) ? (int?)ammonia : null;

            //Humidity
            double humidity;
            statisticData.FemaleData.Humidity = double.TryParse(textBuilder.ToString(), NumberStyles.Float,
               CultureInfo.InvariantCulture, out humidity)
               ? (double?)humidity
               : null;

            if (lenght > SHORT_MESSAGE_LENGHT_EXTENDED)
            {
               statisticData.MaleData.Temperature = statisticData.FemaleData.Temperature;
               statisticData.MaleData.CarbonDioxide = statisticData.FemaleData.CarbonDioxide;
               statisticData.MaleData.Ammonia = statisticData.FemaleData.Ammonia;
               statisticData.MaleData.Humidity = statisticData.FemaleData.Humidity;
            }

            return true;
         }
         catch
         {
            return false;
         }
      }

      private static StatisticData.StatData GetStatData(StringBuilder textBuilder)
      {
         string str;

         var statData = new StatisticData.StatData();
         var cult = CultureInfo.InvariantCulture;

         // Cnt
         DeleteWord(textBuilder, out str);
         statData.Count = int.Parse(str);

         // Avg
         DeleteWord(textBuilder, out str);
         statData.Average = double.Parse(str, cult);

         // Gain
         DeleteWord(textBuilder, out str);
         statData.Gain = double.Parse(str, cult);

         // Sigma
         DeleteWord(textBuilder, out str);
         statData.Sigma = double.Parse(str, cult);

         // CV
         DeleteWord(textBuilder, out str);
         statData.Cv = double.Parse(str, cult);

         // Uni
         DeleteWord(textBuilder, out str);
         statData.Uniformity = double.Parse(str, cult);

         return statData;
      }

      private static bool DecodeBasic(string text, out StatisticData statisticData)
      {
         // SCALE 123456789012345 DAY 999 31.12.9999 23:59:59PM Cnt 9999 Avg 99.999 Gain -99.999 Sig 9.999 Cv 999 Uni 999
         // SCALE 123456789012345 DAY 999 31.12.9999 23:59:59PM FEMALES: Cnt 9999 Avg 99.999 Gain -99.999 Sig 9.999 Cv 999 Uni 999 MALES: Cnt 9999 Avg 99.999 Gain -99.999 Sig 9.999 Cv 999 Uni 999
         statisticData = new StatisticData();
         char[] delimeters = { ' ', '\t', '\n' };
         var lenght = text.ToLower().Split(delimeters).Length;
         var textBuilder = new StringBuilder(text);
         try
         {
            // Delete SCALES
            string str;
            DeleteWord(textBuilder, out str);

            // Delete vah
            DeleteWord(textBuilder, out str);
            statisticData.ScaleName = str;

            // Delete DAY
            DeleteWord(textBuilder, out str);

            // Delete day
            DeleteWord(textBuilder, out str);
            statisticData.DayNumber = int.Parse(str);

            // Delete date
            DeleteWord(textBuilder, out str);

            var date = DateTimeParser.ParseDate(str);

            DeleteWord(textBuilder, out str);

            if (Char.IsNumber(str[0]))
            {
               var time = DateTimeParser.ParseTime(str);
               date = new DateTime(date.Year, date.Month, date.Day, time.Hour, time.Minute, time.Second);
               DeleteWord(textBuilder, out str);
            }

            statisticData.Date = date;


            if (lenght > SHORT_MESSAGE_LENGHT)
            {
               DeleteWord(textBuilder, out str);
               statisticData.FemaleData = GetSmsData(textBuilder);
               DeleteWord(textBuilder, out str);
               DeleteWord(textBuilder, out str);
               statisticData.MaleData = GetSmsData(textBuilder);
            }
            else
            {
               statisticData.FemaleData = GetSmsData(textBuilder);
            }
            return CheckSmsDataFormat(statisticData);
         }
         catch
         {
            // Neco se nepovedlo, neplatny format SMS
            return false;
         }
      }

      private static bool CheckSmsDataFormat(StatisticData d)
      {
         if (d.DayNumber < 0 ||
             !CheckStatDataFormat(d.MaleData) || !CheckStatDataFormat(d.FemaleData))
         {
            return false;
         }
         return true;
      }

      private static bool CheckStatDataFormat(StatisticData.StatData d)
      {
         if (d == null)
         {
            return true;
         }
         if (d.Average < 0 || d.Count < 0 || d.Cv < 0 || d.Sigma < 0 || d.Uniformity < 0)
         {
            return false;
         }
         return true;
      }

      private static StatisticData.StatData GetSmsData(StringBuilder textBuilder)
      {

         string str;

         var statData = new StatisticData.StatData();

         // Cnt
         // DeleteWord(textBuilder, out str);
         DeleteWord(textBuilder, out str);
         statData.Count = int.Parse(str);

         // Avg
         DeleteWord(textBuilder, out str);
         DeleteWord(textBuilder, out str);
         var culture = CheckLanguage(str);
         statData.Average = double.Parse(str, culture);

         // Gain
         DeleteWord(textBuilder, out str);
         DeleteWord(textBuilder, out str);
         culture = CheckLanguage(str);
         statData.Gain = double.Parse(str, culture);

         // Sigma
         DeleteWord(textBuilder, out str);
         DeleteWord(textBuilder, out str);
         culture = CheckLanguage(str);
         statData.Sigma = double.Parse(str, culture);

         // CV vypoctu na desetinu (v SMS je to jen na cele cislo)
         DeleteWord(textBuilder, out str);
         DeleteWord(textBuilder, out str);
         culture = CheckLanguage(str);
         statData.Cv = double.Parse(str, culture);

         // Uni
         DeleteWord(textBuilder, out str);
         if (textBuilder.ToString().Split(' ').Length > 1)
         {
            DeleteWord(textBuilder, out str);
            statData.Uniformity = int.Parse(str);
         }
         else
         {
            statData.Uniformity = int.Parse(textBuilder.ToString());
         }

         return statData;
      }

      private static CultureInfo CheckLanguage(string number)
      {
         if (number.Contains(','))
         {
            return new CultureInfo("de-DE");
         }
         else
         {
            return new CultureInfo("en-US");
         }
      }
   }
}