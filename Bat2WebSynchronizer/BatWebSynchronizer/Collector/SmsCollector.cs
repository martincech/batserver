﻿using System;
using System.ComponentModel;
using System.Linq;
using Bat2Library.Sms;
using Bat2Library.Sms.Workers;
using BatWebSynchronizer.Device;
using BatWebSynchronizer.PermanentStorage;
using BatWebSynchronizer.Properties;
using BatWebSynchronizer.Synchronizer;
using Usb;
using Usb.MODEM;

namespace BatWebSynchronizer.Collector
{
   public class SmsCollector : DeviceDataCollector<Modem>
   {
      private readonly IStoredData storage;
      // Background workers for reading the SMS and status info
      private SmsReadWorkers smsReadWorkers;

      public SmsCollector(IStoredData storage, ILogger log) : base(log)
      {
         this.storage = storage;
         UsbDeviceManager.Instance.DeviceConnected += UsbDeviceConnected;
         UsbDeviceManager.Instance.DeviceDisconnected += UsbDeviceDisconnected;
      }

      /// <summary>
      /// Device connected
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="usbDevice"></param>
      private void UsbDeviceConnected(object sender, UsbDevice usbDevice)
      {

         var device = usbDevice as ModemDevice;
         if (device == null || DeviceList.Any(m => m.SerialNumber == device.SerialNumber)) return;
         Log.Log("Detected modem connection");
         AddToList(new Modem(device));
         UpdatePorts();
      }

      /// <summary>
      /// Update ports with  modems
      /// </summary>
      private void UpdatePorts()
      {
         if (smsReadWorkers != null)
         {
            smsReadWorkers.Stop();
            smsReadWorkers = null;
         }
         if (DeviceList.Count == 0) return;
         smsReadWorkers = new SmsReadWorkers(DeviceList.Select(s => s.UnderlyingDevice).ToList(), SmsReadWorkerEventHandler, (sender, e) => {});
         smsReadWorkers.Start();
      }
      /// <summary>
      /// Gsm event handler witch processes changes on gsm modem
      /// </summary>
      private void SmsReadWorkerEventHandler(object sender, ProgressChangedEventArgs e)
      {
         var status = (SmsStatus)e.UserState;
         var modem = DeviceList.FirstOrDefault(x => x.PortName == status.PortName);
         if (modem != null)
         {
            var prevEvent = modem.Event;
            var data = modem.UpdateStatus(status);
            UpdateAndLogModemStatus(modem, prevEvent, status, data);
         }
      }

      private void UpdateAndLogModemStatus(Modem modem, SmsEvent prevEvent, SmsStatus status, StatisticData statisticData)
      {

         string msg = null;
         if (prevEvent == modem.Event)
         {
            return;
         }
         switch (modem.Event)
         {
            case SmsEvent.RESET:
               msg = string.Format(Resources.SimNoResponse, modem.ModemName);
               break;
            case SmsEvent.SMS_READ_OK:
               Log.Log("New SMS received, parsing.");
               if (statisticData == null) { 
                  msg = Resources.LOG_WRONG_FORMAT;
                  storage.Dump(string.Format("{0} {1}", status.SmsNumber, status.SmsText));
               }
               else
               {
                  OnDataReaded(statisticData);
                  msg = string.Format(Resources.LOG_SMS_RECEIVED, status.SmsText);
               }
               break;
            case SmsEvent.INIT:
               break;
            case SmsEvent.DETECT:
               break;
            case SmsEvent.CHECK:
               break;
            case SmsEvent.SIGNAL:
               break;
            case SmsEvent.OPERATOR:
               break;
            case SmsEvent.STRENGTH:
               break;
            case SmsEvent.READY:
               break;
            case SmsEvent.SMS_SENDING:
               break;
            case SmsEvent.SMS_SEND_OK:
               break;
            case SmsEvent.SMS_SEND_ERROR:
               break;
            case SmsEvent.SMS_READING:
               break;
            case SmsEvent.SMS_READ_ERROR:
               break;
            case SmsEvent.SMS_DELETING:
               break;
            case SmsEvent.SMS_DELETE_OK:
               break;
            case SmsEvent.SMS_DELETE_ERROR:
               break;
            case SmsEvent.CANCEL:
               break;
            default:
               msg = string.Format(Resources.DeviceNoResponse, string.Format("{0}({1})", modem.ModemName, modem.SerialNumber));
               break;
         }
         if (!string.IsNullOrEmpty(msg))
         {
            Log.Log(DateTime.Now, msg);
         }
      }
      /// <summary>
      /// Device disconnected
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="usbDevice"></param>
      private void UsbDeviceDisconnected(object sender, UsbDevice usbDevice)
      {
         var device = usbDevice as ModemDevice;
         if (device == null || DeviceList.All(m => m.SerialNumber != device.SerialNumber)) return;
         Log.Log("Detected modem disconnection");
         var modem = DeviceList.First(m => m.SerialNumber == device.SerialNumber);
         RemoveFromList(modem);
      }
   }
}
