﻿using System;
using System.Collections.Specialized;
using BatWebSynchronizer.Device;
using BatWebSynchronizer.Synchronizer;

namespace BatWebSynchronizer.Collector
{
   public class DeviceDataCollector<T> : DataCollector<T> where T: DataCollector<Scale>, IDevice, IDisposable
   {
      protected ILogger Log { get; private set; }

      public DeviceDataCollector(ILogger log)
      {
         Log = log;
      }

      protected void AddToList(T device)
      {
         device.DataReaded += OnDataReaded;
         device.DeviceList.CollectionChanged += OnScalesChanged;
         DeviceList.Add(device);
      }

      protected void RemoveFromList(T device)
      {
         device.DataReaded -= OnDataReaded;
         device.DeviceList.CollectionChanged -= OnScalesChanged;
         DeviceList.Remove(device);
         using (device) { }
      }

      private void OnDataReaded(object sender, StatisticData data)
      {
         Log.Log(string.Format("Data read from the scale {0}({1})", data.ScaleName, data.ScaleNumber));
         OnDataReaded(data);
      }

      private void OnScalesChanged(object sender, NotifyCollectionChangedEventArgs e)
      {
         switch (e.Action)
         {
            case NotifyCollectionChangedAction.Add:
               foreach (Scale scale in e.NewItems)
               {
                  Log.Log(string.Format("New scale detected: {0}", scale));
               }
               break;
            case NotifyCollectionChangedAction.Remove:
               foreach (Scale scale in e.OldItems)
               {
                  Log.Log(string.Format("Scale connection lost: {0}", scale));
               }
               break;
            case NotifyCollectionChangedAction.Replace:
               break;
            case NotifyCollectionChangedAction.Move:
               break;
            case NotifyCollectionChangedAction.Reset:
               break;
            default:
               break;
         }
      }
   }
}