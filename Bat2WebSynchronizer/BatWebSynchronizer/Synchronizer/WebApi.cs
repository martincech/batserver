using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using BatWebSynchronizer.PermanentStorage;

namespace BatWebSynchronizer.Synchronizer
{
   public class WebApi : IWebApi
   {
      private const string ACCOUNT_ROLE_PATH = "api/v1/AccountRoles";
      private const string STATS_PATH = "/api/V1/Stats";
      private const string AUTH_BASIC = "Basic";
      private const string MEDIA_JSON = "application/json";

      private string server;
      private string userName;
      private string password;

      private int communications;
      private bool isCommunicating;
      private bool isLogged;

      public bool IsLogged
      {
         get { return isLogged; }
         private set
         {
            isLogged = value;
            OnConnectivityStatusChanged();
         }
      }

      public bool IsCommunicating
      {
         get { return isCommunicating; }
         private set
         {
            isCommunicating = value;
            OnConnectivityStatusChanged();
         }
      }

      public event EventHandler ConnectivityStatusChanged;

      public async Task<bool> Login(LoginInfo loginInfo, CancellationToken cancelToken = default(CancellationToken))
      {
         server = loginInfo.ServerUrl;
         userName = loginInfo.UserName;
         password = loginInfo.Password;
         var loginResponse = await Get(ACCOUNT_ROLE_PATH, cancelToken);
         ChangeLoginStateAccordingToResponse(loginResponse);
         return IsLogged;
      }

      public async Task<bool> SyncStatistic(StatisticData message, CancellationToken cancelToken = default(CancellationToken))
      {
         if (!IsLogged)
         {
            return false;
         }
         var postResponse = await PostAsJson(message, STATS_PATH, cancelToken);
         ChangeLoginStateAccordingToResponse(postResponse);
         return postResponse.StatusCode == HttpStatusCode.OK;
      }

      private void ChangeLoginStateAccordingToResponse(HttpResponseMessage response)
      {
         var statusCode = response.StatusCode;
         if (!response.IsSuccessStatusCode || statusCode == HttpStatusCode.Forbidden || statusCode == HttpStatusCode.Unauthorized)
         {
            IsLogged = false;
         }
         else
         {
            IsLogged = true;
         }
      }

      /// <summary>
      /// Call web api get method. 
      /// </summary>
      /// <param name="apiPath">route (e.g. api/Bat2s)</param>
      /// <param name="cancelToken"></param>
      /// <returns>returned object</returns>
      private async Task<HttpResponseMessage> Get(string apiPath, CancellationToken cancelToken = default(CancellationToken))
      {                  
         return await HttpClientRequestAsync(c => c.GetAsync(apiPath, cancelToken));

      }

      private async Task<HttpResponseMessage> HttpClientRequestAsync(Func<HttpClient, Task<HttpResponseMessage>> func)
      {
         var uri = CheckServerAddress(server);
         if (uri == null)
         {
            return new HttpResponseMessage(HttpStatusCode.BadRequest);
         }
         using (var client = new HttpClient())
         {
            CreateBaseHeader(client, uri);
            try
            {
               AddCommunication();
               var res = await func(client);
               return res;
            }
            catch
            {
               return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
            finally
            {
               FinishCommunication();
            }
         }
      }

      private void AddCommunication()
      {
         Interlocked.Increment(ref communications);
         IsCommunicating = communications == 0;
      }

      private void FinishCommunication()
      {
         IsCommunicating = communications != 1;
         Interlocked.Decrement(ref communications);
      }

      private void CreateBaseHeader(HttpClient client, Uri uri)
      {
         client.BaseAddress = uri;
         client.DefaultRequestHeaders.Accept.Clear();
         client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(MEDIA_JSON));
         client.DefaultRequestHeaders.Authorization = CreateHeaderAuthentication(AUTH_BASIC);
      }

      /// <summary>
      /// Call web api post method.
      /// </summary>
      /// <param name="body">sending data</param>
      /// <param name="apiPath">route (e.g. api/Bat2s)</param>
      /// <param name="cancelToken"></param>
      /// <returns>true - data was saved on address</returns>
      private async Task<HttpResponseMessage> PostAsJson<T>(T body, string apiPath, CancellationToken cancelToken = default(CancellationToken))
      {
         return await HttpClientRequestAsync(c => c.PostAsJsonAsync(apiPath, body, cancelToken));
      }

      /// <summary>
      /// Create header authentication.
      /// </summary>
      /// <param name="type">authentication type</param>
      /// <returns>authentication header</returns>
      private AuthenticationHeaderValue CreateHeaderAuthentication(string type)
      {
         if (type.Equals(AUTH_BASIC))
         {
            return new AuthenticationHeaderValue(AUTH_BASIC,
               Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(
                  string.Format("{0}:{1}", userName, password)))
               );
         }
         return null;
      }

      private static Uri CheckServerAddress(string address)
      {
         address = address.EndsWith("/") ? address : address + "/";
         if (!Uri.IsWellFormedUriString(address, UriKind.RelativeOrAbsolute))
         {
            return null;
         }
         Uri uri;
         try
         {
            uri = new Uri(address);
         }
         catch (Exception)
         {
            uri = null;
         }

         if (uri != null && Uri.CheckSchemeName(uri.Scheme) && uri.Scheme == Uri.UriSchemeHttp) return uri;

         // try to add http in front of address
         address = "http://" + address;
         try
         {
            uri = new Uri(address);
         }
         catch (Exception)
         {
            if (uri == null || !Uri.CheckSchemeName(uri.Scheme) || uri.Scheme != Uri.UriSchemeHttp)
            {
               return null;
            }
         }

         return uri;
      }

      protected virtual void OnConnectivityStatusChanged()
      {
         var handler = ConnectivityStatusChanged;
         if (handler != null) handler(this, EventArgs.Empty);
      }
   }


}