using System;

namespace BatWebSynchronizer.Synchronizer
{
   public class StatisticData
   {
      public class StatData
      {
         /// <summary>
         /// Number of birds
         /// </summary>
         public int Count;

         /// <summary>
         /// Average weight
         /// </summary>
         public double Average;

         /// <summary>
         /// Daily gain
         /// </summary>
         public double Gain;

         /// <summary>
         /// Standard deviation
         /// </summary>
         public double Sigma;

         /// <summary>
         /// Coeficient of variation
         /// </summary>
         public double Cv;

         /// <summary>
         /// Uniformity
         /// </summary>
         public double Uniformity;

         /// <summary>
         /// Temperature
         /// </summary>
         public double? Temperature;

         /// <summary>
         /// Carbon dioxide
         /// </summary>
         public int? CarbonDioxide;

         /// <summary>
         /// Ammonia
         /// </summary>
         public int? Ammonia;

         /// <summary>
         /// Humidity
         /// </summary>
         public double? Humidity;
      }
      /// <summary>
      /// Phone number of the scale
      /// </summary>
      public string PhoneNumber;

      /// <summary>
      /// Id. number of the scale
      /// </summary>
      public long ScaleNumber;

      /// <summary>
      /// Name of the scale
      /// </summary>
      public string ScaleName;

      /// <summary>
      /// Day number
      /// </summary>
      public int DayNumber;

      /// <summary>
      /// Date of the day
      /// </summary>
      public DateTime Date;

      /// <summary>
      /// Male data
      /// </summary>
      public StatData MaleData;

      /// <summary>
      /// Female Data
      /// </summary>
      public StatData FemaleData;

      public override string ToString()
      {
         return string.Format("Scale {0} id {1} day {2} date {3}", ScaleName, ScaleNumber, DayNumber, Date);
      }
   }
}