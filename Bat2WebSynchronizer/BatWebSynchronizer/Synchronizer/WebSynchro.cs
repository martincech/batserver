using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using BatWebSynchronizer.PermanentStorage;
using BatWebSynchronizer.Properties;

namespace BatWebSynchronizer.Synchronizer
{
   public class WebSynchro
   {
      private readonly IStoredData storage;
      private readonly IWebApi api;
      private readonly ILogger log;
      private BlockingCollection<StatisticData> toSyncQ;
      private LoginInfo login;
      private CancellationTokenSource cancel;
      private Task synchroTask;
      private const int READ_DATA_TIMEOUT_IN_MS = 1000;

      public WebSynchro(IStoredData storage, IWebApi api, ILogger log)
      {
         this.storage = storage;
         this.api = api;
         this.log = log;
      }

      public void Sync(StatisticData statisticData)
      {
         storage.SaveUnsyncedData(statisticData);
         QueueItem(statisticData);
      }

      public event EventHandler<StatisticData> Synced; 


      private void QueueItem(StatisticData statisticData)
      {
         if (toSyncQ != null)
         {
            toSyncQ.Add(statisticData);
         }
      }

      public void Start()
      {
         if (IsRunning)
         {
            return;
         }
         Log("Starting synchronization");
         ClearQ();
         LoadLoginInfo();
         LoadUnsynchronizedData();
         cancel = new CancellationTokenSource();

         synchroTask = Task.Run(() => Synchronizationtask(), cancel.Token);
      }

      private void ClearQ()
      {
         toSyncQ = new BlockingCollection<StatisticData>();
      }

      public bool IsRunning { get { return synchroTask != null;} }

      private async void Synchronizationtask()
      {
         while (!cancel.IsCancellationRequested)
         {
            try
            {
               if (!api.IsLogged)
               {
                  Log("Not logged, trying to login");
                  if (!await api.Login(login, cancel.Token))
                  {
                     Thread.Sleep(5000);
                     continue;
                  }
                  Log("Logged, synchronizing");
               }

               StatisticData data;
               var flag = toSyncQ.TryTake(out data, READ_DATA_TIMEOUT_IN_MS, cancel.Token);
               if (!flag) continue;

               if (!await api.SyncStatistic(data, cancel.Token))
               {
                  //push back, synchronization failed
                  Log("Synchronization failed, trying again.");
                  QueueItem(data);
               }
               else
               {
                  LogSyncedMessage(data);
                  OnSynced(data);
                  Thread.Sleep(200);
               }
            }
            catch
            {
            }

         }
      }

      private void LogSyncedMessage(StatisticData message)
      {
         Log(DateTime.Now, string.Format(Resources.LOG_SMS_SYNCED, message));
         storage.SaveSyncedData(message);
         storage.SaveUnsyncedData(toSyncQ.ToList());
      }

      private void LoadLoginInfo()
      {
         login = storage.LoadLoginInfo();
      }

      private void LoadUnsynchronizedData()
      {
         var data = storage.LoadUnsyncedData();
         foreach (var smsData in data)
         {
            toSyncQ.Add(smsData);
         }
      }

      public void Stop()
      {
         if (!IsRunning)
         {
            return;
         }
         Log("Stopping synchronization");
         cancel.Cancel();
         synchroTask.Wait();
         synchroTask = null;
         toSyncQ.Dispose();
         toSyncQ = null;
         cancel = null;
         Log("Synchronization stopped");
      }

      private void Log(string message)
      {
         Log(DateTime.Now, message);
      }
      private void Log(DateTime time, string logMessage)
      {
         if (log != null)
         {
            log.Log(time, logMessage);
         }
      }

      protected virtual void OnSynced(StatisticData e)
      {
         var handler = Synced;
         if (handler != null) handler(this, e);
      }
   }
}