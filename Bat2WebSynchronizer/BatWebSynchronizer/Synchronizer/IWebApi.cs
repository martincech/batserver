using System;
using System.Threading;
using System.Threading.Tasks;
using BatWebSynchronizer.PermanentStorage;

namespace BatWebSynchronizer.Synchronizer
{
   public interface IWebApi
   {
      Task<bool> Login(LoginInfo loginInfo, CancellationToken cancelToken = default(CancellationToken));
      Task<bool> SyncStatistic(StatisticData message, CancellationToken cancelToken = default(CancellationToken));
      bool IsLogged { get; }
      bool IsCommunicating { get; }

      event EventHandler ConnectivityStatusChanged;
   }
}