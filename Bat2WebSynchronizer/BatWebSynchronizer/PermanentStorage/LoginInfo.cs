﻿using Utilities;

namespace BatWebSynchronizer.PermanentStorage
{
   public class LoginInfo
   {
      private string password;
      private const string ENCIPHER_HASH_STRING = "AxBy123";

      /// <summary>
      /// Initializes a new instance of the <see cref="T:System.Object"/> class.
      /// </summary>
      public LoginInfo()
      {
         Password = "";
         UserName = "";
         ServerUrl = "Address is missing";
      }

      public string Password
      {
         get { return Encipher.Decrypt(password, ENCIPHER_HASH_STRING); }
         set
         {
            password = Encipher.Encrypt(value, ENCIPHER_HASH_STRING);
         }
      }

      public string UserName { get; set; }

      public string ServerUrl { get; set; }
   }
}