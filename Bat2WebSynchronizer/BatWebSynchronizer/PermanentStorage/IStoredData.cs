﻿using System.Collections.Generic;
using BatWebSynchronizer.Device;
using BatWebSynchronizer.Synchronizer;

namespace BatWebSynchronizer.PermanentStorage
{
   public interface IStoredData
   {
      IEnumerable<StatisticData> LoadUnsyncedData();
      void SaveUnsyncedData(IEnumerable<StatisticData> unsynced);
      void SaveUnsyncedData(StatisticData unsynced);

      IEnumerable<StatisticData> LoadSyncedData();
      void SaveSyncedData(IEnumerable<StatisticData> synced);
      void SaveSyncedData(StatisticData synced);

      IEnumerable<Elo> LoadEloSettings();
      void SaveEloSettings(IEnumerable<Elo> elos);
      void SaveEloSettings(Elo elo);

      void Dump(string message);

      LoginInfo LoadLoginInfo();

      void SaveLoginInfo(LoginInfo info);      
   }
}