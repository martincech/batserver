﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Bat2Library.Sms;
using BatWebSynchronizer.Device;
using BatWebSynchronizer.Synchronizer;

namespace BatWebSynchronizer.PermanentStorage
{
   public class FileStorage : IStoredData
   {
      private readonly string storageDirectory;
      //Data file name for messages in right format
      private const string TOSYNC_FILE = @"\ToSync.xml";
      //Dump file name for messages in wrong format
      private const string DUMP_FILE = @"\Dump.xml";
      //Data file name for synced messages in right format
      private const string SYNCED_FILE = @"\Synced.xml";
      // synchronization info
      private const string LOGIN_FILE = @"\Login.xml";
      private const string ELO_FILE = @"\EloDevices.xml";
      private const string APP_SETTINGS_FILE = @"\UserAppSettings.xml";
      public static string Folder
      {
         get
         {
            var folder = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) + @"\Veit\Bat2WebSynchronizer";
            if (!Directory.Exists(folder))
            {
               Directory.CreateDirectory(folder);
            }
            return folder;
         }
      }

      public FileStorage()
         : this(Folder)
      {
      }

      public FileStorage(string storageDirectory)
      {
         this.storageDirectory = storageDirectory;
      }

      public IEnumerable<StatisticData> LoadUnsyncedData()
      {
         return XmlSerializer.Load<StatisticData>(FilePath(TOSYNC_FILE));
      }

      public void SaveUnsyncedData(IEnumerable<StatisticData> unsynced)
      {
         XmlSerializer.Update(unsynced.ToList(), FilePath(TOSYNC_FILE));
      }

      public void SaveUnsyncedData(StatisticData unsynced)
      {
         XmlSerializer.Append(unsynced, FilePath(TOSYNC_FILE));
      }

      public IEnumerable<StatisticData> LoadSyncedData()
      {
         return XmlSerializer.Load<StatisticData>(FilePath(SYNCED_FILE));
      }

      public void SaveSyncedData(IEnumerable<StatisticData> synced)
      {
         XmlSerializer.Update(synced.ToList(), FilePath(SYNCED_FILE));
      }

      public void SaveSyncedData(StatisticData synced)
      {
         XmlSerializer.Append(synced, FilePath(SYNCED_FILE));
      }

      public IEnumerable<Elo> LoadEloSettings()
      {
         return  XmlSerializer.Load<Elo>(FilePath( ELO_FILE));
      }

      public void SaveEloSettings(IEnumerable<Elo> elos)
      {
         XmlSerializer.Update(elos.ToList(), FilePath(ELO_FILE));
      }

      public void SaveEloSettings(Elo elo)
      {
         var eloList = XmlSerializer.Load<Elo>(FilePath(ELO_FILE));
         var savedElo = eloList.FirstOrDefault(f => f.SerialNumber == elo.SerialNumber);
         if (savedElo != null)
         {
            eloList.Remove(savedElo);
            elo.SynchronizationPeriod = savedElo.SynchronizationPeriod;
         }
         eloList.Add(elo);
         SaveEloSettings(eloList);
      }

      public void Dump(string message)
      {
         XmlSerializer.Append(message, FilePath(DUMP_FILE));
      }

      public LoginInfo LoadLoginInfo()
      {
         var info = XmlSerializer.Load<LoginInfo>(FilePath(LOGIN_FILE));
         return !info.Any() ? new LoginInfo() : info.First();
      }

      public void SaveLoginInfo(LoginInfo info)
      {
         XmlSerializer.Update(new List<LoginInfo> {info}, FilePath(LOGIN_FILE));
      }

      private string FilePath(string file)
      {
         return storageDirectory + file;
      }


      public void SaveBaloonNotification(bool on)
      {
         XmlSerializer.Update(new List<AppSettings>() {new AppSettings {BaloonNotifications = on}},
            FilePath(APP_SETTINGS_FILE));
      }
   }
}