using System;

namespace BatWebSynchronizer
{
   public interface ILogger
   {
      void Log(string message);
      void Log(DateTime time, string message);
   }
}