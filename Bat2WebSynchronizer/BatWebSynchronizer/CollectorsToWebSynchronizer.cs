using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using BatWebSynchronizer.Collector;
using BatWebSynchronizer.Device;
using BatWebSynchronizer.PermanentStorage;
using BatWebSynchronizer.Synchronizer;

namespace BatWebSynchronizer
{
   public class CollectorsToWebSynchronizer : DataCollector<IDevice>
   {
      private readonly ILogger logger;
      private readonly IStoredData storage;
      private readonly IWebApi api;
      private readonly List<object> collectors;

      private WebSynchro synchro;

      public CollectorsToWebSynchronizer(IStoredData storage, IWebApi api, ILogger logger)
      {
         this.storage = storage;
         this.api = api;
         this.logger = logger;

         collectors = new List<object>();
      }

      public void ReloadElos()
      {
         logger.Log("Reloading elo settings.");
         ReloadCollector <ModbusCollector, Elo> (() => new ModbusCollector(storage, logger));
         UpdateElosSettingsFromStorage();
      }

      public void ReloadModems()
      {
         logger.Log("Reloading modem settings.");
         ReloadCollector<SmsCollector, Modem>(() => new SmsCollector(storage, logger));
      }

      private void ReloadCollector<T, U>(Func<T> construct) 
         where T : DataCollector<U> 
         where U: IDevice
      {
         var collector = (T) collectors.FirstOrDefault(c => c is T);
         if (collector != null) return;
         
         collector = construct();
         collectors.Add(collector);
         collector.DataReaded += CollectorDataReaded;
         collector.DeviceList.CollectionChanged += OnDeviceConnectivity;
         AddCollectorDevices<T, U>(collector);
      }

      private void UpdateElosSettingsFromStorage()
      {
         var elosSettings = storage.LoadEloSettings().ToList();
         foreach (var device in DeviceList)
         {
            var dev = (Elo)device;
            var savedSettings = elosSettings.FirstOrDefault(x => x.SerialNumber == dev.SerialNumber);
            if (savedSettings == null) continue;
            dev.SynchronizationPeriod = savedSettings.SynchronizationPeriod;
         }
      }

      private void RemoveCollectorDevice<U>(U device) where U : IDevice
      {
         if (DeviceList.Contains(device))
         {
            DeviceList.Remove(device);
         }
      }
      private void AddCollectorDevices<T, U>(T collector) where T : DataCollector<U> where U : IDevice
      {
         foreach (var device in collector.DeviceList)
         {
            AddCollectorDevice(device);
         }
      }

      private void AddCollectorDevice<U>(U device) where U : IDevice
      {
         if (!DeviceList.Contains(device))
         {
            DeviceList.Add(device);
         }
      }


      private void OnDeviceConnectivity(object sender, NotifyCollectionChangedEventArgs e)
      {
         switch (e.Action)
         {
            case NotifyCollectionChangedAction.Add:
               foreach (var device in e.NewItems)
               {
                  AddCollectorDevice((IDevice)device);
               }
               break;
            case NotifyCollectionChangedAction.Remove:
            case NotifyCollectionChangedAction.Reset:
               foreach (var device in e.OldItems)
               {
                  RemoveCollectorDevice((IDevice)device);
               }
               break;
            default:
               return;
         }
      }

      public void ReloadSynchronizer()
      {
         logger.Log("Reloading synchronization settings.");
         if (synchro == null)
         {
            synchro = new WebSynchro(storage, api, logger);
            synchro.Synced += SynchroOnSynced;
         }
         synchro.Stop();
         synchro.Start();
      }

      private void SynchroOnSynced(object sender, StatisticData statisticData)
      {
         OnDataSynced();
      }

      public event EventHandler DataSynced;

      private void CollectorDataReaded(object sender, StatisticData data)
      {
         if (synchro != null)
         {
            synchro.Sync(data);
         }
         OnDataReaded(data);
      }

      public void Start()
      {
         ReloadElos();
         ReloadModems();
         ReloadSynchronizer();
      }

      public void Stop()
      {
         synchro.Stop();
      }

      protected virtual void OnDataSynced()
      {
         var handler = DataSynced;
         if (handler != null) handler(this, EventArgs.Empty);
      }
   }
}