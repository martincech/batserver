﻿using System;
using System.Drawing;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Media.Imaging;

namespace Bat2WebSynchronizer.Tray.Helpers
{
   public static class IconHelper
   {
      private static BitmapSource _green;
      private static BitmapSource _red;
      private static BitmapSource _yellow;
      private static BitmapSource _gear;
      private static Icon _tray;
      private static Icon _trayErr;

      public static BitmapSource Green
      {
         get { return _green ?? (_green = GetBitMap(Properties.Resources.GREEN)); }
      }

      public static BitmapSource Red
      {
         get { return _red ?? (_red = GetBitMap(Properties.Resources.RED)); }
      }

      public static BitmapSource Yellow
      {
         get { return _yellow ?? (_yellow = GetBitMap(Properties.Resources.YELLOW)); }
      }

      public static BitmapSource Gear
      {
         get { return _gear ?? (_gear = GetBitMap(Properties.Resources.GEAR)); }
      }

      public static Icon Tray
      {
         get { return _tray ?? (_tray = Properties.Resources.ICON); }
      }

      public static Icon TrayErr
      {
         get { return _trayErr ?? (_trayErr = Properties.Resources.ICON_ERR); }
      }

      private static BitmapSource GetBitMap(Bitmap img)
      {
         var hBitmap = img.GetHbitmap();
         return Imaging.CreateBitmapSourceFromHBitmap(
                  hBitmap,
                  IntPtr.Zero,
                  Int32Rect.Empty,
                  BitmapSizeOptions.FromEmptyOptions());
      }
   }
}
