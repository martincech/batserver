﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Windows;
using Windows;
using Bat2WebSynchronizer.Tray.View;
using Bat2WebSynchronizer.Tray.VmToMMapping;
using Desktop.Wpf;
using Desktop.Wpf.Presentation;

namespace Bat2WebSynchronizer.Tray
{
   public partial class App
   {
      public App()
      {
         if (SingleApplication.IsAlreadyRunning())
         {
            Environment.Exit(0);
         }
         InitializeComponent();
         DispatcherHelper.Initialize();
      }

      [STAThread]
      [DebuggerNonUserCode]
      public static void Main()
      {
         AppDomain.CurrentDomain.AssemblyLoad += (sender, args) => { Logger.Write(args.LoadedAssembly.FullName); };
         AppDomain.CurrentDomain.UnhandledException += CurrentDomainOnUnhandledException;

         // init frontend
         var mainWindow = new MainWindow();
         var application = new App {MainWindow = mainWindow};
         // init backend         
         var viewModels = new ViewModels(mainWindow);
         ThreadPool.QueueUserWorkItem(param =>
         {
            viewModels.Start();
         });

         //run
         application.Run();
      }

      private static void CurrentDomainOnUnhandledException(object sender, UnhandledExceptionEventArgs e)
      {
         var ex = (Exception) e.ExceptionObject;
         Logger.Write("Unhandled Error\n\t\t" + ex.Message + "\n\t\t" + ex.StackTrace + "\n");

         MessageBox.Show(ex.Message + ex.StackTrace,
            "Error", MessageBoxButton.OK, MessageBoxImage.Stop);
      }
   }
}
