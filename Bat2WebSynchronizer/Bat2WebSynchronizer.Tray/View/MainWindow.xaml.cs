﻿using System.Threading;
using System.Windows;
using Desktop.Wpf.NotifyIcon;
using Desktop.Wpf.Presentation;

namespace Bat2WebSynchronizer.Tray.View
{
   /// <summary>
   ///    Interaction logic for MainWindow.xaml
   /// </summary>
   public partial class MainWindow 
   {
      public MainWindow()
      {
         InitializeComponent();
      }

      public TaskbarIcon NotifyIcon { get { return notifyIcon;} }
      private void Exit_Click(object sender, RoutedEventArgs e)
      {
         ThreadPool.QueueUserWorkItem(delegate
         {
            var result = MessageBox.Show(Properties.Resources.EXIT_DIALOG, Properties.Resources.EXIT,
               MessageBoxButton.YesNo, MessageBoxImage.Warning);
            DispatcherHelper.RunAsync(() =>
            {
               if (result == MessageBoxResult.Yes)
               {
                  Close();
               }
            });
         }, null);
      }
   }
}
