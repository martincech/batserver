﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using MvvmFramework.Observable;

namespace Bat2WebSynchronizer.Tray.ViewModel
{
   public class LogViewModel : ObservableObject
   {
      private ObservableCollection<KeyValuePair<DateTime, string>> logMessage;

      /// <summary>
      /// Initializes a new instance of the <see cref="T:System.Object"/> class.
      /// </summary>
      public LogViewModel()
      {
         LogMessage = new ObservableCollection<KeyValuePair<DateTime, string>>();
      }

      public ObservableCollection<KeyValuePair<DateTime, string>> LogMessage
      {
         get { return logMessage; }
         private set { SetProperty(ref logMessage, value); }
      }
   }
}