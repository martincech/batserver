﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using Bat2WebSynchronizer.Tray.View;
using BatWebSynchronizer;
using Desktop.Wpf.Applications;
using Desktop.Wpf.NotifyIcon;

namespace Bat2WebSynchronizer.Tray.ViewModel
{
   public class TrayOnlyMainWindowVm : MainVm, ILogger
   {
      private readonly TaskbarIcon notifyIcon;

      private SyncStatus lastMessage;
      private ICommand showLogCommand;
      private ICommand showSettingsCommand;
      private ICommand showEloSettingsCommand;
      private bool showNotification;
      private LogViewModel logViewModel;
      private Action<bool> saveNotificationSettingsAction;
      private int synchronizationHour;
      private int synchronizationMinute;

      public enum SyncStatus
      {
         None,
         CredentialsMissing,
         AutosyncDisabled,
         ServerUrlMissing,
         MessageReceived,
         LogMessage,
         ComPortsMissing
      }


      public TrayOnlyMainWindowVm(TaskbarIcon notifyIcon)
      {
         this.notifyIcon = notifyIcon;
         notifyIcon.TrayBalloonTipClicked += NotifyIconOnTrayBalloonTipClicked;
      }
      
      private void NotifyIconOnTrayBalloonTipClicked(object sender, RoutedEventArgs routedEventArgs)
      {
         notifyIcon.CloseBalloon();
         switch (lastMessage)
         {
            case SyncStatus.CredentialsMissing:
            case SyncStatus.AutosyncDisabled:
            case SyncStatus.ServerUrlMissing:
               ShowSettings();
               break;
            case SyncStatus.MessageReceived:
               ShowLog();
               break;
            case SyncStatus.LogMessage:
               ShowLog();
               break;
         }
      }

      public override WebConnectivityViewModel ConnectivityViewModel
      {
         get { return base.ConnectivityViewModel; }
         set
         {
            base.ConnectivityViewModel = value;
            CheckConnectivitySettings();
         }
      }

      public LogViewModel LogViewModel
      {
         get { return logViewModel; }
         set { SetProperty(ref logViewModel, value); }
      }

      public ICommand ShowSettingsCommand
      {
         get
         {
            // set serverSettings windows allways as window to popup after left click
            return showSettingsCommand ?? (showSettingsCommand = new RelayCommand(ShowSettings));
         }
      }

      public ICommand ShowLogCommand
      {
         get
         {
            // set serverSettings windows allways as window to popup after left click
            return showLogCommand ?? (showLogCommand = new RelayCommand(ShowLog));
         }
      }

      public ICommand ShowEloSettingsCommand
      {
         get
         {
            // set serverSettings windows allways as window to popup after left click
            return showEloSettingsCommand ?? (showEloSettingsCommand = new RelayCommand<DeviceViewModel>(ShowElo));
         }
      }

      public Action<bool> SaveNotificationSettingsAction
      {
         get { return saveNotificationSettingsAction; }
         set { SetProperty(ref saveNotificationSettingsAction, value); }
      }


      public int SynchronizationHour
      {
         get { return synchronizationHour; }
         set
         {
            SetProperty(ref synchronizationHour, value);
            foreach (var eloVm in DeviceList.Where(d=>d is EloViewModel).Cast<EloViewModel>())
            {
               //eloVm.Hour = value;
            }
         }
      }

      public int SynchronizationMinute
      {
         get { return synchronizationMinute; }
         set
         {
            SetProperty(ref synchronizationMinute, value);
            foreach (var eloVm in DeviceList.Where(d => d is EloViewModel).Cast<EloViewModel>())
            {
               //eloVm.Minute = value;
            }
         }
      }

      public bool ShowNotification
      {
         get { return showNotification; }
         set
         {
            SetProperty(ref showNotification, value);
            SaveNotificationSettings();
         }
      }

      private void SaveNotificationSettings()
      {
         if (SaveNotificationSettingsAction != null)
         {
            SaveNotificationSettingsAction(ShowNotification);
         }
      }

      private void ShowSettings()
      {
         ShowPopupAndStayOpened(new WebConnectivityView { DataContext = ConnectivityViewModel });
      }

      private void ShowLog()
      {
         ShowPopupAndStayOpened(new LogView {DataContext = LogViewModel});
      }

      public void ShowElo(DeviceViewModel device)
      {
         if (!(device is EloViewModel))
         {
            return;
         }
         SelectedDevice = (EloViewModel) device;
         ShowPopupAndStayOpened(new EloView {DataContext = SelectedDevice });
      }

      private void ShowPopupAndStayOpened(UIElement view)
      {
         notifyIcon.TrayPopup = view;
         notifyIcon.TrayPopupResolved.Closed -= PopupWindowOnClosed;
         notifyIcon.TrayPopupResolved.Closed += PopupWindowOnClosed;
         notifyIcon.ShowTrayPopup();
      }

      /// <summary>
      ///    Service for closing popup window.
      /// </summary>
      private void PopupWindowOnClosed(object sender, EventArgs eventArgs)
      {
         if (notifyIcon.TrayPopup is WebConnectivityView)
         {
            OnConnectivityViewClosed();
         }
         if (notifyIcon.TrayPopup is EloView)
         {
            OnEloViewClosed();
         }
      }

      private void OnConnectivityViewClosed()
      {
         CheckConnectivitySettings();
      }

      private void OnEloViewClosed()
      {
         if (SelectedDevice == null) return;
         SelectedDevice = null;
      }
      
      public void Log(string message)
      {
         Log(DateTime.Now, message);
      }

      public void Log(DateTime time, string message)
      {
         Dispatcher.BeginInvoke(DispatcherPriority.Background, (Action) (() =>
         {
            if (LogViewModel != null)
            {
               LogViewModel.LogMessage.Add(new KeyValuePair<DateTime, string>(time, message));
            }
            ShowBalooonMessage(SyncStatus.LogMessage, message);
         }));

      }


      private void CheckConnectivitySettings()
      {
         if (ConnectivityViewModel == null)
         {
            return;
         }
         if (string.IsNullOrEmpty(ConnectivityViewModel.UserName))
         {
            ShowBalooonMessage(SyncStatus.CredentialsMissing);
         }
         else if (string.IsNullOrEmpty(ConnectivityViewModel.Password))
         {
            ShowBalooonMessage(SyncStatus.CredentialsMissing);
         }
         else if (string.IsNullOrEmpty(ConnectivityViewModel.ServerUrl))
         {
            ShowBalooonMessage(SyncStatus.ServerUrlMissing);
         }
      }

      private void ShowBalooonMessage(SyncStatus message, string messageText = null)
      {
         if (!ShowNotification)
         {
            return;
         }

         if (lastMessage == message)
         {
            return;
         }
         string tip;

         switch (message)
         {
            case SyncStatus.AutosyncDisabled:
               tip = Properties.Resources.MESSAGE_AUTOSYNC_DISABLED;
               notifyIcon.ShowBalloonTip(Properties.Resources.TITLE_BAT2, tip, BalloonIcon.Warning);
               lastMessage = SyncStatus.AutosyncDisabled;
               break;
            case SyncStatus.CredentialsMissing:
               tip = Properties.Resources.MESSAGE_MISSING_CREDENTIALS;
               notifyIcon.ShowBalloonTip(Properties.Resources.TITLE_BAT2, tip, BalloonIcon.Error);
               lastMessage = SyncStatus.CredentialsMissing;
               break;
            case SyncStatus.ServerUrlMissing:
               tip = Properties.Resources.MESSAGE_MISSING_SERVERURL;
               notifyIcon.ShowBalloonTip(Properties.Resources.TITLE_BAT2, tip, BalloonIcon.Error);
               lastMessage = SyncStatus.CredentialsMissing;
               break;
            case SyncStatus.MessageReceived:
               tip = Properties.Resources.MESSAGE_SMS_RECEIVED;
               notifyIcon.ShowBalloonTip(Properties.Resources.TITLE_BAT2, tip, BalloonIcon.Info);
               lastMessage = SyncStatus.MessageReceived;
               break;
            case SyncStatus.ComPortsMissing:
               tip = Properties.Resources.MESSAGE_MISSING_COMPORT;
               notifyIcon.ShowBalloonTip(Properties.Resources.TITLE_BAT2, tip, BalloonIcon.Error);
               lastMessage = SyncStatus.MessageReceived;
               break;
            case SyncStatus.LogMessage:
               if (!string.IsNullOrEmpty(messageText))
               {
                  notifyIcon.ShowBalloonTip(Properties.Resources.TITLE_BAT2, messageText, BalloonIcon.Info);
                  lastMessage = SyncStatus.LogMessage;
               }
               break;
         }
         notifyIcon.ResetBalloonCloseTimer();
      }


   }
}
