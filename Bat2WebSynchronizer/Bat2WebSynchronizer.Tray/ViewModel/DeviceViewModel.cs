﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Windows.Threading;
using BatWebSynchronizer.Device;
using MvvmFramework.Observable;

namespace Bat2WebSynchronizer.Tray.ViewModel
{
   public abstract class DeviceViewModel : ObservableObject
   {
      
      private readonly IDevice deviceModel;
      private ObservableCollection<ScaleViewModel> scales;
      protected readonly Dispatcher Dispatcher;


      protected DeviceViewModel(IDevice deviceModel)
      {
         this.deviceModel = deviceModel;
         Scales = new ObservableCollection<ScaleViewModel>();
         deviceModel.DeviceList.CollectionChanged += ScalesOnCollectionChanged;
         Dispatcher = Dispatcher.CurrentDispatcher;

      }

      private void ScalesOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
      {
         Dispatcher.BeginInvoke(DispatcherPriority.DataBind, (Action) (() =>
         {
            switch (e.Action)
            {
               case NotifyCollectionChangedAction.Add:
                  foreach (var scale in e.NewItems)
                  {
                     Scales.Add(new ScaleViewModel((Scale)scale));
                  }
                  break;
               case NotifyCollectionChangedAction.Remove:
               case NotifyCollectionChangedAction.Replace:
               case NotifyCollectionChangedAction.Move:
                  foreach (var scale in e.OldItems)
                  {
                     Scales.Remove(new ScaleViewModel((Scale)scale));
                  }
                  break;

               case NotifyCollectionChangedAction.Reset:
                  Scales.Clear();
                  break;
               default:
                  throw new ArgumentOutOfRangeException();
            }
         }));

      }

      public string Description
      {
         get { return deviceModel.Description; }
      }

      public string ProductName
      {
         get { return deviceModel.ProductName; }
      }

      public string SerialNumber
      {
         get { return deviceModel.SerialNumber; }
      }

      public string PortName
      {
         get { return deviceModel.PortName; }
      }

      public ObservableCollection<ScaleViewModel> Scales
      {
         get { return scales; }
         private set { SetProperty(ref scales, value); }
      }
   }
}