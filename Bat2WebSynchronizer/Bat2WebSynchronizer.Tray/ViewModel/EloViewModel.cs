﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Windows.Threading;
using BatWebSynchronizer.Device;
using Desktop.Wpf.Applications;

namespace Bat2WebSynchronizer.Tray.ViewModel
{
   public class EloViewModel : DeviceViewModel
   {
      private readonly Elo eloModel;
      private RelayCommand startScanEloCommand;
      private RelayCommand stopScanEloCommand;
      private RelayCommand saveEloSettingsCommand;
      public EloViewModel(Elo eloModel) : base(eloModel)
      {
         this.eloModel = eloModel;
         //eloModel.ScanningStatusChanged += OnScanningStatusChanged;
      }

      private void OnScanningStatusChanged(object sender, EventArgs eventArgs)
      {
         Dispatcher.BeginInvoke(DispatcherPriority.Background, (Action) (() =>
         {
            //RaisePropertyChanged(() => CurrentlyScanningAddress);
            //RaisePropertyChanged(() => Scanning);
            CommandsChanged();
         }));

      }

      public byte AddressFrom
      {
         get { return eloModel.AddressFrom; }
         set
         {
            eloModel.AddressFrom = value;
            ChangeAndSaveEloModel();
         }
      }

      private void ChangeAndSaveEloModel([CallerMemberName] string propertyName = null)
      {
         Debug.Assert(propertyName != null);
         RaisePropertyChanged(propertyName);
         eloModel.SaveSettings();
      }

      public byte AddressTo
      {
         get { return eloModel.AddressTo; }
         set
         {
            eloModel.AddressTo = value;
            ChangeAndSaveEloModel();
         }
      }

      public int Mode
      {
         get { return eloModel.Mode; }
         set
         {
            eloModel.Mode = value;
            ChangeAndSaveEloModel();
         }
      }

      public int Parity
      {
         get { return eloModel.Parity; }
         set
         {
            eloModel.Parity = value;
            ChangeAndSaveEloModel();
         }
      }

      public int BaudRate
      {
         get { return eloModel.BaudRate; }
         set
         {
            eloModel.BaudRate = value;
            ChangeAndSaveEloModel();
         }
      }

      public int PortNumber
      {
         get { return eloModel.PortNumber; }
      }

      //public int Hour
      //{
      //   get { return eloModel.Hour; }
      //   set
      //   {
      //      eloModel.Hour = value;
      //      ChangeAndSaveEloModel();
      //   }
      //}

      //public int Minute
      //{
      //   get { return eloModel.Minute; }
      //   set
      //   {
      //      eloModel.Minute = value;
      //      ChangeAndSaveEloModel();
      //   }
      //}

      //public bool Scanning
      //{
      //   get { return eloModel.Scanning; }
      //}

      //public int CurrentlyScanningAddress
      //{
      //   get { return eloModel.CurrentlyScanningAddress; }
      //}


      /// <summary>
      ///    Start scanning command
      /// </summary>
      public RelayCommand StartScanEloCommand
      {
         get
         {
             return startScanEloCommand ?? (startScanEloCommand = new RelayCommand(
                 () =>
                 {
                     //TODO
                     CommandsChanged();
                 }));
             //, () => !eloModel.Scanning));
         }
      }

      public RelayCommand StopScanEloCommand
      {
         get
         {
             return stopScanEloCommand ?? (stopScanEloCommand = new RelayCommand(
                 () =>
                 {
                     //TODO
                     CommandsChanged();
                 }));
             //, () => eloModel.Scanning));
         }
      }

      private void CommandsChanged()
      {
         StartScanEloCommand.RaiseCanExecuteChanged();
         StopScanEloCommand.RaiseCanExecuteChanged();
      }
   }
}
