using System;
using System.Windows.Threading;
using Bat2Library.Sms;
using BatWebSynchronizer.Device;

namespace Bat2WebSynchronizer.Tray.ViewModel
{
   public class ModemViewModel : DeviceViewModel
   {
      private readonly Modem modemModel;

      public ModemViewModel(Modem modemModel) : base(modemModel)
      {
         this.modemModel = modemModel;
         this.modemModel.StatusChanged += OnModemStatusChanged;
      }

      private void OnModemStatusChanged(object sender, EventArgs eventArgs)
      {
         Dispatcher.BeginInvoke(DispatcherPriority.Background, (Action) (() =>
         {
            RaisePropertyChanged(() => SignalStrength);
            RaisePropertyChanged(() => Event);
            RaisePropertyChanged(() => ModemName);
         }));

      }

      public int SignalStrength
      {
         get { return modemModel.SignalStrength; }
      }

      public SmsEvent Event
      {
         get { return modemModel.Event; }
      }

      public string ModemName
      {
         get { return modemModel.ModemName; }
      }
   }
}