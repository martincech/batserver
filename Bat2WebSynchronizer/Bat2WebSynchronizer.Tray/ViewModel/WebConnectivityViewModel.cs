﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;
using System.Windows.Input;
using BatWebSynchronizer.PermanentStorage;
using BatWebSynchronizer.Synchronizer;
using MvvmFramework.Observable;

namespace Bat2WebSynchronizer.Tray.ViewModel
{
   /// <summary>
   ///    Program data holder
   /// </summary>
   public class WebConnectivityViewModel : ValidatableObservableObject
   {
      private readonly LoginInfo login;
      private readonly IWebApi api;
      private ICommand saveLoginInfoCommand;
      private int receivedCount;
      private int syncedCount;


      public WebConnectivityViewModel(LoginInfo login, IWebApi api)
      {
         this.login = login;
         this.api = api;
         api.ConnectivityStatusChanged += OnConnectivityChanged;
      }

      private void OnConnectivityChanged(object sender, EventArgs eventArgs)
      {
         RaisePropertyChanged(() => IsUserLogged);
         RaisePropertyChanged(() => IsBusy);
      }

      public bool IsUserLogged
      {
         get { return api.IsLogged; }
      }

      public bool IsBusy
      {
         get { return api.IsCommunicating; }
      }

      public string Password
      {
         get { return login.Password; }
         set
         {
            login.Password = value;
            RaisePropertyChanged();
         }
      }

      public string UserName
      {
         get { return login.UserName; }
         set
         {
            login.UserName = value;
            RaisePropertyChanged();
         }
      }

      public string ServerUrl
      {
         get { return login.ServerUrl; }
         set
         {
            var prevValue = login.ServerUrl;
            login.ServerUrl = value;
            if (!Validate())
            {
               login.ServerUrl = prevValue;
            }
            RaisePropertyChanged();
         }
      }

      public int ReceivedCount
      {
         get { return receivedCount; }
         set { SetProperty(ref receivedCount, value); }
      }

      public int SyncedCount
      {
         get { return syncedCount; }
         set { SetProperty(ref syncedCount, value); }
      }


      public ICommand SaveLoginInfoCommand
      {
         get { return saveLoginInfoCommand; }
         set { SetProperty(ref saveLoginInfoCommand, value); }
      }

      protected override IEnumerable<ValidationResult> AdditionalValidationRules(object value, string propertyName)
      {
         var errorMessage = "";
         IEnumerable<string> objects = new[] {""};

         if (propertyName.Equals("ServerUrl"))
         {
            var valid = true;

            var url = (string) value;
            if (string.IsNullOrEmpty(url) || !Regex.IsMatch(url, "^[a-zA-Z0-9]"))
            {
               valid = false;
            }

            if (valid)
            {
               url = url.ToLower();
               if (!url.StartsWith("http://") && !url.StartsWith("https://"))
               {
                  url = "http://" + url;
               }

               var urlchk =
                  new Regex(
                     @"((http|https)://)+(([a-zA-Z0-9\._-]+\.[a-zA-Z]{2,15})|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(/[a-zA-Z0-9\&amp;%_\./-~-]*)?",
                     RegexOptions.Singleline | RegexOptions.IgnoreCase);

               valid = urlchk.IsMatch(url);
            }

            if (!valid)
            {
               objects = new[] {"ServerUrl"};
               errorMessage = "error";
            }
         }


         if (!errorMessage.Equals(""))
         {
            var valRes = new List<ValidationResult>
            {
               new ValidationResult(errorMessage, objects)
            };
            return valRes;
         }

         return base.AdditionalValidationRules(value, propertyName);
      }
   }
}
