﻿using BatWebSynchronizer.Device;
using MvvmFramework.Observable;

namespace Bat2WebSynchronizer.Tray.ViewModel
{
   public class ScaleViewModel : ObservableObject
   {
      private readonly Scale scaleModel;

      public ScaleViewModel(Scale scaleModel)
      {
         this.scaleModel = scaleModel;
      }


      public string Address
      {
         get
         {
            return scaleModel.Address;
            //int address = 0;
            //int.TryParse(scaleModel.Address, out address);
            //return address;
         }
         set
         {
            scaleModel.Address = value;
            RaisePropertyChanged();
         }
      }

      public string Name
      {
         get { return scaleModel.Name; }
         set
         {
            scaleModel.Name = value;
            RaisePropertyChanged();
         }
      }

      public bool Connected
      {
         get { return scaleModel.Connected; }
         set
         {
            scaleModel.Connected = value;
            RaisePropertyChanged();
         }
      }

      public override string ToString()
      {
         return string.Format("Scale {0} [Address {1}]", Name, Address);
      }
   }
}
