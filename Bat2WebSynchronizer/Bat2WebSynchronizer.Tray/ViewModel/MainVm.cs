using System.Collections.ObjectModel;
using System.Windows.Threading;
using MvvmFramework.Observable;

namespace Bat2WebSynchronizer.Tray.ViewModel
{
   public class MainVm : ObservableObject
   {
      private WebConnectivityViewModel connectivityViewModel;
      private ObservableCollection<DeviceViewModel> deviceList;
      private DeviceViewModel selectedDevice;
      protected Dispatcher Dispatcher { get; private set; }

      public MainVm()
      {
         DeviceList = new ObservableCollection<DeviceViewModel>();
         Dispatcher = Dispatcher.CurrentDispatcher;
      }

      public virtual WebConnectivityViewModel ConnectivityViewModel
      {
         get { return connectivityViewModel; }
         set { SetProperty(ref connectivityViewModel, value); }
      }

      public ObservableCollection<DeviceViewModel> DeviceList
      {
         get { return deviceList; }
         private set { SetProperty(ref deviceList, value); }
      }

      public DeviceViewModel SelectedDevice
      {
         get { return selectedDevice; }
         set { SetProperty(ref selectedDevice, value); }
      }
   }
}