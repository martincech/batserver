﻿using System;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using System.Windows.Threading;
using Bat2WebSynchronizer.Tray.View;
using Bat2WebSynchronizer.Tray.ViewModel;
using BatWebSynchronizer;
using BatWebSynchronizer.Device;
using BatWebSynchronizer.PermanentStorage;
using BatWebSynchronizer.Synchronizer;
using Desktop.Wpf.Applications;
using Desktop.Wpf.NotifyIcon;

namespace Bat2WebSynchronizer.Tray.VmToMMapping
{
   public class ViewModels
   {
      private readonly MainWindow mainWindow;
      private readonly FileStorage storage;
      private readonly WebApi api;
      private LogViewModel logViewModel;
      private WebConnectivityViewModel connectivityVm;
      private TrayOnlyMainWindowVm trayOnlyMainWindowVm;
      private CollectorsToWebSynchronizer cws;

      public ViewModels(MainWindow mainWindow)
      {
         this.mainWindow = mainWindow;
         storage = new FileStorage();
         api = new WebApi();
         InitMainWindowVm(mainWindow.NotifyIcon);
         InitLogVm();
         InitConnectivityVm();
         InitCws();
         mainWindow.DataContext = trayOnlyMainWindowVm;
      }

      private void InitLogVm()
      {
         logViewModel = new LogViewModel();
         trayOnlyMainWindowVm.LogViewModel = logViewModel;
      }

      private void InitConnectivityVm()
      {
         connectivityVm = new WebConnectivityViewModel(storage.LoadLoginInfo(), api)
         {
            SaveLoginInfoCommand = new RelayCommand(SaveLogin)
         };
         trayOnlyMainWindowVm.ConnectivityViewModel = connectivityVm;
      }

      private void SaveLogin()
      {
         storage.SaveLoginInfo(new LoginInfo
         {
            ServerUrl = connectivityVm.ServerUrl,
            Password = connectivityVm.Password,
            UserName = connectivityVm.UserName
         });
         cws.ReloadSynchronizer();
      }

      private void InitCws()
      {
         cws = new CollectorsToWebSynchronizer(storage, api, trayOnlyMainWindowVm);
         cws.DataReaded += (sender, status) => connectivityVm.ReceivedCount++;
         cws.DataSynced += (sender, data) => connectivityVm.SyncedCount++;
         cws.DeviceList.CollectionChanged += (sender, e) =>
         {
            mainWindow.Dispatcher.BeginInvoke(DispatcherPriority.Background, (Action) (() =>
            {
               switch (e.Action)
               {
                  case NotifyCollectionChangedAction.Add:
                     foreach (var device in e.NewItems)
                     {
                        if (device is Elo)
                        {
                           trayOnlyMainWindowVm.DeviceList.Add(new EloViewModel((Elo) device));
                        }
                        if (device is Modem)
                        {
                           trayOnlyMainWindowVm.DeviceList.Add(new ModemViewModel((Modem) device));
                        }
                     }
                     break;
                  case NotifyCollectionChangedAction.Remove:
                  case NotifyCollectionChangedAction.Replace:
                  case NotifyCollectionChangedAction.Move:
                     foreach (var device in e.OldItems)
                     {
                        Func<DeviceViewModel, bool> check = null;
                        if (device is Elo)
                        {
                           check = vm => vm is EloViewModel && vm.SerialNumber == ((IDevice)device).SerialNumber;
                        }
                        if (device is Modem)
                        {
                           check = vm => vm is ModemViewModel && vm.SerialNumber == ((IDevice)device).SerialNumber;
                        }
                        Debug.Assert(check != null);
                        var dev = trayOnlyMainWindowVm.DeviceList.FirstOrDefault(check);
                        if (dev != null)
                        {
                           trayOnlyMainWindowVm.DeviceList.Remove(dev);
                        }
                     }
                     break;
                  case NotifyCollectionChangedAction.Reset:
                     trayOnlyMainWindowVm.DeviceList.Clear();                     
                     break;
                  default:
                     throw new ArgumentOutOfRangeException();
               }
            }));
         };
      }

      private void InitMainWindowVm(TaskbarIcon notifyIcon)
      {
         trayOnlyMainWindowVm = new TrayOnlyMainWindowVm(notifyIcon)
         {
            SaveNotificationSettingsAction = v => SaveNotificationSettingsAction(v)
         };
      }

      private void SaveNotificationSettingsAction(bool on)
      {
         storage.SaveBaloonNotification(on);
      }


      public void Start()
      {
         cws.Start();
      }
   }
}