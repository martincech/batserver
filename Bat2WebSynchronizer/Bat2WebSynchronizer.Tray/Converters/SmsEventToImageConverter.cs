﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media.Imaging;
using Bat2Library.Sms;
using Bat2WebSynchronizer.Tray.Helpers;

namespace Bat2WebSynchronizer.Tray.Converters
{
   public class SmsEventToImageConverter : IValueConverter
   {
      public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
      {

         var ev = value as SmsEvent? ?? SmsEvent.INIT;
         BitmapSource img;
         switch (ev)
         {
            case SmsEvent.RESET:
            case SmsEvent.INIT:
            case SmsEvent.DETECT:
            case SmsEvent.CHECK:
            case SmsEvent.SIGNAL:
            case SmsEvent.OPERATOR:
            case SmsEvent.STRENGTH:
               img = IconHelper.Yellow;
               break;
            case SmsEvent.SMS_SENDING:
            case SmsEvent.SMS_SEND_OK:
            case SmsEvent.SMS_SEND_ERROR:
            case SmsEvent.SMS_READING:
            case SmsEvent.SMS_READ_OK:
            case SmsEvent.SMS_READ_ERROR:
            case SmsEvent.SMS_DELETING:
            case SmsEvent.SMS_DELETE_OK:
            case SmsEvent.SMS_DELETE_ERROR:
            case SmsEvent.READY:
               img = IconHelper.Green;
               break;
            default:
               img = IconHelper.Red;
               break;
         }
         return img;
      }

      public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
      {
         throw new NotImplementedException();
      }
   }
}
