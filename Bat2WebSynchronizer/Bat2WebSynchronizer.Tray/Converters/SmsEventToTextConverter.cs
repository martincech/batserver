﻿using System;
using System.Globalization;
using System.Windows.Data;
using Bat2Library.Sms;
using Bat2WebSynchronizer.Tray.Properties;

namespace Bat2WebSynchronizer.Tray.Converters
{
   public class SmsEventToTextConverter : IValueConverter
   {
      public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
      {
         var ev = value as SmsEvent? ?? SmsEvent.INIT;
         string msg;
         switch (ev)
         {
            case SmsEvent.RESET:
            case SmsEvent.INIT:
            case SmsEvent.DETECT:
            case SmsEvent.CHECK:
            case SmsEvent.SIGNAL:
            case SmsEvent.OPERATOR:
            case SmsEvent.STRENGTH:
               msg = Resources.MODEM_DETECT;
               break;
            case SmsEvent.SMS_SENDING:
            case SmsEvent.SMS_SEND_OK:
            case SmsEvent.SMS_SEND_ERROR:
            case SmsEvent.SMS_READING:
            case SmsEvent.SMS_READ_OK:
            case SmsEvent.SMS_READ_ERROR:
            case SmsEvent.SMS_DELETING:
            case SmsEvent.SMS_DELETE_OK:
            case SmsEvent.SMS_DELETE_ERROR:
            case SmsEvent.READY:
               msg = Resources.MODEM_CONECTED;
               break;

            default:
               msg = Resources.MODEM_DISCONECTED;
               break;
         }
         return msg;
      }

      public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
      {
         throw new NotImplementedException();
      }
   }
}
