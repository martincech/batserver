﻿using System;
using System.Globalization;
using System.Windows.Data;
using Bat2WebSynchronizer.Tray.Helpers;

namespace Bat2WebSynchronizer.Tray.Converters
{
   public class BoolToImageConverter : IValueConverter
   {
      public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
      {
         var logged = (bool)value;
         var img = IconHelper.Red;
         if (logged)
         {
            img = IconHelper.Green;
         }
         return img;
      }

      public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
      {
         throw new NotImplementedException();
      }
   }
}
