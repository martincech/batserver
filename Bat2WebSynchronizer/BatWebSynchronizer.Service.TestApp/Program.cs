﻿using System;
using System.Collections.Specialized;
using BatWebSynchronizer.PermanentStorage;
using BatWebSynchronizer.Synchronizer;

namespace BatWebSynchronizer.Service.TestApp
{
   class Program
   {
      static void Main(string[] args)
      {
         var app = new TestApp();
         app.Start();


         Console.ReadKey();
         app.Stop();
      }




      public class TestApp : ILogger
      {
         private readonly CollectorsToWebSynchronizer collectorsToWebSynchronizer;

         public TestApp()
         {
            var storage = new FileStorage();
            var api = new WebApi();
            collectorsToWebSynchronizer = new CollectorsToWebSynchronizer(storage, api, this);
            collectorsToWebSynchronizer.DeviceList.CollectionChanged +=
               delegate(object sender, NotifyCollectionChangedEventArgs args)
               {
                  
               };
         }

         public void Start()
         {
            collectorsToWebSynchronizer.Start();
         }

         public void Stop()
         {
            collectorsToWebSynchronizer.Stop();
         }

         #region Implementation of ILogger

         public void Log(string message)
         {
            Log(DateTime.Now, message);
         }

         public void Log(DateTime time, string message)
         {
            Console.WriteLine(time + "\t" + message);
         }

         #endregion


      }

   }
}
