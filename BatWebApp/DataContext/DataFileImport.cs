﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Xml;
using DataModel;

namespace DataContext
{
   /// <summary>
   /// This class contains routines to import data into database from
   /// <see cref="XmlDocument"/>s.
   /// </summary>
   public class DataFileImport
   {
      /// <summary>
      /// Import statistics from xml document, as user
      /// </summary>
      /// <param name="user">user who imports data to database</param>
      /// <param name="document">document to import</param>
      /// <returns>count of newly added statistic objects, <see cref="Stat"/></returns>
      public static int Import(User user, XmlDocument document)
      {
         using (var context = new BatModelContainer())
         {
            //Parse xml document
            var xmlParser = new Bat2Library.Utilities.XmlFileParser(document);
            xmlParser.Parse();

            var statsToSave = new List<Stat>();
            foreach (var report in xmlParser.Reports)
            {
               var stat = new Stat
               {
                  Scale = GetScale(context, user, report.ScaleName), 
                  Date = report.Date
               };

               var oldStat = context.Stats.FirstOrDefault(x => x.ScaleId == stat.Scale.Id && x.Date.Equals(stat.Date));
               if (oldStat != null)
               {
                  stat = oldStat;
               }

               stat.Sex = report.Sex;
               stat.Day = report.Day;
               stat.Count = report.Count;
               stat.Average = new decimal(report.Average);
               stat.Gain = new decimal(report.Gain);
               stat.Sigma = new decimal(report.Sigma);
               stat.Uni = new decimal(report.Uniformity);
               stat.Cv = new decimal(report.Cv);

               if (oldStat == null)
               {
                  statsToSave.Add(stat);
               }
               else
               {
                  context.Entry(stat).State = EntityState.Modified;
               }
            }

            foreach (var stat in statsToSave)
            {
               context.Stats.Add(stat);
            }
            context.SaveChanges();
            return statsToSave.Count;
         }
      }


      private static Scale GetScale(BatModelContainer context, User user, string scaleName)
      {
         var dbScale =
                  context.Scales.FirstOrDefault(
                     x => x.Name.Equals(scaleName) && x.Users.Any(u => u.Id.Equals(user.Id)));
         if (dbScale == null)
         {
            dbScale =
               context.Scales.FirstOrDefault(
                  x => x.Name.Equals(scaleName) && x.Users.Any(u => u.Id.Equals(user.ParentId)));

            if (dbScale == null)
            {
               dbScale = new Scale
               {
                  Name = scaleName,
                  Users = context.Users.Where(p => p.Id == user.Id).ToList(),
                  Company = context.Users.First(p => p.Id == user.Id).Company
               };
               context.Scales.Add(dbScale);
               context.SaveChanges();
            }
         }
         return dbScale;
      }
   }
}