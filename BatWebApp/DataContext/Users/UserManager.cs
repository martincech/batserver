﻿using System;
using DataModel;
using Microsoft.AspNet.Identity;

namespace DataContext
{
   public class UserManager : UserManager<User,Guid>
   {
      public UserManager()
         : this(new BatModelContainer())
      {
      }

      public UserManager(BatModelContainer db) 
         : this(new UserStore(db))
      {
      }

      protected UserManager(IUserStore<User, Guid> store) 
         : base(store)
      {
      }
   }
}
