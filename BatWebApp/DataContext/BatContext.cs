﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading;
using Bat2Library;
using DataModel;

namespace DataContext
{
   public class BatContext : IDisposable
   {
      public Guid UserId { get; private set; }
      private BatModelContainer Container;

      public BatContext(Guid userId)
      {
         UserId = userId;
         Container = new BatModelContainer();
      }

      #region User utils

      public bool CreateDefaultContext()
      {
         // TODO
         //dataContext.SaveChanges();
         //if (!dataContext.Users.Any())
         //{
         //   var us = dataContext.Users.Create();

         //   us.UserName = "admin";
         //   us.Id = Guid.NewGuid();
         //   us.CreationDate = DateTime.Now;
         //   us.EmailConfirmed = true;
         //   us.Company = new Company() { Name = "Veit" };
         //   us.PasswordHash = "AFvF/VX+na2ePxTjK3EWWdag4+xWIGLmta/ozU0yYayOoe/FDnyqlAGELj2rTMjOvw=="; //Admin_123
         //   us.SecurityStamp = "a3decdf4-4505-405c-8d83-06a0a0999a2d";
         //   us.Roles.Add(new Role() { });
         //   dataContext.Users.Add(us);
         //}
         //dataContext.SaveChanges();
         return false;
      }

      public Guid GetFarmerAdminForCompany(int companyId)
      {
         var fadmin = Container.Users.FirstOrDefault(x => x.CompanyId == companyId && x.Parent.CompanyId != companyId);
         return fadmin == null ? Guid.Empty : fadmin.Id;
      }

      public List<Guid> GetVisibleUsers()
      {
         return GetVisibleUsers(UserId);
      }

      private List<Guid> GetVisibleUsers(Guid userId)
      {
         var result = new List<Guid>();

         var user = Container.Users.Find(userId);

         if (user == null)
         {
            return result;
         }

         result.Add(user.Id);
         result.AddRange(Container.Users.Where(u => u.CompanyId == user.CompanyId).Select(s => s.Id));
         return result;
      }

      public void LinkUser(Guid parentId, Guid childId, int company)
      {
         if (parentId != Guid.Empty)
         {
            var child = Container.Users.Find(childId);
            child.ParentId = parentId;
            child.CompanyId = company;

            if (Thread.CurrentPrincipal.IsInRole(Constants.COMPANY_ADMIN) &&
                !Thread.CurrentPrincipal.IsInRole(Constants.ADMIN))
            {
               child.CompanyId = Container.Users.Find(UserId).CompanyId;
            }
            Container.Entry(child).State = EntityState.Modified;
            Container.SaveChanges();
         }
      }

      public List<User> GetCompanyUsers(int companyId)
      {
         var role = Container.Roles.FirstOrDefault(x => x.Name == Constants.USER);
         List<User> users = new List<User>();
         if (role == null)
         {
            return users;
         }

         if (Thread.CurrentPrincipal.IsInRole(Constants.ADMIN))
         {
            users = role.Users.Where(x => x.CompanyId == companyId).ToList();
         }

         var user = Container.Users.FirstOrDefault(u => u.Id == UserId);
         if (user == null)
         {
            return users;
         }

         if (Thread.CurrentPrincipal.IsInRole(Constants.COMPANY_ADMIN) && user.CompanyId == companyId)
         {
            users = role.Users.Where(x => x.CompanyId == companyId).ToList();
         }
         return users;
      }

      #endregion

      #region Master Data access

      public IQueryable<Company> Companies
      {
         get
         {
            if (Thread.CurrentPrincipal.IsInRole(Constants.ADMIN))
            {
               return Container.Companies.AsQueryable();
            }
            return Container.Companies.Where(c => c.Users.Any(u => u.Id == UserId));
         }
      }

      public IQueryable<User> Users
      {
         get
         {
            if (Thread.CurrentPrincipal.IsInRole(Constants.ADMIN))
            {
               return Container.Users.AsQueryable();
            }

            var user = Container.Users.Find(UserId);

            if (Thread.CurrentPrincipal.IsInRole(Constants.COMPANY_ADMIN) && user.Company != null)
            {
               return user.Company.Users.Where(w => w.Roles.Select(s => s.Name != Constants.ADMIN).Any()).AsQueryable();
            }
           
            return Container.Users.Where(u => u.Id == user.Id);        
         }
      }

      public IQueryable<Scale> Scales
      {
         get
         {
            if (Thread.CurrentPrincipal.IsInRole(Constants.ADMIN))
            {
               return Container.Scales.AsQueryable();
            }
            if (Thread.CurrentPrincipal.IsInRole(Constants.COMPANY_ADMIN))
            {
               var firstOrDefault = Container.Users.FirstOrDefault(p => p.Id == UserId);
               if (firstOrDefault != null)
               {
                  int? companyId = firstOrDefault.CompanyId;
                  return Container.Scales.Where(p => p.CompanyId == companyId).AsQueryable();
               }
            }
            return Container.Users.Find(UserId).Scales.AsQueryable();
         }
      }

      public IQueryable<Flock> Flocks
      {
         get
         {
            if (Thread.CurrentPrincipal.IsInRole(Constants.ADMIN))
            {
               return Container.Flocks.AsQueryable();
            }
            if (Thread.CurrentPrincipal.IsInRole(Constants.COMPANY_ADMIN))
            {
               var firstOrDefault = Container.Users.FirstOrDefault(p => p.Id == UserId);
               if (firstOrDefault != null)
               {
                  int? companyId = firstOrDefault.CompanyId;
                  return Container.Flocks.Where(p => p.Scale.CompanyId == companyId).AsQueryable();
               }
            }
            return Container.Flocks.Where(x => x.Scale.Users.Any(u => u.Id.Equals(UserId)));
         }
      }

      public IQueryable<FlockV2> FlocksV2
      {
         get
         {
            if (Thread.CurrentPrincipal.IsInRole(Constants.ADMIN))
            {
               return Container.FlockV2.AsQueryable();
            }

            var user = Container.Users.Find(UserId);
            if (user.Company != null)
            {
               int? companyId = user.CompanyId;
               var configIds = ConfigurationsV2.Select(s => s.Id);
               return Container.FlockV2.Where(w => configIds.Contains(w.Id)).AsQueryable();
            }
            return new List<FlockV2>().AsQueryable();
         }
      }

      public IQueryable<Curve> Curves
      {
         get
         {
            if (Thread.CurrentPrincipal.IsInRole(Constants.ADMIN))
            {
               return Container.Curves.AsQueryable();
            }
            var user = Container.Users.Find(UserId);
            return user.Company != null ? user.Company.Curves.AsQueryable() : new List<Curve>().AsQueryable();
         }
      }

      public IQueryable<CurveValue> CurveValues
      {
         get
         {
            if (Thread.CurrentPrincipal.IsInRole(Constants.ADMIN))
            {
               return Container.CurveValues.AsQueryable();
            }
            return Curves.SelectMany(c => c.CurveValues);
         }
      }

      public IQueryable<Stat> Stats
      {
         get
         {
            if (Thread.CurrentPrincipal.IsInRole(Constants.ADMIN))
            {
               return Container.Stats.AsQueryable();
            }
            if (Thread.CurrentPrincipal.IsInRole(Constants.COMPANY_ADMIN))
            {
               int? companyId = Container.Users.FirstOrDefault(p => p.Id == UserId).CompanyId;
               return Container.Stats.Where(p => p.Scale.CompanyId == companyId).AsQueryable();
            }
            return Container.Stats.Where(x => x.Scale.Users.Any(u => u.Id.Equals(UserId)));
         }
      }

      public IQueryable<ConfigurationsV2> ConfigurationsV2
      {
         get
         {
            if (Thread.CurrentPrincipal.IsInRole(Constants.ADMIN))
            {
               return Container.ConfigurationsV2.AsQueryable();
            }
            int? companyId = Container.Users.FirstOrDefault(p => p.Id == UserId).CompanyId;
            return Container.ConfigurationsV2.Where(p => p.CompanyId == companyId || p.CompanyId == null).AsQueryable();
         }
      }

      public void SaveChanges()
      {
         Container.SaveChanges();
      }

      #endregion

      #region Flock functions

      public bool CanCreateFlock(Flock flock)
      {
         return Flocks.FirstOrDefault(
            f => f.Name.Equals(flock.Name) && DateTime.Compare(f.StartDate, flock.StartDate) == 0 &&
                 f.ScaleId == flock.ScaleId) == null;
      }

      public bool CanUpdateFlock(Flock flock)
      {
         var dbFlock = Flocks.FirstOrDefault(
            f => f.Name.Equals(flock.Name) && DateTime.Compare(f.StartDate, flock.StartDate) == 0 &&
                 f.ScaleId == flock.ScaleId);
         return dbFlock == null || dbFlock.Id == flock.Id;
      }

      public Flock GetFlock(int id)
      {
         return Flocks.FirstOrDefault(x => x.Id == id);
      }

      public void SaveFlock(Flock flock)
      {
         if (flock.Id == 0)
         {
            Container.Flocks.Add(flock);
         }
         else
         {
            Container.Entry(flock).State = EntityState.Modified;
         }

         Container.SaveChanges();
      }

      public IQueryable<Flock> GetOpenFlocks()
      {
         return Flocks.Where(x => x.EndDate == null);
      }


      public List<Flock> GetOpenFlocks(FilterModel param, SearchModel info, out int totalRecords)
      {
         var filtered = GetOpenFlocks();
         filtered = ApplySearch(filtered, info);
         filtered = ApplyOrdering(filtered, info);
         totalRecords = filtered.Count();
         filtered = filtered.ToList().Skip(param.DisplayStart).Take(param.DisplayLength).AsQueryable();
         return filtered.ToList();
      }

      public List<Flock> GetClosedFlocks(FilterModel param, SearchModel info, out int totalRecords)
      {
         var filtered = Flocks.Where(x => x.EndDate != null);
         filtered = ApplySearch(filtered, info);
         filtered = ApplyOrdering(filtered, info);
         totalRecords = filtered.Count();
         filtered = filtered.ToList().Skip(param.DisplayStart).Take(param.DisplayLength).AsQueryable();
         return filtered.ToList();
      }


      public void DeleteFlocks(int[] ids)
      {
         foreach (var id in ids)
         {
            var flock = Container.Flocks.Find(id);
            Container.Users.ToList().ForEach(u => { u.SelectedFlocks.Remove(flock); });
            Container.Flocks.Remove(flock);
         }

         Container.SaveChanges();
      }

      public void CloseFlocks(int[] ids, DateTime closedDate)
      {
         foreach (var id in ids)
         {
            var flock = Container.Flocks.Find(id);
            flock.EndDate = closedDate;
            Container.Entry(flock).State = EntityState.Modified;
         }

         Container.SaveChanges();
      }

      public void ReopenFlocks(int[] ids)
      {
         foreach (var id in ids)
         {
            var flock = Container.Flocks.Find(id);
            flock.EndDate = null;
            Container.Entry(flock).State = EntityState.Modified;
         }

         Container.SaveChanges();
      }

      public void UpdateFlocksMaxDay(List<Flock> flocks)
      {
         foreach (var item in flocks)
         {
            var hasMaxDay = Stats.Any(x => x.ScaleId == item.ScaleId && x.Date >= item.StartDate);
            item.MaxDay = hasMaxDay
               ? Stats.Where(x => x.ScaleId == item.ScaleId && x.Date >= item.StartDate).Max(
                  x => x.Day)
               : item.MaxDay;
         }
      }

      public void ChangeSelectedFlock(Guid userId, int flockId, bool selected)
      {
         var user = Users.First(x => x.Id == userId);
         var flock = Flocks.First(x => x.Id == flockId);
         if (selected)
         {
            if (!user.SelectedFlocks.Contains(flock))
            {
               user.SelectedFlocks.Add(flock);
            }
         }
         else
         {
            if (user.SelectedFlocks.Contains(flock))
            {
               user.SelectedFlocks.Remove(flock);
            }
         }
         Container.SaveChanges();
      }

      #endregion

      #region Curve Functions

      public void SaveCurve(Curve curve)
      {
         if (curve.Id == 0)
         {
            curve.Company = Container.Users.Find(UserId).Company;
            Container.Curves.Add(curve);
         }
         else
         {
            Container.Entry(curve).State = EntityState.Modified;
         }

         Container.SaveChanges();
      }




      public void DeleteCurves(int[] ids)
      {
         foreach (var id in ids)
         {
            var curve = Container.Curves.Find(id);
            var curveValues = curve.CurveValues.ToList();
            foreach (var curveValue in curveValues)
            {
               Container.CurveValues.Remove(curveValue);
            }
            Container.Curves.Remove(curve);
         }

         Container.SaveChanges();
      }

      public void DeleteCurvesData(int id)
      {
         var curveData = Container.CurveValues.Find(id);

         Container.CurveValues.Remove(curveData);


         Container.SaveChanges();
      }

      #endregion

      #region Generic Order, Search

      protected IQueryable<T> ApplyOrdering<T>(IQueryable<T> source, SearchModel info)
      {
         if (info.NumOfSorts > 0)
         {
            var orderCols =
               info.Infos.Where(x => x.SortOrder > 0).OrderBy(x => x.SortOrder);

            var ci = orderCols.First();
            var orderedSource = source.OrderBy(ci.Name, ci.SortDir);

            return orderCols.Skip(1)
               .Aggregate(orderedSource, (current, columnInfo) => current.ThenBy(columnInfo.Name, columnInfo.SortDir));
         }
         return source;
      }

      protected IQueryable<T> ApplySearch<T>(IQueryable<T> source, SearchModel info)
      {
         return source.WhereContains(info.Infos.Where(x => x.IsSearchable).Select(x => x.Name).ToList(),
            info.SearchText);
      }

      #endregion

      #region Stats & Graphs functions

      public void DeleteStats(int[] ids)
      {
         foreach (var id in ids)
         {
            var stats = Container.Stats.Find(id);
            Container.Stats.Remove(stats);
         }

         Container.SaveChanges();
      }

      public Stat AddStat(Stat stat, int scaleId)
      {
         var scale = Container.Scales.FirstOrDefault(x => x.Id == scaleId);
         if (scale != null)
         {
            stat.Scale = scale;
            Container.Stats.Add(stat);
            Container.SaveChanges();
            return stat;
         }
         else
         {
            return null;
         }
      }

      public Stat UpdateStat(Stat stat, int scaleId)
      {
         var existingStat = Container.Stats.FirstOrDefault(x => 
            x.ScaleId == scaleId
            && x.Date.Year == stat.Date.Year
            && x.Date.Month == stat.Date.Month
            && x.Date.Day == stat.Date.Day
            && x.Sex == stat.Sex);
         if (existingStat != null)
         {
            existingStat.Day = stat.Day;
            existingStat.Count = stat.Count;
            existingStat.Average = stat.Average;
            existingStat.Gain = stat.Gain;
            existingStat.Sigma = stat.Sigma;
            existingStat.Cv = stat.Cv;
            existingStat.Uni = stat.Uni;
            existingStat.Temperature = stat.Temperature;
            existingStat.CarbonDioxide = stat.CarbonDioxide;
            existingStat.Ammonia = stat.Ammonia;
            existingStat.Humidity = stat.Humidity;
            Container.SaveChanges();
         }
         return existingStat;
      }

      public Stat AddUpdateStat(Stat stat, int scaleId)
      {
         var updatedStat = UpdateStat(stat, scaleId);
         if (updatedStat == null)
         {
            updatedStat = AddStat(stat, scaleId);
         }
         return updatedStat;
      }

      public void SaveStat(Stat stat)
      {
         if (stat.Id == 0)
         {
            Container.Stats.Add(stat);
         }
         else
         {
            Container.Entry(stat).State = EntityState.Modified;
         }
         Container.SaveChanges();
      }

      public bool SaveStat(Stat stat, int scaleSerial)
      {
         if (stat == null)
         {
            return false;
         }

         var scale = Scales.FirstOrDefault(x => x.SerialNumber == scaleSerial);
         if (scale == null)
         {
            var user = Container.Users.FirstOrDefault(u => u.Id == UserId);
            if (user == null)
            {
               return false;
            }

            var users = new List<User>() { user };
            var stats = new List<Stat>() { stat };

            scale = new Scale()
         {
            Name = scaleSerial.ToString(),
            SerialNumber = scaleSerial,
            Users = users,
            Stat = stats,
            Company = user.Company
         };
            user.Scales.Add(scale);

         }
         else
         {

            var existingStats = scale.Stat.Where(x => x.Date == stat.Date && x.Day == stat.Day);
            var existingStat = scale.Stat.FirstOrDefault(x => x.Date == stat.Date && x.Day == stat.Day);

            //check if exist more than 1 record 
            if (existingStats.Count() > 1)
            {
               //get record for specific sex
               existingStat = existingStats.FirstOrDefault(x => x.Date == stat.Date && x.Day == stat.Day && x.Sex == stat.Sex);

               if (existingStat == null && stat.Sex == SexE.SEX_UNDEFINED)
               {
                  existingStat = existingStats.FirstOrDefault(x => x.Date == stat.Date && x.Day == stat.Day && x.Sex == SexE.SEX_FEMALE);
                  stat.Sex = SexE.SEX_FEMALE;
               }
            }

            if (existingStat != null && existingStat.Sex == stat.Sex)
            {
               existingStat.Average = stat.Average;
               existingStat.Count = stat.Count;
               existingStat.Cv = stat.Cv;
               existingStat.Gain = stat.Gain;
               existingStat.Sex = stat.Sex;
               existingStat.Sigma = stat.Sigma;
               existingStat.Uni = stat.Uni;
               Container.Entry(existingStat).State = EntityState.Modified;
            }
            else if (existingStat != null)
            {
               if (existingStat.Sex == SexE.SEX_UNDEFINED && stat.Sex == SexE.SEX_MALE)
               {
                  existingStat.Sex = SexE.SEX_FEMALE;
                  Container.Entry(existingStat).State = EntityState.Modified;
                  scale.Stat.Add(stat);
               }
               else if (existingStat.Sex == SexE.SEX_MALE && stat.Sex == SexE.SEX_UNDEFINED)
               {
                  stat.Sex = SexE.SEX_FEMALE;
                  scale.Stat.Add(stat);
               }
               else
               {
                  scale.Stat.Add(stat);
               }

            }
            else
            {
               scale.Stat.Add(stat);
            }


         }

         Container.SaveChanges();
         return true;
      }


      protected IQueryable<StatFlock> PreselectStats(int[] flockIds)
      {
         var flocks = Flocks.Where(x => flockIds.Contains(x.Id));
         var filtered =
            Stats.Join(flocks, x => x.ScaleId, y => y.ScaleId,
               (stat, flock) => new StatFlock { Stat = stat, Flock = flock }).Where(
                  x => x.Stat.Date >= x.Flock.StartDate);

         return filtered;
      }

      public IQueryable<T> FilterStartEndSex<T>(IQueryable<T> filtered, FilterModel info)
         where T : StatFlock
      {
         if (info.From != DateTime.MinValue)
         {
            filtered = filtered.Where(x => x.Stat.Date >= info.From);
         }

         if (info.To != DateTime.MinValue)
         {
            filtered = filtered.Where(x => x.Stat.Date <= info.To);
         }
         if (info.Sex != SexE.SEX_UNDEFINED)
         {
            filtered = filtered.Where(x => x.Stat.Sex == info.Sex);
         }

         if (info.ScaleId > 0)
         {
            filtered = filtered.Where(x => x.Stat.Scale.Id == info.ScaleId);
         }

         return filtered;
      }

      public static decimal Lagrange(int x, CurveValue[] curveValues)
      {
         decimal sum = 0;
         for (int i = 0, n = curveValues.Length; i < n; i++)
         {
            if (x - curveValues[i].Day == 0)
            {
               return curveValues[i].Weight;
            }
            var product = curveValues[i].Weight;
            for (var j = 0; j < n; j++)
            {
               if ((i == j) || (curveValues[i].Day - curveValues[j].Day == 0))
               {
                  continue;
               }
               product *= (x - curveValues[i].Day) / (decimal)(curveValues[i].Day - curveValues[j].Day);
            }
            sum += product;
         }
         return sum;
      }

      public static decimal Linear(int x, CurveValue[] curveValues)
      {
         if (x < curveValues[0].Day)
         {
            return curveValues[0].Weight +
                   (x - curveValues[0].Day) * (curveValues[1].Weight - curveValues[0].Weight) /
                   (curveValues[1].Day - curveValues[0].Day);
         }

         var size = curveValues.Count();
         return curveValues[size - 2].Weight +
                (x - curveValues[size - 2].Day) * (curveValues[size - 1].Weight - curveValues[size - 2].Weight) /
                (curveValues[size - 1].Day - curveValues[size - 2].Day);
      }

      public static decimal Spline(int x, CurveValue[] curveValues)
      {
         var np = curveValues.Count();
         if (np <= 1) return 0;

         var a = new decimal[np];
         var h = new decimal[np];
         for (var i = 1; i <= np - 1; i++)
         {
            h[i] = curveValues[i].Day - curveValues[i - 1].Day;
         }
         if (np > 2)
         {
            if (x < curveValues[0].Day || x > curveValues[np - 1].Day)
            {
               return Linear(x, curveValues);
            }

            var sub = new decimal[np - 1];
            var diag = new decimal[np - 1];
            var sup = new decimal[np - 1];

            for (var i = 1; i <= np - 2; i++)
            {
               diag[i] = (h[i] + h[i + 1]) / 3;
               sup[i] = h[i + 1] / 6;
               sub[i] = h[i] / 6;
               a[i] = (curveValues[i + 1].Weight - curveValues[i].Weight) / h[i + 1] -
                      (curveValues[i].Weight - curveValues[i - 1].Weight) / h[i];
            }
            // SolveTridiag is a support function, see Marco Roello's original code
            // for more information at
            // http://www.codeproject.com/useritems/SplineInterpolation.asp
            SolveTridiag(sub, diag, sup, ref a, np - 2);
         }

         var gap = 0;
         decimal previous = 0;
         // At the end of this iteration, "gap" will contain the index of the interval
         // between two known values, which contains the unknown z, and "previous" will
         // contain the biggest z value among the known samples, left of the unknown z
         for (var i = 0; i < curveValues.Count(); i++)
         {
            if (curveValues[i].Day < x && curveValues[i].Day > previous)
            {
               previous = curveValues[i].Day;
               gap = i + 1;
            }
         }

         //TODO:Bug index out of range
         decimal y = 0;
         try
         {
            var x1 = x - previous;
            var x2 = h[gap] - x1;
            y = ((-a[gap - 1] / 6 * (x2 + h[gap]) * x1 + curveValues[gap - 1].Weight) * x2 +
                 (-a[gap] / 6 * (x1 + h[gap]) * x2 + curveValues[gap].Weight) * x1) / h[gap];
         }
         catch (Exception e)
         {
         }

         return y;
      }

      private static void SolveTridiag(decimal[] sub, decimal[] diag, decimal[] sup, ref decimal[] b, int n)
      {
         int i;
         /*                  factorization and forward substitution */
         for (i = 2; i <= n; i++)
         {
            sub[i] = sub[i] / diag[i - 1];
            diag[i] = diag[i] - sub[i] * sup[i - 1];
            b[i] = b[i] - sub[i] * b[i - 1];
         }
         b[n] = b[n] / diag[n];
         for (i = n - 1; i >= 1; i--)
         {
            b[i] = (b[i] - sup[i] * b[i + 1]) / diag[i];
         }
      }

      public List<StatFlock> CalcWeightDiffs(List<StatFlock> fList, Curve growthCurve)
      {
         if (growthCurve == null) return fList;

         foreach (var statFlock in fList)
         {
            statFlock.Diff =
               Math.Round(
                  statFlock.Stat.Average -
                  Spline(statFlock.Stat.Day, growthCurve.CurveValues.OrderBy(value => value.Day).ToArray()), 3);
         }
         return fList;
      }

      public List<StatFlock> GetStats(FilterModel param, SearchModel info, out int totalRecords,
                  bool ignoreCountFilter = false, int? selectedCurveId = null)
      {
         if (param.SelectedFlocks == null || !param.SelectedFlocks.Any())
         {
            totalRecords = 0;
            return new List<StatFlock>();
         }

         var flockIds = param.SelectedFlocks.ToArray();
         var temp = PreselectStats(flockIds);
         var filtered = FilterStartEndSex(temp, param); //.Select(x => x.Stat);
         if (param.FlockId != null)
         {
            filtered = filtered.Where(i => i.Flock.Id == param.FlockId);
         }
         filtered = ApplySearch(filtered, info);
         filtered = info.NumOfSorts > 0 ? ApplyOrdering(filtered, info) : filtered.OrderBy(x => x.Stat.Date);
         totalRecords = filtered.Count();

         if (!ignoreCountFilter)
         {
            filtered = filtered.Skip(param.DisplayStart).Take(param.DisplayLength);
         }

         // Get curve from index
         Curve growthCurve = null;
         if (selectedCurveId != null)
         {
            growthCurve = Curves.FirstOrDefault(c => c.Id == selectedCurveId);
         }
         return CalcWeightDiffs(filtered.ToList(), growthCurve);
      }

      public List<WeighingStatFlock> GetAllStats(FilterModel param, SearchModel info,
         out int totalRecords, bool ignoreCountFilter = false)
      {
         var allStats = Stats;

         var temp = from c in allStats
                    select new WeighingStatFlock { Stat = c, Id = c.Id, Flock = null };
         var filtered = FilterStartEndSex(temp, param); //.Select(x => x.Stat);
         filtered = ApplySearch(filtered, info);
         filtered = ApplyOrdering(filtered, info);
         totalRecords = filtered.Count();
         if (!ignoreCountFilter)
         {
            filtered = filtered.ToList().Skip(param.DisplayStart).Take(param.DisplayLength).AsQueryable();
         }
         return filtered.ToList();
      }

      public List<StatFlock> GetGraphData(int[] flockIds, FilterModel param)
      {
         var filtered = PreselectStats(flockIds);
         filtered = FilterStartEndSex(filtered, param);
         filtered = filtered.OrderBy(x => x.Stat.Day).ThenBy(x => x.Flock.Name);
         return filtered.ToList();
      }
      #endregion

      #region Scale functions

      public List<Scale> GetScalesForUser(Guid userId)
      {
         //return Container.Scales.Where(x => x.Users.Any(u => u.Id.Equals(userId))).ToList();

         if (userId == UserId)
         {
            return Scales.ToList();
         }
         return Container.Scales.Where(x => x.Users.Any(u => u.Id.Equals(userId))).ToList();
      }

      public void ReassignScales(Guid userId, ICollection<int> selectedScales)
      {
         var oldAssigned = GetScalesForUser(userId);
         var adminScales = GetScalesForUser(UserId);
         var farmer = Container.Users.Find(userId);


         foreach (var item in oldAssigned)
         {
            if (!selectedScales.Contains(item.Id))
            {
               var scale = Scales.First(x => x.Id == item.Id);
               scale.Users.Remove(farmer);
               Container.Entry(scale).State = EntityState.Modified;
            }
         }

         foreach (var item in adminScales)
         {
            if (selectedScales.Contains(item.Id))
            {
               var scale = Scales.First(x => x.Id == item.Id);
               scale.Users.Add(farmer);
               Container.Entry(scale).State = EntityState.Modified;
            }
         }

         Container.SaveChanges();
      }

      public int CreateScale(string name)
      {
         var dbScale = Scales.FirstOrDefault(x => x.Name == name && x.Users.Any(u => u.Id.Equals(UserId)));

         if (dbScale == null)
         {
            var user = Container.Users.Find(UserId);
            dbScale = Container.Scales.Create();
            dbScale.Name = name;
            dbScale.Users.Add(user);
            dbScale.Company = user.Company;
            Container.Scales.Add(dbScale);
            Container.SaveChanges();
         }

         return dbScale.Id;
      }

      public Scale CreateScale(int serialNumber, string scaleName, string phoneNumber, string farmName)
      {
         return CreateScale(serialNumber, scaleName, phoneNumber, farmName, null);
      }


      /// <summary>
      /// Get scale if exists.
      /// </summary>
      /// <param name="scaleName">scale name</param>
      /// <returns>scale if exists, or null if not</returns>
      public Scale GetScaleIfExist(string scaleName)
      {
         var user = Container.Users.Find(UserId);
         var isAdmin = user.Roles.FirstOrDefault(r => r.Name.Equals(Constants.ADMIN)) != null;

         return Container.Scales.FirstOrDefault(x => x.Name.Equals(scaleName) &&
               (isAdmin || x.CompanyId == user.CompanyId));
      }

      //Create scale
      public Scale CreateScale(int serialNumber, string scaleName)
      {
         var dbScale = GetScaleIfExist(scaleName);
         if (dbScale == null)
         {
            var user = Container.Users.Find(UserId);
            dbScale = Container.Scales.Create();
            dbScale.PhoneNumber = "";
            dbScale.Name = scaleName ?? "";
            dbScale.FarmName = "";
            dbScale.SerialNumber = serialNumber;
            if (user.CompanyId != null)
            {
               dbScale.Users.Add(user);
            }
            dbScale.CompanyId = user.CompanyId;

            Container.Scales.Add(dbScale);
            Container.SaveChanges();
         }
         else
         {
            return null;
         }

         return dbScale;
      }


      public Scale CreateScale(int serialNumber, string scaleName, string phoneNumber, string farmName, Stat stat)
      {
         var user = Container.Users.Find(UserId);
         var dbScale = Scales.FirstOrDefault(x => x.Name == scaleName && x.SerialNumber == serialNumber &&
                  (user.CompanyId == null || user.CompanyId == x.CompanyId) /*&& x.Users.Any(u => u.Id.Equals(UserId))*/);

         if (dbScale == null)
         {
            dbScale = Container.Scales.Create();
            dbScale.PhoneNumber = phoneNumber;
            dbScale.Name = scaleName ?? "";
            dbScale.FarmName = farmName;
            dbScale.SerialNumber = serialNumber;
            if (user.CompanyId != null)
            {
               dbScale.Users.Add(user);
            }
            dbScale.CompanyId = user.CompanyId;
            if (stat != null)
            {
               dbScale.Stat.Add(stat);
            }

            Container.Scales.Add(dbScale);
            Container.SaveChanges();
         }
         else
         {
            return null;
         }

         return dbScale;
      }

      public bool UpdateScale(int id, int? serialNumber, string scaleName, string farmName, string phoneNumber)
      {
         var scale = Container.Scales.FirstOrDefault(x => x.Id == id);
         if (scale == null)
         {
            return false;
         }

         if (serialNumber != null)
         {
            scale.SerialNumber = (int)serialNumber;
         }

         if (scaleName != null)
         {
            scale.Name = scaleName;
         }
         if (farmName != null)
         {
            scale.FarmName = farmName;
         }

         if (phoneNumber != null)
         {
            scale.PhoneNumber = phoneNumber;
         }
         Container.SaveChanges();
         return true;
      }

      public bool UpdateScale(int id, IEnumerable<Guid> selectedUsers)
      {
         var scale = Container.Scales.First(x => x.Id == id);
         scale.Users.Clear();
         foreach (var userId in selectedUsers)
         {
            var user = Container.Users.First(x => x.Id == userId);
            scale.Users.Add(user);
         }
         Container.SaveChanges();
         return true;
      }

      public bool UpdateScale(Scale newScale, int scaleId)
      {
         var scale = Container.Scales.First(x => x.Id == scaleId);

         if (scale != null)
         {
            scale.FarmName = newScale.FarmName;
            scale.Name = newScale.Name ?? "";
            scale.PhoneNumber = newScale.PhoneNumber;
            scale.SerialNumber = newScale.SerialNumber;
         }
         return true;
      }

      public bool UpdateScaleCompanies(int id, IEnumerable<int> selectedCompanies)
      {
         var scale = Container.Scales.First(x => x.Id == id);
         if (selectedCompanies.Count() != 1)
         {
            return false;
         }
         var companyId = selectedCompanies.First();
         if (scale.CompanyId == companyId)
         {
            return true;
         }
         scale.Users.Clear();
         scale.CompanyId = companyId;
         Container.SaveChanges();
         return true;
      }

      public void DeleteScales(int[] ids)
      {
         foreach (var id in ids)
         {
            DeleteScale(id);
         }
      }

      public bool DeleteScale(int id)
      {
         var scale = Scales.FirstOrDefault(x => x.Id == id);
         if (scale == null)
         {
            return false;
         }
         foreach (var user in Container.Users.ToList())
         {
            foreach (var flock in scale.Flocks)
            {
               user.SelectedFlocks.Remove(flock);
            }
         }
         if (scale.Configurations != null)
         {
            Container.Configurations.RemoveRange(scale.Configurations);
         }

         if (scale.ConfigurationsV2 != null)
         {
            foreach (var conf in scale.ConfigurationsV2.ToList())
            {
               DeleteScaleConfig(conf.Id);
            }
         }

         Container.SaveChanges();
         scale.Users.Clear();
         Container.SaveChanges();
         Container.Scales.Remove(scale);
         Container.SaveChanges();
         return true;
      }

      public Scale CreateScale(string name, int serialNumber)
      {
         var dbScale = Scales.FirstOrDefault(x => x.SerialNumber == serialNumber && x.Users.Any(u => u.Id.Equals(UserId)));

         if (dbScale == null)
         {
            var user = Container.Users.Find(UserId);
            dbScale = Container.Scales.Create();
            dbScale.SerialNumber = serialNumber;
            dbScale.Name = name;
            dbScale.Users.Add(user);
            dbScale.Company = user.Company;
            Container.Scales.Add(dbScale);
            Container.SaveChanges();
         }

         return dbScale;
      }

      public bool ChangeScaleType(int id, string scaleType)
      {
         var scale = Scales.FirstOrDefault(x => x.Id == id);
         if (scale == null)
         {
            return false;
         }
         switch (scaleType)
         {

            case "Bat1":
               break;
            case "Bat2":
               scale.ConfigurationsV2.Add(new ConfigurationsV2() { Company = scale.Company, Name = Resources.Resources.Bat2NewConfig });
               break;
            case "Bat2+":
               break;
            case "Bat3":
               break;
         }
         Container.SaveChanges();
         return true;
      }

      public ConfigurationsV2 CreateScaleConfig(int id, int? templateId, string name)
      {
         var scale = Scales.FirstOrDefault(x => x.Id == id);
         if (scale == null)
         {
            return null;
         }
         var template = new ConfigurationsV2();
         if (templateId != null)
         {
            var temp = ConfigurationsV2.FirstOrDefault(f => f.Id == templateId);
            if (temp != null)
            {
               Container.ConfigurationsV2.Add(template);
               Container.Entry(template).CurrentValues.SetValues(Container.Entry(temp).CurrentValues);
               template.FlockV2 = new List<FlockV2>();
               foreach (var flockV2 in temp.FlockV2)
               {
                  var flock = new FlockV2();
                  flock.Index = flockV2.Index;
                  flock.InitialFemale = flockV2.InitialFemale;
                  flock.InitialMale = flockV2.InitialFemale;
                  flock.Name = flockV2.Name;
                  flock.UseCurves = flockV2.UseCurves;
                  flock.UseGender = flockV2.UseGender;
                  flock.WeighFrom = flockV2.WeighFrom;
                  flock.WeighTo = flockV2.WeighTo;
                
                  if (flockV2.CurveFemale != null)
                  {
                     flock.CurveFemale = new Curve();
                     flock.CurveFemale.Name = flockV2.CurveFemale.Name;
                     flock.CurveFemale.CompanyId = temp.CompanyId;
                     flock.CurveFemale.CurveValues =
                        flockV2.CurveFemale.CurveValues.Select(s => new CurveValue() {Day = s.Day, Weight = s.Weight})
                           .ToList();
                  }
                 
                  if (flockV2.CurveMale != null)
                  {
                     flock.CurveMale = new Curve();
                     flock.CurveMale.Name = flockV2.CurveMale.Name;
                     flock.CurveMale.CompanyId = temp.CompanyId;
                     flock.CurveMale.CurveValues =
                        flockV2.CurveMale.CurveValues.Select(s => new CurveValue() { Day = s.Day, Weight = s.Weight })
                           .ToList();
                  }

                  template.FlockV2.Add(flock);
               }
            }
         }
         template.Name = name;
         template.CompanyId = scale.CompanyId;
         template.Active = true;
         template.ScaleId = scale.Id;
         template.Id = 0;
         Container.ConfigurationsV2.Add(template);
         SaveChanges();
         return template;
      }

      public bool SaveScaleConfigs(int scaleId, List<ConfigurationsV2> configurationsV2)
      {
         var scale = Scales.First(f => f.Id == scaleId);
         foreach (var conf in scale.ConfigurationsV2.ToList())
         {
            DeleteScaleConfig(conf.Id);
         }

         scale.ConfigurationChanged = true;
         foreach (var config in configurationsV2)
         {
            scale.ConfigurationsV2.Add(config);
         }
         SaveChanges();
         return true;
      }


      public bool SaveTemplateConfigs(List<ConfigurationsV2> configurationsV2)
      {
         var user =  Container.Users.Find(UserId);
         foreach (var conf in configurationsV2)
         {
            conf.Id = 0;
            conf.ScaleId = null;
            conf.Scale = null;
            conf.CompanyId = user.CompanyId;
         }
         Container.ConfigurationsV2.AddRange(configurationsV2);
         SaveChanges();
         return true;
      }

      public bool ScaleConfigName(int id, string name)
      {
         var config = ConfigurationsV2.FirstOrDefault(f => f.Id == id);
         if (config != null)
         {
            config.Name = name;
            SaveChanges();
         }
         return true;
      }

      public bool DeleteScaleConfig(int id)
      {
         var config = Container.ConfigurationsV2.FirstOrDefault(f => f.Id == id);
         if (config == null)
         {
            return false;
         }

         foreach (var flock in config.FlockV2.ToList())
         {
            DeleteScaleFlock(flock.Id);
         }

         Container.ConfigurationsV2.Remove(config);
         SaveChanges();
         return true;
      }

      public List<int?> GetConfigCurveIds()
      {
         var configCurves = Container.FlockV2.Where(w => w.CurveFemale_Id != null).Select(s => s.CurveFemale_Id).ToList();
         configCurves.AddRange(Container.FlockV2.Where(w => w.CurveMale_Id != null).Select(s => s.CurveMale_Id).ToList());
         return configCurves;
      }


      public bool DeleteScaleFlock(int id)
      {
         var flock = Container.FlockV2.FirstOrDefault(f => f.Id == id);
         if (flock == null)
         {
            return false;
         }

         if (flock.CurveFemale != null)
         {
            Container.Curves.Remove(flock.CurveFemale);
         }

         if (flock.CurveMale != null)
         {
            Container.Curves.Remove(flock.CurveMale);
         }

         Container.FlockV2.Remove(flock);
         SaveChanges();
         return true;
      }


      #endregion

      #region Company administration

      public Company CreateCompany(string name)
      {
         var company = Container.Companies.Create();
         company.Name = name;
         Container.Companies.Add(company);
         Container.SaveChanges();
         return company;
      }

      public void DeleteCompany(int id)
      {
         var company = Container.Companies.Find(id);
         if (company == null)
         {
            return;
         }
         DeleteScales(company.Scales.Select(x => x.Id).ToArray());
         company.Users.Select(s =>
         {
            s.Roles.Clear();
            return s;
         }).ToList();
         company.Curves.Clear();
         Container.Companies.Remove(company);
         Container.SaveChanges();
      }

      #endregion

      #region Implementation of IDisposable

      /// <summary>
      /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
      /// </summary>
      public void Dispose()
      {
         Container.Dispose();
      }

      #endregion
   }


   public class StatFlock
   {
      public decimal? Diff = null;
      public Flock Flock { get; set; }
      public Stat Stat { get; set; }
   }

   public class WeighingStatFlock : StatFlock
   {
      public int Id { get; set; }
   }
}