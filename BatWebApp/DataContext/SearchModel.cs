﻿using System.Collections.Generic;

namespace DataContext
{
   public class SearchModel
   {
      public class ColumnInfo
      {
         public string Name;
         public bool IsSearchable;
         public int SortOrder;
         public string SortDir;
      }

      public List<ColumnInfo> Infos = new List<ColumnInfo>();
      public int NumOfColumns = 0;
      public string SearchText = "";
      public int NumOfSorts = 0;

   }
}