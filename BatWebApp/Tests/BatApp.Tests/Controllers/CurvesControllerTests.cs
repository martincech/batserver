﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using DataModel;
using System.Web.Mvc;
using BatApp.Controllers;
using DataContext;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using BatApp.Models;
using BatApp.Helpers;

namespace BatApp.Tests.Controllers
{
   [TestClass]
   public class CurvesControllerTests
   {
      private MockCurvesController controller;
      private ICollection<User> users;
      private User user;

      [TestInitialize]
      public void Init()
      {
         users = new Collection<User>();
         controller = new MockCurvesController();
         users = SampleData.GetCreatedUsers();

         using (var dataContext = new BatModelContainer())
         {
            if (dataContext.Database.Exists())
            {
               dataContext.Database.Delete();
               dataContext.SaveChanges();
            }
            dataContext.Database.Create();
            dataContext.Users.AddRange(users).ToList();
            dataContext.SaveChanges();
            user = users.FirstOrDefault();
            controller.SetExecutiveUser(user);            
         }
      }


      [TestMethod]
      public void GetCurves_WhenUserIsFromSameCompany()
      {
         var curves = SampleData.CreateCurves(user.Company, 5);
         using (var dataContext = new BatModelContainer())
         {
            dataContext.Curves.AddRange(curves);
            dataContext.SaveChanges();
         }
         var newUser = users.Where(u => u.CompanyId == user.CompanyId && u.Roles.Where(r => r.Name == Constants.ADMIN).Count() == 0).FirstOrDefault();
         controller.SetExecutiveUser(newUser);
         JsonResult result = controller.GetCurves();
         IEnumerable<object> data = (IEnumerable<object>)result.Data;
         Assert.AreEqual(5, data.Count());
      }

      [TestMethod]
      public void GetCurves_WhenUserIsFromDifferentCompany()
      {
         var curves = SampleData.CreateCurves(user.Company, 5);
         using (var dataContext = new BatModelContainer())
         {
            dataContext.Curves.AddRange(curves);
            dataContext.SaveChanges();
         }
         var newUser = users.Where(u => u.CompanyId != user.CompanyId && u.Roles.Where(r => r.Name == Constants.ADMIN).Count() == 0).FirstOrDefault();
         controller.SetExecutiveUser(newUser);
         JsonResult result = controller.GetCurves();
         IEnumerable<object> data = (IEnumerable<object>)result.Data;
         Assert.AreEqual(0, data.Count());
      }

      [TestMethod]
      public void Create_Curve()
      {
         string curveName = "TestCurve";
         JsonResult result = controller.Create("TestCurve");
         Assert.AreEqual((result.Data as string).ToLowerInvariant(),"success");
         var curve = controller.BatContext().Curves.Where(c => c.Company.Id == user.Company.Id);
         Assert.IsNotNull(curve);
         Assert.AreEqual(1,curve.Count());
         Assert.AreEqual(curveName, curve.First().Name);

         JsonNetResult result2 = controller.Create("TestCurve") as JsonNetResult;
         Assert.IsTrue(result2.Data.ToString().Contains("error"));

         result2 = controller.Create("") as JsonNetResult;
         Assert.IsTrue(result2.Data.ToString().Contains("error"));

      }

      [TestMethod]
      public void Delete_Curves()
      {
         string curveName = "TestCurve";
         var curve = controller.BatContext().Curves.Where(c => c.Company.Id == user.Company.Id);
         Assert.AreEqual(0, curve.Count());
         Curve addCurve = new Curve() { Name = curveName, CurveValues = SampleData.CreateCurveValues(5), CompanyId = user.CompanyId };
         using (var dataContext = new BatModelContainer())
         {
            dataContext.Curves.Add(addCurve);
            dataContext.SaveChanges();
         }
         curve = controller.BatContext().Curves.Where(c => c.CompanyId == user.CompanyId);      
         Assert.AreEqual(1, curve.Count());
    

         JsonResult result = controller.DeleteCurves(new int[] { addCurve.Id});
         Assert.AreEqual((result.Data as string).ToLowerInvariant(), "success");
         curve = controller.BatContext().Curves.Where(c => c.CompanyId == user.CompanyId);
         Assert.IsNotNull(curve);
         Assert.AreEqual(0, curve.Count());
      }

      [TestMethod]
      public void Edit_Curve()
      {
         string curveName = "TestCurve";
         var curve = controller.BatContext().Curves.Where(c => c.Company.Id == user.Company.Id);
         Assert.AreEqual(0, curve.Count());
         Curve addCurve = new Curve() { Name = curveName, CurveValues = SampleData.CreateCurveValues(5), CompanyId = user.CompanyId };
         using (var dataContext = new BatModelContainer())
         {
            dataContext.Curves.Add(addCurve);
            dataContext.SaveChanges();
         }
         curve = controller.BatContext().Curves.Where(c => c.CompanyId == user.CompanyId);       
         Assert.AreEqual(1, curve.Count());
         var editedCurve = curve.First();
         string newName = "ChangedCurveName";
         editedCurve.Name = newName;

         JsonResult result = controller.Edit(editedCurve);
         Assert.AreEqual((result.Data as string).ToLowerInvariant(), "success");
         curve = controller.BatContext().Curves.Where(c => c.CompanyId == user.CompanyId);
         Assert.IsNotNull(curve);
         Assert.AreEqual(1, curve.Count());
         Assert.AreEqual(newName, curve.First().Name);
      }

      [TestMethod]
      public void CurveHandler_GetCurvePoints()
      {
         using (var dataContext = new BatModelContainer())
         {
            dataContext.Curves.AddRange(SampleData.CreateCurves(user.Company, 5));
            dataContext.SaveChanges();
         }
         var result = controller.CurveHandler(new JQueryDataTablesRequest() { curveId = controller.BatContext().Curves.First().Id });
         Assert.IsNotNull(result);
         var dataEnumerable = (result.Data as JQueryDataTablesResponse).aaData as IEnumerable<object>;
         Assert.AreEqual(5, dataEnumerable.Count());
         Assert.AreEqual(3, (dataEnumerable.First() as IEnumerable<object>).Count());
         //var data = JsonConvert.DeserializeObject<List<CurveValue>>(result.ToString());
      }

      [TestMethod]
      public void AddData_CurvePoints()
      {
         var curve = SampleData.CreateCurves(user.Company, 1).First();
          using (var dataContext = new BatModelContainer())
         {
            dataContext.Curves.Add(curve);
            dataContext.SaveChanges();
         }
         int initCurves = curve.CurveValues.Count();

         var result = controller.AddData(curve.Id, "1", 6);
         Assert.AreEqual(1,result);
         var userCurve = controller.BatContext().Curves.Where(c => c.CompanyId == user.CompanyId).First();
         Assert.AreEqual(initCurves + 1, userCurve.CurveValues.Count);
         Assert.AreEqual(1, userCurve.CurveValues.Where(d=>d.Day == 6).First().Weight);

         result = controller.AddData(curve.Id, "2", 6);
         Assert.AreEqual(-1, result);
         userCurve = controller.BatContext().Curves.Where(c => c.CompanyId == user.CompanyId).First();
         Assert.AreEqual(initCurves + 1, userCurve.CurveValues.Count);
         Assert.AreEqual(1, userCurve.CurveValues.Where(d => d.Day == 6).First().Weight);

         result = controller.AddData(null, "2", 6);
         Assert.AreEqual(-1, result);

         result = controller.AddData(curve.Id, "", 6);
         Assert.AreEqual(-1, result);

         result = controller.AddData(curve.Id, "2", null);
         Assert.AreEqual(-1, result);
      }

      [TestMethod]
      public void DeleteData_CurvePoints()
      {
         var curve = SampleData.CreateCurves(user.Company, 1).First();
         using (var dataContext = new BatModelContainer())
         {
            dataContext.Curves.Add(curve);
            dataContext.SaveChanges();
         }

         var initValuesCount = curve.CurveValues.Count();

         var result = controller.DeleteData(curve.CurveValues.First().Id);
         Assert.AreEqual("Success", result.Data);
         Assert.AreEqual(initValuesCount-1, controller.BatContext().Curves.Where(p=>p.Id == curve.Id).First().CurveValues.Count);
      }

      [TestMethod]
      public void UpdateData_CurvePoints()
      {
         var curve = SampleData.CreateCurves(user.Company, 1).First();
         using (var dataContext = new BatModelContainer())
         {
            dataContext.Curves.Add(curve);
            dataContext.SaveChanges();
         }

         int newValue = 8;
         Assert.AreNotEqual(newValue, curve.CurveValues.First().Day);
         var update = new CurveEntryUpdate() { ColumnId = 1, Id = curve.CurveValues.First().Id, Value = newValue.ToString() };
         var result = controller.UpdateData(update);
         Assert.AreEqual(newValue, int.Parse(result));
         Assert.AreEqual(newValue, controller.BatContext().Curves.Where(c => c.CompanyId == user.CompanyId).First().CurveValues.First().Day);

         double newValueD = 1.2;
         Assert.AreNotEqual(newValueD, curve.CurveValues.First().Weight);
         var update2 = new CurveEntryUpdate() { ColumnId = 2, Id = curve.CurveValues.First().Id, Value = newValueD.ToString()};
         result = controller.UpdateData(update2);
         Assert.AreEqual(newValueD, double.Parse(result));
         Assert.AreEqual(newValueD, controller.BatContext().Curves.Where(c => c.CompanyId == user.CompanyId).First().CurveValues.First().Weight);

      }
   }

   internal class MockCurvesController : CurvesController
   {
      public Guid UserGuid()
      {
         return CurrentUserId;
      }

      public BatContext BatContext()
      {
         return Context;
      }

      public User GetCurrentUser()
      {
         return CurrentUser;
      }
   }
}
