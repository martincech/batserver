﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using DataModel;
using System.Collections.ObjectModel;
using BatApp.Controllers;
using DataContext;
using BatApp.Models;

namespace BatApp.Tests.Controllers
{
   [TestClass]
   public class WeighingsControllerTests
   {
      private MockWeighingsController controller;
      private ICollection<User> users;
      private User user;
      private ICollection<Stat> stats;

      [TestInitialize]
      public void Init()
      {
         users = new Collection<User>();
         controller = new MockWeighingsController();
         users = SampleData.GetCreatedUsers();
         var scale1 = SampleData.CreateScales(users.ElementAt(0).Company, 2);
         var scale2 = SampleData.CreateScales(users.ElementAt(5).Company, 2);
         stats = SampleData.CreateStats(scale1.First(),15);
         stats.Union(SampleData.CreateStats(scale2.First(), 15));
         

         using (var dataContext = new BatModelContainer())
         {
            if (dataContext.Database.Exists())
            {
               dataContext.Database.Delete();
            }
            dataContext.Database.Create();
            dataContext.Users.AddRange(users).ToList();
            dataContext.Stats.AddRange(stats);
            dataContext.SaveChanges();
            user = users.FirstOrDefault();
            controller.SetExecutiveUser(user);           
         }
      }

      [TestMethod]
      public void WeighingsHandler()
      {

         using (var dataContext = new BatModelContainer())
         {
            var scale = new Scale() 
            {
               CompanyId = (int)user.CompanyId,
               Name = "Scale",
               SerialNumber = 123
            };
            scale.Flocks = SampleData.CreateFlocks(scale, 1);
            dataContext.Scales.Add(scale);
            dataContext.SaveChanges();
         }

         var result = controller.WeighingsHandler(new JQueryDataTablesRequest()
         {
            curveId = 0,      //controller.BatContext().Curves.First().Id,
            from = new DateTime(),
            to = new DateTime(),
            sEcho = 1,
            iColumns = 11,
            iDisplayLength = 135,
            iDisplayStart = 0,
            iSortingCols = 1,
            sColumns= ",,,,,,,,,",
            scaleId = 0      //controller.BatContext().Scales.First().Id
         });
      }

      [TestMethod]
      public void DeleteStats()
      {
         var statsId = controller.BatContext().Stats.Where(w=>w.Id < 10).Select(s=>s.Id).ToArray();
         Assert.IsTrue(controller.BatContext().Stats.Select(s => s.Id).Intersect(statsId).Any());
         var result = controller.DeleteStats(statsId);
         Assert.AreEqual("success", result.Data.ToString().ToLower());
         Assert.IsTrue(!controller.BatContext().Stats.Select(s=>s.Id).Intersect(statsId).Any());
      }
   }

   internal class MockWeighingsController : WeighingsController
   {
      public Guid UserGuid()
      {
         return CurrentUserId;
      }

      public BatContext BatContext()
      {
         return Context;
      }

      public User GetCurrentUser()
      {
         return CurrentUser;
      }
   }

}
