﻿using System;
using BatApp.Infrastructure;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using DataModel;
using System.Web.Mvc;
using DataContext;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using BatApp.Areas.UserAccounts.Controllers;
using BatApp.Areas.UserAccounts.Models.Manage;
using System.Reflection;
using System.Threading.Tasks;

namespace BatApp.Tests.Controllers.UserAccounts
{
   [TestClass]
   public class ManageControllerTests
   {
      private MockManageController controller;
      private ICollection<User> users;
      private User user;

      [TestInitialize]
      public void Init()
      {
         users = new Collection<User>();
         controller = new MockManageController();
         users = SampleData.GetCreatedUsers();
         PrivateObject po = new PrivateObject(controller);
         
         using (var dataContext = new BatModelContainer())
         {
            if (dataContext.Database.Exists())
            {
               dataContext.Database.Delete();
            }
            dataContext.Database.Create();
            dataContext.Users.AddRange(users).ToList();
            dataContext.SaveChanges();
            user = users.FirstOrDefault();
            controller.SetExecutiveUser(user);
         }

         var bat = new BatModelContainer();

         var uM = controller.GetType().BaseType.GetField("userManager", BindingFlags.Instance | BindingFlags.NonPublic);
         uM.SetValue(controller, ApplicationUserManager.Create(bat));

         var aRM = new ApplicationRoleManager(bat);

         var rM = controller.GetType().BaseType.GetField("roleManager", BindingFlags.Instance | BindingFlags.NonPublic);
         rM.SetValue(controller, aRM); 
      }

      [TestMethod]
      public void Index() 
      { 
         var result = controller.Index(1,null,"");
         Assert.IsNotNull(result);
      }

      [TestMethod]
      public void CreateUser_GetRoles()
      {  
         var result = controller.CreateUser();
         var data = result.Model as CreateUserViewModel;
         Assert.IsNotNull(data);
         Assert.AreEqual(4,data.InitialRoles.Count);

      }

      [TestMethod]
      public void CreateUser_Post()
      {
        
            Dictionary<string, bool> roles = new Dictionary<string, bool>();
            roles.Add(Constants.USER, true);
            //Does not work !! throwing exception in debug test
            var result = controller.CreateUser(new CreateUserViewModel()
            {
               Username = "NewTestUser",
               Company = user.CompanyId,
               Password = "NewUser_123",
               ConfirmPassword = "NewUser_123",
               Email = "email@email.com",
               InitialRoles = roles
            }).Result;
            Assert.IsNotNull(result);

            var newUser = controller.BatContext().Users.First(f => f.UserName == "NewTestUser");
            Assert.IsNotNull(newUser);
            Assert.AreEqual("email@email.com", newUser.Email);
           // Assert.AreEqual(roles.Select(s=>s.Key), newUser.Roles.Select(s=>s.Name));
            Assert.IsTrue(newUser.Roles.Select(s => s.Name).Contains(Constants.USER));
        
      }

      [TestMethod]
      public void EditUser_Get()
      {
         var result = controller.EditUser(user.Id).Result;
         var data = (result as ViewResult).Model as EditUserViewModel;
         Assert.IsNotNull(data);
         Assert.AreEqual(user.Id, data.UserId);
         Assert.AreEqual(user.UserName,data.Username);
         Assert.AreEqual(user.CompanyId, data.Company);
         Assert.AreEqual(user.UserName, data.Username);
         Assert.AreEqual(user.Comment, data.Comment);
      }

      [TestMethod]
      public void EditUser_Post()
      {
         Task.Run(async () =>
       {

          Dictionary<string, bool> roles = new Dictionary<string, bool>();
          roles.Add(Constants.ADMIN, true);
          //roles.Add(Constants.CAN_IMPORT_DATA, true);

          ActionResult ar = await controller.EditUser(new EditUserViewModel()
          {
             Username = "EditedUserAdmin",
             Roles = roles,
             UserId = users.Last().Id,
             Company = users.Last().CompanyId
          });

          Assert.IsNotNull(ar);
          var newUser = controller.BatContext().Users.First(f => f.UserName == "EditedUserAdmin");
          Assert.IsNotNull(newUser);
       }).GetAwaiter().GetResult();
         var edUser = controller.BatContext().Users.First(f => f.UserName == "EditedUserAdmin");
         Assert.IsNotNull(edUser);
      }

      [TestMethod]
      public void DeleteUser()
      {
         Assert.IsTrue(controller.BatContext().Users.Select(s => s.Id).Contains(users.Last().Id));
         var result = controller.DeleteConfirmed(users.Last().Id);
         Assert.IsNotNull(result);
         Assert.IsFalse(controller.BatContext().Users.Select(s => s.Id).Contains(users.Last().Id));
      }
   }

   internal class MockManageController : ManageController
   {
      public Guid UserGuid()
      {
         return CurrentUserId;
      }

      public BatContext BatContext()
      {
         return Context;
      }

      public User GetCurrentUser()
      {
         return CurrentUser;
      }
   }
}
