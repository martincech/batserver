﻿using System;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.UI.WebControls;
using Bat2Library;
using BatApp.Controllers;
using DataModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BatApp.Tests.Controllers
{
   [TestClass]
   public class DataPostControllerTests
   {
      private const int PACKAGE_SIZE_M = 36;
      private const int PACKAGE_SIZE_MF = 60;

      private const string PHONE_NUMBER = "722554564";

      private const string BASIC_NO_SEX =
          PHONE_NUMBER + " " + "SCALE 123456789012345 DAY 999 31.12.9999 23:59:59PM Cnt 9999 Avg 99.999 Gain -99.999 Sig 9.999 Cv 999 Uni 999";

      private const string BASIC_MALE_FEMALE =
          PHONE_NUMBER + " " + "SCALE 123456789012345 DAY 999 31.12.9999 23:59:59PM FEMALES: Cnt 1 Avg 1 Gain 1 Sig 1 Cv 1 Uni 1 MALES: Cnt 9999 Avg 99.999 Gain -99.999 Sig 9.999 Cv 999 Uni 999";

      private const string EXTENDED_NO_SEX =
          PHONE_NUMBER + " " + "7FFFFFFF 999 12/31/9999 99999 99.999 -99.999 9.999 99.9 99.9 -999.99 9999999 9999999 99.999";

      private const string EXTENDED_MALE_FEMALE =
          PHONE_NUMBER + " " + "7FFFFFFF 999 12/31/9999 1 1 1 1 1 1 99999 99.999 -99.999 9.999 99.9 99.9 -999.99 9999999 9999999 99.999";

      [TestInitialize]
      public void Init()
      {
         //BaseControllerFake.InitNinject();
         SampleData.CreateDatabase();
      }

      [TestMethod]
      public void Bat2_DataMale_SendOk()
      {
         using (var db = new BatModelContainer())
         {
            var user = db.Users.FirstOrDefault(u => u.Roles.Any(r => r.Name == Constants.ADMIN));
            Assert.IsNotNull(user, "No admin user in test DB!");
            var controller = new DataPostController();

            PublishedData sampleM = new PublishedData()
            {
               DateTime = new DateTime(2014, 12, 31, 23, 59, 58),
               Day = 2,
               Male = new GenderStats()
               {
                  Average = 3,
                  Count = 4,
                  Cv = 5,
                  Gain = 6,
                  Sigma = 7,
                  Target = 8,
                  Uniformity = 9
               },
               SerialNumber = 88
            };

            byte[] dataM = MessageParser.GenerateData(sampleM);
            Assert.AreEqual(PACKAGE_SIZE_M, dataM.Length);
            controller.SetExecutiveUser(user);
            controller.ControllerContext.RequestContext.HttpContext.Request.InputStream.Write(dataM, 0, dataM.Length);
            var response = controller.Bat2Post(1);
            Assert.AreEqual(HttpStatusCode.OK, (HttpStatusCode)response.StatusCode);
            var sn = sampleM.SerialNumber.ToString();
            var scale = db.Scales.FirstOrDefault(s => s.Name == sn);
            Assert.IsNotNull(scale);

            var statM = scale.Stat.FirstOrDefault(s => s.Sex == SexE.SEX_UNDEFINED);
            Assert.IsNotNull(statM);
            Assert.AreEqual(sampleM.Day, statM.Day);
            GS_S_AreEqual(sampleM.Male, statM);
         }
      }

      [TestMethod]
      public void Bat2_DataMaleFemale_SendOk()
      {
         using (var db = new BatModelContainer())
         {
            var user = db.Users.FirstOrDefault(u => u.Roles.Any(r => r.Name == Constants.ADMIN));
            Assert.IsNotNull(user, "No admin user in test DB!");
            var controller = new DataPostController();

            PublishedData sampleMF = new PublishedData()
            {
               DateTime = new DateTime(2014, 12, 31, 10, 17, 19),
               Day = 2,
               Male = new GenderStats()
               {
                  Average = 3,
                  Count = 4,
                  Cv = 5,
                  Gain = 6,
                  Sigma = 7,
                  Target = 8,
                  Uniformity = 9
               },
               Female = new GenderStats()
               {
                  Average = 33,
                  Count = 44,
                  Cv = 55,
                  Gain = 66,
                  Sigma = 77,
                  Target = 88,
                  Uniformity = 99
               },
               SerialNumber = 99
            };

            byte[] dataMF = MessageParser.GenerateData(sampleMF);
            Assert.AreEqual(PACKAGE_SIZE_MF, dataMF.Length);
            controller.SetExecutiveUser(user);
            controller.ControllerContext.RequestContext.HttpContext.Request.InputStream.Write(dataMF, 0, dataMF.Length);
            var response = controller.Bat2Post(1);
            Assert.AreEqual(HttpStatusCode.OK, (HttpStatusCode)response.StatusCode);

            var sn = sampleMF.SerialNumber.ToString();

            var scale = db.Scales.FirstOrDefault(s => s.Name == sn);
            Assert.IsNotNull(scale);
            var statM = scale.Stat.FirstOrDefault(s => s.Sex == SexE.SEX_MALE);
            Assert.IsNotNull(statM);
            Assert.AreEqual(sampleMF.Day, statM.Day);
            GS_S_AreEqual(sampleMF.Male, statM);

            var statF = scale.Stat.FirstOrDefault(s => s.Sex == SexE.SEX_FEMALE);
            Assert.IsNotNull(statF);
            Assert.AreEqual(sampleMF.Day, statF.Day);
            GS_S_AreEqual(sampleMF.Female, statF);
         }
      }

      [TestMethod]
      public void Bat2Sms_DataUndefined_SendOk()
      {
         //"SCALE 123456789012345 DAY 999 31.12.9999 23:59:59PM Cnt 9999 Avg 99.999 Gain -99.999 Sig 9.999 Cv 999 Uni 999";
         Bat2Sms_SendOk(BASIC_NO_SEX, 999,
            new Stat()
            {
               Count = 9999,
               Average = new decimal(99.999),
               Cv = 999,
               Gain = new decimal(-99.999),
               Sigma = new decimal(9.999),
               Uni = 999
            });
      }

      [TestMethod]
      public void Bat2Sms_DataMaleFemale_SendOk()
      {
         //"SCALE 123456789012345 DAY 999 31.12.9999 23:59:59PM FEMALES: Cnt 1 Avg 1 Gain 1 Sig 1 Cv 1 Uni 1 MALES: Cnt 9999 Avg 99.999 Gain -99.999 Sig 9.999 Cv 999 Uni 999";
         Bat2Sms_SendOk(BASIC_MALE_FEMALE, 999,
            new Stat()
            {
               Count = 1,
               Average = 1,
               Cv = 1,
               Gain = 1,
               Sigma = 1,
               Uni = 1
            },
            new Stat()
            {
               Count = 9999,
               Average = new decimal(99.999),
               Cv = 999,
               Gain = new decimal(-99.999),
               Sigma = new decimal(9.999),
               Uni = 999
            });
      }

      [TestMethod]
      public void Bat2Sms_DataUndefined_Extended_SendOk()
      {
         //"7FFFFFFF 999 12/31/9999 99999 99.999 -99.999 9.999 99.9 99.9 -999.99 9999999 9999999 99.999"
         Bat2Sms_SendOk(EXTENDED_NO_SEX, 999,
            new Stat()
            {
               Count = 99999,
               Average = new decimal(99.999),
               Cv = new decimal(99.9),
               Gain = new decimal(-99.999),
               Sigma = new decimal(9.999),
               Uni = new decimal(99.9),
               Temperature = (decimal?) -999.99,
               CarbonDioxide = 9999999,
               Ammonia = 9999999,
               Humidity = (decimal?) 99.999
            });
      }

      [TestMethod]
      public void Bat2Sms_DataMaleFemale_Extended_SendOk()
      {
         //"7FFFFFFF 999 12/31/9999 1 1 1 1 1 1 99999 99.999 -99.999 9.999 99.9 99.9 -999.99 9999999 9999999 99.999"
         Bat2Sms_SendOk(EXTENDED_MALE_FEMALE, 999,
            new Stat()
            {
               Count = 1,
               Average = 1,
               Cv = 1,
               Gain = 1,
               Sigma = 1,
               Uni = 1,
               Temperature = (decimal?) -999.99,
               CarbonDioxide = 9999999,
               Ammonia = 9999999,
               Humidity = (decimal?) 99.999
            },
            new Stat()
            {
               Count = 99999,
               Average = new decimal(99.999),
               Cv = new decimal(99.9),
               Gain = new decimal(-99.999),
               Sigma = new decimal(9.999),
               Uni = new decimal(99.9),
               Temperature = (decimal?) -999.99,
               CarbonDioxide = 9999999,
               Ammonia = 9999999,
               Humidity = (decimal?) 99.999
            });
      }

      
      private void Bat2Sms_SendOk(string smsMessage, int day, Stat female, Stat male = null)
      {
         using (var db = new BatModelContainer())
         {
            var user = db.Users.FirstOrDefault(u => u.Roles.Any(r => r.Name == Constants.ADMIN));
            Assert.IsNotNull(user, "No admin user in test DB!");
            var controller = new DataPostController();

            var message = System.Text.Encoding.UTF8.GetBytes(smsMessage);

            controller.SetExecutiveUser(user);
            controller.ControllerContext.RequestContext.HttpContext.Request.InputStream.Write(message, 0, message.Length);
            var response = controller.Bat2SmsPost();
            Assert.AreEqual(HttpStatusCode.OK, (HttpStatusCode)response.StatusCode);

            int serialNumber = 0;
            Scale scale = null;
            if (int.TryParse(smsMessage.Split(' ')[1], NumberStyles.HexNumber, CultureInfo.InvariantCulture,
               out serialNumber))
            {
               scale = db.Scales.FirstOrDefault(s => s.SerialNumber == serialNumber);
            }
            else
            {
               var scaleName = smsMessage.Split(' ')[2];
               scale = db.Scales.FirstOrDefault(s => s.Name == scaleName);
            }
            Assert.IsNotNull(scale);

            SexE sex = SexE.SEX_UNDEFINED;
            if (male != null)
            {
               var statM = scale.Stat.FirstOrDefault(s => s.Sex == SexE.SEX_MALE);
               Assert.IsNotNull(statM);
               Assert.AreEqual(day, statM.Day);
               Stats_AreEqual(male, statM);

               sex = SexE.SEX_FEMALE;
            }

            var statF = scale.Stat.FirstOrDefault(s => s.Sex == sex);
            Assert.IsNotNull(statF);
            Assert.AreEqual(day, statF.Day);
            Stats_AreEqual(female, statF);
         }
      }

      private void GS_S_AreEqual(GenderStats expected, Stat actual)
      {
         Assert.AreEqual((double)expected.Average / 10, actual.Average);
         Assert.AreEqual((int)expected.Count, actual.Count);
         Assert.AreEqual((double)expected.Cv / 10, actual.Cv);
         Assert.AreEqual((double)expected.Gain / 10, actual.Gain);
         Assert.AreEqual((double)expected.Sigma / 10, actual.Sigma);
         Assert.AreEqual((double)expected.Uniformity / 10, actual.Uni);
      }

      private void Stats_AreEqual(Stat expected, Stat actual)
      {
         Assert.AreEqual(expected.Average, actual.Average);
         Assert.AreEqual(expected.Count, actual.Count);
         Assert.AreEqual(expected.Cv, actual.Cv);
         Assert.AreEqual(expected.Gain, actual.Gain);
         Assert.AreEqual(expected.Sigma, actual.Sigma);
         Assert.AreEqual(expected.Uni, actual.Uni);
         Assert.AreEqual(expected.Temperature, actual.Temperature);
         Assert.AreEqual(expected.CarbonDioxide, actual.CarbonDioxide);
         Assert.AreEqual(expected.Ammonia, actual.Ammonia);
         Assert.AreEqual(expected.Humidity, actual.Humidity);

      }

      /*
      [TestMethod]
      public void Bat2_Authorized_ReturnOk()
      {
         using (var db = new BatModelContainer())
         {
            var user = db.Users.FirstOrDefault(u => u.Roles.Any(r => r.Name == Constants.ADMIN));
            Assert.IsNotNull(user, "No CID user in test DB!");
            var name = user.UserName;
            const string password = "Admin_123";

            var controller = new DataPostController();
            var context = new ControllerContext(
               new RequestContext(
                  BaseControllerFake.CreateHttpContext("/DataPost/Bat2", userName: name, password: password),
                  new RouteData()), controller);
            controller.ActionInvoker.InvokeAction(context, "Bat2");
            Assert.AreEqual(HttpStatusCode.OK, context.RequestContext.HttpContext.Response.StatusCode);
         }
      }

      [TestMethod]
      public void Bat2_Unauthenticated_ReturnNOk()
      {
         using (var db = new BatModelContainer())
         {
            var user = db.Users.FirstOrDefault(u => u.Roles.Any(r => r.Name == Constants.USER));
            Assert.IsNotNull(user, "No CID user in test DB!");
            var name = user.UserName + "fake";
            const string password = "Admin_123";

            var controller = new DataPostController();
            var context = new ControllerContext(
               new RequestContext(
                  BaseControllerFake.CreateHttpContext("/DataPost/Bat2", userName: name, password: password),
                  new RouteData()), controller);
            controller.ActionInvoker.InvokeAction(context, "Bat2");
            Assert.AreNotEqual(HttpStatusCode.OK, context.RequestContext.HttpContext.Response.StatusCode);
            Assert.AreEqual(HttpStatusCode.Unauthorized, context.RequestContext.HttpContext.Response.StatusCode);
         }
      }

      [TestMethod]
      public void Bat2_Unauthorized_ReturnNOk()
      {
         using (var db = new BatModelContainer())
         {
            var user = db.Users.FirstOrDefault(u => u.Roles.Any(r => r.Name != Constants.USER));
            Assert.IsNotNull(user, "No CID user in test DB!");
            var name = user.UserName;
            const string password = "Admin_123";

            var controller = new DataPostController();
            var context = new ControllerContext(
               new RequestContext(
                  BaseControllerFake.CreateHttpContext("/DataPost/Bat2", userName: name, password: password),
                  new RouteData()), controller);
            controller.ActionInvoker.InvokeAction(context, "Bat2");
            Assert.AreNotEqual(HttpStatusCode.OK, context.RequestContext.HttpContext.Response.StatusCode);
            Assert.AreEqual(HttpStatusCode.Unauthorized, context.RequestContext.HttpContext.Response.StatusCode);
         }
      }
      */
   }
}