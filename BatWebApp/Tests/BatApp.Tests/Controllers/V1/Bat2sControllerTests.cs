﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using BatApp.Controllers.V1;
using DataModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BatApp.Tests.Controllers.V1
{
   [TestClass]
   public class Bat2sControllerTests
   {
      [TestInitialize]
      public void Init()
      {
         ICollection<User> users = new Collection<User>();
         var baseController = new MockController();
         users = SampleData.GetCreatedUsers();

         using (var dataContext = new BatModelContainer())
         {
            if (dataContext.Database.Exists())
            {
               dataContext.Database.Delete();
            }
            dataContext.Database.Create();
            dataContext.Users.AddRange(users);
            var scale1 = new Scale { Name = "scale01", SerialNumber = 123456 };
            var scale2 = new Scale { Name = "scale02", SerialNumber = 987654 };
            dataContext.Scales.Add(scale1);
            dataContext.Scales.Add(scale2);
            var user = users.FirstOrDefault();
            dataContext.SaveChanges();
            baseController.SetExecutiveUser(user);
         }
      }

      [TestMethod]
      public void GetAllScales()
      {        
         var controller = new Bat2sController();

         var s = controller.Get();
         var scales = s as IList<Scale> ?? s.ToList();
         Assert.AreEqual(2, scales.Count());
         Assert.AreEqual("scale01", scales.First().Name);
         Assert.AreEqual(123456, scales.First().SerialNumber);
      }

      [TestMethod]
      public void GetScaleById_ScaleExist()
      {
         var controller = new Bat2sController();
         using (var dataContext = new BatModelContainer())
         {
            var s = dataContext.Scales.FirstOrDefault();
            Assert.IsNotNull(s);

            var scale = controller.Get(s.Id);
            Assert.IsNotNull(scale);
            Assert.AreEqual(s.SerialNumber, scale.SerialNumber);
            Assert.AreEqual(s.Name, scale.Name);
         }
      }

      [TestMethod]
      public void GetScaleById_ScaleNonExist()
      {
         var controller = new Bat2sController();
         using (var dataContext = new BatModelContainer())
         {
            var maxId = dataContext.Scales.Max(s => s.Id);
            var scale = controller.Get(maxId + 1);
            Assert.IsNull(scale);
         }
      }

      [TestMethod]
      public void CreateScale_ByScale()
      {
         var controller = new Bat2sController();
         using (var dataContext = new BatModelContainer())
         {
            var count = dataContext.Scales.Count();
            var scale = new Scale { Name = "super Scale", SerialNumber = 11399 };
            var response = controller.Post(scale);

            //check answer
            Assert.IsNotNull(response);
            Assert.AreEqual("super Scale", response.Name);
            Assert.AreEqual(11399, response.SerialNumber);

            //check db
            Assert.AreEqual(count + 1, dataContext.Scales.Count());
            var insertedScale = dataContext.Scales.FirstOrDefault(s => s.Name.Equals("super Scale"));
            Assert.IsNotNull(insertedScale);
         }
      }

      [TestMethod]
      public void CreateScale_ByParameters()
      {
         var controller = new Bat2sController();
         using (var dataContext = new BatModelContainer())
         {
            var count = dataContext.Scales.Count();
            var response = controller.Post(112233, "scale003", "911", "farm03");

            //check answer
            Assert.AreEqual(200, response.StatusCode);
            var deserObj = Newtonsoft.Json.JsonConvert.DeserializeObject<Scale>(response.StatusDescription);
            Assert.AreEqual("scale003", deserObj.Name);
            Assert.AreEqual(112233, deserObj.SerialNumber);
            Assert.AreEqual("911", deserObj.PhoneNumber);
            Assert.AreEqual("farm03", deserObj.FarmName);

            //check db
            Assert.AreEqual(count + 1, dataContext.Scales.Count());
            var insertedScale = dataContext.Scales.FirstOrDefault(s => s.SerialNumber == 112233);
            Assert.IsNotNull(insertedScale);
         }
      }

      [TestMethod]
      public void DeleteScale()
      {
         var controller = new Bat2sController();       
         using (var dataContext = new BatModelContainer())
         {
            CreateScale_ByParameters();
            var scales = dataContext.Scales.ToList();
            var scale = scales.FirstOrDefault(s => s.SerialNumber == 112233);
            Assert.IsNotNull(scale);      
            var response = controller.Delete(scale.Id);

            //check answer
            Assert.AreEqual(200, response.StatusCode);
            //check db
            Assert.AreEqual(scales.Count() - 1, dataContext.Scales.Count());
            var delScale = dataContext.Scales.FirstOrDefault(s => s.Id == scale.Id);
            Assert.IsNull(delScale);
         }
      }

      [TestMethod]
      public void DeleteScale_NonExisting()
      {
         var controller = new Bat2sController();
         using (var dataContext = new BatModelContainer())
         {
            var scales = dataContext.Scales.ToList();
            var maxId = scales.Max(s => s.Id);
            var response = controller.Delete(maxId + 1);

            //check answer
            Assert.AreEqual(202, response.StatusCode);
            //check db
            Assert.AreEqual(scales.Count(), dataContext.Scales.Count());         
         }
      }

      [TestMethod]
      public void DeleteMoreScales()
      {
         var controller = new Bat2sController();
         using (var dataContext = new BatModelContainer())
         {
            var scales = dataContext.Scales.ToList();
            var ids = scales.Select(s => s.Id).ToArray();       
            var response = controller.Delete(ids);

            //check answer
            Assert.AreEqual(200, response.StatusCode);
            //check db
            Assert.AreEqual(0, dataContext.Scales.Count());

            foreach (var scale in scales)
            {
               var delScale = dataContext.Scales.FirstOrDefault(s => s.Id == scale.Id);
               Assert.IsNull(delScale);
            }
         }
      }
   }
}
