﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Bat2Library;
using Bat2Library.Bat2Old.DB.Helpers;
using BatApp.Controllers.V1;
using DataModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BatApp.Tests.Controllers.V1
{
   [TestClass]
   public class Bat2OldStatisticsControllerTests
   {
      [TestInitialize]
      public void Init()
      {
         ICollection<User> users = new Collection<User>();
         var baseController = new MockController();
         users = SampleData.GetCreatedUsers();

         using (var dataContext = new BatModelContainer())
         {
            if (dataContext.Database.Exists())
            {
               dataContext.Database.Delete();
            }
            dataContext.Database.Create();
            dataContext.Users.AddRange(users);
            var scale1 = new Scale { Name = "scale01", SerialNumber = 123456 };
            dataContext.Scales.Add(scale1);
            var user = users.FirstOrDefault();
            dataContext.SaveChanges();
            baseController.SetExecutiveUser(user);
         }
      }


      [TestMethod]
      public void CreateStatistic_Sex_Undefined()
      {
         var controller = new Bat2OldStatisticsController();
         using (var dataContext = new BatModelContainer())
         {
            var origStatCount = dataContext.Stats.Count();

            var phoneNumber = "0609111222";
            short id = 32767;
            short day = 1;
            var date = DateTime.Now;

            short count = 10;
            var avg = 5;
            var gain = 2;
            var sigma = 100;
            short cv = 50;
            short uni = 88;

            var statistic = new Statistic
            {
               Count = count,
               Average = avg,
               Gain = gain,
               Sigma = sigma,
               Cv = cv,
               Uni = uni
            };
            var sms = new Bat2Library.Bat2Old.DB.Sms
            {
               GsmNumber = phoneNumber,
               Id = id,
               DayNumber = day,
               DayDate = date,
               FemaleStat = statistic
            };

            var s = controller.Post(sms);
            var stats = s as IList<Stat> ?? s.ToList();
            Assert.IsNotNull(s);
            Assert.AreEqual(dataContext.Stats.Count(), origStatCount + stats.Count());

            var statId = stats.First().Id;
            var dbStat = dataContext.Stats.FirstOrDefault(x => x.Id == statId);
            Assert.IsNotNull(dbStat);
            Assert.AreEqual(phoneNumber, dbStat.Scale.PhoneNumber);
            Assert.AreEqual(id.ToString(), dbStat.Scale.Name);
            Assert.AreEqual(day, dbStat.Day);
            Assert.AreEqual(date.ToShortDateString(), dbStat.Date.ToShortDateString());
            Assert.AreEqual(SexE.SEX_UNDEFINED, dbStat.Sex);
            Assert.AreEqual(count, dbStat.Count);
            Assert.AreEqual(avg, dbStat.Average);
            Assert.AreEqual(gain, dbStat.Gain);
            Assert.AreEqual(sigma, dbStat.Sigma);
            Assert.AreEqual(cv, dbStat.Cv);
            Assert.AreEqual(uni, dbStat.Uni);
         }
      }


      [TestMethod]
      public void CreateStatistic_Sex_Both()
      {
         var controller = new Bat2OldStatisticsController();
         using (var dataContext = new BatModelContainer())
         {
            var origStatCount = dataContext.Stats.Count();

            short id = 16384;
            short day = 1;
            var date = DateTime.Now;
          
            short count = 10;
            var avg = 5;
            var gain = 2;
            var sigma = 100;
            short cv = 50;
            short uni = 88;

            var statistic = new Statistic
            {
               Count = count,
               Average = avg,
               Gain = gain,
               Sigma = sigma,
               Cv = cv,
               Uni = uni
            };
            var sms = new Bat2Library.Bat2Old.DB.Sms
            {
               Id = id,
               DayNumber = day,
               DayDate = date,
               FemaleStat = statistic,
               MaleStat = statistic
            };

            var s = controller.Post(sms);
            var stats = s as IList<Stat> ?? s.ToList();
            Assert.IsNotNull(s);
            Assert.AreEqual(dataContext.Stats.Count(), origStatCount + stats.Count());

            var statFId = stats.First(x => x.Sex == SexE.SEX_FEMALE).Id;
            var dbStatF = dataContext.Stats.FirstOrDefault(x => x.Id == statFId && x.Sex == SexE.SEX_FEMALE);
            Assert.IsNotNull(dbStatF);
            Assert.IsTrue(string.IsNullOrEmpty(dbStatF.Scale.PhoneNumber));     
            Assert.AreEqual(day, dbStatF.Day);
            Assert.AreEqual(date.ToShortDateString(), dbStatF.Date.ToShortDateString());        
            Assert.AreEqual(count, dbStatF.Count);
            Assert.AreEqual(avg, dbStatF.Average);
            Assert.AreEqual(gain, dbStatF.Gain);
            Assert.AreEqual(sigma, dbStatF.Sigma);
            Assert.AreEqual(cv, dbStatF.Cv);
            Assert.AreEqual(uni, dbStatF.Uni);

            var statMId = stats.First(x => x.Sex == SexE.SEX_MALE).Id;
            var dbStatM = dataContext.Stats.FirstOrDefault(x => x.Id == statMId && x.Sex == SexE.SEX_MALE);
            Assert.IsNotNull(dbStatM);
            Assert.IsTrue(string.IsNullOrEmpty(dbStatM.Scale.PhoneNumber));     
            Assert.AreEqual(day, dbStatM.Day);
            Assert.AreEqual(date.ToShortDateString(), dbStatM.Date.ToShortDateString());
            Assert.AreEqual(count, dbStatM.Count);
            Assert.AreEqual(avg, dbStatM.Average);
            Assert.AreEqual(gain, dbStatM.Gain);
            Assert.AreEqual(sigma, dbStatM.Sigma);
            Assert.AreEqual(cv, dbStatM.Cv);
            Assert.AreEqual(uni, dbStatM.Uni);
         }
      }


      [TestMethod]
      public void CreateStatistic_MissingData()
      {
         var controller = new Bat2OldStatisticsController();     
         short id = 32767;
         short day = 1;
         var date = DateTime.Now;
         
         var sms = new Bat2Library.Bat2Old.DB.Sms
         {
            Id = id,
            DayNumber = day,
            DayDate = date
         };

         var s = controller.Post(sms);
         Assert.IsNull(s);                 
      }

   }
}
