﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using BatApp.Controllers.V1;
using DataModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BatApp.Tests.Controllers.V1
{
   [TestClass]
   public class Bat2OldCurvesControllerTests
   {
      [TestInitialize]
      public void Init()
      {
         ICollection<User> users = new Collection<User>();
         var baseController = new MockController();
         users = SampleData.GetCreatedUsers();

         using (var dataContext = new BatModelContainer())
         {
            if (dataContext.Database.Exists())
            {
               dataContext.Database.Delete();
            }
            dataContext.Database.Create();
            dataContext.Users.AddRange(users);
            var scale1 = new Scale { Name = "scale01", SerialNumber = 123456 };
            dataContext.Scales.Add(scale1);
            var user = users.FirstOrDefault();
            dataContext.SaveChanges();
            baseController.SetExecutiveUser(user);
         }
      }

      [TestMethod]
      public void CreateCurve_WithRecords()
      {
         var controller = new Bat2OldCurvesController();
         using (var dataContext = new BatModelContainer())
         {
            var origCurvesCount = dataContext.Curves.Count();
            var name = "Test curve name";
            var records = new Dictionary<int, double>
            {
               { 0, 2.2},
               { 1, 3.2},
               { 2, 3.8},
               { 3, 2.5},
               { 4, 2.2},
               { 5, 2.2},
               { 6, 6.2},
               { 7, 7.48}
            };
            var curve = new Bat2Library.Bat2Old.DB.GrowthCurve
            {
                Name = name,
                Records = records
            };
            var c = controller.Post(curve);
            Assert.IsNotNull(c);
            Assert.AreEqual(dataContext.Curves.Count(), origCurvesCount + 1);

            var dbCurve = dataContext.Curves.FirstOrDefault(x => x.Name.Equals(name));
            Assert.IsNotNull(dbCurve);
            Assert.AreEqual(records.Count, dbCurve.CurveValues.Count);

            foreach (var item in dbCurve.CurveValues)
            {
               if (!records.ContainsKey(item.Day))
               {
                  throw new Exception("Incorrect curve values at database");
               }

               double value;
               records.TryGetValue(item.Day, out value);
               Assert.AreEqual(value, item.Weight);
            }
         }
      }

      [TestMethod]
      public void CreateCurve_WithoutRecords()
      {
         var controller = new Bat2OldCurvesController();
         using (var dataContext = new BatModelContainer())
         {
            var origCurvesCount = dataContext.Curves.Count();
            var name = "Test curve name22";
            var curve = new Bat2Library.Bat2Old.DB.GrowthCurve { Name = name };
            var c = controller.Post(curve);
            Assert.IsNotNull(c);
            Assert.AreEqual(dataContext.Curves.Count(), origCurvesCount + 1);

            var dbCurve = dataContext.Curves.FirstOrDefault(x => x.Name.Equals(name));
            Assert.IsNotNull(dbCurve);
            Assert.AreEqual(0, dbCurve.CurveValues.Count);
         }        
      }

      [TestMethod]
      public void CreateCurve_MissingName()
      {
         var controller = new Bat2OldCurvesController();
         var curve = new Bat2Library.Bat2Old.DB.GrowthCurve { Name = "" };
         var c = controller.Post(curve);
         Assert.IsNull(c);                        
      }

      [TestMethod]
      public void CreateCurve_ExistingName()
      {
         var controller = new Bat2OldCurvesController();
         using (var dataContext = new BatModelContainer())
         {
            var origCurvesCount = dataContext.Curves.Count();
            var name = "Test curve name3";
            var curve = new Bat2Library.Bat2Old.DB.GrowthCurve { Name = name };
            var c = controller.Post(curve);
            Assert.IsNotNull(c);
            Assert.AreEqual(dataContext.Curves.Count(), origCurvesCount + 1);

            c = controller.Post(curve);
            Assert.IsNull(c);
         }       
      }


   }
}
