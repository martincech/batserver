﻿using System;
using System.Linq;
using DataModel;
using Microsoft.AspNet.Identity;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DataContext.Tests.UnitTests
{
   [TestClass]
   public class UserManagerTests
   {
      [TestInitialize]
      public void Init()
      {
         var db = new BatModelContainer();
         using (var manager = new UserManager(db))
         {
            var user = manager.FindByName("admin");
            if (user == null)
            {
               user = db.Users.Create();
               user.Id = Guid.NewGuid();
               user.UserName = "admin";
               user.Email = "admin@admin.cz";
               user.CreationDate = DateTime.Now;
               user.IsAnonymous =
                  user.TwoFactorEnabled = user.LockoutEnabled = user.PhoneNumberConfirmed = user.EmailConfirmed = false;
               user.AccessFailedCount = 0;
               var uRole = db.Roles.First(role => role.Name == DataModel.Constants.ADMIN);
               user.Roles.Add(uRole);
               var id = manager.Create(user, "Admin_789123");
            }
         }
      }

      [TestMethod]
      public void CanFindAdminUser()
      {
         var db = new BatModelContainer();
         using (var manager = new UserManager(db))
         {
            var user = manager.FindByName("admin");
            Assert.IsNotNull(user);
         }
      }

      [TestMethod]
      public void AdminUser_InAdminRole()
      {
         var db = new BatModelContainer();
         using (var manager = new UserManager(db))
         {
            var user = manager.FindByName("admin");
            Assert.IsTrue(manager.IsInRole(user.Id, DataModel.Constants.ADMIN));
         }
      }

      [TestMethod]
      public void CanCreateAndDeleteUser()
      {
         var db = new BatModelContainer();
         using (var manager = new UserManager(db))
         {
            var user = db.Users.Create();
            user.Id = Guid.NewGuid();
            user.UserName = "DummyUser";
            user.Email = "DummyUser@DummyUser.cz";

            user.IsAnonymous =
               user.TwoFactorEnabled = user.LockoutEnabled = user.PhoneNumberConfirmed = user.EmailConfirmed = false;
            user.AccessFailedCount = 0;
            var uRole = db.Roles.First(role => role.Name == DataModel.Constants.ADMIN);
            user.Roles.Add(uRole);
            user.CreationDate = DateTime.Now;
            var id = manager.Create(user, "DummyUser_123");
            Assert.IsTrue(id.Succeeded);

            id = manager.Delete(user);
            Assert.IsTrue(id.Succeeded);
         }
      }

      [TestMethod]
      public void CanCreateUserIdentity()
      {
         var db = new BatModelContainer();
         using (var manager = new UserManager(db))
         {
            var user = manager.FindByName("admin");
            var userIdentity = manager.CreateIdentity(user, DefaultAuthenticationTypes.ApplicationCookie);
            Assert.IsNotNull(userIdentity);
         }
      }
   }
}