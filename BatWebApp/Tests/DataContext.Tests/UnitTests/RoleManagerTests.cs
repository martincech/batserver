﻿using DataModel;
using Microsoft.AspNet.Identity;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DataContext.Tests.UnitTests
{
   [TestClass]
   public class RoleManagerTests
   {
      [TestMethod]
      public void CanAddAndRemoveRole()
      {
         using (var manager = new RoleManager())
         {
            const string dumyrole = "DumyRole";
            var id = manager.Create(new Role() {Name = dumyrole});
            Assert.IsTrue(id.Succeeded);
            Assert.IsTrue(manager.RoleExists(dumyrole));
            var role = manager.FindByName(dumyrole);
            Assert.IsNotNull(role);
            manager.Delete(role);
            Assert.IsFalse(manager.RoleExists(dumyrole));
         }
      }


   }
}
