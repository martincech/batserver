﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using DataModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Bat2Library;

namespace DataContext.Tests.UnitTests
{
   /// <summary>
   /// Summary description for BatContextTests
   /// </summary>
   [TestClass]
   public class BatContextTests
   {

      private BatContext context;

      private Stat statData = null;
      private Stat newStatData = null;

      private int scaleSN;


      [TestInitialize]
      public void Init()
      {
         var company = new Company { Id = 0, Name = "VEIT" };
         var user = new User { UserName = "Pepa", Id = Guid.NewGuid(), CreationDate = DateTime.Now, Company = company, CompanyId = company.Id };
         company.Users = new Collection<User> { user };
         user.Roles = new Collection<Role>
         {
            new Role {Id = Guid.NewGuid(), Name = Constants.ADMIN, Users = new Collection<User> {user}},
            new Role {Id = Guid.NewGuid(), Name = Constants.COMPANY_ADMIN, Users = new Collection<User> {user}}
         };

         using (var dataContext = new BatModelContainer())
         {
            if (dataContext.Database.Exists())
            {
               dataContext.Database.Delete();
            }
            dataContext.Database.Create();
            Assert.IsTrue(dataContext.Database.Exists());
            dataContext.Users.Add(user);
            dataContext.SaveChanges();
         }
         context = new BatContext(user.Id);

         scaleSN = 1;

         statData = new Stat()
         {
            Average = new decimal(1.2),
            Count = 20,
            Cv = new decimal(1.3),
            Date = DateTime.Now,
            Day = 1,
            Gain = new decimal(1.3),
            Sex = SexE.SEX_UNDEFINED,
            Sigma = new decimal(1.33),
            Uni = new decimal(1.11)
         };

         newStatData = new Stat()
         {
            Average = 5,
            Count = 999,
            Cv = 5,
            Date = DateTime.Now,
            Day = 5,
            Gain = 5,
            Sex = SexE.SEX_UNDEFINED,
            Sigma = 5,
            Uni = 5
         };

      }

      [TestMethod]
      public void SaveStat_Insert_Female()
      {
         var stat = Copy(statData);
         stat.Sex = SexE.SEX_FEMALE;
         var statBase = Copy(stat);
         SaveStat_Insert(stat);
         Assert.IsTrue(AreEqual(statBase, stat));
      }

      [TestMethod]
      public void SaveStat_Update_Female()
      {
         SaveStat_Insert_Female();
         var newStat = Copy(newStatData);
         newStat.Sex = SexE.SEX_FEMALE;
         SaveStat_Update(newStat);
      }

      [TestMethod]
      public void SaveStat_Insert_Male()
      {
         var stat = Copy(statData);
         stat.Sex = SexE.SEX_MALE;
         var statBase = Copy(stat);
         SaveStat_Insert(stat);
         Assert.IsTrue(AreEqual(statBase, stat));
      }

      [TestMethod]
      public void SaveStat_Update_Male()
      {
         SaveStat_Insert_Male();
         var newStat = Copy(newStatData);
         newStat.Sex = SexE.SEX_MALE;
         SaveStat_Update(newStat);
      }

      [TestMethod]
      public void SaveStat_Insert_Undefined()
      {
         var stat = Copy(statData);
         stat.Sex = SexE.SEX_UNDEFINED;
         var statBase = Copy(stat);
         SaveStat_Insert(stat);
         Assert.IsTrue(AreEqual(statBase, stat));
      }

      [TestMethod]
      public void SaveStat_Update_Undefined()
      {
         SaveStat_Insert_Undefined();
         var newStat = Copy(newStatData);
         newStat.Sex = SexE.SEX_UNDEFINED;
         SaveStat_Update(newStat);
      }

      [TestMethod]
      public void SaveStat_Insert_Male_UndefinedToFemale()
      {
         SaveStat_Insert_Undefined();
         var stat = context.Stats.ToList().FirstOrDefault(x => x.Scale.SerialNumber == scaleSN && x.Sex == SexE.SEX_FEMALE);
         Assert.IsNull(stat);
         SaveStat_Insert_Male();
         stat = context.Stats.ToList().FirstOrDefault(x => x.Scale.SerialNumber == scaleSN && x.Sex == SexE.SEX_FEMALE);
         Assert.IsNotNull(stat);
      }

      [TestMethod]
      public void SaveStat_Insert_Undefined_UndefinedToFemale()
      {
         SaveStat_Insert_Male();
         var stat = context.Stats.ToList().FirstOrDefault(x => x.Scale.SerialNumber == scaleSN && x.Sex == SexE.SEX_FEMALE);
         Assert.IsNull(stat);

         var statMale = Copy(statData);
         statMale.Sex = SexE.SEX_UNDEFINED;
         SaveStat_Insert(statMale);

         stat = context.Stats.ToList().FirstOrDefault(x => x.Scale.SerialNumber == scaleSN && x.Sex == SexE.SEX_FEMALE);
         Assert.IsNotNull(stat);
      }

      private void SaveStat_Insert(Stat stat)
      {
         Assert.IsTrue(context.SaveStat(stat, scaleSN));
         var existingStat = context.Stats.ToList().Where(x => x.Scale.SerialNumber == scaleSN && x.Date.Date == stat.Date.Date && x.Day == stat.Day && x.Sex == stat.Sex);
         Assert.IsNotNull(existingStat);
         Assert.AreEqual(1, existingStat.Count());
         Assert.IsTrue(AreEqual(stat, existingStat.FirstOrDefault()));
      }

      private void SaveStat_Update(Stat newStat)
      {
         var existingStat = context.Stats.ToList().Where(x => x.Scale.SerialNumber == scaleSN && x.Date.Date == statData.Date.Date && x.Day == statData.Day && x.Sex == newStat.Sex);
         Assert.IsNotNull(existingStat);
         Assert.AreEqual(1, existingStat.Count());
         Assert.IsFalse(AreEqual(newStat, existingStat.FirstOrDefault()));

         SaveStat_Insert(newStat);
         Assert.AreEqual(2, context.Stats.Count());
      }


      private Stat Copy(Stat stat)
      {
         return new Stat()
         {
            Average = stat.Average,
            Count = stat.Count,
            Cv = stat.Cv,
            Date = stat.Date,
            Day = stat.Day,
            Gain = stat.Gain,
            Sex = stat.Sex,
            Sigma = stat.Sigma,
            Uni = stat.Uni
         };
      }

      private bool AreEqual(Stat expected, Stat actual)
      {
         if (expected == null && actual == null)
         {
            return true;
         }

         if (expected == null || actual == null)
         {
            return false;
         }

         return Math.Abs(expected.Average - actual.Average) < (decimal) 0.001 &&
                expected.Count == actual.Count &&
                Math.Abs(expected.Cv - actual.Cv) < (decimal) 0.001 &&
                expected.Day == actual.Day &&
                Math.Abs(expected.Gain - actual.Gain) < (decimal) 0.001 &&
                Math.Abs(expected.Sigma - actual.Sigma) < (decimal) 0.001 &&
                Math.Abs(expected.Uni - actual.Uni) < (decimal) 0.001 &&
                expected.Date == actual.Date &&
                expected.Sex == actual.Sex;
      }

   }
}
