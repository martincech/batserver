﻿using System;
using System.IO;
using System.Net;
using System.Windows;
using BatUploader.DataUploadService;
using Microsoft.Win32;

namespace BatUploader
{
   /// <summary>
   /// Interaction logic for MainWindow.xaml
   /// </summary>
   public partial class MainWindow
   {
      public MainWindow()
      {
         InitializeComponent();
         //Trust all certificates
         ServicePointManager.ServerCertificateValidationCallback =
            ((sender, certificate, chain, sslPolicyErrors) => true);
      }

      private void Button_Click_1(object sender, RoutedEventArgs e)
      {
         string result;
         try
         {
            var service = new ProtectedDataUploadClient();
            var fs = File.Open(FileNameTextBox.Text, FileMode.Open);

            StatusInfo.Content = "Import in Progress";

            result = service.LoadFile(Password.Password, UserName.Text, fs);
            fs.Close();

            if (Application.Current.Properties["DeleteFile"] != null && result.StartsWith("Ok."))
            {
               File.Delete(FileNameTextBox.Text);
            }
         }
         catch (Exception exception)
         {
            result = "An error occurred. \n" + exception.Message;
            Console.WriteLine(exception);
         }
         StatusInfo.Content = result;
      }

      private void button1_Click(object sender, RoutedEventArgs e)
      {
         // Create OpenFileDialog
         var dlg = new OpenFileDialog {DefaultExt = ".xml", Filter = "Xml documents (.xml)|*.xml"};

         // Set filter for file extension and default file extension

         // Display OpenFileDialog by calling ShowDialog method
         var result = dlg.ShowDialog();

         // Get the selected file name and display in a TextBox
         if (result == true)
         {
            // Open document
            var filename = dlg.FileName;
            FileNameTextBox.Text = filename;
         }
      }

      private void Window_Loaded_1(object sender, RoutedEventArgs e)
      {
         if (Application.Current.Properties["FileName"] != null)
         {
            var fname = Application.Current.Properties["FileName"].ToString().Trim();

            try
            {
               if (File.Exists(fname))
               {
                  FileNameTextBox.Text = fname;
                  FileNameTextBox.IsEnabled = false;
                  button1.IsEnabled = false;
               }
            }
            catch (Exception exception)
            {
               Console.WriteLine(exception);
            }
         }
      }
   }
}