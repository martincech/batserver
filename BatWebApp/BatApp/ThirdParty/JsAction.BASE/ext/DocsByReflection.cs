﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml;

namespace JsAction.ext
{
   /// <summary>
   /// Utility class to provide documentation for various types where available with the assembly
   /// </summary>
   public class DocsByReflection
   {
      /// <summary>
      /// Provides the documentation comments for a specific method
      /// </summary>
      /// <param name="methodInfo">The MethodInfo (reflection data ) of the member to find documentation for</param>
      /// <returns>The XML fragment describing the method</returns>
      public static XmlElement XmlFromMember(MethodInfo methodInfo)
      {
         // Calculate the parameter string as this is in the member name in the XML
         var parametersString = "";
         foreach (var parameterInfo in methodInfo.GetParameters())
         {
            if (parametersString.Length > 0)
            {
               parametersString += ",";
            }

            //VIN: 19.02.2012 ==> BUG-FIX can't handle generic types specialization.
            string fullname;
            if (parameterInfo.ParameterType.FullName.Contains("`1"))
            {
               var idxStart = parameterInfo.ParameterType.FullName.IndexOf("[[", StringComparison.Ordinal) + 2;
               var idxEnd = parameterInfo.ParameterType.FullName.IndexOf(",", StringComparison.Ordinal);
               fullname =
                  parameterInfo.ParameterType.FullName.Substring(0,
                     parameterInfo.ParameterType.FullName.IndexOf("`1", StringComparison.Ordinal)) + "{";
               fullname += string.Format("{0}}}",
                  parameterInfo.ParameterType.FullName.Substring(idxStart, idxEnd - idxStart));
            }
            else
               fullname = parameterInfo.ParameterType.FullName;
            parametersString += fullname;
         }

         //AL: 15.04.2008 ==> BUG-FIX remove “()” if parametersString is empty
         if (parametersString.Length > 0)
            return XmlFromName(methodInfo.DeclaringType, 'M', methodInfo.Name + "(" + parametersString + ")");
         return XmlFromName(methodInfo.DeclaringType, 'M', methodInfo.Name);
      }

      /// <summary>
      /// Provides the documentation comments for a specific member
      /// </summary>
      /// <param name="memberInfo">The MemberInfo (reflection data) or the member to find documentation for</param>
      /// <returns>The XML fragment describing the member</returns>
      public static XmlElement XmlFromMember(MemberInfo memberInfo)
      {
         // First character [0] of member type is prefix character in the name in the XML
         return XmlFromName(memberInfo.DeclaringType, memberInfo.MemberType.ToString()[0], memberInfo.Name);
      }

      /// <summary>
      /// Provides the documentation comments for a specific type
      /// </summary>
      /// <param name="type">Type to find the documentation for</param>
      /// <returns>The XML fragment that describes the type</returns>
      public static XmlElement XmlFromType(Type type)
      {
         // Prefix in type names is T
         return XmlFromName(type, 'T', "");
      }

      /// <summary>
      /// Obtains the XML Element that describes a reflection element by searching the 
      /// members for a member that has a name that describes the element.
      /// </summary>
      /// <param name="type">The type or parent type, used to fetch the assembly</param>
      /// <param name="prefix">The prefix as seen in the name attribute in the documentation XML</param>
      /// <param name="name">Where relevant, the full name qualifier for the element</param>
      /// <returns>The member that has a name that describes the specified reflection element</returns>
      private static XmlElement XmlFromName(Type type, char prefix, string name)
      {
         string fullName;

         if (String.IsNullOrEmpty(name))
         {
            fullName = prefix + ":" + type.FullName;
         }
         else
         {
            fullName = prefix + ":" + type.FullName + "." + name;
         }

         var xmlDocument = XmlFromAssembly(type.Assembly);

         XmlElement matchedElement = null;

         Debug.Assert(xmlDocument != null, "xmlDocument != null");
         var element = xmlDocument["doc"];
         if (element != null)
         {
            var xmlElements = element["members"];
            if (xmlElements != null)
               foreach (
                  var xmlElement in
                     xmlElements.Cast<XmlElement>()
                        .Where(xmlElement => xmlElement.Attributes["name"].Value.Equals(fullName)))
               {
                  if (matchedElement != null)
                  {
                     throw new DocsByReflectionException("Multiple matches to query", null);
                  }

                  matchedElement = xmlElement;
               }
         }

         if (matchedElement == null)
         {
            throw new DocsByReflectionException("Could not find documentation for specified element", null);
         }

         return matchedElement;
      }

      /// <summary>
      /// A cache used to remember Xml documentation for assemblies
      /// </summary>
      private static readonly Dictionary<Assembly, XmlDocument> Cache = new Dictionary<Assembly, XmlDocument>();

      /// <summary>
      /// A cache used to store failure exceptions for assembly lookups
      /// </summary>
      private static readonly Dictionary<Assembly, Exception> FailCache = new Dictionary<Assembly, Exception>();

      /// <summary>
      /// Obtains the documentation file for the specified assembly
      /// </summary>
      /// <param name="assembly">The assembly to find the XML document for</param>
      /// <returns>The XML document</returns>
      /// <remarks>This version uses a cache to preserve the assemblies, so that 
      /// the XML file is not loaded and parsed on every single lookup</remarks>
      public static XmlDocument XmlFromAssembly(Assembly assembly)
      {
         if (FailCache.ContainsKey(assembly))
         {
            throw FailCache[assembly];
         }

         try
         {
            if (!Cache.ContainsKey(assembly))
            {
               // load the docuemnt into the cache
               Cache[assembly] = XmlFromAssemblyNonCached(assembly);
            }

            return Cache[assembly];
         }
         catch (Exception exception)
         {
            FailCache[assembly] = exception;
            throw;
         }
      }

      /// <summary>
      /// Loads and parses the documentation file for the specified assembly
      /// </summary>
      /// <param name="assembly">The assembly to find the XML document for</param>
      /// <returns>The XML document</returns>
      private static XmlDocument XmlFromAssemblyNonCached(Assembly assembly)
      {
         var assemblyFilename = assembly.CodeBase;

         const string prefix = "file:///";

         if (assemblyFilename.StartsWith(prefix))
         {
            StreamReader streamReader;

            try
            {
               streamReader = new StreamReader(Path.ChangeExtension(assemblyFilename.Substring(prefix.Length), ".xml"));
            }
            catch (FileNotFoundException exception)
            {
               throw new DocsByReflectionException(
                  "XML documentation not present (make sure it is turned on in project properties when building)",
                  exception);
            }

            var xmlDocument = new XmlDocument();
            xmlDocument.Load(streamReader);
            return xmlDocument;
         }
         throw new DocsByReflectionException("Could not ascertain assembly filename", null);
      }
   }

   /// <summary>
   /// An exception thrown by the DocsByReflection library
   /// </summary>
   [Serializable]
   public class DocsByReflectionException : Exception
   {
      /// <summary>
      /// Initializes a new exception instance with the specified
      /// error message and a reference to the inner exception that is the cause of
      /// this exception.
      /// </summary>
      /// <param name="message">The error message that explains the reason for the exception.</param>
      /// <param name="innerException">The exception that is the cause of the current exception, or null if none.</param>
      public DocsByReflectionException(string message, Exception innerException)
         : base(message, innerException)
      {
      }
   }
}