﻿using System;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using BatApp.Helpers;
using BatApp.Models;
using DataModel;
using JsAction;

namespace BatApp.Controllers
{
   [BatApp.Infrastructure.Filters.Authorize]
   public class CurvesController : BaseController
   {
      //
      // GET: /Curves/
      [ActionName("StandardGrowthPage")]
      public ActionResult StandardGrowthPage()
      {
         return View();
      }

      /// <summary>
      /// Create new curve with specified name
      /// </summary>
      /// <param name="curveName"></param>
      /// <returns></returns>
      [JsAction(CacheRequest = false)]
      [HttpPost]
      public JsonResult Create(string curveName)
      {
         if (String.IsNullOrEmpty(curveName))
         {
            return new JsonNetResult
            {
               Data = new
               {
                  error = Resources.Resources.ValueCannotBeNullOrEmpty
               }
            };
         }

         if (Context.Curves.Any(x => x.Name == curveName))
         {
            return new JsonNetResult
            {
               Data = new
               {
                  error = Resources.Resources.NameAlreadyExistsError
               }
            };
         }
         var curve = new Curve { Name = curveName, CompanyId = CurrentUser.CompanyId };
         Context.SaveCurve(curve);
         return Json("success");
      }

      /// <summary>
      /// Edit existing curve
      /// </summary>
      /// <param name="curve"></param>
      /// <returns></returns>
      [JsAction(CacheRequest = false)]
      [HttpPost]
      public JsonResult Edit(Curve curve)
      {
         Context.SaveCurve(curve);
         return Json("success");
      }

      [JsAction(CacheRequest = false)]
      [HttpPost]
      public JsonResult DeleteCurves(int[] ids)
      {
         Context.DeleteCurves(ids);
         return Json("Success");
      }

      [JsAction(CacheRequest = false)]
      [HttpGet]
      public JsonResult GetCurves()
      {

         var configCurves = Context.GetConfigCurveIds();
         var result = from c in Context.Curves
                      where !configCurves.Contains(c.Id)
                      orderby c.Name
                      select new { id = c.Id, name = c.Name };

         return new JsonResult { Data = result.ToArray(), JsonRequestBehavior = JsonRequestBehavior.AllowGet };
      }

      [JsAction(CacheRequest = false)]
      [HttpGet]
      public JsonResult CurveHandler(JQueryDataTablesRequest param)
      {
         if (Context.Curves == null)
         {
            return new JsonNetResult
            {
               Data = new JQueryDataTablesResponse(param.sEcho, 0, 0),
               JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
         }
         var curve = Context.Curves.FirstOrDefault(c => c.Id == param.curveId);
         if (curve == null)
         {
            return new JsonNetResult
            {
               Data = new JQueryDataTablesResponse(param.sEcho, 0, 0),
               JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
         }

         var curveData = curve.CurveValues.OrderBy(c => c.Day).
            Select(c =>
               new[]
               {
                  Convert.ToString(c.Id),
                  Convert.ToString(c.Day),
                  Convert.ToString(c.Weight, CultureInfo.InvariantCulture)
               }).ToArray();

         return new JsonNetResult
         {
            Data = new JQueryDataTablesResponse(param.sEcho, curveData.Count(), curveData.Count(), curveData),
            JsonRequestBehavior = JsonRequestBehavior.AllowGet
         };
      }

      [JsAction(CacheRequest = false)]
      [HttpPost]
      public int AddData(int? curveId, string weight, int? day)
      {
         if (curveId == null || string.IsNullOrEmpty(weight) || day == null)
         {
            Response.Write("Enter valid values");
            Response.StatusCode = 404;
            Response.End();
            return -1;
         }

         if (Context.Curves.First(x => x.Id == curveId).CurveValues.Any(x => x.Day == day))
         {
            Response.Write("Day'" + day + "' already exists");
            Response.StatusCode = 404;
            Response.End();
            return -1;
         }

         var cv = new CurveValue
         {
            Day = (int)day,
            Weight = Decimal.Parse(weight.Replace(",", "."), CultureInfo.InvariantCulture)
         };
         Context.Curves.First(x => x.Id == curveId).CurveValues.Add(cv);
         Context.SaveChanges();

         return 1;
      }

      [JsAction(CacheRequest = false)]
      [HttpPost]
      public JsonResult DeleteData(int id)
      {
         Context.DeleteCurvesData(id);
         return Json("Success");
      }

      [HttpPost]
      public string UpdateData(CurveEntryUpdate updateEntry)
      {
         if (updateEntry.Id == 0) return updateEntry.Value;

         var cv = Context.CurveValues.FirstOrDefault(c => c.Id == updateEntry.Id);
         if (cv == null)
         {
            return updateEntry.Value;
         }
         switch (updateEntry.ColumnId)
         {
            case 1:
               cv.Day = int.Parse(updateEntry.Value);
               break;
            case 2:
               {
                  var value = updateEntry.Value.Replace(",", ".");
                  cv.Weight = Decimal.Parse(value, CultureInfo.InvariantCulture);
               }
               break;
            default:
               return updateEntry.Value;
         }

         Context.SaveChanges();

         return updateEntry.Value;
      }
   }

   public class CurveEntryUpdate
   {
      public int Id { get; set; }
      public string Value { get; set; }
      public int ColumnId { get; set; }
   }
}