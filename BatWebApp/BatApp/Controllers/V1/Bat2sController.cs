﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Mvc;
using DataModel;

namespace BatApp.Controllers.V1
{
   /// <summary>
   /// Bat2 controller
   /// </summary>
   public class Bat2sController : BaseApiController
   {
      /// <summary>
      /// Get all scales
      /// </summary>
      /// <returns>list of scales</returns>
      public IEnumerable<Scale> Get()
      {
         return Context.Scales;
      }

      /// <summary>
      /// Get selected scale.
      /// </summary>
      /// <param name="id">scale id</param>
      /// <returns>scale</returns>
      public Scale Get(int id)
      {
         return Context.Scales.FirstOrDefault(s => s.Id == id);
      }

      /// <summary>
      /// Create new scale
      /// </summary>
      /// <param name="scale">new scale</param>
      /// <returns></returns>
      public Scale Post(Scale scale)
      {
         var scaleName = scale.Name ?? "";
         return Context.CreateScale(scale.SerialNumber, scaleName);
      }

      /// <summary>
      /// Create new scale
      /// </summary>
      /// <param name="serialNumber">serial number</param>
      /// <param name="scaleName">scale name. Parameter is optional.</param>
      /// <param name="farmName">farm name. Parameter is optional.</param>
      /// <param name="phoneNumber">phone number. Parameter is optional.</param>
      /// <returns></returns>
      public HttpStatusCodeResult Post(int serialNumber, string scaleName = null, string phoneNumber = null, string farmName = null)
      {
         scaleName = scaleName ?? "";
         phoneNumber = phoneNumber ?? "";
         farmName = farmName ?? "";
         return CreateScale(serialNumber, scaleName, phoneNumber, farmName);
      }

      /// <summary>
      /// Update scale
      /// </summary>
      /// <param name="id">scale id</param>
      /// <param name="scale">scale</param>
      /// <returns></returns>
      public HttpStatusCodeResult Put(int id, Scale scale)
      {
         if (!Context.UpdateScale(scale, id))
         {
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Scale wasn't updated");
         }
         var json = Newtonsoft.Json.JsonConvert.SerializeObject(scale);
         return new HttpStatusCodeResult(HttpStatusCode.OK, json);
      }

      /// <summary>
      /// Delete scale
      /// </summary>
      /// <param name="id">scale id</param>
      /// <returns></returns>
      public HttpStatusCodeResult Delete(int id)
      {
         if (!Context.DeleteScale(id))
         {
            return new HttpStatusCodeResult(HttpStatusCode.Accepted, "Delete wasn't success because scale doesn't exist");
         }
         return new HttpStatusCodeResult(HttpStatusCode.OK, "Scale was deleted");
      }

      /// <summary>
      /// Delete selected scales
      /// </summary>
      /// <param name="ids">list of scales id</param>
      /// <returns></returns>
      public HttpStatusCodeResult Delete(int[] ids)
      {
         var notDeleted = ids.Where(id => !Context.DeleteScale(id)).ToList();

         var status = HttpStatusCode.OK;
         var msg = "Scales was deleted";
         if (notDeleted.Any())
         {
            status = HttpStatusCode.Accepted;
            msg = "Some scale wasn't delete because it isn't exist. Their id are: " + string.Join(",", notDeleted);
         }

         return new HttpStatusCodeResult(status, msg);
      }

      #region Private helpers

      private HttpStatusCodeResult CreateScale(int serialNumber, string scaleName, string phoneNumber, string farmName)
      {
         var newScale = Context.CreateScale(serialNumber, scaleName, phoneNumber, farmName);
         if (newScale == null)
         {
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Scale wasn't created");
         }
         var json = Newtonsoft.Json.JsonConvert.SerializeObject(newScale);
         return new HttpStatusCodeResult(HttpStatusCode.OK, json);
      }

      #endregion
   }
}
