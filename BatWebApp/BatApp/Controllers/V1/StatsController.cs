﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Web.Mvc;
using BatApp.Services.DataPublication;
using BatWebSynchronizer.Synchronizer;
using DataModel;

namespace BatApp.Controllers.V1
{
   /// <summary>
   /// Statistic controller
   /// </summary>
   public class StatsController : BaseApiController
   {
      /// <summary>
      /// Get all statistics.
      /// </summary>
      /// <param name="key">scale id</param>
      /// <returns>list of statistics</returns>
      public IEnumerable<Stat> Get(int key)
      {
         return Context.Stats.Where(u => u.ScaleId == key);
      }

      /// <summary>
      /// Get choosen statistic.
      /// </summary>
      /// <param name="key">scale id</param>
      /// <param name="id">statistic id</param>
      /// <returns>statistic</returns>
      public Stat Get(int key, int id)
      {
         return Context.Stats.FirstOrDefault(u => u.ScaleId == key && u.Id == id);
      }

      /// <summary>
      /// Saved statistic. Expected sms data at message body.
      /// Restriction: only company admin can synchronized data.
      /// </summary>    
      /// <returns></returns>
      public HttpStatusCodeResult Post(StatisticData smsData)
      {      
         if (!Thread.CurrentPrincipal.IsInRole(Constants.COMPANY_ADMIN))
         {
            return new HttpStatusCodeResult(HttpStatusCode.Forbidden, "Only company admin have access");
         }

         var saver = new StatisticsSaver(Context);
         if (!saver.ParseAndSave(new SmsDataParser(), smsData))
         {
            return new HttpStatusCodeResult(HttpStatusCode.NotAcceptable, "Data wasn't saved");
         }

         return new HttpStatusCodeResult(HttpStatusCode.OK, "Data was saved successfully");
      }
   }
}
