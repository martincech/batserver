﻿using System.Threading;
using DataModel;

namespace BatApp.Controllers.V1
{
    public class AccountRolesController : BaseApiController
    {
       /// <summary>
       /// Return current user role
       /// </summary>
       /// <returns></returns>
       public string Get()
       {
          if (Thread.CurrentPrincipal.IsInRole(Constants.ADMIN))
          {
             return "admin";
          }
          if (Thread.CurrentPrincipal.IsInRole(Constants.COMPANY_ADMIN))
          {
             return "company admin";
          }
          return "user";
       }
    }
}
