﻿using System;
using System.CodeDom;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Mvc;
using Bat2Library;
using Bat2Library.Utilities;
using BatApp.Helpers;
using BatApp.Models;
using BatLibrary;
using DataContext;
using DataModel;
using JsAction;

namespace BatApp.Controllers
{
   [Infrastructure.Filters.Authorize]
   public class WeighingsController : BaseController
   {
      private const string UNIT_KG = "kg";
      private const string UNIT_LBS = "lbs";

      private const string FILE_CSV = ".csv";
      private const string FILE_XLS = ".xls";
    
      //
      // GET: /Weighings/
      public ActionResult WeighingsPage()
      {
         return View();
      }

      [HandleError(ExceptionType = typeof(HttpException), View = "Error")]
      public ActionResult Error(int id)
      {
         return View();
      }

      [JsAction(CacheRequest = false)]
      [HttpGet]
      public JsonResult WeighingsHandler(JQueryDataTablesRequest param)
      {
         int totalRecords;

         var stats = Context.GetAllStats(param.ToFilterModel(), ParseRequest(), out totalRecords);
        
         return new JsonNetResult
         {
            Data = new JQueryDataTablesResponse(param.sEcho, totalRecords, totalRecords, stats),
            JsonRequestBehavior = JsonRequestBehavior.AllowGet
         };
      }

      /// <summary>
      /// Delete all statistics as specified by their ids
      /// </summary>
      /// <param name="ids">ids of statistics to delete</param>
      /// <returns></returns>
      [JsAction(CacheRequest = false)]
      [HttpPost]
      [Infrastructure.Filters.Authorize(Roles = Constants.ADMIN + ", " + Constants.COMPANY_ADMIN + ", " + Constants.FLOCK_MANAGEMENT)]
      public JsonResult DeleteStats(int[] ids)
      {
         Context.DeleteStats(ids);
         return Json("Success");
      }

      [HttpPost]
      public ActionResult ImportStats()
      {
         if (Request.Files.Count == 0)
         {
            return Json(Resources.Resources.ImportFileUploadFailed);
         }

         //Load data
         Dictionary<string, byte[]> files = new Dictionary<string, byte[]>();
         int i = 1;
         foreach (string file in Request.Files)
         {
            HttpPostedFileBase hpf = Request.Files[file];
            if (hpf == null)
            {
               continue;
            }
            byte[] bufferData = new byte[hpf.InputStream.Length];
            hpf.InputStream.Read(bufferData, 0, bufferData.Length);
            files.Add(i + " : " + hpf.FileName, bufferData);
            i++;
         }

         //Check if files are same
         if (files.Count == 2)
         {
            if (files.First().Value.SequenceEqual(files.Last().Value))
            {
               Response.StatusCode = (int) HttpStatusCode.BadRequest;
               return Json(Resources.Resources.ImportErrSameFile);
            }
         }

         //Get uploaded files unit
         var unit = Weight.WeightUnits.KG;
         if (Request.Form.Get("unit") == UNIT_LBS)
         {
            unit = Weight.WeightUnits.LB;
         }

         Dictionary<string, List<Report>> reports = new Dictionary<string, List<Report>>();
         int totalRecords = 0;
         foreach (var file in files)
         {
            List<Report> reportList = null;
            //parse csv files
            if (file.Key.ToLower().Contains(FILE_CSV))
            {
               var parser = new CsvParser(file.Value);
               var data = parser.Parse();
               totalRecords += parser.FoundRecords;
               if (data != null)
                  reportList = data.ToList();
            }
            //parse excel files
            if (file.Key.ToLower().Contains(FILE_XLS))
            {
               var parser = new ExcelParser(file.Value);
               var data = parser.Parse();
               totalRecords += parser.FoundRecords;
               if (data != null)
                  reportList = data.ToList();
            }

            //No records in correct format found 
            if (reportList == null || reportList.Count == 0)
            {
               Response.StatusCode = (int) HttpStatusCode.BadRequest;
               return Json(string.Format(Resources.Resources.ImportErrEmpty, file.Key));
            }
            reports.Add(file.Key, reportList);
         }

         //check if user didnt swap the files
         if (reports.Count == 2 && reports.First().Value.First().Sex == SexE.SEX_MALE && reports.Last().Value.Last().Sex == SexE.SEX_UNDEFINED)
         {
            var newReports = reports.ToList();
            reports = new Dictionary<string, List<Report>>();
            reports.Add(newReports.Last().Key, newReports.Last().Value);
            reports.Add(newReports.First().Key, newReports.First().Value);
         }

         int numberOfRecors = 0;
       
         bool isMale = false;

         //Save report to DB
         foreach (var report in reports)
         {
            foreach (var reportData in report.Value)
            {
               //Second file always contains males
               if (isMale)
               {
                  reportData.Sex = SexE.SEX_MALE;
               }
               int scaleSN;
               if (!int.TryParse(reportData.ScaleName, out scaleSN))
               {
                  continue;
               }
               if (Context.SaveStat(ReportToStat.GetStat(reportData, unit), scaleSN))
               {
                  numberOfRecors++;
               }
            }
            isMale = true;
         }

         Response.StatusCode = (int) HttpStatusCode.OK;
         return Json(new {Imported = numberOfRecors, NotImported = totalRecords - numberOfRecors });
      }

   }
}