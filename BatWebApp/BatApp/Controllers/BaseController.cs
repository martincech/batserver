using System;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using BatApp.Helpers;
using DataContext;
using DataModel;
using Microsoft.AspNet.Identity;
using IdentityExtensions = BatApp.Areas.UserAccounts.Helpers.IdentityExtensions;

namespace BatApp.Controllers
{
   public abstract class BaseController : Controller
   {
      private BatContext context;
      protected HttpCookie CultureCookie;

      protected BatContext Context
      {
         get { return context ?? (context = new BatContext(CurrentUserId)); }
      }

      protected Guid CurrentUserId
      {
         get { return IdentityExtensions.GetUserId(User.Identity); }
      }

      protected User CurrentUser
      {
         get { return Context.Users.First(u => u.Id == CurrentUserId); }
      }

      protected void AddErrors(IdentityResult result)
      {
         foreach (var error in result.Errors)
         {
            ModelState.AddModelError("", error);
         }
      }

      protected SearchModel ParseRequest()
      {
         var info = new SearchModel();
         if (string.IsNullOrEmpty(Request["iColumns"])) return info;

         info.NumOfColumns = int.Parse(Request["iColumns"]);

         for (var i = 0; i < info.NumOfColumns; i++)
         {
            var ci = new SearchModel.ColumnInfo
            {
               Name = Request["mDataProp_" + i.ToString(CultureInfo.InvariantCulture)],
               IsSearchable = bool.Parse(Request["bSearchable_" + i.ToString(CultureInfo.InvariantCulture)])
            };

            info.Infos.Add(ci);
         }

         if (!string.IsNullOrEmpty(Request["iSortingCols"]))
         {
            info.NumOfSorts = int.Parse(Request["iSortingCols"]);

            for (var i = 0; i < info.NumOfSorts; i++)
            {
               var columnIndex = int.Parse(Request["iSortCol_" + i.ToString(CultureInfo.InvariantCulture)]);
               var order = Request["sSortDir_" + i.ToString(CultureInfo.InvariantCulture)];

               var ci = info.Infos[columnIndex];
               ci.SortOrder = i + 1;
               ci.SortDir = order;
            }
         }

         info.SearchText = Request["sSearch"];

         return info;
      }

      /// <summary>
      /// Begins to invoke the action in the current controller context.
      /// </summary>
      /// <returns>
      /// Returns an IAsyncController instance.
      /// </returns>
      /// <param name="callback">The callback.</param><param name="state">The state.</param>
      protected override IAsyncResult BeginExecuteCore(AsyncCallback callback, object state)
      {
         SetCulture();
         return base.BeginExecuteCore(callback, state);
      }

      protected override void ExecuteCore()
      {
         SetCulture();
         base.ExecuteCore();
      }

      private void SetCulture()
      {
        
         string cultureName = null;
         // Attempt to read the culture cookie from Request
         CultureCookie = Request.Cookies["_culture"];
         if (CultureCookie != null)
         {
            cultureName = CultureCookie.Value;
         }
         else
         {
            if (Request.UserLanguages != null && Request.UserLanguages.Any())
            {
               cultureName = Request.UserLanguages[0]; // obtain it from HTTP header AcceptLanguages
            }
         }

         // Validate culture name
         cultureName = CultureHelper.GetImplementedCulture(cultureName);
         if (CultureCookie == null)
         {
            HttpCookie myCookie = new HttpCookie("_culture");
            myCookie.Value = cultureName;
            myCookie.Expires = DateTime.Now.AddDays(360);
            Response.Cookies.Add(myCookie);
         }
         // Modify current thread's cultures            
         Thread.CurrentThread.CurrentCulture = new CultureInfo(cultureName);
         Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;
      }
   }
}