﻿using System;
using System.Net;
using System.Web.Mvc;
using BatApp.Infrastructure.Filters;
using DataModel;
using System.IO;
using Bat2Library;
using BatApp.Services.DataPublication;
using System.Text;

namespace BatApp.Controllers
{

   [IdentityBasicAuthentication]
   //[BatApp.Infrastructure.Filters.AuthorizeService(Roles = Constants.CAN_IMPORT_DATA)]
   [BatApp.Infrastructure.Filters.Authorize(Roles = Constants.ADMIN + ", " + Constants.COMPANY_ADMIN + ", "+Constants.FLOCK_MANAGEMENT, NoRedirect = true)]
   public class DataPostController : BaseController
   {
      // GET: /DataPost/Bat2/version
      [HttpGet]
      public HttpStatusCodeResult Bat2(int? version)
      {
         return new HttpStatusCodeResult(HttpStatusCode.OK);
      }

      // POST: /DataPost/Bat2/version
      [HttpPost, ActionName("Bat2")]
      public HttpStatusCodeResult Bat2Post(int? version)
      {
         byte[] data = new byte[Request.InputStream.Length];
         Request.InputStream.Position = 0;
         Request.InputStream.Read(data,0,data.Length);

         StatisticsSaver saver = new StatisticsSaver(Context);
         
         if (!saver.ParseAndSave(new BinaryDataParser(),data))
         {
            return new HttpStatusCodeResult(HttpStatusCode.NotAcceptable);
         }
         return new HttpStatusCodeResult(HttpStatusCode.OK);
      }

       // POST: /DataPost/Bat2Sms
      [HttpPost, ActionName("Bat2Sms")]
      public HttpStatusCodeResult Bat2SmsPost()
      {
          byte[] data = new byte[Request.InputStream.Length];
          Request.InputStream.Position = 0;
          Request.InputStream.Read(data, 0, data.Length);
          string message = Encoding.UTF8.GetString(data, 0, data.Length);

          StatisticsSaver saver = new StatisticsSaver(Context);
          if (!saver.ParseAndSave(new StringDataParser(),message)) 
          {
              return new HttpStatusCodeResult(HttpStatusCode.NotAcceptable);
          }
          return new HttpStatusCodeResult(HttpStatusCode.OK);
      }

     
   }
}