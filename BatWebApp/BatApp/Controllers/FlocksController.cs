﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;
using Bat2Library;
using BatApp.Helpers;
using BatApp.Models;
using DataContext;
using DataModel;
using JsAction;

namespace BatApp.Controllers
{
   /// <summary>
   /// This is controller for flocks
   /// </summary>
   [Infrastructure.Filters.Authorize]
   public class FlocksController : BaseController
   {
      [ActionName("ManagementPage")]
      [Authorize(Roles = Constants.ADMIN + ", " + Constants.COMPANY_ADMIN + ", " + Constants.FLOCK_MANAGEMENT)]
      public ActionResult ManagementPage()
      {
         return View();
      }

      [ActionName("ActualPage")]
      public ActionResult ActualPage()
      {
         return View();
      }

      [ActionName("ClosedPage")]
      public ActionResult ClosedPage()
      {
         return View();
      }

      #region ManagementPage json methods

      /// <summary>
      /// Create new <see cref="Flock"/>
      /// </summary>
      /// <param name="flock">flock to be created</param>
      /// <returns></returns>
      [JsAction(CacheRequest = false)]
      [HttpPost]
      public JsonResult Create(Flock flock)
      {
         if (flock == null || string.IsNullOrEmpty(flock.Name))
         {
            return new JsonNetResult
            {
               Data = new
               {
                  error = Resources.Resources.FlockNameMandatory
               }
            };
         }

         if (!Context.CanCreateFlock(flock))
         {
            return new JsonNetResult
            {
               Data = new
               {
                  error = Resources.Resources.FlockAlreadyExistsError
               }
            };
         }
         if (!String.IsNullOrEmpty(flock.NewScale))
         {
            flock.ScaleId = Context.CreateScale(flock.NewScale);
         }
         Context.SaveFlock(flock);
         return new JsonNetResult
         {
            Data = new
            {
            }
         };
      }

      /// <summary>
      /// Edit existing <see cref="Flock"/>
      /// </summary>
      /// <param name="flock"></param>
      /// <returns></returns>
      [JsAction(CacheRequest = false)]
      [HttpPost]
      public JsonResult Edit(Flock flock)
      {
         if (flock.Name.Equals(string.Empty))
         {
            return new JsonNetResult
            {
               Data = new
               {
                  error = Resources.Resources.FlockNameMandatory
               }
            };
         }
         if (!Context.CanUpdateFlock(flock))
         {
            return new JsonNetResult
            {
               Data = new
               {
                  error = Resources.Resources.FlockAlreadyExistsError
               }
            };
         }

         // if flock can't be created, but can be updated,
         // flock don't have any change and we don't save it
         if (Context.CanCreateFlock(flock))
         {
            Context.SaveFlock(flock);   
         }
         return new JsonNetResult
         {
            Data = new
            {
            }
         };
      }

      /// <summary>
      /// Delete all flocks as specified by their ids
      /// </summary>
      /// <param name="ids">ids of flocks to delete</param>
      /// <returns></returns>
      [JsAction(CacheRequest = false)]
      [HttpPost]
      public JsonResult DeleteFlocks(int[] ids)
      {
         Context.DeleteFlocks(ids);
         return Json("Success");
      }

      /// <summary>
      /// Close flocks, aka set closing date to specific date.
      /// </summary>
      /// <param name="ids">ids of flocks to close</param>
      /// <param name="closedDate">closed date of flocks</param>
      /// <returns>status result</returns>
      [JsAction(CacheRequest = false)]
      [HttpPost]
      public JsonResult CloseFlocks(int[] ids, DateTime closedDate)
      {
         Context.CloseFlocks(ids, closedDate);
         return Json("Success");
      }

      /// <summary>
      /// Close flocks, aka set closing date to null value.
      /// </summary>
      /// <param name="ids">ids of flocks to reopen</param>
      /// <returns>status result</returns>
      [JsAction(CacheRequest = false)]
      [HttpPost]
      public JsonResult ReopenFlocks(int[] ids)
      {
         Context.ReopenFlocks(ids);
         return Json("Success");
      }

      /// <summary>
      /// Get all existing flocks, opened and closed.
      /// </summary>
      /// <returns>Flock object with name and id properties.</returns>
      [JsAction(CacheRequest = false)]
      [HttpGet]
      public JsonResult GetAllFlocks()
      {
         var flocks = Context.Flocks.Select(x => new { name = x.Name, id = x.Id }).OrderBy(x => x.name);
         return new JsonNetResult { Data = flocks.ToArray(), JsonRequestBehavior = JsonRequestBehavior.AllowGet };
      }

      /// <summary>
      /// Gets all opened flocks.
      /// </summary>
      /// <returns>Flock object with name and id properties.</returns>
      [JsAction(CacheRequest = false)]
      [HttpGet]
      public JsonResult GetOpenedFlocks()
      {
         var flocks = Context.GetOpenFlocks().Select(x => new { name = x.Name, id = x.Id }).OrderBy(x => x.name);
         return new JsonNetResult { Data = flocks.ToArray(), JsonRequestBehavior = JsonRequestBehavior.AllowGet };
      }

      /// <summary>
      /// Gets flocks names by their ids.
      /// </summary>
      /// <param name="ids">ids of flocks to get names for</param>
      /// <returns>Flock names - objects with name property</returns>
      [JsAction(CacheRequest = false)]
      [HttpGet]
      public JsonResult GetFlockNameById(int[] ids)
      {
         var selectedFlocks = CurrentUser.SelectedFlocks.Select(x => x.Id);
         var flocks = Context.Flocks.Where(x => ids.Contains(x.Id)).OrderBy(x => x.Name).Select(x => new { name = x.Name, selected = selectedFlocks.Contains(x.Id) });
         return new JsonNetResult { Data = flocks.ToArray(), JsonRequestBehavior = JsonRequestBehavior.AllowGet };
      }

      /// <summary>
      /// Get single flock as specified by id.
      /// </summary>
      /// <param name="id">id of flock to be loaded.</param>
      /// <returns><see cref="Flock"/> object which fully describes a single flock</returns>
      [JsAction(CacheRequest = false)]
      [HttpGet]
      public JsonResult GetFlock(int id)
      {
         return new JsonNetResult { Data = Context.GetFlock(id), JsonRequestBehavior = JsonRequestBehavior.AllowGet };
      }

      #endregion

      [JsAction(CacheRequest = false)]
      [HttpGet]
      public JsonResult GetScales()
      {
         var scales = Context.Scales.OrderBy(x => x.Name).ToArray();
         return new JsonNetResult { Data = scales, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
      }

      [JsAction(CacheRequest = false)]
      [HttpGet]
      public JsonResult GetCurves()
      {
         var curves = Context.Curves.ToArray();
         return new JsonNetResult { Data = curves, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
      }

      [JsAction(CacheRequest = false)]
      [HttpGet]
      public JsonResult StatisticsHandler(JQueryDataTablesRequest param, int? growthCurveId)
      {
         int totalRecords;

         if (HttpContext.Request.QueryString["selectedFlocks"].Length > 0)
         {
            try
            {
               var data = HttpContext.Request.QueryString["selectedFlocks"].Split(',');
               param.selectedFlocks = data.Select(n => Convert.ToInt32(n)).ToArray();
            }
            catch (Exception e) { }
         }
         var stats = Context.GetStats(param.ToFilterModel(), ParseRequest(), out totalRecords, selectedCurveId: growthCurveId);

         return new JsonNetResult
         {
            Data = new JQueryDataTablesResponse(param.sEcho, totalRecords, totalRecords, stats),
            JsonRequestBehavior = JsonRequestBehavior.AllowGet
         };
      }

      private double GetStat(Stat stat, PropertyInfo prop)
      {
         return (double)prop.GetValue(stat, null);
      }

      [JsAction(CacheRequest = false)]
      [HttpGet]
      public JsonResult GetGraphData(int[] flockIds, int? flockId, string graphType, string graphType2, string sex, DateTime? from, DateTime? to)
      {
         if (flockIds == null)
         {
            return new JsonNetResult
            {
               Data = null,
               JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
         }

         var selected = CurrentUser.SelectedFlocks.Select(sc => sc.Id).ToList();
         if (flockId != null)
         {
            flockIds = new[] { (int)flockId };
            selected.Clear();
            selected.Add((int)flockId);
         }

         int xMaxValue = int.MinValue;
         int xMinValue = int.MaxValue;

         double yMinValue = double.MaxValue;
         double yMaxValue = double.MinValue;

         double yMinValue2 = double.MaxValue;
         double yMaxValue2 = double.MinValue;

         if (graphType == null)
         {
            graphType = "Average";
         }
         var stat = typeof(Stat).GetProperty(graphType);
         var xNameValue = Resources.Resources.Days;
         var yNameValue = Resources.Resources.ResourceManager.GetString(graphType + "MenuItem");

         PropertyInfo stat2 = null;
         var yNameValue2 = "";
         if (!string.IsNullOrEmpty(graphType2))
         {
            stat2 = typeof(Stat).GetProperty(graphType2);
            yNameValue2 = Resources.Resources.ResourceManager.GetString(graphType2 + "MenuItem");
         }

         var rawList = Context.Flocks.Where(f => flockIds.Contains(f.Id)).ToList();
         var sexE = SexE.SEX_UNDEFINED;
         if (sex != null)
         {
            sexE = sex == Resources.Resources.SexMale
               ? SexE.SEX_MALE
               : sex == Resources.Resources.SexFemale ? SexE.SEX_FEMALE : SexE.SEX_UNDEFINED;

            if (sexE != SexE.SEX_UNDEFINED)
            {
               rawList = rawList.Where(f => f.Scale.Stat.Any(s => s.Sex == sexE)).ToList();
            }
         }

         Func<SexE, Flock, GraphHelper> getFlocks = new Func<SexE, Flock, GraphHelper>((sexValue, f) =>
         {
            return new GraphHelper()
            {
               id = f.Id,
               name = f.Name,
               isGrowthCurve = false,
               active = selected.Contains(f.Id),
               sex = sexValue == SexE.SEX_UNDEFINED ? "" : sexValue == SexE.SEX_MALE ? Resources.Resources.SexMale : Resources.Resources.SexFemale,
               data = f.Scale.Stat.Where(w => w.Sex == sexValue).Select(s =>
               {
                  var obVal = stat.GetValue(s, null);
                  object obVal2 = null;
                  if (stat2 != null)
                  {
                     obVal2 = stat2.GetValue(s, null);
                  }
                 
                  if (obVal == null && obVal2 == null)
                  {
                     return null;
                  }

                  if (f.StartDate.CompareTo(s.Date) > 0)
                  {
                     return null;
                  }

                  if (f.EndDate != null && ((DateTime)f.EndDate).CompareTo(s.Date) < 0)
                  {
                     return null;
                  }

                  //Filter 
                  if (from != null)
                  {
                     if (DateTime.Compare(s.Date, (DateTime)from) < 0)
                     {
                        return null;
                     }
                  }

                  if (to != null)
                  {
                     if (DateTime.Compare((DateTime)to, s.Date) < 0)
                     {
                        return null;
                     }
                  }

                  decimal? value = null;
                  if (obVal != null)
                  {
                     if (obVal is int?)
                     {
                        value = (decimal) (int) obVal;
                     }
                     else
                     {
                        value = (decimal) obVal;
                     }
                  }


                  decimal? value2 = null;
                  if (obVal2 != null)
                  {
                     if (obVal2 is int?)
                     {
                        value2 = (int) obVal2;
                     }
                     else
                     {
                        value2 = (decimal) obVal2;
                     }
                  }

                  return new PoinHelper
                  {
                     id = s.Id,
                     x = s.Day,
                     y = value,
                     y2 = value2,
                     date = s.Date
                  };
               }).Where(m => m != null).OrderBy(o=>o.x).ToList()
            };
         });

         List<GraphHelper> graphsData = new List<GraphHelper>();

         if (sexE != SexE.SEX_FEMALE)
         {
            //add male
            graphsData.AddRange(rawList.Select(f => getFlocks(SexE.SEX_MALE, f)).Where(w => w.data.Count != 0).ToList());
         }
         if (sexE != SexE.SEX_MALE)
         {
            //add female
            graphsData.AddRange(rawList.Select(f => getFlocks(SexE.SEX_FEMALE, f)).Where(w => w.data.Count != 0).ToList());
         }
         if (sexE != SexE.SEX_MALE && sexE != SexE.SEX_FEMALE)
         {
            //add undefined
            graphsData.AddRange(rawList.Select(f => getFlocks(SexE.SEX_UNDEFINED, f)).Where(w => w.data.Count != 0).ToList());
         }

         graphsData = graphsData.OrderBy(p => p.name).ToList();

         foreach (var gData in graphsData)
         {
            var last = gData.data.Where(g => g.date.HasValue).OrderBy(o => o.date).Last();
            var s = Context.Stats.First(x => x.Id == last.id);

            var date = new DateTime(DateTime.Now.Year,DateTime.Now.Month,DateTime.Now.Day,0,0,0).AddDays(-1);

            gData.stat = new StatsHelper()
            {
               ammonia = s.Ammonia,
               average = s.Average,
               carbonDioxide = s.CarbonDioxide,
               count = s.Count,
               cv = s.Cv,
               date = s.Date,
               day = s.Day,
               gain = s.Gain,
               humidity = s.Humidity,
               temperature = s.Temperature,
               sigma = s.Sigma,
               uni = s.Uni,
               isUpdated = date <= s.Date
            };
         }

         var configCurves = Context.GetConfigCurveIds();
         if (graphType == "Average")
         {
            foreach (var curve in Context.Curves.Where(w => !configCurves.Contains(w.Id)))
            {
               graphsData.Add(new GraphHelper
               {
                  id = curve.Id,
                  name = curve.Name,
                  isGrowthCurve = true,
                  active = selected.Contains(curve.Id),
                  sex = "",
                  data = curve.CurveValues.ToList().OrderBy(o => o.Day).Select((CurveValue s) => new PoinHelper { x = s.Day, y = s.Weight }).ToList()
               });
            }
         }

         var xVals = graphsData.SelectMany(x => x.data.Select(y => y.x));
         var yVals = graphsData.SelectMany(x => x.data.Select(y => y.y)).Where(w => w.HasValue);
         var yVals2 = graphsData.SelectMany(x => x.data.Select(y => y.y2)).Where(w=>w.HasValue);
         if (!xVals.Any())
         {
            xMaxValue = 1;
            xMinValue = 0;
         }
         else
         {
            xMaxValue = xVals.Max();
            xMinValue = xVals.Min();
         }

         if (!yVals.Any())
         {
            yMaxValue = 1;
            yMinValue = 0;
         }
         else
         {
            yMaxValue = (double)yVals.Max();
            yMinValue = (double)yVals.Min();
         }

         if (!yVals2.Any())
         {
            yMaxValue2 = 1;
            yMinValue2 = 0;
         }
         else
         {
            yMaxValue2 = (double)yVals2.Max();
            yMinValue2 = (double)yVals2.Min();
         }


         var data = new
         {
            xMax = xMaxValue + xMaxValue / 25,
            xMin = xMinValue - xMaxValue / 25,
            yMax = yMaxValue + yMaxValue / 25,
            yMin = yMinValue - yMaxValue / 25,
            yMax2 = yMaxValue2 + yMaxValue2 / 25,
            yMin2 = yMinValue2 - yMaxValue2 / 25,
            xName = xNameValue,
            yName = yNameValue,
            yName2 = yNameValue2,
            data = graphsData.ToArray()
         };

         //stat.GetValue(cv, null)
         return new JsonNetResult
         {
            Data = data,
            JsonRequestBehavior = JsonRequestBehavior.AllowGet
         };
      }

      private string GetFlockName(StatFlock stat)
      {
         var sex = "";
         if (stat.Stat.Sex == SexE.SEX_MALE)
         {
            sex = Resources.Resources.SexMale;
         }
         if (stat.Stat.Sex == SexE.SEX_FEMALE)
         {
            sex = Resources.Resources.SexFemale;
         }
         return stat.Stat.Sex == SexE.SEX_UNDEFINED ? stat.Flock.Name : stat.Flock.Name + " - " + sex;
      }

      [JsAction(CacheRequest = false)]
      [HttpGet]
      public JsonResult ActualFlocksHandler(JQueryDataTablesRequest param)
      {
         int totalRecords;

         var displayedFlocks = Context.GetOpenFlocks(param.ToFilterModel(), ParseRequest(), out totalRecords);
         Context.UpdateFlocksMaxDay(displayedFlocks);

         return new JsonNetResult
         {
            Data = new JQueryDataTablesResponse(param.sEcho, totalRecords, totalRecords, displayedFlocks),
            JsonRequestBehavior = JsonRequestBehavior.AllowGet
         };
      }

      [JsAction(CacheRequest = false)]
      [HttpGet]
      public JsonResult ClosedFlocksHandler(JQueryDataTablesRequest param)
      {
         int totalRecords;
         var displayedFlocks = Context.GetClosedFlocks(param.ToFilterModel(), ParseRequest(), out totalRecords);

         return new JsonNetResult
         {
            Data = new JQueryDataTablesResponse(param.sEcho, totalRecords, totalRecords, displayedFlocks),
            JsonRequestBehavior = JsonRequestBehavior.AllowGet
         };
      }


      [JsAction(CacheRequest = false)]
      [HttpPost]
      public JsonResult SaveUserSelectedFlocks(int[] flockIds)
      {
         var selFlocks = CurrentUser.SelectedFlocks;
         selFlocks.Clear();
         if (flockIds != null && flockIds.Length > 0)
         {
            foreach (
               var flock in
                  flockIds.Select(flockId => Context.Flocks.FirstOrDefault(f => f.Id == flockId))
                     .Where(flock => flock != null))
            {
               selFlocks.Add(flock);
            }
         }
         Context.SaveChanges();
         return Json("Success");
      }

      [JsAction(CacheRequest = false)]
      [HttpPost]
      public JsonResult ChangeSelectedFlock(int flockId, bool selected)
      {
         Context.ChangeSelectedFlock(CurrentUserId, flockId, selected);
         return Json("Success");
      }

      [JsAction(CacheRequest = false)]
      [HttpGet]
      public JsonResult GetClosedFlocks()
      {
         var closedFlocks = Context.Flocks.Where(x => x.EndDate != null); // = CurrentUser.SelectedFlocks;
         return new JsonNetResult
         {
            Data = closedFlocks.Select(x => new { id = x.Id, name = x.Name }).OrderBy(x => x.name).ToArray(),
            JsonRequestBehavior = JsonRequestBehavior.AllowGet
         };
      }

      /// <summary>
      /// Export filtered actual statitstics into csv file
      /// </summary>
      /// <returns>csv file</returns>
      [HttpGet]
      public FileResult ExportStats(JQueryDataTablesRequest param, bool? isFlock)
      {
         int totalRecords;
         string fileName;
         IEnumerable<StatFlock> stats;
         if (isFlock != null && (bool)isFlock)
         {
            fileName = Resources.Resources.CsvFileActualStats;
            if (HttpContext.Request.QueryString["selectedFlocks"].Length > 0)
            {
               try
               {
                  var data = HttpContext.Request.QueryString["selectedFlocks"].Split(',');
                  param.selectedFlocks = data.Select(n => Convert.ToInt32(n)).ToArray();
               }
               catch (Exception e)
               {
               }
            }
            stats = Context.GetStats(param.ToFilterModel(), ParseRequest(), out totalRecords, true);
         }
         else
         {
            fileName = Resources.Resources.CsvFileWeighingDataStats;
            stats = Context.GetAllStats(param.ToFilterModel(), ParseRequest(), out totalRecords, true);

         }
         byte[] dataResult = StatToCsv.GetCsvInBytes(stats, true);
         return File(dataResult, "text/csv", fileName);
      }

      [JsAction(CacheRequest = false)]
      [HttpGet]
      public JsonResult GetFlockNames(JQueryDataTablesRequest param, bool? isFlock)
      {
         int totalRecords;
         if (HttpContext.Request.QueryString["selectedFlocks"].Length > 0)
         {
            try
            {
               var data = HttpContext.Request.QueryString["selectedFlocks"].Split(',');
               param.selectedFlocks = data.Select(n => Convert.ToInt32(n)).ToArray();
            }
            catch (Exception e)
            {
            }
         }
         var selectedFlocks = CurrentUser.SelectedFlocks.Select(x => x.Id);
         var stats = Context.GetStats(param.ToFilterModel(), ParseRequest(), out totalRecords, true);
         var names = stats.Select(x => new { name = x.Flock.Name, id = x.Flock.Id, selected = selectedFlocks.Contains(x.Flock.Id), sexName = GetFlockName(x), sex = x.Stat.Sex }).Distinct().ToArray();

         return Json(names, JsonRequestBehavior.AllowGet);
      }

      public class GraphHelper
      {
         public int id { get; set; }
         public string name { get; set; }
         public bool isGrowthCurve { get; set; }
         public bool active { get; set; }
         public string sex { get; set; }
         public List<PoinHelper> data { get; set; }
         public StatsHelper stat { get; set; }
      }

      public class PoinHelper
      {
         public int id { get; set; }
         public int x { get; set; }
         public Nullable<decimal> y { get; set; }
         public Nullable<decimal> y2 { get; set; }
         public Nullable<DateTime> date { get; set; }
      }

      public class StatsHelper
      {
         public DateTime date { get; set; }
         public int day { get; set; }
         public int count { get; set; }
         public decimal average { get; set; }
         public decimal gain { get; set; }
         public decimal sigma { get; set; }
         public decimal cv { get; set; }
         public decimal uni { get; set; }
         public Nullable<decimal> temperature { get; set; }
         public Nullable<int> carbonDioxide { get; set; }
         public Nullable<int> ammonia { get; set; }
         public Nullable<decimal> humidity { get; set; }
         public bool isUpdated { get; set; }
      }
   }


}