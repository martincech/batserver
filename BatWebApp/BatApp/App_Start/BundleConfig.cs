﻿using System.Web.Optimization;

namespace BatApp
{
   public class BundleConfig
   {
      // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
      public static void RegisterBundles(BundleCollection bundles)
      {
         bundles.Add(new StyleBundle("~/Content/common").Include(
            "~/Content/bootstrap.css",
            "~/Content/bootstrap-responsive.css",
            "~/Content/themes/base/all.css",
            "~/Content/AmCharts/export.css",
            "~/Content/Site.css"
            ));
         bundles.Add(new StyleBundle("~/Content/themes/base/themes").Include(
            "~/Content/themes/base/all.css"
           ));

         bundles.Add(new ScriptBundle("~/Scripts/common").Include(
            "~/Scripts/modernizr-{version}.js",
            "~/Scripts/jquery-{version}.js",
            "~/Scripts/jquery.unobtrusive-ajax.js",
            "~/Scripts/jquery.validate*",
            "~/Scripts/jquery.printElement.min.js",
            "~/Scripts/bootstrap.js",
            "~/Scripts/jquery-ui-{version}.js",
            "~/Scripts/jquery.cookie.js",
            "~/Scripts/jquery.jeditable.js",
            "~/Scripts/jquery-confirm.js",
            "~/Scripts/knockout-{version}.js",
            "~/Scripts/knockout.validation.js",
            "~/Scripts/knockout.validation.de-DE.js",
            "~/Scripts/knockout.validation.en-US.js",
            "~/Scripts/knockout.validation.cs-CZ.js",
            "~/Scripts/respond.js",
            "~/Scripts/datepicker-de-DE.js",
            "~/Scripts/datepicker-en-US.js",
            "~/Scripts/datepicker-cs-CZ.js",
            "~/Scripts/globalize/globalize.js",
            "~/Scripts/globals.js"
            ));

         bundles.Add(new StyleBundle("~/Styles/PagedList").Include(
            "~/Content/PagedList.css"
            ));

         bundles.Add(new StyleBundle("~/Styles/UserAccounts").Include(
            "~/Content/UserAccounts.css"
            ));

         bundles.Add(new StyleBundle("~/Content/DataTables-1.10.3/css/bundle").Include(
            //"~/Content/DataTables-1.10.3/css/dataTables.bootstrap.css",
            "~/Content/DataTables-1.10.3/css/dataTables.scroller.css",
            "~/Content/DataTables-1.10.3/css/jquery.dataTables.css",
            "~/Content/DataTables-1.10.3/css/jquery.dataTables_themeroller.css"
            ));

         bundles.Add(new ScriptBundle("~/Scripts/DataTables").Include(
            "~/Scripts/DataTables-1.10.3/jquery.dataTables.js",
            "~/Scripts/DataTables-1.10.3/dataTables.scroller.js",
            "~/Scripts/DataTables-1.10.3/jquery.dataTables.columnFilter.js",
            "~/Scripts/DataTables-1.10.3/jquery.dataTables.editable.js"
            ));

         bundles.Add(new ScriptBundle("~/Scripts/charts").Include(
            "~/Scripts/AmCharts/amcharts.js",
            "~/Scripts/AmCharts/xy.js",
            "~/Scripts/AmCharts/plugins/export/export.js",
            "~/Scripts/AmCharts/lang/de.js",
            "~/Scripts/AmCharts/lang/cs.js"          
            ));

#if DEBUG
         BundleTable.EnableOptimizations = false;
#else
         BundleTable.EnableOptimizations = true;
#endif
      }
   }
}