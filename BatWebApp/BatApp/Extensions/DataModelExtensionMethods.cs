﻿using System.Linq;
using DataModel;

namespace BatApp.Extensions
{
   public static class DataModelExtensionMethods
   {
      public static DTOs.Scale MapTo(this Scale model)
      {
         var confs = model.ConfigurationsV2.Select(MapTo).ToList();
         return new DTOs.Scale
         {
            Id = model.Id,
            Name = model.Name,
            SerialNumber = model.SerialNumber,
            PhoneNumber = model.PhoneNumber,
            FarmName = model.FarmName,
            ConfigurationsV2 = confs
         };
      }

      public static DTOs.CurveValue MapTo(this CurveValue model)
      {
         return new DTOs.CurveValue
         {
            Id = model.Id,
            CurveId = model.CurveId,
            Day = model.Day,
            Weight = model.Weight
         };
      }

      public static DTOs.Curve MapTo(this Curve model)
      {
         if (model == null) return null;

         var curveValues = model.CurveValues.Select(MapTo).ToList();
         return new DTOs.Curve
         {
            Id = model.Id,
            Name = model.Name,
            CurveValues = curveValues
         };
      }

      public static DTOs.FlockV2 Mapto(this FlockV2 model)
      {
         return new DTOs.FlockV2
         {
            Id = model.Id,
            Name = model.Name,
            UseCurves = model.UseCurves,
            UseGender = model.UseGender,
            WeighFrom = model.WeighFrom,
            WeighTo = model.WeighTo,
            ConfigurationV2Id = model.ConfigurationV2_Id,
            CurveFemaleId = model.CurveFemale_Id,
            CurveMaleId = model.CurveMale_Id,
            InitialFemale = model.InitialFemale,
            InitialMale = model.InitialMale,
            Index = model.Index,
            CurveFemale = MapTo(model.CurveFemale),
            CurveMale = MapTo(model.CurveMale)
         };
      }

      public static DTOs.ConfigurationV2 MapTo(this ConfigurationsV2 model)
      {
         var flocks = model.FlockV2.Select(Mapto).ToList();
         return new DTOs.ConfigurationV2
         {
            Id = model.Id,
            Name = model.Name,
            Backlight = model.Backlight,
            ScaleId = model.ScaleId,
            Active = model.Active,
            Statistics = MapTo(model.Statistics),
            Gsm = MapTo(model.Gsm),
            ScaleConfig = MapTo(model.ScaleConfig),
            Correction = MapTo(model.Correction),
            FlockV2 = flocks
         };
      }

      public static DTOs.Statistics MapTo(this Statistics model)
      {
         return new DTOs.Statistics
         {
            HistogramRange = model.HistogramRange,
            UniformityRange = model.UniformityRange
         };
      }

      public static DTOs.Gsm MapTo(this Gsm model)
      {
         return new DTOs.Gsm
         {
            Use = model.Use,
            SendMidnight = model.SendMidnight,
            SendRequest = model.SendRequest,
            CheckNumbers = model.CheckNumbers,
            Period1 = model.Period1,
            Period2 = model.Period2,
            Day1 = model.Day1,
            SendHour = model.SendHour,
            SendMin = model.SendMin,
            Number0 = model.Number0,
            Number1 = model.Number1,
            Number2 = model.Number2,
            Number3 = model.Number3,
            Number4 = model.Number4
         };
      }

      public static DTOs.Rs485 MapTo(this Rs485 model)
      {
         return new DTOs.Rs485
         {
            Speed = model.Speed,
            Parity = model.Parity,
            Protocol = model.Protocol,
            ReplyDelay = model.ReplyDelay,
            SilentInterval = model.SilentInterval
         };
      }

      public static DTOs.ScaleConfig MapTo(this ScaleConfig model)
      {
         return new DTOs.ScaleConfig
         {
            MaleMarginAbove = model.MaleMarginAbove,
            MaleMarginBelow = model.MaleMarginBelow,
            FemaleMarginAbove = model.FemaleMarginAbove,
            FemaleMarginBelow = model.FemaleMarginBelow,
            Filter = model.Filter,
            Stabilization = model.Stabilization,
            StabilizationTime = model.StabilizationTime,
            GainAutomaticMode = model.GainAutomaticMode,
            SaveUnpon = model.SaveUnpon,
            Units = model.Units
         };
      }

      public static DTOs.Correction MapTo(this Correction model)
      {
         return new DTOs.Correction
         {
            UseCurve = model.UseCurve,
            Day1 = model.Day1,
            Day2 = model.Day2,
            UniformityRange = model.UniformityRange
         };
      }

   }
}
