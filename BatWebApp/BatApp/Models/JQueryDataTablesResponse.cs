﻿using System;
using System.Collections;

namespace BatApp.Models
{
   /// <summary>
   /// Represents the required data for a response from a request by DataTables.
   /// </summary>
   public class JQueryDataTablesResponse
   {
      public JQueryDataTablesResponse(int sEcho,
         int totalRecords,
         int totalDisplayRecords,
         IEnumerable items = null)
      {
         aaData = items;
         iTotalRecords = totalRecords;
         iTotalDisplayRecords = totalDisplayRecords;
         this.sEcho = sEcho;
      }

      /// <summary>
      /// Initializes a new instance of the <see cref="T:System.Object"/> class.
      /// </summary>
      public JQueryDataTablesResponse(string error)
      {
         this.error = error;
         iTotalRecords = iTotalDisplayRecords = sEcho = 0;
         aaData = null;

      }

      // ReSharper disable InconsistentNaming

      /// <summary>
      /// Sets the Total records, before filtering (i.e. the total number of records in the database)
      /// </summary>
      public int iTotalRecords { get; private set; }

      /// <summary>
      /// Sets the Total records, after filtering 
      /// (i.e. the total number of records after filtering has been applied - 
      /// not just the number of records being returned in this result set)
      /// </summary>
      public int iTotalDisplayRecords { get; private set; }

      /// <summary>
      /// Sets an unaltered copy of sEcho sent from the client side. This parameter will change with each 
      /// draw (it is basically a draw count) - so it is important that this is implemented. 
      /// Note that it strongly recommended for security reasons that you 'cast' this parameter to an 
      /// integer in order to prevent Cross Site Scripting (XSS) attacks.
      /// </summary>
      public int sEcho { get; private set; }

      /// <summary>
      /// Sets the data in a 2D array (Array of JSON objects). Note that you can change the name of this 
      /// parameter with sAjaxDataProp.
      /// </summary>
      public IEnumerable aaData { get; private set; }

      /// <summary>
      /// Error string for error response
      /// </summary>
      public String error { get; private set; }
      // ReSharper restore InconsistentNaming
   }
}