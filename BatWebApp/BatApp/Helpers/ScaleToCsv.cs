﻿using System.Collections.Generic;
using System.Text;
using DataModel;
using Utilities;

namespace BatApp.Helpers
{
   public class ScaleToCsv
   {
      /// <summary>
      /// Creta csv file and convert it into byte array
      /// </summary>
      /// <param name="scales">scales to be stored in csv file</param>
      /// <param name="delimiter">delimiter</param>
      /// <param name="deciamlSeparator">deciaml separator</param>
      /// <returns>csv file as byte array</returns>
      public static byte[] GetCsvInBytes(IEnumerable<Scale> scales, string delimiter, string deciamlSeparator)
      {
         Csv csv = new Csv(delimiter, deciamlSeparator, Encoding.Default);
        
         int column = 0;
         csv.AddString(column++, Resources.Resources.ScalesSerialNumberTitle);
         csv.AddString(column++, Resources.Resources.ScalesScaleNameTitle);
         csv.AddString(column++, Resources.Resources.ScalesFarmNameTitle);
         csv.AddString(column++, Resources.Resources.ScalesPhoneNumberTitle);
         csv.SaveLine();
         foreach (var scale in scales)
         {
            column = 0;
            csv.AddInteger(column++, scale.SerialNumber);
            csv.AddString(column++, scale.Name ?? "");
            csv.AddString(column++, scale.FarmName ?? "");
            csv.AddString(column++, scale.PhoneNumber?? "");
            csv.SaveLine();
         }
         return csv.SaveToBytes();
      }
   }
}
