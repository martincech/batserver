﻿using DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Bat2Library;
using Bat2Library.Utilities;
using BatLibrary;

namespace BatApp.Helpers
{
   public class ReportToStat
   {
      public static List<Stat> GetStats(IEnumerable<Report> reports, Weight.WeightUnits unit)
      {
         return reports.Select(x=> GetStat(x,unit)).ToList(); 
      }

      public static Stat GetStat(Report report, Weight.WeightUnits unit)
      {
         if (report == null)
         {
            return null;
         }
         
         return new Stat()
         {
            Average = new decimal(ConvertWeight.Convert(report.Average, unit, Weight.WeightUnits.KG)),
            Count = report.Count,
            Cv = new decimal(report.Cv),
            Date = report.Date,
            Day = report.Day,
            Gain = new decimal(ConvertWeight.Convert(report.Gain, unit, Weight.WeightUnits.KG)),
            Sex = report.Sex,
            Sigma = new decimal(ConvertWeight.Convert(report.Sigma, unit, Weight.WeightUnits.KG)),
            Uni =  new decimal(report.Uniformity)
         };
      }

   }
}