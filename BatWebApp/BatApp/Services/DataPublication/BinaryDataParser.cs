﻿using Bat2Library;
using DataModel;
using System;


namespace BatApp.Services.DataPublication
{
    /// <summary>
    /// Parse binary data
    /// </summary>
    public class BinaryDataParser : IDataParser
    {
        //Constat for converting base unit type to g
        private const int UNIT_FACTOR = 10;

        public Stat Male { get; set; }

        public Stat Female { get; set; }

        public string PhoneNumber { get; set; }

        public string ScaleName { get; set; }

        public int ScaleNumber { get; set; }

        /// <summary>
        /// Parse data
        /// </summary>
        /// <param name="data">collection of binary data</param>
        /// <returns>true if parsing succeed</returns>
        public bool ParseData(object data)
        {
            if (!(data is byte[]))
            {
                return false;
            }
            byte[] binData = (byte[])data;

            PublishedData parsedData;
            if(!MessageParser.Parse(binData, out parsedData))
            {
                return false;
            }
            ScaleName = parsedData.SerialNumber.ToString();
            ScaleNumber = (int)parsedData.SerialNumber;
            Stat male = MapStat(parsedData.Male);
            male.Day = parsedData.Day;
            male.Date = (DateTime)parsedData.DateTime;
            male.Sex = SexE.SEX_UNDEFINED;
            Male = male;

            if (parsedData.Female != null)
            {
                Stat female = MapStat(parsedData.Female);
                female.Day = parsedData.Day;
                female.Date = (DateTime) parsedData.DateTime;
                female.Sex = SexE.SEX_FEMALE;
                Male.Sex = SexE.SEX_MALE;
                Female = female;
            }
            else
            {
                Female = null;
            }

            return true;
        }

        private Stat MapStat(GenderStats data)
        {
            var stat = new Stat
            {
                Average = new decimal(Math.Round((double) data.Average/UNIT_FACTOR, 1)),
                Count = (int) data.Count,
                Cv = new decimal(Math.Round((double) data.Cv/UNIT_FACTOR, 1)),
                Gain = new decimal(Math.Round((double) data.Gain/UNIT_FACTOR, 1)),
                Sigma = new decimal(Math.Round((double) data.Sigma/UNIT_FACTOR, 1)),
                Uni = new decimal(Math.Round((double) data.Uniformity/UNIT_FACTOR, 1))
            };
            return stat;
        }
    }
}