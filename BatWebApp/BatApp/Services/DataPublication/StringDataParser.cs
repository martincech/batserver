﻿using System;
using System.Linq;
using Bat2Library;
using BatWebSynchronizer.Collector;
using BatWebSynchronizer.Synchronizer;
using DataModel;

namespace BatApp.Services.DataPublication
{
   /// <summary>
   /// Parse string data
   /// </summary>
   public class StringDataParser : IDataParser
   {
      public Stat Male { get; set; }

      public Stat Female { get; set; }

      public string PhoneNumber { get; set; }

      public string ScaleName { get; set; }

      public int ScaleNumber { get; set; }

      public bool ParseData(object data)
      {
         if (!(data is string))
         {
            return false;
         }
         var strData = (string)data;
         StatisticData smsData;
         PhoneNumber = strData.Split(' ').First();
         strData = strData.Remove(0, PhoneNumber.Length + 1);
         if (!SmsDecode.Decode(strData, out smsData))
         {
            return false;
         }
         if (!Map(smsData))
         {
            return false;
         }
         ScaleNumber = (int)smsData.ScaleNumber;
         ScaleName = smsData.ScaleName;
         return true;
      }

      private bool Map(StatisticData smsData)
      {
         try
         {
            var female = MapStat(smsData.FemaleData);
            female.Day = smsData.DayNumber;
            female.Date = smsData.Date;
            female.ScaleId = (int)smsData.ScaleNumber;
            female.Sex = SexE.SEX_UNDEFINED;

            if (smsData.MaleData != null)
            {
               var male = MapStat(smsData.MaleData);
               male.Day = smsData.DayNumber;
               male.Date = smsData.Date;
               male.ScaleId = (int)smsData.ScaleNumber;
               male.Sex = SexE.SEX_MALE;
               female.Sex = SexE.SEX_FEMALE;
               Male = male;
            }
            Female = female;
         }
         catch (Exception)
         {
            return false;
         }
         return true;
      }

      private Stat MapStat(StatisticData.StatData data)
      {
         return new Stat
         {
            Average = new decimal(data.Average),
            Count = data.Count,
            Cv = new decimal(data.Cv),
            Gain = new decimal(data.Gain),
            Sigma = new decimal(data.Sigma),
            Uni = new decimal(data.Uniformity),
            Temperature = (decimal?) data.Temperature,
            CarbonDioxide = data.CarbonDioxide,
            Ammonia = data.Ammonia,
            Humidity = (decimal?) data.Humidity
         };
      }
   }
}