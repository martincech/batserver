﻿using System;
using Bat2Library;
using BatWebSynchronizer.Synchronizer;
using DataModel;

namespace BatApp.Services.DataPublication
{
   /// <summary>
   /// Parse SmsData
   /// </summary>
   public class SmsDataParser : IDataParser
   {
      public Stat Male { get; set; }

      public Stat Female { get; set; }

      public string PhoneNumber { get; set; }

      public string ScaleName { get; set; }

      public int ScaleNumber { get; set; }

      public bool ParseData(object data)
      {
         var smsData = (StatisticData)data;
         if (smsData == null || (smsData.MaleData == null && smsData.FemaleData == null))
         {
            return false;
         }

         PhoneNumber = smsData.PhoneNumber;
         ScaleName = smsData.ScaleName;
         ScaleNumber = (int)smsData.ScaleNumber;
         if (smsData.MaleData != null)
         {
            Male = Map(smsData.MaleData, smsData.Date,smsData.DayNumber, smsData.FemaleData == null ? SexE.SEX_UNDEFINED : SexE.SEX_MALE);
         }

         if (smsData.FemaleData != null)
         {
            Female = Map(smsData.FemaleData, smsData.Date, smsData.DayNumber, smsData.MaleData == null ? SexE.SEX_UNDEFINED : SexE.SEX_FEMALE);
         }

         return true;
      }

      private Stat Map(StatisticData.StatData stat,DateTime date,int day,SexE sex)
      {
         return new Stat()
           {
              Ammonia = stat.Ammonia,
              CarbonDioxide = stat.CarbonDioxide,
              Day = day,
              Date = date,
              Count = stat.Count,
              Average = new decimal(stat.Average),
              Cv = new decimal(stat.Cv),
              Gain = new decimal(stat.Gain),
              Humidity = (decimal?) stat.Humidity,
              Sigma = new decimal(stat.Sigma),
              Temperature = (decimal?) stat.Temperature,
              Uni = new decimal(stat.Uniformity),
              Sex = sex
           };
      }
   }
}
