﻿using System;
using DataContext;
using DataModel;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;

namespace BatApp.Infrastructure
{
   public class ApplicationRoleManager : RoleManager
   {
      public ApplicationRoleManager(BatModelContainer db)
         : base(db)
      {
      }

      public ApplicationRoleManager(IRoleStore<Role, Guid> store)
         : base(store)
      {
      }

      public static ApplicationRoleManager Create(IdentityFactoryOptions<ApplicationRoleManager> options,  IOwinContext context)
      {
         var manager = new ApplicationRoleManager(context.Get<ApplicationDbContext>());
         return manager;
      }
   }
}