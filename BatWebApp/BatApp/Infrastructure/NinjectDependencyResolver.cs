﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using DataModel;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Ninject;

namespace BatApp.Infrastructure
{
   public class NinjectDependencyResolver : IDependencyResolver
   {
      private readonly IKernel kernel;

      /// <summary>
      /// Initializes a new instance of the <see cref="T:System.Object"/> class.
      /// </summary>
      public NinjectDependencyResolver(IKernel kernel)
      {
         this.kernel = kernel;
         AddBindings();
      }

      private void AddBindings()
      {
         kernel.Bind<BatModelContainer>().ToMethod((context) => ApplicationDbContext.Create());
         kernel.Bind<SignInManager<User, Guid>>().To<ApplicationSignInManager>();
         kernel.Bind<UserManager<User, Guid>>()
            .ToMethod((context) => ApplicationUserManager.Create(kernel.Get<BatModelContainer>()));
         kernel.Bind<RoleManager<Role, Guid>>().To<ApplicationRoleManager>();

      }

      #region Implementation of IDependencyResolver

      /// <summary>
      /// Resolves singly registered services that support arbitrary object creation.
      /// </summary>
      /// <returns>
      /// The requested service or object.
      /// </returns>
      /// <param name="serviceType">The type of the requested service or object.</param>
      public object GetService(Type serviceType)
      {
         return kernel.TryGet(serviceType);
      }

      /// <summary>
      /// Resolves multiply registered services.
      /// </summary>
      /// <returns>
      /// The requested services.
      /// </returns>
      /// <param name="serviceType">The type of the requested services.</param>
      public IEnumerable<object> GetServices(Type serviceType)
      {
         return kernel.GetAll(serviceType);
      }

      #endregion
   }
}