﻿using System;
using System.Net;
using System.Security.Principal;
using System.Text;
using System.Web.Mvc;
using System.Web.Mvc.Filters;

namespace BatApp.Infrastructure.Filters
{
   public abstract class BasicAuthenticationAttribute : FilterAttribute, IAuthenticationFilter
   {
      public string Realm { get; set; }

      protected abstract IPrincipal Authenticate(string userName, string password);

      public void OnAuthentication(AuthenticationContext filterContext)
      {
         var request = filterContext.HttpContext.Request;
         var values = request.Headers.GetValues(HttpRequestHeader.Authorization.ToString());

         if (values == null || values.Length != 1 || values[0] == null || !values[0].StartsWith("Basic "))
         {
            // No authentication was attempted (for this authentication method).
            // Do not set Principal (which would indicate success), set Result (which indicate an error).
            filterContext.Result = new HttpStatusCodeResult(HttpStatusCode.Unauthorized);
            return;
         }

         var encodedCredentials = values[0].Substring("Basic ".Length).TrimStart(' ');

         if (String.IsNullOrEmpty(encodedCredentials))
         {
            // Authentication was attempted but failed. Set Result to indicate an error.
            filterContext.Result = new HttpStatusCodeResult(HttpStatusCode.Unauthorized, "Missing credentials");
            return;
         }

         var userNameAndPasword = ExtractUserNameAndPassword(encodedCredentials);

         if (userNameAndPasword == null)
         {
            // Authentication was attempted but failed. Set Result to indicate an error.
            filterContext.Result = new HttpStatusCodeResult(HttpStatusCode.Unauthorized, "Invalid credentials");
            return;
         }

         var userName = userNameAndPasword.Item1;
         var password = userNameAndPasword.Item2;

         var principal = Authenticate(userName, password);

         if (principal == null)
         {
            // Authentication was attempted but failed. Set Result to indicate an error.
            filterContext.Result = new HttpStatusCodeResult(HttpStatusCode.Unauthorized, "Invalid username or password");
         }
         else
         {
            // Authentication was attempted and succeeded. Set Principal to the authenticated user.
            filterContext.Principal = principal;
         }
      }

      public void OnAuthenticationChallenge(AuthenticationChallengeContext filterContext)
      {
         string challenge;

         if (String.IsNullOrEmpty(Realm))
         {
            challenge = "Basic";
         }
         else
         {
            // A correct implementation should verify that Realm does not contain a quote character unless properly
            // escaped (precededed by a backslash that is not itself escaped).
            challenge = "Basic realm=\"" + Realm + "\"";
         }

         filterContext.ChallengeWith(challenge);
      }

      private static Tuple<string, string> ExtractUserNameAndPassword(string authorizationParameter)
      {
         byte[] credentialBytes;

         try
         {
            credentialBytes = Convert.FromBase64String(authorizationParameter);
         }
         catch (FormatException)
         {
            return null;
         }

         // The currently approved HTTP 1.1 specification says characters here are ISO-8859-1.
         // However, the current draft updated specification for HTTP 1.1 indicates this encoding is infrequently
         // used in practice and defines behavior only for ASCII.
         var encoding = Encoding.ASCII;
         // Make a writable copy of the encoding to enable setting a decoder fallback.
         encoding = (Encoding) encoding.Clone();
         // Fail on invalid bytes rather than silently replacing and continuing.
         encoding.DecoderFallback = DecoderFallback.ExceptionFallback;
         string decodedCredentials;

         try
         {
            decodedCredentials = encoding.GetString(credentialBytes);
         }
         catch (DecoderFallbackException)
         {
            return null;
         }

         if (String.IsNullOrEmpty(decodedCredentials))
         {
            return null;
         }

         var colonIndex = decodedCredentials.IndexOf(':');

         if (colonIndex == -1)
         {
            return null;
         }

         var userName = decodedCredentials.Substring(0, colonIndex);
         var password = decodedCredentials.Substring(colonIndex + 1);
         return new Tuple<string, string>(userName, password);
      }
   }
}