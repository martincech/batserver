﻿using System.Web.Mvc;

namespace BatApp.Infrastructure.Filters
{
   public class AuthorizeAttribute : System.Web.Mvc.AuthorizeAttribute
   {
      private bool _noRedirect;

      public bool NoRedirect
      {
         get { return _noRedirect; }
         set { _noRedirect = value; }
      }

      protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
      {

         if (!filterContext.RequestContext.HttpContext.User.Identity.IsAuthenticated)
         {
            filterContext.Result = new System.Web.Mvc.HttpStatusCodeResult((int) System.Net.HttpStatusCode.Unauthorized);
         }
         else
         {
            if (NoRedirect)
            {
               filterContext.Result = new System.Web.Mvc.HttpStatusCodeResult((int) System.Net.HttpStatusCode.Forbidden);
            }
            else
            {
               filterContext.HttpContext.Server.TransferRequest("/UserAccounts/Account/Forbidden");
            }
         }
      }
   }
}