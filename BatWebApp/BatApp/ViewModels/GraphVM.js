﻿var gl = globals[$.cookie("_culture")];

var GraphVM = {
    //all flocks {id,name}
    flocks: ko.observableArray(),
    //filtered flocks data {graph data}
    flockData: ko.observableArray(),
    flockIndex: ko.observable(),
    curves: ko.observableArray(),
    curveId: ko.observable(),
    // Filters
    from: ko.observable(),
    to: ko.observable(),
    sex: ko.observable(-1),
    flock: ko.observable(),
    clearing: ko.observable(true),
    // Graphs params
    graphType: ko.observable('Average'),
    graphType2: ko.observable(''),
    toolTips: new Array(),
    //Graph type options collection
    graphTypes: ko.observableArray()
};

GraphVM.flocksIds = ko.computed(function () {
    return ko.utils.arrayMap(GraphVM.flocks(), function (flock) {
        return flock.id;
    });
});

GraphVM.flockId = ko.computed(function () {
    if (GraphVM.flock()) {
        return GraphVM.flock().id;
    } else {
        return null;
    }
});

//************************************************************
// Filter changed listeners
//************************************************************
GraphVM.flock.subscribe(function () {
    if (GraphVM.clearing())
        return;
    GraphVM.ClearActiveFlock();
    GraphVM.CreateGraph();
    if (statTable) {
        statTable.fnDraw();
    }
});

GraphVM.graphType2.subscribe(function () {
    GraphVM.CreateGraph();
});

GraphVM.from.subscribe(function (from) {
    if (GraphVM.clearing()) {
        return;
    }
    if (!GraphVM.isValidDate(from)) {
        return;
    }
    GraphVM.CreateGraph();
    if (statTable) {
        statTable.fnDraw();
    }
});

GraphVM.to.subscribe(function (to) {
    if (GraphVM.clearing()) {
        return;
    }
    if (!GraphVM.isValidDate(to)) {
        return;
    }
    GraphVM.CreateGraph();
    if (statTable) {
        statTable.fnDraw();
    }
});

GraphVM.isValidDate = function (dateString) {
    var date = dateString.split(".");
    if (date.length !== 3) {
        return false;
    }

    function isInt(value) {
        return !isNaN(value) && (function (x) { return (x | 0) === x; })(parseFloat(value));
    }

    if (!isInt(date[0]) || !isInt(date[1]) || !isInt(date[2])) {
        return false;
    }

    var day = parseInt(date[0]);
    if (day < 1 || day > 31) {
        return false;
    }

    var month = parseInt(date[1]);
    if (month < 1 || month > 12) {
        return false;
    }

    var year = parseInt(date[2]);
    if (year < 1900 || year > 2999) {
        return false;
    }
    return true;
}

GraphVM.sex.subscribe(function () {
    if (GraphVM.clearing())
        return;
    GraphVM.ClearActiveFlock();
    GraphVM.CreateGraph();
    if (statTable) {
        statTable.fnDraw();
    }
});

GraphVM.curveId.subscribe(function (curve) {
    d3.selectAll(".curve").style("visibility", "hidden");
    if (curve) {
        d3.selectAll(".curve_" + curve).style("visibility", "visible");
        $.cookie("growthCurve", curve);
    } else {
        $.cookie("growthCurve", "");
    }
});


//************************************************************
// Scroll listeners
//************************************************************
GraphVM.ScrollCurve = function (data, event) {
    if (IsScrollUp(event)) {
        if (event.target.selectedIndex < event.target.length - 1) {
            event.target.selectedIndex = event.target.selectedIndex + 1;
            GraphVM.curveId(event.target.value);
        }
    } else {
        if (event.target.selectedIndex > 0) {
            event.target.selectedIndex = event.target.selectedIndex - 1;
            GraphVM.curveId(event.target.value);
        }
    }
};

GraphVM.ScrollGraph = function (data, event) {
    if (IsScrollUp(event)) {
        if (event.target.selectedIndex < event.target.length - 1) {
            event.target.selectedIndex = event.target.selectedIndex + 1;
            if (event.target.value === GraphVM.graphType()) {
                event.target.selectedIndex = event.target.selectedIndex + 1;
            }
            GraphVM.graphType2(event.target.value);
        }
    } else {
        if (event.target.selectedIndex > 0) {
            event.target.selectedIndex = event.target.selectedIndex - 1;
            if (event.target.value === GraphVM.graphType()) {
                event.target.selectedIndex = event.target.selectedIndex - 1;
            }
            GraphVM.graphType2(event.target.value);
        }
    }
};

GraphVM.ScrollFlock = function (data, event) {
    if (IsScrollUp(event)) {
        GraphVM.SetActiveFlock(GraphVM.flockIndex(), +1);
    } else {
        GraphVM.SetActiveFlock(GraphVM.flockIndex(), -1);
    }
};

GraphVM.SetActiveFlock = function (index, step) {
    function show(active) {
        GraphVM.flockIndex(active);
        var act = GraphVM.flocks()[active];
        d3.selectAll(".flock_" + act.id).style("visibility", "visible");
        $("#legend_" + act.id).removeClass("inactive").addClass("active");
        $("#flock-swipe #flock-text").text(act.name);
        $("#flock-swipe .flock-marker").css("background-color", GraphVM.GetColorById(act.id));
        d3.selectAll(".stat_" + act.id).style("display", "inline-block");
        $.cookie(window.location.pathname + "_graphId", act.id);
    }

    function hide(active) {
        GraphVM.flockIndex(null);
        d3.selectAll(".flock_" + GraphVM.flocks()[active].id).style("visibility", "hidden");
        d3.selectAll(".stat_" + GraphVM.flocks()[active].id).style("display", "none");
        $("#legend_" + GraphVM.flocks()[active].id).removeClass("active").addClass("inactive");
    }

    function getNotActive(active, step, limit) {
        active = parseInt(active) + step;

        if (active === limit) {
            return -1;
        }
        while (!$("#legend_" + GraphVM.flocks()[active].id).hasClass("inactive")) {
            active = active + step;
            if (active === limit) {
                active = -1;
                break;
            }
        }
        return active;
    }

    if (isNaN(step) || GraphVM.flocks().length < 1) {
        return;
    }

    var limit = -1;
    if (step > 0) {
        limit = GraphVM.flocks().length;
    }

    if (index === null) {
        var init = -1;
        if (step < 0) {
            init = GraphVM.flocks().length;
        }
        index = getNotActive(init, step, limit);
    } else {
        hide(index);
        index = getNotActive(index, step, limit);
    }


    if (index !== -1) {
        show(index);
    } else {
        GraphVM.ClearActiveFlock();
    }
};

GraphVM.ClearActiveFlock = function () {
    GraphVM.flockIndex(null);
    $(".flock-marker").removeAttr("style");
    $("#flock-text").html('<img src="' + baseURL + 'Content/images/swipe.png" height="40">');
    $.cookie(window.location.pathname + "_graphId", "");
}


//************************************************************
// Export graph data
//************************************************************
GraphVM.ExportGraph = function () {
    Export.Export();
};

//************************************************************
// Get color by id
//************************************************************
GraphVM.GetColorById = function (id) {
    return colors[GraphVM.flocksIds().indexOf(id) % colors.length];
};

//************************************************************
// Graph type chage + clear filtering
//************************************************************
GraphVM.ClearFilters = function () {
    GraphVM.clearing(true);
    GraphVM.from(undefined);
    GraphVM.to(undefined);
    GraphVM.sex('sex');
    GraphVM.flock(undefined);
    GraphVM.clearing(false);
    GraphVM.CreateGraph();
    if (statTable) {
        statTable.fnDraw();
    }
};

GraphVM.ChangeGraphType = function (type, data, event) {
    GraphVM.graphType(type);
    GraphVM.CreateGraph();
    d3.selectAll(".graphTypesMenu li").attr("class", null);
    $(event.target.parentElement).addClass("selected");
};

//************************************************************
// Initial method
//************************************************************
GraphVM.Show = function (type, data, event) {
    $(event.target).tab('show');
    $.cookie(window.location.pathname, type);
    var downloadGraph = false;
    if (type === "graph") {
        GraphVM.CreateGraph();
        GraphVM.clearing(false);
        downloadGraph = true;
    }
    document.getElementById("exportData").style.display = downloadGraph ? "none" : "block";
    document.getElementById("exportGraph").style.display = !downloadGraph ? "none" : "block";
};

//************************************************************
// Creating graph + loading graph data 
//************************************************************
GraphVM.CreateGraph = function () {
    if (GraphVM.graphType() === GraphVM.graphType2()) {
        GraphVM.graphType2("");
        return;
    }

    if (!$("#selectedGraph").hasClass("active")) {
        return;
    }

    JsActions.Flocks.GetGraphData(
       GraphVM.flocksIds, GraphVM.flockId(), GraphVM.graphType, GraphVM.graphType2, GraphVM.sex, GraphVM.from, GraphVM.to,
       {
           success: function (result) {
               if (result) {
                   GraphVM.flocks();
                   GraphVM.DrawGraph(result);
               }
           }
       });
}

GraphVM.CreateToolTips = function (x, y, chartName, sex) {
    var toolTip = "<div class='graph-tip'>";
    var count = 0;
    for (var i = 0; i < GraphVM.toolTips[x].length; i++) {
        var elem = GraphVM.toolTips[x][i];

        if ($("#legend_" + elem.id).hasClass("inactive")) {
            continue;
        }


        if (GraphVM.curveId() !== elem.id) {
            var curve = ko.utils.arrayFirst(GraphVM.curves(), function(item) {
                return elem.name === item.name;
            });
            if (curve) {
                continue;
            }
        }

        if (elem.y === y) {
            if (count > 0) {
                toolTip = toolTip.concat("<br/>");
            }
            if (sex !== "") {
                sex = " " + sex;
            }
            toolTip = toolTip.concat(upFirstLetter(gl.fcTitle.replace("{0}", chartName).replace("{1}", elem.name + sex).replace("[[x]]", x).replace("[[y]]", elem.y)));
            count++;
        }
    }
    toolTip = toolTip.concat("</div>");
    return toolTip;
}
GraphVM.errors = ko.validation.group([GraphVM.from, GraphVM.to]);
ko.applyBindings(GraphVM);

var ChangeSelectedFlock = function (flockId, selected) {
    JsActions.Flocks.ChangeSelectedFlock(flockId, selected, {});
}

GraphVM.DrawGraph = function (dataChart) {
    //************************************************************
    // Data notice the structure
    //************************************************************
    GraphVM.toolTips = new Array();
    var data = dataChart.data;
    GraphVM.flockData(dataChart);
    d3.select("#legend").selectAll("div").remove();
    d3.select("#chartdiv").select("svg").remove();

    //************************************************************
    // Create Margins and Axis and hook our zoom function
    //************************************************************
    var rightSide = 70;
    if (GraphVM.graphType2() === null || GraphVM.graphType2() === "") {
        rightSide = 10;
    }
    var margin = { top: 10, right: rightSide, bottom: 40, left: 55 },
        width = $("#chartdiv").width() - 15 - margin.left - margin.right,
        height = $("#chartdiv").height() - margin.top - margin.bottom;

    var x = d3.scale.linear()
        .domain([dataChart.xMin, dataChart.xMax])
        .range([0, width]);

    var y = d3.scale.linear()
        .domain([dataChart.yMin, dataChart.yMax])
        .range([height, 0]);

    var y2 = d3.scale.linear()
        .domain([dataChart.yMin2, dataChart.yMax2])
        .range([height, 0]);

    var xAxis = d3.svg.axis()
        .scale(x)
       .tickFormat(d3.format("d"))
       .tickSize(-height)
       .tickPadding(10)
       .tickSubdivide(true)
        .orient("bottom");

    var yAxis = d3.svg.axis()
        .scale(y)
       .tickPadding(10)
       .tickSize(-width)
       .tickSubdivide(true)
        .orient("left");

    var yAxis2 = d3.svg.axis()
       .scale(y2)
      .tickPadding(10)
      .tickSize(5)
      .tickSubdivide(true)
       .orient("right");

    var zoom = d3.behavior.zoom()
        .x(x)
        .y(y)
        .scaleExtent([1, 10])
        .on("zoom", zoomed);

    var zoom2 = d3.behavior.zoom()
        .x(x)
        .y(y2)
        .scaleExtent([1, 10]);



    //************************************************************
    // Generate our SVG object
    //************************************************************	
    var svg = d3.select("#chartdiv").append("svg")
        .style("background-color", "#fcfcfc")
        .call(zoom)
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    svg.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis);

    svg.append("g")
        .attr("class", "y axis")
        .call(yAxis);

    svg.append("g")
        .attr("class", "y axis2")
        .attr("transform", "translate(" + (width) + " ,0)")
        .call(yAxis2);

    svg.append("g")
       .attr("class", "y axis")
       .append("text")
       .attr("class", "axis-label")
       .attr("transform", "rotate(-90)")
       .attr("y", (-margin.left) + 10)
       .attr("x", -height / 2)
       .text(dataChart.yName);

    svg.append("g")
      .attr("class", "y axis2")
      .append("text")
      .attr("class", "axis-label")
      .attr("transform", "rotate(-270)")
      .attr("y", -width - 55)
      .attr("x", height / 2 - 40)
      .text(dataChart.yName2);

    svg.append("g")
       .attr("class", "x axis")
       .append("text")
       .attr("class", "axis-label")
       .attr("y", height + 33)
       .attr("x", width / 2)
       .text(dataChart.xName);

    svg.append("clipPath")
       .attr("id", "clip")
       .append("rect")
       .attr("width", width)
       .attr("height", height);

    //************************************************************
    // Create D3 line object and draw data on our SVG object
    //************************************************************
    var line = d3.svg.line()
        .interpolate("linear")
        .defined(function (d) {
            return d !== null && d.y !== null;
        })
        .x(function (d) {
            return x(d.x);
        })
        .y(function (d) {
            return y(d.y);
        });

    var line2 = d3.svg.line()
        .interpolate("linear")
        .defined(function (d) {
            return d !== null && d.y2 !== null;
        })
        .x(function (d) {
            return x(d.x);
        })
        .y(function (d) {
            return y2(d.y2);
        });

    svg.selectAll('.line')
       .data(data)
       .enter()
       .append("path")
        .attr("class", function (d, i) {
            if (d.isGrowthCurve) {
                return "line curve curve_" + d.id;
            } else {
                return "line flock_" + d.id;
            }
        })
       .attr("clip-path", "url(#clip)")
       .style("visibility", "hidden")
       .style("fill", "none")
       .attr('stroke', function (d, i) {
           if (d.isGrowthCurve) {
               return "black";
           } else {
               return GraphVM.GetColorById(d.id);
           }

       })
        .attr("d", function (d) { return line(d.data); });

    svg.selectAll('.line2')
       .data(data)
       .enter()
       .append("path")
        .attr("class", function (d, i) {
            if (d.isGrowthCurve) {
                return "line2 curve curve_" + d.id;
            } else {
                return "line2 flock_" + d.id;
            }
        })
       .attr("clip-path", "url(#clip)")
       .style("visibility", "hidden")
       .style("fill", "none")
       .style("stroke-dasharray", ("3, 2"))
       .style("stroke-width", 3)
       .attr('stroke', function (d, i) {
           if (d.isGrowthCurve) {
               return "black";
           } else {
               return GraphVM.GetColorById(d.id);
           }

       })
        .attr("d", function (d) { return line2(d.data); });

    //************************************************************
    // Draw points on SVG object based on the data given
    //************************************************************
    d3.select("body").selectAll(".tooltip").remove();

    var div = d3.select("body").append("div")
    .attr("class", "tooltip")
    .style("opacity", 0)
    .style("z-index", -1);

    var points = svg.selectAll('.dots')
        .data(data)
        .enter()
        .append("g")
        .attr("style", "visibility:hidden")
        .attr("class", function (d) {
            if (d.isGrowthCurve) {
                return "dots curve curve_" + d.id;
            } else {
                return "dots flock_" + d.id;
            }
        })
       .attr("clip-path", "url(#clip)");

    points.selectAll('.dot')
        .data(function (d, index) {
            var a = [];
            d.data.forEach(function (point, i) {

                if (!GraphVM.toolTips[point.x]) {
                    GraphVM.toolTips[point.x] = new Array();
                }
                if (point.y !== null) {
                    GraphVM.toolTips[point.x].push({ y: point.y, name: d.name, id: d.id });
                    a.push({ 'index': index, 'point': point, isGrowthCurve: d.isGrowthCurve, name: d.name, id: d.id, sex: d.sex });
                }
            });
            return a;
        })
        .enter()
        .append('circle')
        .attr('class', 'dot')
        .attr("r", 2.5)
        .attr('fill', function (d, i) {
            if (d.isGrowthCurve) {
                return "black";
            } else {
                return GraphVM.GetColorById(d.id);
            }
        })
        .attr("transform", function (d) {
            if (d.point === null) {
                return "";
            }
            return "translate(" + x(d.point.x) + "," + y(d.point.y) + ")";
        })
       .on("mouseover", function (d) {
           div.style("z-index", "1")
              .style("opacity", 0.9);
           div.html(GraphVM.CreateToolTips(d.point.x, d.point.y, dataChart.yName, d.sex))
               .style("left", (d3.event.pageX) + "px")
               .style("top", (d3.event.pageY - 38) + "px");
       })
        .on("mouseout", function (d) {
            div.style("opacity", 0)
               .style("z-index", "-1");
        });


    var points2 = svg.selectAll('.dots2')
       .data(data)
       .enter()
       .append("g")
       .attr("style", "visibility:hidden")
       .attr("class", function (d) {
           if (d.isGrowthCurve) {
               return "dots2 curve curve_" + d.id;
           } else {
               return "dots2 flock_" + d.id;
           }
       })
      .attr("clip-path", "url(#clip)");

    points2.selectAll('.dot')
        .data(function (d, index) {
            var a = [];
            d.data.forEach(function (point, i) {

                if (!GraphVM.toolTips[point.x]) {
                    GraphVM.toolTips[point.x] = new Array();
                }
                if (point.y2 !== null) {
                    GraphVM.toolTips[point.x].push({ y: point.y2, name: d.name, id: d.id });
                    a.push({ 'index': index, 'point': point, isGrowthCurve: d.isGrowthCurve, name: d.name, id: d.id, sex: d.sex });
                }
            });
            return a;
        })
        .enter()
        .append('rect')
        .attr('class', 'dot')
        .attr("width", 6)
        .attr("height", 6)
        .attr('fill', function (d, i) {
            if (d.isGrowthCurve) {
                return "black";
            } else {
                return GraphVM.GetColorById(d.id);
            }
        })
        .attr("transform", function (d) {
            if (d.point === null) {
                return "";
            }
            return "translate(" + (x(d.point.x) - 3) + "," + (y2(d.point.y2) - 3) + ")";
        })
       .on("mouseover", function (d) {
           div.style("z-index", "1")
              .style("opacity", 0.9);
           div.html(GraphVM.CreateToolTips(d.point.x, d.point.y2, dataChart.yName2, d.sex))
               .style("left", (d3.event.pageX) + "px")
               .style("top", (d3.event.pageY - 38) + "px");
       })
        .on("mouseout", function (d) {
            div.style("opacity", 0)
               .style("z-index", "-1");
        });

    d3.select("#chartdiv").select("svg")
        .on("touchstart", null);

    d3.select("#swipe").selectAll(".item")
        .data(data)
        .enter()
        .append("label")
        .attr("class", "item")
        .attr("text", "text");

    //************************************************************
    // Apply styles to graph 
    //************************************************************

    d3.selectAll(".domain").style("fill", "none");
    d3.selectAll(".grid .tick")
        .style("stroke", "lightgrey")
        .style("opacity", 0.7)
        .style("shape-rendering", "crispEdges");

    d3.selectAll(".axis path")
      .style("fill", "none")
      .style("stroke", "#bbb")
      .style("shape-rendering", "crispEdges");

    d3.selectAll(".grid path")
        .style("stroke-width", 0);

    d3.selectAll(".axis .axis-label")
        .style(" font-size", "14px");

    d3.selectAll(".axis line")
     .style("stroke", "#e7e7e7")
     .style("shape-rendering", "crispEdges");

    d3.selectAll(".axis2 line")
    .style("stroke", "#bbbbbb")
    .style("shape-rendering", "crispEdges");

    d3.selectAll(".line")
     .style("fill", "none")
     .style("stroke-width", "1.5px");

    d3.selectAll(".dot")
     .style("stroke", "transparent")
     .style("stroke-width", "10px")
     .style("cursor", "pointer");

    //************************************************************
    // Zoom
    //************************************************************
    function zoomed() {
        zoom2.scale(zoom.scale()).translate(zoom.translate());

        svg.select(".x.axis").call(xAxis);
        svg.select(".y.axis").call(yAxis);
        svg.select(".y.axis2").call(yAxis2);
        svg.selectAll('path.line')
            .attr('d', function (d) { return line(d.data); });
        svg.selectAll('path.line2')
           .attr('d', function (d) { return line2(d.data); });
        d3.selectAll(".axis line")
            .style("stroke", "#e7e7e7")
            .style("shape-rendering", "crispEdges");

        d3.selectAll(".axis2 line")
            .style("stroke", "#bbbbbb")
            .style("shape-rendering", "crispEdges");

        points.selectAll('circle').attr("transform", function (d) {
            return "translate(" + x(d.point.x) + "," + y(d.point.y) + ")";
        });

        points2.selectAll('rect').attr("transform", function (d) {
            return "translate(" + (x(d.point.x) - 3) + "," + (y2(d.point.y2) - 3) + ")";
        });
    }


    //************************************************************
    // Creating graph legend
    //************************************************************
    var sc = $.cookie("growthCurve");
    d3.selectAll(".stat-item").remove();

    for (var i = 0; i < data.length; i++) {
        if (data[i].isGrowthCurve) {
            var curveExist = ko.utils.arrayFirst(GraphVM.curves(), function (curve) {
                return curve.id === data[i].id;
            });

            if (!curveExist) {
                GraphVM.curves.push(data[i]);
            }
            // Show last selected growth curve
            if (data[i].id === sc) {
                d3.selectAll(".curve_" + sc).style("visibility", "visible");
                GraphVM.curveId(data[i].id);
            }
            continue;
        }

        //************************************************************
        // Last day stats
        //************************************************************
        var info = d3.select("#stat-info")
            .append("div")
            .style("display", function () {
                if (data[i].active) {
                    return "inline-block";
                } else {
                    return "none";
                }
            })
            .attr("class", "stat-item stat_" + data[i].id);

        var date = data[i].stat.date.split("T")[0].split("-");
        var dateFormated = "<div style='float:left'>" + date[2] + "." + date[1] + "." + date[0] + "</div>";
        if (data[i].stat.isUpdated) {
            dateFormated += "<div class='circle green'></div>";
        } else {
            dateFormated += "<div class='circle red'></div>";
        }
        var statInfo = document.createElement('div');
        d3.select("#table-temp td").style("background-color", GraphVM.GetColorById(data[i].id));
        var html = d3.select("#table-temp")[0];
        statInfo.innerHTML = html[0].outerHTML;
        statInfo.firstChild.removeAttribute("id");
        statInfo.innerHTML = statInfo.innerHTML.replace("{name}", data[i].name + " " + data[i].sex);
        statInfo.innerHTML = statInfo.innerHTML.replace("{date}", dateFormated);
        statInfo.innerHTML = statInfo.innerHTML.replace("{day}", data[i].stat.day);
        statInfo.innerHTML = statInfo.innerHTML.replace("{weight}", data[i].stat.average);
        statInfo.innerHTML = statInfo.innerHTML.replace("{uni}", data[i].stat.uni);
        info.node().appendChild(statInfo);


        //************************************************************
        // Last day stats
        //************************************************************
        if (!d3.select("#legend_" + data[i].id).empty()) {
            continue;
        }

        d3.select("#legend")
            .append("div")
            .attr("class", function (d) {
                if (data[i].active) {
                    return "flock-legend active";
                } else {
                    return "flock-legend inactive";
                }
            })
            .attr("data-flock", data[i].id)
            .attr("id", "legend_" + data[i].id)
            .attr("style", "background-color:" + GraphVM.GetColorById(data[i].id))
            .text(data[i].name);
        if (data[i].active) {
            d3.selectAll(".flock_" + data[i].id)
                .style("visibility", "visible");
        }
    }

    //Show/Hide selected flock event handler
    $(".flock-legend").click(function () {
        var active = $(this).hasClass("active");
        if (active) {
            $(this).addClass("inactive");
            $(this).removeClass("active");
        } else {
            $(this).addClass("active");
            $(this).removeClass("inactive");
        }

        var flockId = $(this).attr("data-flock");

        d3.selectAll(".flock_" + flockId)
            .style("visibility", function (d) {
                var val = "visible";
                if (active) {
                    d3.selectAll(".stat_" + flockId).style("display", "none");
                    val = "hidden";
                } else {
                    d3.selectAll(".stat_" + flockId).style("display", "inline-block");
                }
                return val;
            });

        if (GraphVM.flockIndex() !== null) {
            if ($(this).attr("data-flock") === GraphVM.flocks()[GraphVM.flockIndex()].id) {
                GraphVM.ClearActiveFlock();
                return;
            }
        }

        ChangeSelectedFlock($(this).attr("data-flock"), !active);
    });


    ////************************************************************
    //// Save graph into image
    ////************************************************************

    var exportImage = d3.select("body").append("div")
        .attr("class", "export-image")
        .style("opacity", 0)
        .style("z-index", -1);

    function AddLegend() {
        var textWidth = 160;
        var x = 0;
        var y = height + 70;

        function addLegend(textValue, color) {
            var text = d3.select("svg g")
                .append("g")
                .attr("class", "export-temp")
                .append("text")
                .attr("x", function () {
                    if (x + textWidth > width) {
                        x = 0;
                        y = y + 30;
                        d3.select("svg").attr("height", y + 30);
                    }
                    return x;
                })
                .attr("y", function () {
                    return y;
                })
                .text(function () {
                    return textValue;
                });
            var bbox = text.node().getBBox();
            var rect = d3.select("svg g").append("svg:rect")
                .attr("class", "export-temp")
                .attr("y", function () {
                    return bbox.y - 2;
                })
                .attr("x", function () {
                    return bbox.x - (bbox.height + 5 + 3);
                })
                .attr("width", bbox.height + 5)
                .attr("height", bbox.height + 5)
                .style("fill", color)
                //.style("fill-opacity", ".3")
                .style("stroke", "#666")
                .style("stroke-width", "1.5px");
            x = x + textWidth;
        }

        d3.selectAll(".flock-legend.active").each(function () {
            d3.select("svg").attr("height", y + 30);
            addLegend(this.innerHTML, this.style.backgroundColor);
        });

        if (GraphVM.curveId()) {
            addLegend(document.getElementById('gc-select').options[document.getElementById('gc-select').selectedIndex].text, "black");
        }
    }

    if (!detectIE()) {
        d3.select("svg g")
            .append("g:image")
            .attr("y", 10)
            .attr("x", width - 50)
            .attr("width", 40)
            .attr("height", 40)
            .style("opacity", 0.1)
            .attr("xlink:href", baseURL + "content/images/export-graph.png")
            .on("mouseover", function (d) {
                this.style.opacity = 0.5;
            })
            .on("mouseout", function (d) {
                this.style.opacity = 0.1;
            }).on("click", function () {
                AddLegend();
                d3.selectAll("svg g image").style("opacity", 0);
                saveSvgAsPng(document.getElementById("chartdiv").firstChild, "diagram.png", function (image) {
                    d3.select("svg").attr("height", height + margin.top + margin.bottom);
                    d3.selectAll("svg g image").style("opacity", 0.1);
                    d3.selectAll(".export-temp").remove();
                    if (image !== null) {
                        //TODO: Add IE SUPPORT
                        //var svgElem = document.getElementById("chartdiv").innerHTML;
                        //document.getElementById("chartdiv").innerHTML = "";
                        //document.getElementById("chartdiv").appendChild(image);
                    }
                });
            });

        d3.select("svg g")
         .append("g:image")
         .attr("y", 60)
         .attr("x", width - 50)
         .attr("width", 40)
         .attr("height", 40)
         .style("opacity", 0.1)
         .attr("xlink:href", baseURL + "content/images/print-graph.png")
         .on("mouseover", function (d) {
             this.style.opacity = 0.5;
         })
         .on("mouseout", function (d) {
             this.style.opacity = 0.1;
         }).on("click", function () {
             AddLegend();
             d3.selectAll("svg g image").style("opacity", 0);
             $('#chartdiv').printElement();
             d3.select("svg").attr("height", height + margin.top + margin.bottom);
             d3.selectAll(".export-temp").remove();
             d3.selectAll("svg g image").style("opacity", 0.1);
         });

        if (GraphVM.graphType2() === null || GraphVM.graphType2() === "") {
            d3.selectAll(".axis2").remove();
        }
    }

    ////************************************************************
    //// Grahp reset zoom
    ////************************************************************
    Hammer(document.getElementById("chartdiv")).on("press", function () {

        d3.transition().duration(750).tween("zoom", function () {
            var ix = d3.interpolate(x.domain(), [dataChart.xMin, dataChart.xMax]),
                iy = d3.interpolate(y.domain(), [dataChart.yMin, dataChart.yMax]),
                iy2 = d3.interpolate(y2.domain(), [dataChart.yMin2, dataChart.yMax2]);
            return function (t) {
                zoom.x(x.domain(ix(t))).y(y.domain(iy(t)));
                zoom2.x(x.domain(ix(t))).y(y2.domain(iy2(t)));
                zoomed();
            };
        });
    });

    ////************************************************************
    //// Show temporary flock (swipe/scroll)
    ////************************************************************
    var id = $.cookie(window.location.pathname + "_graphId");
    if (id) {
        var index = GraphVM.flocksIds().indexOf(parseInt(id));
        GraphVM.flockIndex(index);
        var lastActive = GraphVM.flocks()[index];
        d3.selectAll(".flock_" + id).style("visibility", "visible");
        d3.selectAll(".stat_" + id).style("display", "inline-block");
        $("#legend_" + id).removeClass("inactive").addClass("active");
        $("#flock-swipe #flock-text").text(lastActive.name);
        $("#flock-swipe .flock-marker").css("background-color", GraphVM.GetColorById(lastActive.id));
    }
};

//************************************************************
// Hammer touch events
//************************************************************
Hammer(document.getElementById("flock-swipe")).on("tap", function () {
    $("#legend").toggle();
    setTimeout(function () {
        $("#flock-text")[0].scrollIntoView(true);
    }, 50);
});

Hammer(document.getElementById("flock-swipe")).on("swipeleft", function () {
    GraphVM.SetActiveFlock(GraphVM.flockIndex(), -1);
});

Hammer(document.getElementById("flock-swipe")).on("swiperight", function () {
    GraphVM.SetActiveFlock(GraphVM.flockIndex(), 1);
});

$(window).bind('resizeEnd', function () {
    GraphVM.DrawGraph(GraphVM.flockData());
});

$(window).resize(function () {
    if (this.resizeTO) clearTimeout(this.resizeTO);
    this.resizeTO = setTimeout(function () {
        $(this).trigger('resizeEnd');
    }, 200);
});

//************************************************************
// Helpers
//************************************************************
function upFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

//************************************************************
// Lib for export graph data to excel table
//************************************************************
var Export = {};
Export.GetData = function () {
    var datab = GraphVM.flockData();
    var data = GraphVM.flockData().data;
    var arr = [];
    if (!data || data.length === 0) {
        return null;
    }

    for (var i = 0; i < data.length; i++) {
        var elem = document.getElementById('legend_' + data[i].id);
        if (!elem || !elem.classList.contains("active")) {
            continue;
        }
       
        arr.push({
            day: 0,
            y: datab.yName,
            y2: datab.yName2,
            name: data[i].name + " " + data[i].sex
        });

        for (var j = 0; j < data[i].data.length; j++) {
            arr.push({
                day: data[i].data[j].x,
                y: data[i].data[j].y,
                y2: data[i].data[j].y2,
                name: data[i].name + " " + data[i].sex
            });
        }
    }
    var arr2 = [];
    for (var day = 0; day < datab.xMax; day++) {
        arr2[day] = [];
        for (var y = 0; y < arr.length; y++) {
            if (day === arr[y].day) {
                arr2[day].push({
                    name: arr[y].name,
                    y: arr[y].y,
                    y2: arr[y].y2
                });
            };
        }
    }
    return arr2;
};

Export.CreateTable = function (data, extended) {
    var headers = data[0];
    var tableValues = [];
    var row = 0;
    tableValues[row] = [];
    tableValues[row][0] = GraphVM.flockData().xName;
    for (var title = 0; title < headers.length; title++) {
        tableValues[row][title + 1] = headers[title].name;
    }
    for (var f = 1; f < data.length; f++) {
        if (data[f].length === 0) {
            continue;
        }
        row++;
        tableValues[row] = [];
        //add columns
        tableValues[row][0] = f;
        for (var header = 0; header < headers.length; header++) {
            var val = null;
            for (var col = 0; col < data[f].length; col++) {
                if (headers[header].name === data[f][col].name) {
                    val = data[f][col];
                }
            }
            if (val) {
                if (!extended) {
                    tableValues[row][header + 1] = val.y;
                } else {
                    tableValues[row][header + 1] = val.y2;
                }
            } else {
                tableValues[row][header + 1] = "";
            }
        }
    }
   
    return tableValues;
};

Export.Export = function () {
    function datenum(v, date1904) {
        if (date1904) v += 1462;
        var epoch = Date.parse(v);
        return (epoch - new Date(Date.UTC(1899, 11, 30))) / (24 * 60 * 60 * 1000);
    }

    function sheet_from_array_of_arrays(data, opts) {
        var ws = {};
        var range = { s: { c: 10000000, r: 10000000 }, e: { c: 0, r: 0 } };
        for (var R = 0; R !== data.length; ++R) {
            for (var C = 0; C !== data[R].length; ++C) {
                if (range.s.r > R) range.s.r = R;
                if (range.s.c > C) range.s.c = C;
                if (range.e.r < R) range.e.r = R;
                if (range.e.c < C) range.e.c = C;
                var cell = { v: data[R][C] };
                if (cell.v === null) continue;
                var cell_ref = XLSX.utils.encode_cell({ c: C, r: R });

                if (typeof cell.v === 'number') cell.t = 'n';
                else if (typeof cell.v === 'boolean') cell.t = 'b';
                else if (cell.v instanceof Date) {
                    cell.t = 'n'; cell.z = XLSX.SSF._table[14];
                    cell.v = datenum(cell.v);
                }
                else cell.t = 's';

                ws[cell_ref] = cell;
            }
        }
        if (range.s.c < 10000000) ws['!ref'] = XLSX.utils.encode_range(range);
        return ws;
    }

    /* original data */
    var rawdata = Export.GetData();
    var data = Export.CreateTable(rawdata);
    var data2 = Export.CreateTable(rawdata, true);


    function Workbook() {
        if (!(this instanceof Workbook)) return new Workbook();
        this.SheetNames = [];
        this.Sheets = {};
    }

    var sourceData = GraphVM.flockData();
    var ws_name = sourceData.yName;
    var ws_name2 = sourceData.yName2;

    var wb = new Workbook(), ws = sheet_from_array_of_arrays(data);
    var ws2 = null;
    if (ws_name2 !== "") {
        ws2 = sheet_from_array_of_arrays(data2);
    }

    /* add worksheet to workbook */
    wb.SheetNames.push(ws_name);
    wb.Sheets[ws_name] = ws;

    if (ws2) {
        wb.SheetNames.push(ws_name2);
        wb.Sheets[ws_name2] = ws2;
    }
    var wbout = XLSX.write(wb, { bookType: 'xlsx', bookSST: true, type: 'binary' });

    function s2ab(s) {
        var buf = new ArrayBuffer(s.length);
        var view = new Uint8Array(buf);
        for (var i = 0; i !== s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
        return buf;
    }

    saveAs(new Blob([s2ab(wbout)], { type: "application/octet-stream" }), "data.xlsx");
};

//************************************************************
// Collor galery
//************************************************************
var colors = [
       'brown', 'darkgoldenrod', 'burlywood', 'cadetblue', 'chartreuse', 'chocolate',
       'coral', 'cornflowerblue', 'crimson', 'darkblue', 'darkcyan', 'darkgray', 'darkgreen', 'darkkhaki',
       'darkmagenta', 'darkolivegreen', 'darkorange', 'darkorchid', 'darkred', 'darksalmon', 'darkseagreen', 'darkslategray',
       'darkturquoise', 'darkviolet', 'deeppink', 'dimgray', 'dimgrey', 'dodgerblue', 'firebrick', 'forestgreen', 'fuchsia',
       'gainsboro', 'gold', 'gray', 'green', 'greenyellow', 'grey', 'hotpink', 'indianred', 'indigo', 'khaki', 'lawngreen',
       'lightblue', 'lightcoral', 'lightgreen', 'lightgrey', 'lightpink', 'lightsalmon', 'lightseagreen', 'lightskyblue',
       'lightslategray', 'lightsteelblue', 'lightyellow', 'limegreen', 'linen', 'magenta', 'maroon', 'mediumaquamarine',
       'mediumblue', 'mediumorchid', 'mediumseagreen', 'mediumslateblue', 'mediumspringgreen', 'mediumturquoise',
       'mediumvioletred', 'midnightblue', 'mistyrose', 'navajowhite', 'navy', 'olive', 'olivedrab', 'orange', 'orangered',
       'orchid', 'palegoldenrod', 'palegreen', 'paleturquoise', 'palevioletred', 'papayawhip', 'peachpuff', 'peru', 'pink',
       'plum', 'powderblue', 'purple', 'red', 'rosybrown', 'royalblue', 'saddlebrown', 'salmon', 'sandybrown', 'seagreen',
       'sienna', 'silver', 'skyblue', 'slateblue', 'slategray', 'springgreen', 'steelblue', 'tan', 'teal', 'thistle', 'tomato',
       'turquoise', 'violet', 'wheat', 'yellow', 'yellowgreen'];