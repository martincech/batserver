﻿/* VIEWMODELS */
var creatingNewScale = false;
var actualTable;
var actualHeght = 305;

ko.validation.locale($.cookie("_culture"));
ko.validation.init({
    decorateElement: true
});

/* VM for actual flock */
var ActualFlock = {
   id: ko.observable(),
   name: ko.observable().extend({
      required: true,
      minLength: 1
   }),
   scaleid: ko.observable().extend({
      required: {
         onlyIf: function () {
            return !creatingNewScale;
         }
      }
   }),
   startDate: ko.observable().extend({
      required: true,
      formattedDate: { format: 'dmy', delimiter: '.' }
   }),
   note: ko.observable(),
   newScale: ko.observable().extend({
      required: {
         onlyIf: function () {
            return creatingNewScale;
         }
      },
      minLength: 3
   }),
   curveId: ko.observable(),
   curveFemId: ko.observable(),
   curves: ko.observableArray()
};

/* VM for closing date */
var ClosedDate = {
   endDate: ko.observable().extend({
      required: true,
      formattedDate: { format: 'dmy', delimiter: '.' }
   })
}

/* VM for Actual flocks object */
var ActualFlocksVM = {
   actualSelected: ko.observableArray(),
   actualSelectedCount: ko.observable(0),
   actualFlock: ko.observable(ActualFlock),
   totalActualRows: ko.observable(0),
   scales: ko.observableArray(),
   closedDate: ko.observable(ClosedDate)
};

/* FUNCTIONS */

var calcDataTableHeight = function () {
    var height = $(window).height() - 303;
    if (height < 427) {
        height = 427;
    }
    return height;
};

//Check if value has hex number format
function IsHex(hex) {
   var number = parseInt(hex, 16);
   $("#scale-exist").css("display", "none");
   if (number.toString(16) === hex.toLowerCase()) {
      return number < parseInt("7FFFFFFF", 16);
   }
   return (number.toString(16) === hex.toLowerCase());
}

var createActualTable = function() {
   // inicialization
   if (typeof actualTable == 'undefined') {
      actualTable = $('#actualFlocksTable').dataTable({
         "language": {
             "url": baseURL+"Content/DataTables/Internationalisation/" + $.cookie("_culture") + ".txt"
         },
         "sScrollY": calcDataTableHeight(),
         "sScrollX": "100%",
         "sScrollXInner": "99.99%",
         "bServerSide": true,
         "sAjaxSource": baseURL + "Flocks/ActualFlocksHandler",
         "bProcessing": true,
         "sDom": "frtiS",
         "fnRowCallback": function(nRow, aData, iDisplayIndex) {
            if (jQuery.inArray(aData.Id, ActualFlocksVM.actualSelected()) != -1) {
               $(nRow).addClass('row_selected');
            }
            return nRow;
         },

         "fnDrawCallback": function() {
            ActualFlocksVM.totalActualRows(actualTable.fnGetData().length);
            $("#actualRowsCount").text("[" + ActualFlocksVM.totalActualRows() + "]");
         },

         "fnInitComplete": function(oSettings, json) {
            ActualFlocksVM.totalActualRows(actualTable.fnGetData().length);
            $("#actualRowsCount").text("[" + ActualFlocksVM.totalActualRows() + "]");

         },
         "aoColumns": [
            {
               "bVisible": false,
               "bSearchable": false,
               "bSortable": false,
               "data": "Id"

            },
            { "data": "Name" },
            {
               "data": "Scale.Name"
            },
            {
               "render": function(data, type, row) {
                  var x = new Date(data);

                  return $.datepicker.formatDate('dd.mm.yy', x);
               },
               "data": "StartDate"
            },
            {
               "data": "MaxDay",
               "bSearchable": false,
               "bSortable": false
            }
           
         ]
      });
   } else {
      $(window).trigger('resize');
      actualTable.fnDraw();
      $(window).trigger('resize');
   }
};

var getSelectorValues = function(isEditMode) {
   getScales(isEditMode);
};

var getScales = function(isEditMode) {
   JsActions.Flocks.GetScales({
      success: function (data) {
         data.forEach(function (item) {
            var name = item.Name;
            //if (IsHex(name)) {
            //   name = parseInt(name, 16);
            //}
            item.Name = name;
         });
         ActualFlocksVM.scales(data);
         getCurves(isEditMode);
      }
   });
}

var getCurves = function(isEditMode) {
   JsActions.Flocks.GetCurves({
      success: function(data) {
         ActualFlocksVM.actualFlock().curves(data);
         if (isEditMode) getActualFlock();
      }
   });
}

// delete flocks from database
ActualFlocksVM.deleteFlocks = function() {
   if (ActualFlocksVM.actualSelected().length > 0) {
      JsActions.Flocks.DeleteFlocks(
         ActualFlocksVM.actualSelected(),
         {
            success: function() {
               actualTable.fnDraw();
               ActualFlocksVM.actualSelectedCount(0);
               ActualFlocksVM.actualSelected.removeAll();
               $('.modal').modal('hide');
            }
         });
   }
};

// close selected flocks with giving end date
ActualFlocksVM.closeFlocks = function() {
   if (ActualFlocksVM.closedDateErrors().length == 0) {
      JsActions.Flocks.CloseFlocks(
         ActualFlocksVM.actualSelected(),
         ActualFlocksVM.closedDate().endDate,
         {
            success: function(result) {
               actualTable.fnDraw(true);
               ActualFlocksVM.actualSelectedCount(0);
               ActualFlocksVM.actualSelected.removeAll();
               $('.modal').modal('hide');
            }
         });
   } else {
      ActualFlocksVM.closedDateErrors.showAllMessages();
   }
};

// get flock according to its id
ActualFlocksVM.getFlock = function() {

   if (ActualFlocksVM.actualSelected().length == 1) {
      ActualFlocksVM.makeActualDefaults();
      getSelectorValues(true);
   }
};

var getActualFlock = function() {
   JsActions.Flocks.GetFlock(
      ActualFlocksVM.actualSelected(),
      {
         success: function(result) {
            ActualFlock.id(result.Id);
            ActualFlock.name(result.Name);
            var x = new Date(result.StartDate);
            ActualFlock.startDate($.datepicker.formatDate('dd.mm.yy', x));
            ActualFlock.scaleid(result.ScaleId);
            ActualFlock.note(result.Note);
            ActualFlock.curveId(result.CurveId);
            ActualFlock.curveFemId(result.CurveFemId);
            ActualFlocksVM.actualFlock(ActualFlock);
         }
      });
};

ActualFlocksVM.createFlock = function() {
   if (ActualFlocksVM.errors().length == 0) {
      JsActions.Flocks.Create(
         this.actualFlock(),
         {
            success: function(result) {
               if (result.error != null) {
                  alert(result.error);
                  return;
               }
               actualTable.fnDraw(true);
               $('.modal').modal('hide');
            }
         });
   } else {
      ActualFlocksVM.errors.showAllMessages();
   }
};

ActualFlocksVM.editFlock = function() {
   if (ActualFlocksVM.errors().length == 0) {
      JsActions.Flocks.Edit(
         this.actualFlock(),
         {
            success: function (result) {
               if (result.error != null) {
                  alert(result.error);
                  return;
               }
               actualTable.fnDraw(true);
               $('.modal').modal('hide');
            }
         });
   } else {
      ActualFlocksVM.errors.showAllMessages();
   }
};

ActualFlocksVM.beforeCreateFlock = function() {
   ActualFlocksVM.makeActualDefaults();
   getSelectorValues(false);
};

ActualFlocksVM.makeActualDefaults = function(obj) {
   if (obj == undefined) {
      obj = {};
      obj.id = undefined;
      obj.name = undefined;
      obj.newScale = undefined;
      obj.scaleid = $("#scaleSelector option:first").val();
      obj.curveId = undefined;
      obj.curveFemId = undefined;
      obj.startDate = undefined;
      obj.note = undefined;
   }
   creatingNewScale = false;
   ActualFlocksVM.actualFlock().id(obj.id);
   ActualFlocksVM.actualFlock().name(obj.name);
   ActualFlocksVM.actualFlock().newScale(obj.newScale);
   ActualFlocksVM.actualFlock().scaleid(obj.scaleid);
   ActualFlocksVM.actualFlock().curveId(obj.curveId);
   ActualFlocksVM.actualFlock().curveFemId(obj.curveFemId);
   ActualFlocksVM.actualFlock().startDate(obj.startDate);
   ActualFlocksVM.actualFlock().note(obj.note);

   $("#scaleSelectContainer").css("display", "block");
   $("#newScaleName").css("display", "none");
};

ActualFlocksVM.makeClosedDateDefaults = function() {
   ActualFlocksVM.closedDate().endDate(undefined);
};

ActualFlocksVM.createScale = function() {
   ActualFlocksVM.unbind();
   $("#scaleSelectContainer").css("display", "none");
   ActualFlocksVM.actualFlock().scaleid("");
   ActualFlocksVM.actualFlock().newScale("");
   $("#newScaleName").css("display", "inline-block");
};

ActualFlocksVM.unbind = function() {
   // unbind the scale selector from datasourcec
   ActualFlocksVM.scales(undefined);
   creatingNewScale = true;
}

ActualFlocksVM.actualFlock().scaleid.subscribe(function() {
   if (creatingNewScale == true) {
      ActualFlocksVM.actualFlock().newScale(ActualFlocksVM.actualFlock().scaleid);
   };
});

/* OnDOM ready */

$(document).ready(function () {
   
   createActualTable();
   $("#cStartDate").datepicker({ dateFormat: "dd.mm.yy" });
   $("#eStartDate").datepicker({ dateFormat: "dd.mm.yy" });
   $("#endDate").datepicker({ dateFormat: "dd.mm.yy" });
   $('.validationMessage').tooltip();

   var lastChecked;
   $('#actualFlocksTable tbody').on("click", "tr", function(event) {

      if (!lastChecked) {
         lastChecked = this;
      }
      var aData;
      var iId;
      if (event.shiftKey) {
         var start = $('#actualFlocksTable tbody tr').index(this);
         var end = $('#actualFlocksTable tbody tr').index(lastChecked);

         for (var i = Math.min(start, end); i <= Math.max(start, end); i++) {
            if (!$('#actualFlocksTable tbody tr').eq(i).hasClass('row_selected')) {
               aData = actualTable.fnGetData(i);
               iId = aData.Id;
               if (jQuery.inArray(iId, ActualFlocksVM.actualSelected()) == -1) {
                  ActualFlocksVM.actualSelected()[ActualFlocksVM.actualSelected().length++] = iId;
                  ActualFlocksVM.actualSelectedCount(ActualFlocksVM.actualSelectedCount() + 1);
               }

               $('#actualFlocksTable tbody tr').eq(i).addClass("row_selected");
            }
         }

         // Clear browser text selection mask
         if (window.getSelection) {
            if (window.getSelection().empty) { // Chrome
               window.getSelection().empty();
            } else if (window.getSelection().removeAllRanges) { // Firefox
               window.getSelection().removeAllRanges();
            }
         } else if (document.selection) { // IE?
            document.selection.empty();
         }
      } else {
         aData = actualTable.fnGetData(this);
         iId = aData.Id;
         $(this).toggleClass('row_selected');

         if (jQuery.inArray(iId, ActualFlocksVM.actualSelected()) == -1) {
            ActualFlocksVM.actualSelected()[ActualFlocksVM.actualSelected().length++] = iId;
            ActualFlocksVM.actualSelectedCount(ActualFlocksVM.actualSelectedCount() + 1);
         } else {

            ActualFlocksVM.actualSelectedCount(ActualFlocksVM.actualSelectedCount() - 1);
            ActualFlocksVM.actualSelected(jQuery.grep(ActualFlocksVM.actualSelected(), function(value) {
               return value != iId;
            }));
         }
      }
      lastChecked = this;
   });

   // tab with actual flocks loader
   $('#actualFlocks a').click(function(e) {
      $("#closedRowsCount").text("");
      e.preventDefault();
      $(this).tab('show');
      createActualTable();
   });

   CreateMouseWheelBinding("#select01");
   CreateMouseWheelBinding("#editMaleDefCurve");
   CreateMouseWheelBinding("#editFemaleDefCurve");

   CreateMouseWheelBinding("#scaleSelector");
   CreateMouseWheelBinding("#createMaleDefCurve");
   CreateMouseWheelBinding("#createFemaleDefCurve");
});

ActualFlocksVM.errors = ko.validation.group(ActualFlock);
ActualFlocksVM.closedDateErrors = ko.validation.group(ClosedDate);
ko.applyBindings(ActualFlocksVM, document.getElementById("actualFlocksForm"));