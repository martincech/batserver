﻿/* VIEWMODELS */
var weighingsTable;

var WeighingsVM = {
    scales: ko.observableArray(),
    from: ko.observable().extend({
        formattedDate: { format: 'dmy', delimiter: '.' }
    }),
    to: ko.observable().extend({
        formattedDate: { format: 'dmy', delimiter: '.' }
    }),
    sex: ko.observable(),
    scaleId: ko.observable(),
    weighingSelected: ko.observableArray(),
    weighingSelectedCount: ko.observable(0)
};

/* FUNCTIONS */

var calcDataTableHeight = function () {
    var height = $(window).height() - 387;
    if (height < 427) {
        height = 427;
    }
    return height;
};

//Check if value has hex number format
function IsHex(hex) {
   var number = parseInt(hex, 16);
   $("#scale-exist").css("display", "none");
   if (number.toString(16) === hex.toLowerCase()) {
      return number < parseInt("7FFFFFFF", 16);
   }
   return (number.toString(16) === hex.toLowerCase());
}

var createWeighingsStatTable = function () {
    if (typeof weighingsTable === 'undefined') {
        weighingsTable = $('#weighingTable').dataTable({
            "language": {
                "url": baseURL + "Content/DataTables/Internationalisation/" + $.cookie("_culture") + ".txt"
            },
            "sScrollY": calcDataTableHeight(),
            "sScrollX": "100%",
            "sScrollXInner": "99.99%",
            "bScrollCollapse": false,
            "bServerSide": true,
            "sAjaxSource": baseURL + "Weighings/WeighingsHandler",
            "bProcessing": true,
            "sDom": "frtiS",
            "fnRowCallback": function (nRow, aData, iDisplayIndex) {
                if (jQuery.inArray(aData[0], WeighingsVM.weighingSelected()) !== -1) {
                    $(nRow).addClass('row_selected');
                }
                return nRow;
            },
            "fnServerParams": function (aoData) {
                aoData.push(
                {
                    "name": "scaleId",
                    "value": WeighingsVM.scaleId()
                },
                {
                    "name": "from",
                    "value": WeighingsVM.from()
                },
                {
                    "name": "to",
                    "value": WeighingsVM.to()
                },
                {
                    "name": "sex",
                    "value": WeighingsVM.sex()
                });
            },
            aaSorting: [[0, 'desc']],
            "aoColumns": [
               {
                   "mDataProp": "Id",
                   "bSearchable": false,
                   "bSortable": false,
                   "bVisible": false
               },
               {
                   "render": function (data, type, row) {
                       var x = new Date(data);

                       return $.datepicker.formatDate('dd.mm.yy', x);
                   },
                   "data": "Stat.Date",
                   "bSearchable": false,
                   "bSortable": true,
               },
               {
                  "mDataProp": "Stat.Scale.Name"
               },
               { "mDataProp": "Stat.Day" },
               { "mDataProp": "Stat.Count" },
               { "mDataProp": "Stat.Average" },
               { "mDataProp": "Stat.Gain" },
               { "mDataProp": "Stat.Sigma" },
               { "mDataProp": "Stat.Cv" },
               {
                   "bSearchable": false,
                   "bSortable": false,
                   "mDataProp": "Stat.Uni"
               },
               { "mDataProp": "Stat.Temperature", "bVisible": false },
               { "mDataProp": "Stat.CarbonDioxide", "bVisible": false },
               { "mDataProp": "Stat.Ammonia", "bVisible": false },
               { "mDataProp": "Stat.Humidity", "bVisible": false },
               {
                   "render": function (data, type, row) {
                       if (data === 0) {
                           return $("#SexMale").val();
                       }

                       if (data === 1) {
                           return $("#SexFemale").val();
                       }
                       return "";
                   },
                   "bSearchable": false,
                   "bSortable": false,
                   "mDataProp": "Stat.Sex"
               }
            ]
        });
        $(window).trigger('resize');
    } else {
        $(window).trigger('resize');
        weighingsTable.fnDraw();
        $(window).trigger('resize');
    }
};

var getScales = function () {
    JsActions.Flocks.GetScales({
       success: function (data) {
          data.forEach(function (item) {
             var name = item.Name;
             //if (IsHex(name)) {
             //   name = parseInt(name, 16);
             //}
             item.Name = name;
          });
          WeighingsVM.scales(data);
        }
    });
};

WeighingsVM.from.subscribe(function () {
    if (WeighingsVM.from.isValid()) {
        WeighingsVM.refreshBindings();
    }
});

WeighingsVM.to.subscribe(function () {
    if (WeighingsVM.to.isValid()) {
        WeighingsVM.refreshBindings();
    }
});

var sexSubscribe = function () {
    $("#sexSelection").val($("#sexSelection option:first").val());

    $("#sexSelection").change(function () {
        if ($("#sexSelection").val() === $("#sexSelection option:first").val()) {
            WeighingsVM.sex('');
        } else {
            WeighingsVM.sex($(this).val());
        }
    });
};

WeighingsVM.scaleId.subscribe(function () {
    WeighingsVM.refreshBindings();
});

WeighingsVM.refreshBindings = function () {
    weighingsTable.fnDraw();
};

WeighingsVM.sex.subscribe(function () {
    WeighingsVM.refreshBindings();
});


WeighingsVM.clearFilters = function () {
    WeighingsVM.from(undefined);
    WeighingsVM.to(undefined);
    WeighingsVM.sex(undefined);
    WeighingsVM.scaleId(undefined);
    $("#sexSelection").val($("#sexSelection option:first").val());
    $("#scaleSelection").val($("#scaleSelection option:first").val());
};

// delete weighings from database
WeighingsVM.deleteWeighings = function () {
    if (WeighingsVM.weighingSelected().length > 0) {
        JsActions.Weighings.DeleteStats(
           WeighingsVM.weighingSelected(),
           {
               success: function () {
                   weighingsTable.fnDraw();
                   WeighingsVM.weighingSelectedCount(0);
                   WeighingsVM.weighingSelected.removeAll();
                   $('.modal').modal('hide');
               }
           });
    }
};

// delete weighings from database
WeighingsVM.importStats = function (data) {
    // document.getElementById('importFiles').submit();
};


// export shown weighings from database
WeighingsVM.exportWeighings = function () {
    var params = weighingsTable.oApi._fnAjaxParameters(weighingsTable.dataTable().fnSettings());
    params.push(
    {
        "name": "scaleId",
        "value": WeighingsVM.scaleId()
    },
    {
        "name": "from",
        "value": WeighingsVM.from()
    },
    {
        "name": "to",
        "value": WeighingsVM.to()
    },
    {
        "name": "sex",
        "value": WeighingsVM.sex()
    },
    {
        "name": "isFlock",
        "value": "false"
    });
    location.href = baseURL + 'Flocks/ExportStats?' + $.param(params);
};

/* OnDOM ready */

$(window).resize(function () {
    $('.dataTables_scrollBody').css('height', calcDataTableHeight());
});

$(document).ready(function () {
    createWeighingsStatTable();
    sexSubscribe();
    getScales();

    $('#importFilesButton').on('click', function () {

        var data = new FormData();
        var files = $("#fileUploadFemale").get(0).files;
        // Add the uploaded image content to the form data collection

        if (files.length > 0 && $('#fileUploadFemale').next().text()) {
            data.append("femaleData", files[0]);
        } else {
            $('#errFileMissing').show();
            return;
        }

        if ($("#sharedTitle").is(":hidden") && $('#fileUploadMale').next().text()) {
            files = $("#fileUploadMale").get(0).files;
            data.append("maleData", files[0]);
        }

        var selectedVal = "";
        var selected = $("#radioUnit input[type='radio']:checked");
        if (selected.length > 0) {
            selectedVal = selected.val();
        }
        data.append("unit", selectedVal);

        $("#progresBar").css('display', 'block');

        // Make Ajax request with the contentType = false, and procesDate = false
        var ajaxRequest = $.ajax({
            type: "POST",
            url: baseURL + "Weighings/ImportStats",
            contentType: false,
            processData: false,
            data: data,
            error: function (message) {
                WeighingsVM.ShowlDialog(message.responseJSON, false);
            },
            success: function (message) {
                var imported = parseInt(message.Imported);
                var notImported = parseInt(message.NotImported);

                var messageString = "";

                if (imported > 0) {
                    var imp = $("#Imported").html();
                    messageString = imp.replace("{0}", imported) + "<br/>";
                }

                if (notImported > 0) {
                    imp = $("#NotImported").html();
                    messageString += imp.replace("{0}", imported);
                }

                WeighingsVM.ShowlDialog(messageString, true);
            }
        });
    });


    WeighingsVM.ShowlDialog = function (message, reload) {
        $("#progresBar").css('display', 'none');
        $("#dialogResult").html(message);
        $("#dialog-confirm").modal('show');
        $("#dialog-confirm").zIndex($("#importFiles").zIndex() + 2);
        $("div:last").zIndex($("#importFiles").zIndex() + 1);
        $('#dialog-confirm').on('hidden.bs.modal', function () {
            if (reload) {
                location.reload();
            }
        });
    }

    var lastChecked;
    $('#weighingTable tbody').on("click", "tr", function (event) {

        if (!lastChecked) {
            lastChecked = this;
        }
        var aData;
        var iId;
        if (event.shiftKey) {
            var start = $('#weighingTable tbody tr').index(this);
            var end = $('#weighingTable tbody tr').index(lastChecked);

            for (var i = Math.min(start, end) ; i <= Math.max(start, end) ; i++) {
                if (!$('#weighingTable tbody tr').eq(i).hasClass('row_selected')) {
                    aData = weighingsTable.fnGetData(i);
                    iId = aData.Id;
                    if (jQuery.inArray(iId, WeighingsVM.weighingSelected()) === -1) {
                        WeighingsVM.weighingSelected()[WeighingsVM.weighingSelected().length++] = iId;
                        WeighingsVM.weighingSelectedCount(WeighingsVM.weighingSelectedCount() + 1);
                    }

                    $('#weighingTable tbody tr').eq(i).addClass("row_selected");
                }
            }

            // Clear browser text selection mask
            if (window.getSelection) {
                if (window.getSelection().empty) { // Chrome
                    window.getSelection().empty();
                } else if (window.getSelection().removeAllRanges) { // Firefox
                    window.getSelection().removeAllRanges();
                }
            } else if (document.selection) { // IE?
                document.selection.empty();
            }
        } else {
            aData = weighingsTable.fnGetData(this);
            iId = aData.Id;
            $(this).toggleClass('row_selected');

            if (jQuery.inArray(iId, WeighingsVM.weighingSelected()) === -1) {
                WeighingsVM.weighingSelected()[WeighingsVM.weighingSelected().length++] = iId;
                WeighingsVM.weighingSelectedCount(WeighingsVM.weighingSelectedCount() + 1);
            } else {

                WeighingsVM.weighingSelectedCount(WeighingsVM.weighingSelectedCount() - 1);
                WeighingsVM.weighingSelected(jQuery.grep(WeighingsVM.weighingSelected(), function (value) {
                    return value !== iId;
                }));
            }
        }
        lastChecked = this;
    });
});

WeighingsVM.errors = ko.validation.group([WeighingsVM.from, WeighingsVM.to]);
ko.applyBindings(WeighingsVM, document.getElementById("weighingsForm"));

$('input[type=file]').change(function (e) {
    $in = $(this);
    $in.next().html($in.val().replace(/.*(\/|\\)/, ''));
    if ($('#fileUploadFemale').next().text()) {
        $('#errFileMissing').hide();
    }
});

$('#clearForm').click(function () {
    $('#uploadForm').trigger('reset');
    $in = $('#fileUploadMale');
    $in.next().html('');
    $in = $('#fileUploadFemale');
    $in.next().html('');
});

$('#showMale').click(function () {
    $('#femaleTitle').css('display', 'block');
    $('#sharedTitle').hide();
    $(this).hide();
    $('#maleImport').show();
});

$('#hideMale').click(function () {
    $('#femaleTitle').css('display', 'none');
    $('#sharedTitle').show();
    $('#showMale').show();
    $('#maleImport').hide();
});

$('#dialog-ok').click(function () {
    $("#dialog-confirm").modal('hide');
});

$('#dialog-close').click(function () {
    $("#dialog-confirm").modal('hide');
});