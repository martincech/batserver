﻿var chart;
var activeGraph;
var curveIndex;
var dataProvider;
var gl = globals[$.cookie("_culture")];

var dataChart;

/* VIEWMODELS */
var GraphVM = {
    flocksIds: ko.observableArray(),
    allFlocksIds: ko.observableArray(),
    flocksNames: ko.observableArray(),
    flocksNameList: ko.observableArray(),
    from: ko.observable().extend({
        formattedDate: { format: 'dmy', delimiter: '.' }
    }),
    to: ko.observable().extend({
        formattedDate: { format: 'dmy', delimiter: '.' }
    }),
    sex: ko.observable(),
    curves: ko.observableArray(),
    curveId: ko.observable(),
    flockId: ko.observable()
};

/* FUNCTIONS */

GraphVM.clearFilters = function () {
    GraphVM.from(undefined);
    GraphVM.to(undefined);
    GraphVM.sex(undefined);
    GraphVM.flockId(undefined);
    GraphVM.flocksIds.removeAll();

    ko.utils.arrayForEach(GraphVM.allFlocksIds(), function (item) {
        GraphVM.flocksIds.push(item);
    });

    $("#sexSelection").val($("#sexSelection option:first").val());
    $("#flockSelection").val($("#flockSelection option:first").val());
};

GraphVM.refreshBindings = function () {
    statTable.fnDraw();
    run($(".graphTypesMenu li.selected a").attr('title'));
};

GraphVM.from.subscribe(function () {
    if (GraphVM.from.isValid()) {
        GraphVM.refreshBindings();
    }
});

GraphVM.to.subscribe(function () {
    if (GraphVM.to.isValid()) {
        GraphVM.refreshBindings();
    }
});

GraphVM.sex.subscribe(function () {
    GraphVM.refreshBindings();
});

GraphVM.flockId.subscribe(function () {
    GraphVM.refreshBindings();
});

var sexSubscribe = function () {
    $("#sexSelection").val($("#sexSelection option:first").val());
    $("#sexSelection").change(function () {
        if ($("#sexSelection").val() == $("#sexSelection option:first").val()) {
            GraphVM.sex('');
        } else {
            GraphVM.sex($(this).val());
        }
    });
};

//var getAvailableCurves = function () {
//    JsActions.Flocks.GetCurves(
//       {
//           success: function (result) {
//               GraphVM.curves(result);
//           }
//       });
//};

var getGraphData = function (graphType) {
    JsActions.Flocks.GetGraphData(
       GraphVM.flocksIds(),
       graphType,
       { from: GraphVM.from, to: GraphVM.to, sex: GraphVM.sex, flockId: GraphVM.flockId },
       {
           success: function (result) {
               drawGraph(result);
           }
       });
};

var getCurveData = function (graphType) {
    JsActions.Flocks.GetCurveGraphData(
        0,
        GraphVM.flocksIds(),
        { statType: graphType, from: GraphVM.from, to: GraphVM.to, sex: GraphVM.sex, flockId: GraphVM.flockId },
        {
            success: function (result) {
                drawGraph(result);
            },
            error: function (res) {
                drawGraph("");
            }
        }
    );
}

var ChangeSelectedFlock = function (flockId, selected) {
        JsActions.Flocks.ChangeSelectedFlock(flockId,selected,{});
}

var run = function (graphType) {
    var params = statTable.oApi._fnAjaxParameters(statTable.dataTable().fnSettings());
    params.push(
    {
        "name": "selectedFlocks",
        "value": GraphVM.flocksIds().toString()
    },
    {
        "name": "from",
        "value": GraphVM.from()
    },
    {
        "name": "to",
        "value": GraphVM.to()
    },
    {
        "name": "sex",
        "value": GraphVM.sex()
    },
    {
        "name": "isFlock",
        "value": "true"
    },
    {
        "name": "flockId",
        "value": GraphVM.flockId()
    });

    $.ajax({
        type: 'GET',
        url: baseURL + "Flocks/GetFlockNames",
        data: params,
        dataType: 'json',
        cache: false,
        success: function (result) {
            GraphVM.flocksNames.removeAll();
            $(result).each(function () {
                GraphVM.flocksNames.push(this);
            });
            if (graphType === $(".graphTypesMenu li a").first().attr('title')) {
                getCurveData(graphType);
            } else {
                getGraphData(graphType);
            }

        }
    });
};

/* Am charts definitions */


AmCharts.exportPrint = {
    "format": "PRINT",
    "label": gl.print
};

var drawGraph = function (result) {

    //************************************************************
    // Data notice the structure
    //************************************************************
    GraphVM.curves.removeAll();
    
    var data = result.data;
    dataChart = result;
    d3.select("#legend").selectAll("div").remove();
    d3.select("#chartdiv").select("svg").remove();


    //************************************************************
    // Create Margins and Axis and hook our zoom function
    //************************************************************
    var margin = { top: 20, right: 10, bottom: 30, left: 50 },
        width = $("#chartdiv").width() - margin.left - margin.right,
        height = $("#chartdiv").height() - margin.top - margin.bottom;

    var x = d3.scale.linear()
        .domain([result.xMin, result.xMax])
        .range([0, width]);

    var y = d3.scale.linear()
        .domain([result.yMin, result.yMax])
        .range([height,0]);

    var xAxis = d3.svg.axis()
        .scale(x)
       .tickSize(-height)
       .tickPadding(10)
       .tickSubdivide(true)
        .orient("bottom");

    var yAxis = d3.svg.axis()
        .scale(y)
       .tickPadding(10)
       .tickSize(-width)
       .tickSubdivide(true)
        .orient("left");

    var zoom = d3.behavior.zoom()
        .x(x)
        .y(y)
        .scaleExtent([1, 10])
        .on("zoom", zoomed);





    //************************************************************
    // Generate our SVG object
    //************************************************************	
    var svg = d3.select("#chartdiv").append("svg")
       .call(zoom)
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
       .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    svg.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis);

    svg.append("g")
        .attr("class", "y axis")
        .call(yAxis);

    svg.append("g")
       .attr("class", "y axis")
       .append("text")
       .attr("class", "axis-label")
       .attr("transform", "rotate(-90)")
       .attr("y", (-margin.left) + 10)
       .attr("x", -height / 2)
       .text(dataChart.yName);

    svg.append("clipPath")
       .attr("id", "clip")
       .append("rect")
       .attr("width", width)
       .attr("height", height);





    //************************************************************
    // Create D3 line object and draw data on our SVG object
    //************************************************************
    var line = d3.svg.line()
        .interpolate("linear")
        .defined(function(d) {
            return d != null;
        })
        .x(function(d) {
             return x(d.x);
        })
        .y(function(d) {
         return y(d.y);
    });



    svg.selectAll('.line')
       .data(data)
       .enter()
       .append("path")
        .attr("class", function (d, i) {
            if (d.isGrowthCurve) {
                return "line curve curve_" + d.id;
            } else {
                return "line flock_" + d.id;
            }
        })

       .attr("clip-path", "url(#clip)")
       .attr("style", "visibility:hidden")
       .attr('stroke', function (d, i) {
           return colors[i % colors.length];
       })
        .attr("d", function (d) { return line(d.data); });

    //************************************************************
    // Draw points on SVG object based on the data given
    //************************************************************
    var points = svg.selectAll('.dots')
        .data(data)
        .enter()
        .append("g")
        .attr("style", "visibility:hidden")
        .attr("class", function (d) {
            if (d.isGrowthCurve) {
                return "dots curve curve_" + d.id;
            } else {
                return "dots flock_" + d.id;
            }
        })
       .attr("clip-path", "url(#clip)");

    points.selectAll('.dot')
       .data(function (d, index) {
           var a = [];
           d.data.forEach(function (point, i) {
               a.push({ 'index': index, 'point': point });
           });
           return a;
       })
       .enter()
       .append('circle')
       .attr('class', 'dot')
       .attr("r", 2.5)
       .attr('fill', function (d, i) {
           return colors[d.index % colors.length];
       })
       .attr("transform", function (d) {
           if (d.point == null) {
               return "";
           }
            return "translate(" + x(d.point.x) + "," + y(d.point.y) + ")";
       }
    );

    //d3.select("#chartdiv").select("svg")
    //    .on("mousewheel.zoom", null)
    //    .on("DOMMouseScroll.zoom", null)
    //    .on("wheel.zoom", null);


    d3.select("#chartdiv").select("svg")
        .on("touchstart", null);

    d3.select("#swipe").selectAll(".item")
        .data(data)
        .enter()
        .append("label")
        .attr("class", "item")
        .attr("text", "text");



    //************************************************************
    // Zoom specific updates
    //************************************************************
    function zoomed() {
        svg.select(".x.axis").call(xAxis);
        svg.select(".y.axis").call(yAxis);
        svg.selectAll('path.line').attr('d', function (d) { return line(d.data); });

        points.selectAll('circle').attr("transform", function (d) {
            return "translate(" + x(d.point.x) + "," + y(d.point.y) + ")";
        });
    }


    //************************************************************
    // Legend
    //************************************************************

    for (var i = 0; i < data.length; i++) {
        if (data[i].isGrowthCurve) {
            var curveExist = ko.utils.arrayFirst(GraphVM.curves(), function (curve) {
                return curve.id === data[i].id;
            });

            if (!curveExist) {
                GraphVM.curves.push(data[i]);
            }
            continue;
        }
        d3.select("#legend")
            .append("div")
            .attr("class", function (d) {
                if (data[i].active) {
                    return "flock-legend active";
                } else {
                    return "flock-legend inactive";
                }
            })
            .attr("data-flock", data[i].id)
            .attr("id", "legend_" + data[i].id)
            .attr("style", "background-color:" + colors[i % colors.length])
            .text(data[i].name);
        if (data[i].active) {
            d3.selectAll(".flock_" + data[i].id)
                .attr("style", "visibility:visible");
        }
    }
    $(".flock-legend").click(function () {
        var active = $(this).hasClass("active");
        if (active) {
            $(this).addClass("inactive");
            $(this).removeClass("active");
        } else {
            $(this).addClass("active");
            $(this).removeClass("inactive");
        }

        ChangeSelectedFlock($(this).attr("data-flock"), !active);

        d3.selectAll(".flock_" + $(this).attr("data-flock"))
            .attr("style", function (d) {
                var val = "visible";
                if (active) {
                    val = "hidden";
                } else {
                }
                return "visibility:" + val;
            });
    });

    

    ////************************************************************
    //// Grahp reset
    ////************************************************************

    //Hammer(nav).on("press", function () {
    //    reset();
    //});
    Hammer(chart).on("press", function () {
        reset();
    });

    function reset() {
        d3.transition().duration(750).tween("zoom", function () {
            var ix = d3.interpolate(x.domain(), [dataChart.xMin, dataChart.xMax]),
                iy = d3.interpolate(y.domain(), [dataChart.yMin, dataChart.yMax]);
            return function (t) {
                zoom.x(x.domain(ix(t))).y(y.domain(iy(t)));
                zoomed();
            };
        });
    }
    //Show last selected growth curve
    var sc =  $.cookie("growthCurve");
    d3.selectAll(".curve_" + sc).style("visibility", "visible");
    $("#gwcurveSelector").val(sc);

    //Show active graph
    var prevActive = $.cookie(window.location.pathname + "_graphId");
    if (prevActive && dataChart.data[prevActive]) {
        active = prevActive;
        d3.selectAll(".flock_" + dataChart.data[active].id).style("visibility", "visible");
        $("#legend_" + dataChart.data[active].id).removeClass("inactive").addClass("active");
    }
};

/* Support functions */

function IsScrollUp(event) {
    var up = false;
    if (!(typeof event.originalEvent.detail === 'undefined' || event.originalEvent.detail === null)) {
        if (event.originalEvent.detail >= 0) {
            up = true;
        } else {
            up = false;
        }
    }

    if (!(typeof event.originalEvent.wheelDelta === 'undefined' || event.originalEvent.wheelDelta === null)) {
        if (event.originalEvent.wheelDelta >= 0) {
            up = false;
        } else {
            up = true;
        }
    }
    return up;
}

/* OnDOM ready */

$(document).ready(function () {


    $('#selectedGraph a').click(function (e) {
        $.cookie(window.location.pathname, "graphs");

        $("#gwcurveSelector")[0].selectedIndex = 0;
        e.preventDefault();
        $(this).tab('show');
        run($(".graphTypesMenu li.selected a").attr('title'));
        $("#exportData").hide();
    });

    $('#selectedStat a').click(function (e) {
        $.removeCookie(window.location.pathname);
        $("#exportData").show();
    });

    $('.graphTypesMenu li a').click(function () {

        $("#gwcurveSelector")[0].selectedIndex = 0;

        $('.graphTypesMenu li').each(function () {
            if ($(this).hasClass("selected")) {
                $(this).removeClass("selected");
            }
        });
        $(this).parent().addClass("selected");

    });

    $('.graphTypesMenu a').click(function () {
        var graphType = $(this).attr('title');

        if (graphType == $('.graphTypesMenu li a:first').attr('title')) {
            $("#curveSelection").show();
        } else {
            $("#curveSelection").hide();
        }

        if (GraphVM.flocksIds().length > 0) {
            if (graphType == $('.graphTypesMenu li a:first').attr('title')) {
                run(graphType);
            } else {
                getGraphData(graphType);
            }
        }
    });

    sexSubscribe();
});

ko.applyBindings(GraphVM);

function resizedw() {
    drawGraph(dataChart);
}

var doit;
window.onresize = function () {
    clearTimeout(doit);
    doit = setTimeout(resizedw, 100);
};

$("#gwcurveSelector").change(function () {
    d3.selectAll(".curve").style("visibility", "hidden");
    d3.selectAll(".curve_" + this.value).style("visibility", "visible");
    $.cookie("growthCurve", this.value);
});


//************************************************************
// Hammer swipe
//************************************************************
var nav = document.getElementById("flock-swipe");
var chart = document.getElementById("chartdiv");
var active = null;
d3.selectAll(".flock_" + active).style("visibility", "visible");;
Hammer(nav).on("swipeleft", function () {
    if (active != null) {
        d3.selectAll(".flock_" + dataChart.data[active].id).style("visibility", "hidden");
        $("#legend_" + dataChart.data[active].id).removeClass("active").addClass("inactive");
    } else {
        active = -1;
    }

    active++;

    if (active >= dataChart.data.length)
        active = 0;

    while (!$("#legend_" + dataChart.data[active].id).hasClass("inactive")) {
        active++;
        if (active >= dataChart.data.length) {
            break;
        }
    }

    if (active >= dataChart.data.length)
        active = 0;
    d3.selectAll(".flock_" + +dataChart.data[active].id).style("visibility", "visible");
    $("#legend_" + dataChart.data[active].id).removeClass("inactive").addClass("active");
    $("#flock-swipe #flock-text").text(dataChart.data[active].name);
    $("#flock-swipe .flock-marker").css("background-color", colors[active % colors.length]);
    $.cookie(window.location.pathname + "_graphId", active);
});

Hammer(nav).on("swiperight", function () {
    if (active != null) {
        d3.selectAll(".flock_" + +dataChart.data[active].id).style("visibility", "hidden");
        $("#legend_" + dataChart.data[active].id).removeClass("active").addClass("inactive");
    } else {
        active = dataChart.data.length;
    }
    active--;

    if (active < 0)
        active = dataChart.data.length - 1;

    while (!$("#legend_" + dataChart.data[active].id).hasClass("inactive")) {
        active--;
        if (active < 0) {
            break;
        }
    }

    if (active < 0)
        active = dataChart.data.length - 1;
    
    d3.selectAll(".flock_" + +dataChart.data[active].id).style("visibility", "visible");
    $("#legend_" + dataChart.data[active].id).removeClass("inactive").addClass("active");
    $("#flock-swipe #flock-text").text(dataChart.data[active].name);
    $("#flock-swipe .flock-marker").css("background-color", colors[active % colors.length]);
    $.cookie(window.location.pathname + "_graphId", active);
});

Hammer(nav).on("tap", function () {
    $("#legend").toggle();
        setTimeout(function () { $("#flock-text")[0].scrollIntoView(true); }, 50);
});

var colors = [
       'blue', 'darkgoldenrod', 'brown', 'burlywood', 'cadetblue', 'chartreuse', 'chocolate',
       'coral', 'cornflowerblue', 'crimson', 'darkblue', 'darkcyan', 'darkgray', 'darkgreen', 'darkkhaki',
       'darkmagenta', 'darkolivegreen', 'darkorange', 'darkorchid', 'darkred', 'darksalmon', 'darkseagreen', 'darkslategray',
       'darkturquoise', 'darkviolet', 'deeppink', 'dimgray', 'dimgrey', 'dodgerblue', 'firebrick', 'forestgreen', 'fuchsia',
       'gainsboro', 'gold', 'gray', 'green', 'greenyellow', 'grey', 'hotpink', 'indianred', 'indigo', 'khaki', 'lawngreen',
       'lightblue', 'lightcoral', 'lightgreen', 'lightgrey', 'lightpink', 'lightsalmon', 'lightseagreen', 'lightskyblue',
       'lightslategray', 'lightsteelblue', 'lightyellow', 'limegreen', 'linen', 'magenta', 'maroon', 'mediumaquamarine',
       'mediumblue', 'mediumorchid', 'mediumseagreen', 'mediumslateblue', 'mediumspringgreen', 'mediumturquoise',
       'mediumvioletred', 'midnightblue', 'mistyrose', 'navajowhite', 'navy', 'olive', 'olivedrab', 'orange', 'orangered',
       'orchid', 'palegoldenrod', 'palegreen', 'paleturquoise', 'palevioletred', 'papayawhip', 'peachpuff', 'peru', 'pink',
       'plum', 'powderblue', 'purple', 'red', 'rosybrown', 'royalblue', 'saddlebrown', 'salmon', 'sandybrown', 'seagreen',
       'sienna', 'silver', 'skyblue', 'slateblue', 'slategray', 'springgreen', 'steelblue', 'tan', 'teal', 'thistle', 'tomato',
       'turquoise', 'violet', 'wheat', 'yellow', 'yellowgreen'];