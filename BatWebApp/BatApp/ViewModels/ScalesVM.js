﻿var scalesTable;
var gl = globals[$.cookie("_culture")];

//************************************************************
// Scale View Model
//************************************************************
var ScalesVM = {
    serialNumber: ko.observable("").extend({
        required: true,
        validation: {
            validator: IsHex,
            message: gl.invalidSerialNumber
        }
    }),
    phoneNumber: ko.observable("").extend({
        validation: {
            validator: IsPhone,
            message: gl.invalidPhone
        }
    }),
    farmName: ko.observable(""),
    scaleName: ko.observable(""),
    scalesSelected: ko.observableArray(),
    users: ko.observableArray(),
    usersSelected: ko.observableArray(),
    scaleId: "",
    companies: ko.observableArray(),
    companiesSelected: ko.observableArray(),
    companySelected: ko.observable()
};
ScalesVM.errors = ko.validation.group(ScalesVM);
ko.validation.locale($.cookie("_culture"));
ko.validation.init({
    decorateElement: true
});


//************************************************************
// Operations with scale list
//************************************************************
ScalesVM.DeleteScales = function (data) {
    JsActions.Scales.DeleteScales(
        ko.toJS(ScalesVM.scalesSelected),
        {
            success: function () {
                scalesTable.api().ajax.reload(null, false);
                ScalesVM.scalesSelected.removeAll();
                $("#deleteScales").modal('hide');
            }
        });
};

//Get available users for specific scale 
ScalesVM.GetScaleUsers = function (scaleId) {
    ScalesVM.scaleId = scaleId;
    JsActions.Scales.GetScaleUsers(
        ScalesVM.scaleId,
        {
            success: function (data) {
                ScalesVM.users.removeAll();
                $.each(data.data.users, function (key, val) {
                    ScalesVM.users.push(val);
                });
                ScalesVM.usersSelected.removeAll();
                $.each(data.data.usersSelected, function (key, val) {
                    ScalesVM.usersSelected.push(val);
                });
                $("#changeUsers").modal("show");
            }
        });
};

//Set users for specific scale
ScalesVM.SetScaleUsers = function () {
    JsActions.Scales.SetScaleUsers(
        ScalesVM.scaleId,
        ScalesVM.usersSelected(),
        {
            success: function () {
                $("#changeUsers").modal("hide");
                scalesTable.api().ajax.reload(null, false);
            }
        });
};

//Show dialog to change scale company  - admin only
ScalesVM.GetScaleCompanies = function (scaleId) {
    ScalesVM.scaleId = scaleId;
    JsActions.Scales.GetScaleCompanies(
        ScalesVM.scaleId,
        {
            success: function (data) {
                ScalesVM.companies.removeAll();
                $.each(data.data.companies, function (key, val) {
                    ScalesVM.companies.push(val);
                });
                ScalesVM.companiesSelected.removeAll();
                $.each(data.data.companiesSelected, function (key, val) {
                    ScalesVM.companiesSelected.push(val);
                });
                $("#changeCompany").modal("show");
            }
        });
};

//Change scale owner(company) - admin only
ScalesVM.SetScaleCompanies = function () {
    JsActions.Scales.SetScaleCompanies(
        ScalesVM.scaleId,
        ScalesVM.companiesSelected(),
        {
            success: function () {
                $("#changeCompany").modal("hide");
                scalesTable.api().ajax.reload(null, false);
            }
        });
};

//Export list of scales to file
ScalesVM.ExportScales = function () {
    var sorting = scalesTable.fnSettings().aaSorting[0];
    var params = {
        column: sorting[0],
        orientation: sorting[1],
        search: $("#scalesTable_filter input").val()
    };

    var str = Object.keys(params).map(function (key) {
        return encodeURIComponent(key) + '=' + encodeURIComponent(params[key]);
    }).join('&');

    location.href = baseURL + 'Scales/ExportScales?' + str;
};

//Create new scale
ScalesVM.CreateScale = function () {
    $("#scale-exist").css("display", "none");
    if (ScalesVM.errors().length > 0) {
        if (ScalesVM.serialNumber().length === 0) {
            ScalesVM.serialNumber(" ");
            ScalesVM.serialNumber();
        }
        return;
    }

    if (ScaleExist(ScalesVM.serialNumber())) {
        $("#scale-exist").css("display", "block");
        return;
    }
    $("#scale-exist").css("display", "none");

    JsActions.Scales.CreateScale(
        this.serialNumber(),
        this.scaleName(),
        this.farmName(),
        this.phoneNumber(),
        {
            success: function () {
                scalesTable.api().ajax.reload(null, false);
                ScalesVM.serialNumber("");
                ScalesVM.scaleName("");
                ScalesVM.farmName("");
                ScalesVM.phoneNumber("");
                ScalesVM.scalesSelected.removeAll();
                $("#createScale").modal('hide');
            }
        });
};

//Change scale type
ScalesVM.ChangeScaleType = function (model, event) {
    JsActions.Scales.ChangeScaleType(
        ScalesVM.scaleId,
        this,
        {
            success: function() {
                scalesTable.api().ajax.reload(null, false);
                $("#changeConfig").modal('hide');
            }
        }
    );
}


//************************************************************
// Scale list helper methods
//************************************************************
//Check if scale with serialNumber already exist
function ScaleExist(serialNumber) {
    var isSame = false;
    $('#scalesTable tbody tr').each(function () {
        if (serialNumber.toLowerCase() === this.firstChild.innerHTML.toLowerCase()) {
            isSame = true;
            return false;
        }
    });
    return isSame;
}

//Check if value has hex number format
function IsHex(hex) {
    var number = parseInt(hex, 16);
    $("#scale-exist").css("display", "none");
    if (number.toString(16) === hex.toLowerCase()) {
        return number < parseInt("7FFFFFFF", 16);
    }
    return (number.toString(16) === hex.toLowerCase());
}

//Check if value has phone number format
function IsPhone(number) {
    if (number === "") return true;
    var filter = /^[+]?[()/0-9. -]{9,}$/;
    return filter.test(number);
}


//************************************************************
// Create table of scales
//************************************************************
var createScalesStatTable = function () {
    if (typeof scalesTable === 'undefined') {
        scalesTable = $('#scalesTable').dataTable({
            "language": {
                "url": baseURL + "Content/DataTables/Internationalisation/" + $.cookie("_culture") + ".txt"
            },
            "bServerSide": false,
            "sAjaxSource": baseURL + "Scales/ScalesHandler",
            "bProcessing": true,
            "sScrollX": "100%",
            "bPaginate": false,
            "bLengthChange": false,
            "order": [[0, "desc"]],
            "oTableTools": {
                "sRowSelect": "multi"
            },
            "aoColumns": [
                {
                    "bSearchable": false,
                    "bSortable": true,
                    "bVisible": false

                },
               { "bSortable": true, "bVisible": false },
               {
                  "bSortable": true
                  //"mRender": function (oObj) {
                  //   if (IsHex(oObj)) {
                  //      return parseInt(oObj, 16);
                  //   }
                  //   return oObj;
                  //}
               },
                { "bSortable": true, "bVisible": false },
                { "bSortable": true },
                {
                    "bSearchable": false,
                    "bVisible": ($("#Role").val() === "Admin" || $("#Role").val() === "CompanyAdmin"),
                    "bSortable": true,
                    "mRender": function (oObj) {
                        if (oObj === null) {
                            return "";
                        }
                        return '<a href="#" class="scaleUsers">' + oObj + '</a>';
                    }
                },
                 {
                     "bSearchable": false,
                     "bVisible": false,
                     //"bVisible": ($("#Role").val() === "Admin" || $("#Role").val() === "CompanyAdmin"),
                     "bSortable": true,
                     "mRender": function (oObj, param, data) {
                         if (oObj === null) {
                             return "";
                         }
                         var link = "";
                         if (oObj === 2) {
                             link += '<a href="' + baseURL + 'Scales/Bat2plus/' + data[0] + '">Bat2+</a>';
                         } else if (oObj === 1) {
                             link += '<a href="' + baseURL + 'Scales/Bat2/' + data[0] + '">Bat2</a>';
                         }
                         link += '<a href="#" style="float:right" class="scaleConfig"></a>';
                         return link;
                     }
                 },
                {
                    "bSearchable": false,
                    "bVisible": $("#Role").val() === "Admin",
                    "bSortable": true,
                    "mRender": function (oObj) {
                        if (oObj === null) {
                            return "";
                        }
                        return '<a href="#" class="scaleCompany">' + oObj + '</a>';
                    }
                }
            ]
        });
        scalesTable.makeEditable({
            "aoColumns": [
                //{ event: 'taphold dblclick', onblur: "submit", placeholder: "" },
                //{ event: 'taphold dblclick', onblur: "submit", placeholder: "" },
                { event: 'taphold dblclick', onblur: "submit", placeholder: "" },
                { event: 'taphold dblclick', onblur: "submit", placeholder: "" }
            ],
            fnOnEditing: function (input, cellIndex) {
                var serialNumber;
                var scaleName;
                var farmName;
                var phoneNumber;
                //if ($(input).closest("td").index() == 0) {
                //    serialNumber = $(input).val();
                //    if (!IsHex(serialNumber) || ScaleExist(serialNumber)) {
                //        var form = $(input)[0].parentElement;
                //        if (form.children.length === 1) {
                //            var div = document.createElement("div");
                //            div.id = "errorDiv";
                //            div.innerHTML = gl.invalidSerialNumber;
                //            form.appendChild(div);
                //        }
                //        return false;
                //    }
                //}
                if ($(input).closest("td").index() === 0) {
                    scaleName = $(input).val();
                }
                //if ($(input).closest("td").index() == 2) {
                //    phoneNumber = $(input).val();
                //    if (!IsPhone(phoneNumber)) {
                //        var formPhone = $(input)[0].parentElement;
                //        if (formPhone.children.length === 1) {
                //            var divPhone = document.createElement("div");
                //            divPhone.id = "errorDiv";
                //            divPhone.innerHTML = gl.invalidPhone;
                //            formPhone.appendChild(divPhone);
                //        }
                //        return false;
                //    }
                //}
                if ($(input).closest("td").index() === 1) {
                    farmName = $(input).val();
                }
                JsActions.Scales.UpdateScale(
                    input.parents("tr").attr("id"),
                    parseInt(serialNumber, 16),
                    scaleName,
                    farmName,
                    phoneNumber,
                    {
                        success: function () {
                            scalesTable.api().ajax.reload(null, false);
                            setTimeout(function () { selectRows(0); }, 50);
                        }
                    });
                return false;
            }
        });
    } else {
       scalesTable.fnDraw();
    }
};

$("#exportScales").click(function () {
    ScalesVM.ExportScales();
});

jQuery(document).on("mobileinit", function () {
    jQuery.mobile.autoInitializePage = false;
});

$(document).ready(function () {
    $("#delScales").css("opacity", 0.5);
    createScalesStatTable();
    $('.sorting, .sorting_asc, .sorting_desc').dblclick(function () {
        setTimeout(function () {
            scalesTable.fnSort([[0, 'desc']]);
        }, 10);
        return false;
    });

    $('#scalesTable tbody').on('click', 'tr', function (event) {
        if ($(event.target).is("a")) {
            if ($(event.target).hasClass("scaleUsers")) {
                ScalesVM.GetScaleUsers(event.currentTarget.id);
            } else if ($(event.target).hasClass("scaleConfig")) {
                ScalesVM.scaleId = event.currentTarget.id;
                $("#changeConfig").modal("show");
            } else if ($(event.target).hasClass("scaleCompany")) {
                ScalesVM.GetScaleCompanies(event.currentTarget.id);
            } else {
                return true;
            }
            return false;
        }

        var id = this.id;
        setTimeout(function () {
            var index = ScalesVM.scalesSelected.indexOf(id);
            if (index === -1) {
                ScalesVM.scalesSelected.push(id);
            } else {
                ScalesVM.scalesSelected.splice(index, 1);
            }
            selectRows(id);

        }, 10);
    });

    ScalesVM.scalesSelected.subscribe(function (changes) {
        if (ScalesVM.scalesSelected().length > 0) {
            $("#delScales").css("opacity", 1);
            $("#delScales").parent().attr('href', "#");
            $("#delScales").parent().attr('data-target', "#deleteScales");
        } else {
            $("#delScales").css("opacity", 0.5);
            $("#delScales").parent().removeAttr('href');
            $("#delScales").parent().removeAttr('data-target');
        }
    }, null, "arrayChange");
});

function selectRows(id) {
    $('#scalesTable tbody tr').each(function () {
        if (this.id === id) {
            return true;
        }
        $(this).removeClass('row_selected');
        var selected = ScalesVM.scalesSelected.indexOf(this.id);
        if (selected !== -1) {
            $(this).addClass('row_selected');
        }
    });
}
ko.applyBindings(ScalesVM, document.getElementById("scalesForm"));