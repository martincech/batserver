﻿var Bat2VM = {
    activeTab: ko.observable('flocks'),
    scaleId: ko.observable(),
    //Configurations
    configs: ko.observableArray(),
    configId: ko.observable(),
    config: ko.pureComputed({
        read: function () {
            var config = ko.utils.arrayFirst(Bat2VM.configs(), function (item) {
                return item.Id === Bat2VM.configId();
            });
            return config;
        },
        write: function (value) {
            Bat2VM.configId(value.Id);
        },
        owner: Bat2VM
    }),
    //Templates
    templates: ko.observableArray(),
    template: ko.observable('new'),
    templateId: ko.observable(),
    createTemplates: ko.observableArray(),
    newTemplateId: ko.observable(),
    newTemplateName: ko.observable(),
    deleteTemplates: ko.observableArray(),
    oldTemplateId: ko.observable(),
    templateList: ko.pureComputed(function () {

        if (!Bat2VM.templates() && !Bat2VM.createTemplates()) {
            return [];
        }

        if (!Bat2VM.templates()) {
            return Bat2VM.createTemplates();
        }
        if (!Bat2VM.createTemplates()) {
            return Bat2VM.templates();
        }
        var list = Bat2VM.templates().concat(Bat2VM.createTemplates());
        return ko.utils.arrayFilter(list, function (item) {
            return !ko.utils.arrayFirst(Bat2VM.deleteTemplates(), function (first) {
                return item.Id == first.Id;
            });
        });
    }),
    //Flocks
    flockAll: ko.observableArray(),
    flockId: ko.observable(),
    flocks: ko.pureComputed(function () {
        if (Bat2VM.config()) {
            return ko.utils.arrayFilter(Bat2VM.flockAll(), function (item) {
                return item.ConfigurationV2_Id === Bat2VM.config().Id;
            });
        }
        return null;
    }),
    flock: ko.pureComputed({
        read: function () {
            if (Bat2VM.flocks()) {
                var flock = ko.utils.arrayFirst(Bat2VM.flocks(), function (item) {
                    return item.Id === Bat2VM.flockId();
                });
                return flock;
            }
            return null;
        },
        write: function (value) {
            Bat2VM.flockId(value.Id);
        },
        owner: Bat2VM
    }),
    //Curve validation result
    validCurve: ko.observable({
        male: {
            unique: true,
            day: true,
            weight: true
        },
        female: {
            unique: true,
            day: true,
            weight: true
        }
    }),
    curves: ko.observableArray(),
    curve: ko.observable(),
    curveSex: "female",
    //Datatables
    tableMale: null,
    tableFemale: null,
    //Localization of datatables
    local: null
};


//*************************************************************
// Flock attributes - notify GUI of changes in non-observable attributes
//*************************************************************
Bat2VM.useGender = ko.computed({
    read: function () {
        if (Bat2VM.flock()) {
            return Bat2VM.flock().UseGender;
        }
        return false;
    },
    write: function (value) {
        Bat2VM.flock().UseGender = value;
        Bat2VM.flockId.notifySubscribers();
    }
});
Bat2VM.useCurves = ko.computed({
    read: function () {
        if (Bat2VM.flock()) {
            return Bat2VM.flock().UseCurves;
        }
        return false;
    },
    write: function (value) {
        Bat2VM.flock().UseCurves = value;
        Bat2VM.flockId.notifySubscribers();
    }
});

//*************************************************************
// Extend model with custom property binding !knockout cant compare int & string in radiobutton
//*************************************************************
//Backlight
Bat2VM.backlight = ko.computed({
    read: function () {
        if (Bat2VM.config()) {
            return Bat2VM.config().Backlight.toString();
        }
        return "0";
    },
    write: function (value) {
        Bat2VM.config().Backlight = parseInt(value);
    }
});

//Rs485
Bat2VM.rs485Parity = ko.computed({
    read: function () {
        if (Bat2VM.config()) {
            return Bat2VM.config().Rs485.Parity.toString();
        }
        return "0";
    },
    write: function (value) {
        Bat2VM.config().Rs485.Parity = parseInt(value);
    }
});
Bat2VM.rs485Protocol = ko.computed({
    read: function () {
        if (Bat2VM.config()) {
            return Bat2VM.config().Rs485.Protocol.toString();
        }
        return "0";
    },
    write: function (value) {
        Bat2VM.config().Rs485.Protocol = parseInt(value);
    }
});

//Scale
Bat2VM.scaleSaveUnpon = ko.computed({
    read: function () {
        if (Bat2VM.config()) {
            return Bat2VM.config().ScaleConfig.SaveUnpon.toString();
        }
        return "0";
    },
    write: function (value) {
        Bat2VM.config().ScaleConfig.SaveUnpon = parseInt(value);
    }
});
Bat2VM.scaleUnits = ko.computed({
    read: function () {
        if (Bat2VM.config()) {
            return Bat2VM.config().ScaleConfig.Units.toString();
        }
        return "0";
    },
    write: function (value) {
        Bat2VM.config().ScaleConfig.Units = parseInt(value);
    }
});

//*************************************************************
// Load confgiuration data
//*************************************************************
//Load list of configurations form server
Bat2VM.LoadData = function () {
    var url = document.URL;
    var arr = url.split('/');
    var scaleId = arr[arr.length - 1];
    var n = parseInt(scaleId);
    if (n === +n && n === (n | 0)) {
        Bat2VM.scaleId(parseInt(n));
        JsActions.Scales.GetScaleConfigs(
            n,
            {
                success: function (data) {
                    ko.utils.arrayPushAll(Bat2VM.configs, data.configs);
                    ko.utils.arrayPushAll(Bat2VM.templates, data.templates);
                }
            });
        Bat2VM.LoadFlocks();
    }
};
//Load list of flocks from server
Bat2VM.LoadFlocks = function () {
    JsActions.Scales.GetAllFlocks(
        Bat2VM.scaleId(),
        {
            success: function (data) {
                Bat2VM.flockAll.removeAll();
                ko.utils.arrayPushAll(Bat2VM.flockAll, data.flocks);
            }
        });
};

//*************************************************************
// Change active cofig tab
//*************************************************************
Bat2VM.Show = function (model, e) {
    [].forEach.call(e.target.parentNode.parentNode.children, function (node) {
        node.removeAttribute("class");
    });
    e.target.parentNode.className = "selected";
    model.activeTab(this);
};

//*************************************************************
// Config - Add/Delete/Update 
//*************************************************************
//Create new configuratiuon
Bat2VM.CreateConfig = function (form) {
    var templateId = form.template.value;
    var name = form.name.value;
    JsActions.Scales.CreateConfig(
        Bat2VM.scaleId(),
        templateId,
        name,
    {
        success: function (data) {
            Bat2VM.configs.push(data.config);
            Bat2VM.configId(data.config.Id);
            ko.utils.arrayPushAll(Bat2VM.flockAll, data.flocks);
            $(form.parentNode).modal("hide");
            Bat2VM.SaveNotification();
        }
    });
};

//Edit configuration name
Bat2VM.EditConfigName = function (form) {
    var config = Bat2VM.config();
    config.Name = form.name.value;
    JsActions.Scales.ChangeConfigName(
        Bat2VM.configId(),
        config.Name,
        {
            success: function () {
                Bat2VM.configs.replace(Bat2VM.config(), config);
                $("#config-list :selected").text(config.Name);
                $('.modal').modal('hide');
                Bat2VM.SaveNotification();
            }
        });
};

//Delete configuration
Bat2VM.DeleteConfig = function (form) {
    var modalWindow = form.parentNode;
    JsActions.Scales.DeleteConfig(
        Bat2VM.configId(),
        {
            success: function () {
                Bat2VM.configs.remove(Bat2VM.config());
                Bat2VM.configId(null);
                $(modalWindow).modal("hide");
                Bat2VM.SaveNotification();
            }
        });
};
//Save configurations
Bat2VM.SaveConfigs = function () {
    if (!Bat2VM.config()) {
        return;
    }
    JsActions.Scales.SaveConfigs(
        Bat2VM.scaleId(),
        Bat2VM.configs,
        Bat2VM.flockAll,
        {
            success: function () {
                Bat2VM.SaveNotification();
            }
        });
};

//*************************************************************
// Flocks - Add/Delete/Update 
//*************************************************************
//Create new flock
Bat2VM.CreateFlock = function () {
    var flock = {
        UseCurves: true,
        UseGender: true,
        ConfigurationV2_Id: Bat2VM.configId(),
        WeighFrom: 0,
        WeighTo: 0,
        CurveFemale_Id: 0,
        CurveMale_Id: 0,
        InitialFemale: 0,
        InitialMale: 0,
        Index: 0
    }

    var val = Bat2VM.flockId();

    for (var i = 0; i < 10; i++) {
        var any = ko.utils.arrayFirst(Bat2VM.flocks(), function (item) {
            return item.Index === i;
        });

        if (!any) {
            flock.Name = i;
            flock.Index = i;
            flock.Id = -i - 1;
            Bat2VM.flockAll.push(flock);
            break;
        }
    }
    Bat2VM.flockId(flock.Id);
};
//Delete flock
Bat2VM.DeleteFlock = function () {
    Bat2VM.flockId(null);
    Bat2VM.flockAll.remove(function (item) {
        return item.Id === Bat2VM.flockId();
    });
};

//*************************************************************
// Load predefined curves
//*************************************************************
Bat2VM.LoadCurves = function () {
    Bat2VM.curveSex = this.toString();
    JsActions.Scales.LoadCurves({
        success: function (curves) {
            Bat2VM.curves.removeAll();
            ko.utils.arrayPushAll(Bat2VM.curves, curves);
            $("#loadCurves").modal("show");
        }
    });
};

Bat2VM.ChangeCurvePoints = function () {
    if (Bat2VM.curveSex === "female") {
        Bat2VM.flock().CurveFemaleDTO = Bat2VM.curve();
    } else {
        Bat2VM.flock().CurveMaleDTO = Bat2VM.curve();
    }
    Bat2VM.flockId.notifySubscribers();
    $("#loadCurves").modal("hide");
};

//*************************************************************
// Add curve
//*************************************************************
//Add female curve point
Bat2VM.AddFemalePoint = function (form) {
    if (!Bat2VM.flock().CurveFemaleDTO) {
        Bat2VM.flock().CurveFemaleDTO = { CurveValues: [] };
    }

    Bat2VM.IsNewPointValid("female", "day", form.day);
    Bat2VM.IsNewPointValid("female", "weight", form.value);

    if (!Bat2VM.IsPointValid("female")) {
        return;
    }

    Bat2VM.AddPoint(Bat2VM.flock().CurveFemaleDTO, form.day.value, form.value.value, Bat2VM.tableFemale);
    form.day.value++;
};
//Add male curv e point
Bat2VM.AddMalePoint = function (form) {
    if (!Bat2VM.flock().CurveMaleDTO) {
        Bat2VM.flock().CurveMaleDTO = { CurveValues: [] };
    }

    Bat2VM.IsNewPointValid("male", "day", form.day);
    Bat2VM.IsNewPointValid("male", "weight", form.value);

    if (!Bat2VM.IsPointValid("male")) {
        return;
    }

    Bat2VM.AddPoint(Bat2VM.flock().CurveMaleDTO, form.day.value, form.value.value, Bat2VM.tableMale);
    form.day.value++;
};
//Add curve point
Bat2VM.AddPoint = function (curve, day, weight, table) {
    if (!curve.Id) {
        curve.Id = 0;
        curve.Name = "";
        curve.CompanyId = Bat2VM.config().CompanyId;
    }

    var point = { CurveId: 0, Day: day, Weight: weight };

    curve.CurveValues.push(point);

    if (table) {
        //table.fnClearTable();
        table.fnAddData(point);
        table.parent().scrollTop(9999);
        return;
    } else {
        Bat2VM.CreateGCTable();
    }
};
//Delete curve point
Bat2VM.DeletePoint = function (day, sex) {
    if (sex === "female") {
        Bat2VM.flock().CurveFemaleDTO.CurveValues = ko.utils.arrayFilter(Bat2VM.flock().CurveFemaleDTO.CurveValues, function (item) {
            return day != item.Day;
        });
    } else {
        Bat2VM.flock().CurveMaleDTO.CurveValues = ko.utils.arrayFilter(Bat2VM.flock().CurveMaleDTO.CurveValues, function (item) {
            return day != item.Day;
        });
    }
}

//*************************************************************
// Template management
//*************************************************************
Bat2VM.CreateTemplate = function () {
    if (!Bat2VM.newTemplateId() || !Bat2VM.newTemplateName()) {
        return;
    }

    var temp = ko.utils.arrayFirst(Bat2VM.createTemplates(), function (item) {
        return item.Id == Bat2VM.newTemplateId();
    });
    if (!temp) {
        var config = ko.utils.arrayFirst(Bat2VM.configs(), function (item) {
            return Bat2VM.newTemplateId() == item.Id;
        });
        config.Name = Bat2VM.newTemplateName();
        Bat2VM.createTemplates.push(config);
        Bat2VM.oldTemplateId(config.Id);
    }
};

Bat2VM.DeleteTemplate = function () {
    var item = ko.utils.arrayFirst(Bat2VM.templates(), function (item) {
        return item.Id == Bat2VM.oldTemplateId();
    });

    if (item) {
        Bat2VM.deleteTemplates.push(item);
    }

    Bat2VM.createTemplates.remove(function (item) {
        return item.Id == Bat2VM.oldTemplateId();
    });
};
Bat2VM.SaveTemplates = function () {
    var allFlocks = [];
    ko.utils.arrayForEach(Bat2VM.createTemplates(), function (config) {
        ko.utils.arrayForEach(Bat2VM.flockAll(), function (item) {
            if (item.ConfigurationV2_Id === config.Id) {
                allFlocks.push(item);
            }
        });
    });

    JsActions.Scales.ChangeConfigTemplates(
       Bat2VM.createTemplates(),
       allFlocks,
       Bat2VM.deleteTemplates(),
       {
           success: function (data) {
               Bat2VM.createTemplates.removeAll();
               Bat2VM.deleteTemplates.removeAll();
               Bat2VM.templates.removeAll();
               ko.utils.arrayPushAll(Bat2VM.templates, data);
               $("#templateConfig").modal("hide");
               Bat2VM.SaveNotification();
           }
       });
};
//*************************************************************
// Flocks - Create growth curves table
//*************************************************************
//Custom bingding handler 
ko.bindingHandlers.tableData = {
    init: function (element, valueAccessor) {
        var value = valueAccessor();
        var val = ko.unwrap(value);
        var exist = false;
        if (element.id === "female-gc") {
            if (Bat2VM.tableFemale) {
                Bat2VM.tableFemale.fnDestroy();
            }
        } else {
            if (Bat2VM.tableMale) {
                Bat2VM.tableMale.fnDestroy();
            }
        }

        Bat2VM.DefineGCTable($(element), element.id === "female-gc");

    },
    update: function (element, valueAccessor) {
        var value = ko.unwrap(valueAccessor());
        if (value) {
            setTimeout(function () {
                if (!Bat2VM.flock()) {
                    return;
                }
                if (element.id === "female-gc" && Bat2VM.flock().CurveFemaleDTO && Bat2VM.flock().CurveFemaleDTO.CurveValues) {
                    Bat2VM.tableFemale.fnClearTable();
                    if (Bat2VM.flock().CurveFemaleDTO.CurveValues.length > 0) {
                        Bat2VM.tableFemale.fnAddData(Bat2VM.flock().CurveFemaleDTO.CurveValues);
                    }
                }
                if (element.id === "male-gc" && Bat2VM.flock().CurveMaleDTO && Bat2VM.flock().CurveMaleDTO.CurveValues) {
                    Bat2VM.tableMale.fnClearTable();
                    if (Bat2VM.flock().CurveMaleDTO.CurveValues.length > 0) {
                        Bat2VM.tableMale.fnAddData(Bat2VM.flock().CurveMaleDTO.CurveValues);
                    }
                }
            }, 20);
        }
    }
};
//Create curves dataTables
Bat2VM.DefineGCTable = function (element, female) {
    var table = element.dataTable({
        "language": Bat2VM.local,
        data: [],
        "bPaginate": false,
        "sScrollY": "200px",
        "sScrollX": "100%",
        "sScrollXInner": "99.99%",
        //'bAutoWidth': false , 
        "sDom": "t",
        "order": [[0, "asc"]],
        columns: [
            { data: "Day" },
            { data: "Weight" },
            {
                "bSortable": false,
                "render": function (data, type, full, meta) {
                    return "<a class='table-action-deletelink'><div></div></a>";
                }
            }
        ]
    });

    table.makeEditable({
        sUpdateURL: function (value, settings) {
            return (value);
        },
        //fnOnAddeding: function (jInput) {
        //    debugger;
        //},
        //fnOnEditing: function (jInput,a,b,c,d,e) {

        //    return true;
        //},
        fnOnDeleting: function (tr, id, fnDeleteRow, delUrl, a, b, c) {
            var table = $(tr).closest("table");
            var dataTable = null;
            var day = tr.get(0).firstChild.innerHTML;
            if (table[0].id === "female-gc") {
                dataTable = Bat2VM.tableFemale;
                Bat2VM.DeletePoint(day, "female");
            } else {
                dataTable = Bat2VM.tableMale;
                Bat2VM.DeletePoint(day, "male");
            }
            var index = dataTable.fnGetPosition(tr.get(0));
            dataTable.fnDeleteRow(index);
            return false;
        },
        fnOnUpdating: function (tr, id, fnDeleteRow, delUrl, a, b, c) {
            debugger;
            return true;
        },
        aoColumns: [{ event: 'taphold dblclick', onblur: "submit", placeholder: "" }, { event: 'taphold dblclick', onblur: "submit", placeholder: "" }, null],
        sDeleteRowButtonId: ""
    });

    if (female) {
        Bat2VM.tableFemale = table;
        Bat2VM.ResetValidation("female");
    } else {
        Bat2VM.tableMale = table;
        Bat2VM.ResetValidation("male");
    }
};

//*************************************************************
// Validation
//*************************************************************
Bat2VM.ValidateCurve = function (source, model, event) {
    return Bat2VM.IsNewPointValid(this.toString(), source, event.target);
};

//Validation of day or weight
Bat2VM.IsNewPointValid = function (sex, source, input) {
    if (source === "day") {
        Bat2VM.validCurve()[sex].day = Bat2VM.IsDay(input);

        if (Bat2VM.validCurve()[sex].day) {
            if (sex === "male") {
                Bat2VM.validCurve()[sex].unique = Bat2VM.UniqueDay(Bat2VM.flock().CurveMaleDTO, input);
            } else {
                Bat2VM.validCurve()[sex].unique = Bat2VM.UniqueDay(Bat2VM.flock().CurveFemaleDTO, input);
            }
        }
    }
    if (source === "weight") {
        Bat2VM.validCurve()[sex].weight = Bat2VM.IsWeight(input);
    }
    Bat2VM.validCurve.notifySubscribers();
    if (Bat2VM.tableFemale) {
        Bat2VM.tableFemale.fnAdjustColumnSizing();
    }
    if (Bat2VM.tableMale) {
        Bat2VM.tableMale.fnAdjustColumnSizing();
    }
    return true;
};

//Check if day and weight are valid
Bat2VM.IsPointValid = function (sex) {
    return Bat2VM.validCurve()[sex].day && Bat2VM.validCurve()[sex].weight && Bat2VM.validCurve()[sex].unique;
};

//Reset validation
Bat2VM.ResetValidation = function (sex) {
    Bat2VM.validCurve()[sex].day = true;
    Bat2VM.validCurve()[sex].unique = true;
    Bat2VM.validCurve()[sex].weight = true;

    Bat2VM.validCurve.notifySubscribers();

    if (Bat2VM.tableFemale) {
        Bat2VM.tableFemale.fnAdjustColumnSizing();
    }
    if (Bat2VM.tableMale) {
        Bat2VM.tableMale.fnAdjustColumnSizing();
    }
};

//Check if day is unique
Bat2VM.UniqueDay = function (curve, input) {
    var data = ko.utils.arrayFirst(curve.CurveValues, function (item) {
        return input.value == item.Day;
    });
    return data == null;
};

//Check if input has positiv int value > 0
Bat2VM.IsDay = function (input) {
    var str = input.value;
    var n = ~~Number(str);
    return String(n) === str && n > 0;
};

//Check if input has positiv decimal number 0 include
Bat2VM.IsWeight = function (input) {
    return (/^(?:[1-9]\d*|0)?(?:\.\d+)?$/).test(input.value);
};

Bat2VM.SaveNotification = function (input) {
    $(".fadeOut").removeClass("start");
    $(".fadeOut").removeClass("end");
    $(".fadeOut").addClass("start");
    setTimeout(function () {
        $(".fadeOut").addClass("end");
    }, 1000);
};
//*************************************************************
// Knockout binding
//*************************************************************
ko.applyBindings(Bat2VM, document.getElementById("bat2-config"));

//*************************************************************
// Page load
//*************************************************************
$(document).ready(function () {
    //Load localization for datatables
    $.getJSON(baseURL + "Content/DataTables/Internationalisation/" + $.cookie("_culture") + ".txt", function (data) {
        Bat2VM.local = data;
        Bat2VM.LoadData();
    }).fail(function (jqxhr, textStatus, error) {
        Bat2VM.LoadData();
    });
});