﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Ninject;
using Ninject.Activation;
using Ninject.Activation.Blocks;
using Ninject.Components;
using Ninject.Modules;
using Ninject.Parameters;
using Ninject.Planning.Bindings;
using Ninject.Syntax;

namespace Utilities.IOC
{
   public class NinjectIocContainer : IIocContainer, IKernel
   {
      private readonly IKernel kernel;

      /// <summary>
      /// Initializes a new instance of the <see cref="T:System.Object"/> class.
      /// </summary>
      public NinjectIocContainer(IKernel kernel)
      {
         this.kernel = kernel;
      }

      #region Implementation of IIocContainer

      /// <summary>
      /// Register function for creation object of type T
      /// </summary>
      /// <typeparam name="T">type of object to be registered</typeparam>
      /// <param name="function">function which creates an instance of <see cref="T"/></param>
      public void Bind<T>(Func<T> function)
      {
         kernel.Bind<T>().ToMethod( context => function());
      }

      /// <summary>
      /// Get object from this container specified by its type.
      /// </summary>
      /// <typeparam name="T">Generic type to get</typeparam>
      /// <returns>object of type T if this type is registered in IOC</returns>
      /// <exception cref="NotSupportedException">when T is not registered in IOC or is unknown</exception>
      public T Get<T>()
      {
         return (T)Get(typeof (T));
      }

      /// <summary>
      /// Get object from this container as dynamically specified by its type.
      /// </summary>
      /// <param name="t"><see cref="Type"/> of object to get</param>
      /// <returns>object of <see cref="Type"/> <see cref="t"/> if this type is registered in IOC</returns>
      /// /// <exception cref="NotSupportedException">when <see cref="t"/> is not registered in IOC or is unknown</exception>
      public object Get(Type t)
      {
         try
         {
            return kernel.Get(t);
         }
         catch (Exception e)
         {
            throw new NotSupportedException("NinjectIocContainer: Can't resolve type " + t.Name + ": " + e.Message);
         }
      }

      #endregion

      #region IKernel delegates

      #region Implementation of IBindingRoot

      /// <summary>
      /// Declares a binding for the specified service.
      /// </summary>
      /// <typeparam name="T">The service to bind.</typeparam>
      /// <returns>
      /// The fluent syntax.
      /// </returns>
      public IBindingToSyntax<T> Bind<T>()
      {
         return kernel.Bind<T>();
      }

      /// <summary>
      /// Declares a binding for the specified service.
      /// </summary>
      /// <typeparam name="T1">The first service to bind.</typeparam><typeparam name="T2">The second service to bind.</typeparam>
      /// <returns>
      /// The fluent syntax.
      /// </returns>
      public IBindingToSyntax<T1, T2> Bind<T1, T2>()
      {
         return kernel.Bind<T1, T2>();
      }

      /// <summary>
      /// Declares a binding for the specified service.
      /// </summary>
      /// <typeparam name="T1">The first service to bind.</typeparam><typeparam name="T2">The second service to bind.</typeparam><typeparam name="T3">The third service to bind.</typeparam>
      /// <returns>
      /// The fluent syntax.
      /// </returns>
      public IBindingToSyntax<T1, T2, T3> Bind<T1, T2, T3>()
      {
         return kernel.Bind<T1, T2, T3>();
      }

      /// <summary>
      /// Declares a binding for the specified service.
      /// </summary>
      /// <typeparam name="T1">The first service to bind.</typeparam><typeparam name="T2">The second service to bind.</typeparam><typeparam name="T3">The third service to bind.</typeparam><typeparam name="T4">The fourth service to bind.</typeparam>
      /// <returns>
      /// The fluent syntax.
      /// </returns>
      public IBindingToSyntax<T1, T2, T3, T4> Bind<T1, T2, T3, T4>()
      {
         return kernel.Bind<T1, T2, T3, T4>();
      }

      /// <summary>
      /// Declares a binding from the service to itself.
      /// </summary>
      /// <param name="services">The services to bind.</param>
      /// <returns>
      /// The fluent syntax.
      /// </returns>
      public IBindingToSyntax<object> Bind(params Type[] services)
      {
         return kernel.Bind(services);
      }

      /// <summary>
      /// Unregisters all bindings for the specified service.
      /// </summary>
      /// <typeparam name="T">The service to unbind.</typeparam>
      public void Unbind<T>()
      {
         kernel.Unbind<T>();
      }

      /// <summary>
      /// Unregisters all bindings for the specified service.
      /// </summary>
      /// <param name="service">The service to unbind.</param>
      public void Unbind(Type service)
      {
         kernel.Unbind(service);
      }

      /// <summary>
      /// Removes any existing bindings for the specified service, and declares a new one.
      /// </summary>
      /// <typeparam name="T1">The first service to re-bind.</typeparam>
      /// <returns>
      /// The fluent syntax.
      /// </returns>
      public IBindingToSyntax<T1> Rebind<T1>()
      {
         return kernel.Rebind<T1>();
      }

      /// <summary>
      /// Removes any existing bindings for the specified services, and declares a new one.
      /// </summary>
      /// <typeparam name="T1">The first service to re-bind.</typeparam><typeparam name="T2">The second service to re-bind.</typeparam>
      /// <returns>
      /// The fluent syntax.
      /// </returns>
      public IBindingToSyntax<T1, T2> Rebind<T1, T2>()
      {
         return kernel.Rebind<T1, T2>();
      }

      /// <summary>
      /// Removes any existing bindings for the specified services, and declares a new one.
      /// </summary>
      /// <typeparam name="T1">The first service to re-bind.</typeparam><typeparam name="T2">The second service to re-bind.</typeparam><typeparam name="T3">The third service to re-bind.</typeparam>
      /// <returns>
      /// The fluent syntax.
      /// </returns>
      public IBindingToSyntax<T1, T2, T3> Rebind<T1, T2, T3>()
      {
         return kernel.Rebind<T1, T2, T3>();
      }

      /// <summary>
      /// Removes any existing bindings for the specified services, and declares a new one.
      /// </summary>
      /// <typeparam name="T1">The first service to re-bind.</typeparam><typeparam name="T2">The second service to re-bind.</typeparam><typeparam name="T3">The third service to re-bind.</typeparam><typeparam name="T4">The fourth service to re-bind.</typeparam>
      /// <returns>
      /// The fluent syntax.
      /// </returns>
      public IBindingToSyntax<T1, T2, T3, T4> Rebind<T1, T2, T3, T4>()
      {
         return kernel.Rebind<T1, T2, T3, T4>();
      }

      /// <summary>
      /// Removes any existing bindings for the specified services, and declares a new one.
      /// </summary>
      /// <param name="services">The services to re-bind.</param>
      /// <returns>
      /// The fluent syntax.
      /// </returns>
      public IBindingToSyntax<object> Rebind(params Type[] services)
      {
         return kernel.Rebind(services);
      }

      /// <summary>
      /// Registers the specified binding.
      /// </summary>
      /// <param name="binding">The binding to add.</param>
      public void AddBinding(IBinding binding)
      {
         kernel.AddBinding(binding);
      }

      /// <summary>
      /// Unregisters the specified binding.
      /// </summary>
      /// <param name="binding">The binding to remove.</param>
      public void RemoveBinding(IBinding binding)
      {
         kernel.RemoveBinding(binding);
      }

      #endregion

      #region Implementation of IResolutionRoot

      /// <summary>
      /// Determines whether the specified request can be resolved.
      /// </summary>
      /// <param name="request">The request.</param>
      /// <returns>
      /// <c>True</c> if the request can be resolved; otherwise, <c>false</c>.
      /// </returns>
      public bool CanResolve(IRequest request)
      {
         return kernel.CanResolve(request);
      }

      /// <summary>
      /// Determines whether the specified request can be resolved.
      /// </summary>
      /// <param name="request">The request.</param><param name="ignoreImplicitBindings">if set to <c>true</c> implicit bindings are ignored.</param>
      /// <returns>
      /// <c>True</c> if the request can be resolved; otherwise, <c>false</c>.
      /// </returns>
      public bool CanResolve(IRequest request, bool ignoreImplicitBindings)
      {
         return kernel.CanResolve(request, ignoreImplicitBindings);
      }

      /// <summary>
      /// Resolves instances for the specified request. The instances are not actually resolved
      ///             until a consumer iterates over the enumerator.
      /// </summary>
      /// <param name="request">The request to resolve.</param>
      /// <returns>
      /// An enumerator of instances that match the request.
      /// </returns>
      public IEnumerable<object> Resolve(IRequest request)
      {
         return kernel.Resolve(request);
      }

      /// <summary>
      /// Creates a request for the specified service.
      /// </summary>
      /// <param name="service">The service that is being requested.</param><param name="constraint">The constraint to apply to the bindings to determine if they match the request.</param><param name="parameters">The parameters to pass to the resolution.</param><param name="isOptional"><c>True</c> if the request is optional; otherwise, <c>false</c>.</param><param name="isUnique"><c>True</c> if the request should return a unique result; otherwise, <c>false</c>.</param>
      /// <returns>
      /// The created request.
      /// </returns>
      public IRequest CreateRequest(Type service, Func<IBindingMetadata, bool> constraint,
         IEnumerable<IParameter> parameters, bool isOptional, bool isUnique)
      {
         return kernel.CreateRequest(service, constraint, parameters, isOptional, isUnique);
      }

      /// <summary>
      /// Deactivates and releases the specified instance if it is currently managed by Ninject.
      /// </summary>
      /// <param name="instance">The instance to release.</param>
      /// <returns>
      /// <see langword="True"/> if the instance was found and released; otherwise <see langword="false"/>.
      /// </returns>
      public bool Release(object instance)
      {
         return kernel.Release(instance);
      }

      #endregion

      #region Implementation of IServiceProvider

      /// <summary>
      /// Gets the service object of the specified type.
      /// </summary>
      /// <returns>
      /// A service object of type <paramref name="serviceType"/>.-or- null if there is no service object of type <paramref name="serviceType"/>.
      /// </returns>
      /// <param name="serviceType">An object that specifies the type of service object to get. </param>
      public object GetService(Type serviceType)
      {
         return kernel.GetService(serviceType);
      }

      #endregion

      #region Implementation of IDisposable

      /// <summary>
      /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
      /// </summary>
      public void Dispose()
      {
         kernel.Dispose();
      }

      #endregion

      #region Implementation of IDisposableObject

      /// <summary>
      /// Gets a value indicating whether this instance is disposed.
      /// </summary>
      public bool IsDisposed
      {
         get { return kernel.IsDisposed; }
      }

      #endregion

      #region Implementation of IKernel

      /// <summary>
      /// Gets the modules that have been loaded into the kernel.
      /// </summary>
      /// <returns>
      /// A series of loaded modules.
      /// </returns>
      public IEnumerable<INinjectModule> GetModules()
      {
         return kernel.GetModules();
      }

      /// <summary>
      /// Determines whether a module with the specified name has been loaded in the kernel.
      /// </summary>
      /// <param name="name">The name of the module.</param>
      /// <returns>
      /// <c>True</c> if the specified module has been loaded; otherwise, <c>false</c>.
      /// </returns>
      public bool HasModule(string name)
      {
         return kernel.HasModule(name);
      }

      /// <summary>
      /// Loads the module(s) into the kernel.
      /// </summary>
      /// <param name="m">The modules to load.</param>
      public void Load(IEnumerable<INinjectModule> m)
      {
         kernel.Load(m);
      }

      /// <summary>
      /// Loads modules from the files that match the specified pattern(s).
      /// </summary>
      /// <param name="filePatterns">The file patterns (i.e. "*.dll", "modules/*.rb") to match.</param>
      public void Load(IEnumerable<string> filePatterns)
      {
         kernel.Load(filePatterns);
      }

      /// <summary>
      /// Loads modules defined in the specified assemblies.
      /// </summary>
      /// <param name="assemblies">The assemblies to search.</param>
      public void Load(IEnumerable<Assembly> assemblies)
      {
         kernel.Load(assemblies);
      }

      /// <summary>
      /// Unloads the plugin with the specified name.
      /// </summary>
      /// <param name="name">The plugin's name.</param>
      public void Unload(string name)
      {
         kernel.Unload(name);
      }

      /// <summary>
      /// Injects the specified existing instance, without managing its lifecycle.
      /// </summary>
      /// <param name="instance">The instance to inject.</param><param name="parameters">The parameters to pass to the request.</param>
      public void Inject(object instance, params IParameter[] parameters)
      {
         kernel.Inject(instance, parameters);
      }

      /// <summary>
      /// Gets the bindings registered for the specified service.
      /// </summary>
      /// <param name="service">The service in question.</param>
      /// <returns>
      /// A series of bindings that are registered for the service.
      /// </returns>
      public IEnumerable<IBinding> GetBindings(Type service)
      {
         return kernel.GetBindings(service);
      }

      /// <summary>
      /// Begins a new activation block, which can be used to deterministically dispose resolved instances.
      /// </summary>
      /// <returns>
      /// The new activation block.
      /// </returns>
      public IActivationBlock BeginBlock()
      {
         return kernel.BeginBlock();
      }

      /// <summary>
      /// Gets the kernel settings.
      /// </summary>
      public INinjectSettings Settings
      {
         get { return kernel.Settings; }
      }

      /// <summary>
      /// Gets the component container, which holds components that contribute to Ninject.
      /// </summary>
      public IComponentContainer Components
      {
         get { return kernel.Components; }
      }

      #endregion

      #endregion
   }
}
