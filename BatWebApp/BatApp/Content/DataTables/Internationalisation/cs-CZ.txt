﻿{
   "sEmptyTable":     "V tabulce nejsou žádná data",
   "sInfo":           "Zobrazeno _START_ do _END_ z _TOTAL_ záznamů",
   "sInfoEmpty":      "Zobrazeno 0 do 0 z 0 záznamů",
   "sInfoFiltered":   "(filtruji z _MAX_ celkových záznamů)",
   "sInfoPostFix":    "",
   "sInfoThousands":  " ",
   "sLengthMenu":     "Zobrazit _MENU_ záznamů",
   "sLoadingRecords": "Načítání...",
   "sProcessing":     "Zpracování...",
   "sSearch":         "Hledat:",
   "sZeroRecords":    "Nebyly nalezeny žádné odpovídající záznamy",
   "oPaginate": {
      "sFirst":    "První",
      "sLast":     "Poslední",
      "sNext":     "Další",
      "sPrevious": "Předchozí"
   },
    "oAria": {
       "sSortAscending":  ": aktivovat třídění sloupců vzestupně",
       "sSortDescending": ": aktivovat třídění sloupců sestupně"
    }
}