﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace BatApp.Areas.UserAccounts.Models.Company
{
   public class CompanyViewModel
   {
      [HiddenInput(DisplayValue = false)]
      public int Id { get; set; }

      [Required]
      public string Name { get; set; }

      [HiddenInput(DisplayValue = false)]
      public string PreviousUrl { get; set; }
   }
}