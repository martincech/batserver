﻿using PagedList;

namespace BatApp.Areas.UserAccounts.Models.Company
{
   public class IndexViewModel
   {
      public int? UserCompany { get; set; }
      public string Search { get; set; }
      public IPagedList<CompanyViewModel> Companies { get; set; } 
   }
}