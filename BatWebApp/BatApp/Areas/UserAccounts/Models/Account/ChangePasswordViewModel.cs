using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace BatApp.Areas.UserAccounts.Models.Account
{
   public class ChangePasswordViewModel
   {
      [Required]
      [StringLength(100, ErrorMessageResourceType = typeof(Resources.Resources), ErrorMessageResourceName = "MinLength", MinimumLength = 6)]
      [DataType(DataType.Password)]
      [Display(ResourceType = typeof(Resources.Resources), Name = "NewPassword")]
      public string NewPassword { get; set; }

      [DataType(DataType.Password)]
      [Display(ResourceType = typeof(Resources.Resources), Name = "ConfirmNewPassword")]
      [System.ComponentModel.DataAnnotations.Compare("NewPassword", ErrorMessageResourceType = typeof(Resources.Resources), ErrorMessageResourceName = "PasswordConfirmNotMatch")]
      public string ConfirmPassword { get; set; }

      [Required]
      [DataType(DataType.Password)]
      [Display(ResourceType = typeof (Resources.Resources), Name = "CurrentPassword")]
      public string OldPassword { get; set; }

      [HiddenInput(DisplayValue = false)]
      public DateTime? LastChangedDate { get; set; }
   }
}