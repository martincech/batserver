﻿using System.Linq;
using System.Web.Mvc;
using BatApp.Areas.UserAccounts.Models.Company;
using BatApp.Controllers;
using BatApp.Helpers;
using PagedList;
using DataModel;

namespace BatApp.Areas.UserAccounts.Controllers
{
   [BatApp.Infrastructure.Filters.Authorize(Roles = Constants.ADMIN + ", " + Constants.COMPANY_ADMIN)]
   public class CompanyController : BaseController
   {
      //
      // GET: /Company/
      [Authorize(Roles = Constants.ADMIN)]
      public ViewResult Index(int? page, string search)
      {
         var model = new IndexViewModel();
         var user = CurrentUser;
         model.UserCompany = user.CompanyId;

         var comapanies = Context.Companies.Select(c => new CompanyViewModel {Id = c.Id, Name = c.Name}).ToList();
         if (!string.IsNullOrWhiteSpace(search))
         {
            comapanies =
               Context.Companies.Where(w => w.Name.Contains((search)))
                  .Select(c => new CompanyViewModel {Id = c.Id, Name = c.Name})
                  .ToList();
         }

         model.Search = search;
         model.Companies = new PagedList<CompanyViewModel>(comapanies, page ?? 1, 10);
         return View(model);
      }

      //
      // GET: /Company/Create
      [Authorize(Roles = Constants.ADMIN)]
      public ActionResult Create()
      {
         ViewBag.Create = true;
         return View("Edit", new CompanyViewModel());
      }

      //
      // POST: /Company/Create
      [HttpPost]
      public ActionResult Create(CompanyViewModel companyVm)
      {
         if (!ModelState.IsValid) return View("Edit", companyVm);

         if (Context.Companies.Any(c => c.Name == companyVm.Name))
         {
            ModelState.AddModelError("", Resources.Resources.CompanyExists);
            ViewBag.Create = true;
            return View("Edit",companyVm);
         }

         Context.CreateCompany(companyVm.Name);
         return RedirectToAction("Index");
      }

      public JsonNetResult CheckIfCompanyExists(string name)
      {
         if (Context.Companies.Any(x => x.Name == name))
         {
            return new JsonNetResult
            {
               Data = new
               {
                  error = Resources.Resources.CompanyExists
               }
            };
         }
         return new JsonNetResult
         {
            Data = new
            {
            }
         };
      }

      //
      // GET: /Company/Edit/5
      [Authorize(Roles = Constants.ADMIN + ", " + Constants.COMPANY_ADMIN)]
      public ActionResult Edit(int? id = null)
      {
         if (ViewBag.Create != true)
         {
            ViewBag.Create = false;
         }
         Company company;
         if (id == null)
         {
            var user = CurrentUser;
            if (user != null)
            {
               company = user.Company;
            }
            else
            {
               return HttpNotFound();
            }
         }
         else
         {
            if (User.IsInRole(Constants.ADMIN))
            {
               company = Context.Companies.FirstOrDefault(c => c.Id == id);
            }
            else
            {
               company = null;
            }
            if (company == null)
            {
               return HttpNotFound();
            }
         }

         return View(new CompanyViewModel { Id = company.Id, Name = company.Name, PreviousUrl = HttpContext.Request.UrlReferrer.AbsoluteUri });
      }

      //
      // POST: /Company/Edit
      [HttpPost]
      [Authorize(Roles = Constants.ADMIN + ", " + Constants.COMPANY_ADMIN)]
      public ActionResult Edit(CompanyViewModel companyVm)
      {
         if (!ModelState.IsValid)
         {
            return View(companyVm);
         }
         if (Context.Companies.Any(c => c.Name == companyVm.Name && c.Id != companyVm.Id))
         {
            ModelState.AddModelError("", Resources.Resources.CompanyExists);
            ViewBag.Create = false;
            return View(companyVm);
         }

         var company = Context.Companies.First(c => c.Id == companyVm.Id);
         company.Name = companyVm.Name;
         Context.SaveChanges();
         return User.IsInRole(Constants.ADMIN) 
            ? RedirectToAction("Index")
            : (ActionResult)Redirect(companyVm.PreviousUrl);
      }

      //
      // GET: /Company/Delete/5
      [Authorize(Roles = Constants.ADMIN)]
      public ActionResult Delete(int id = 0)
      {
         var company = Context.Companies.FirstOrDefault(c=> c.Id == id);
         if (company == null)
         {
            return HttpNotFound();
         }
         return View(new CompanyViewModel{Name = company.Name, Id = company.Id});
      }

      //
      // POST: /Company/Delete/5

      [HttpPost, ActionName("Delete")]
      public ActionResult DeleteConfirmed(int id)
      {
         Context.DeleteCompany(id);
         return RedirectToAction("Index");
      }
   }
}