﻿using System.Web.Mvc;

namespace BatApp.Areas.UserAccounts
{
   public class UserAccountsAreaRegistration : AreaRegistration
   {
      public override string AreaName
      {
         get { return "UserAccounts"; }
      }

      public override void RegisterArea(AreaRegistrationContext context)
      {
         context.MapRoute(
            "UserAccounts_default",
            "UserAccounts/{controller}/{action}/{id}",
            new {action = "Index", id = UrlParameter.Optional}
            );
      }
   }
}