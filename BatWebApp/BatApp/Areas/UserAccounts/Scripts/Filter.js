﻿var companyIndex = 0;

$("#SelectedCompanies").unbind("mousewheel DOMMouseScroll");
$("#SelectedCompanies").bind("mousewheel DOMMouseScroll", function (event) {

   if (IsScrollUp(event)) {
      if (companyIndex < $("#SelectedCompanies")[0].length - 1) {
         companyIndex++;
      }
   } else {
      if (companyIndex > 0) {
         companyIndex--;
      }
   }

   $("#SelectedCompanies")[0].selectedIndex = companyIndex;
   $("#SelectedCompanies").trigger("change");
   event.preventDefault();

   $.cookie("IsFilterBarShow", 1, { expires: 10 });
});


function IsScrollUp(event) {
   var up = false;
   if (!(typeof event.originalEvent.detail === "undefined" || event.originalEvent.detail === null)) {
      if (event.originalEvent.detail >= 0) {
         up = true;
      } else {
         up = false;
      }
   }

   if (!(typeof event.originalEvent.wheelDelta === "undefined" || event.originalEvent.wheelDelta === null)) {
      if (event.originalEvent.wheelDelta >= 0) {
         up = false;
      } else {
         up = true;
      }
   }
   return up;
}


$(document).ready(function () {
    if ($("#SelectedCompanies")[0]) {
        companyIndex = $("#SelectedCompanies")[0].selectedIndex;
    }

    if ($.cookie("IsFilterBarShow") == 1) {
      $("#filterBar").addClass("in");
   }
   
   $.cookie("IsFilterBarShow", 0, { expires: 10 });
});

