﻿var selectedScales;

//Check if value has hex number format
function IsHex(hex) {
   var number = parseInt(hex, 16);
   $("#scale-exist").css("display", "none");
   if (number.toString(16) === hex.toLowerCase()) {
      return number < parseInt("7FFFFFFF", 16);
   }
   return (number.toString(16) === hex.toLowerCase());
}

$("#company").change(function () {
    $.ajax({
        url: "/UserAccounts/Manage/GetCompanyScales/" + $(this).val(),
        method: "POST",
        success: function (data) {
            $("#SelectedScales").find('option').remove();
            $(data).each(function () {
               var name = this.Text;
               //if (IsHex(name)) {
               //   name = parseInt(name, 16);
               //}
                $("#SelectedScales").append($("<option />").val(this.Value).text(name));
            });
            $("#SelectedScales").val(selectedScales);
        }
    });
});

//$('form').submit(function() {
   
//});


$(".radioCheckBox").change(function (e) {
    $(".radioCheckBox").not(this).attr('checked', false);
    $(".normalCheckBox").attr("checked", false);

    $(".normalCheckBox").parent().hide();
    $("#scalesList").hide();
    if ($(this).hasClass("User")) {
        $(".normalCheckBox").parent().show();
        $("#scalesList").show();
    }

    if ($(this).hasClass("Admin")) {
        $("#companyRow").hide();
    } else {
        $("#companyRow").show();
    }

    $(this).attr("checked", true);
});

$('.radioCheckBox').click(
    function (e) {
        if (!$(this).is(":checked")) {
            e.preventDefault();
        }
    });


$(document).ready(function () {
    $("#roleRow").appendTo("#roleTable");
    $("#roleFooter").appendTo("#roleTable");
    //.css("display","none");
    if (!($("input:checked").length > 0)) {
        $("input:checkbox").first().attr("checked", true);
        $("input:checkbox").first().trigger("change");
    }

    if (!$("input:checked").hasClass("User")) {
        $("#scalesList").hide();
        $("#roleFooter").hide();
    }

    if ($("input:checked").hasClass("Admin")) {
        $("#companyRow").hide();
    }

   

    
    selectedScales = $('#SelectedScales').val();
    $("#company").trigger("change");
});

