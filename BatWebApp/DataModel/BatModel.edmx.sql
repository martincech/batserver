
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 07/04/2016 11:49:17
-- Generated from EDMX file: D:\Repos\BAT Server\BatWebApp\DataModel\BatModel.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [BatApp];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_UserClaim]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Claims] DROP CONSTRAINT [FK_UserClaim];
GO
IF OBJECT_ID(N'[dbo].[FK_UserLogin]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Logins] DROP CONSTRAINT [FK_UserLogin];
GO
IF OBJECT_ID(N'[dbo].[FK_UserRole_User]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[UserRole] DROP CONSTRAINT [FK_UserRole_User];
GO
IF OBJECT_ID(N'[dbo].[FK_UserRole_Role]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[UserRole] DROP CONSTRAINT [FK_UserRole_Role];
GO
IF OBJECT_ID(N'[dbo].[FK_UserUser]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Users] DROP CONSTRAINT [FK_UserUser];
GO
IF OBJECT_ID(N'[dbo].[FK_CompanyUser]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Users] DROP CONSTRAINT [FK_CompanyUser];
GO
IF OBJECT_ID(N'[dbo].[FK_UserFlock_User]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[UserFlock] DROP CONSTRAINT [FK_UserFlock_User];
GO
IF OBJECT_ID(N'[dbo].[FK_UserFlock_Flock]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[UserFlock] DROP CONSTRAINT [FK_UserFlock_Flock];
GO
IF OBJECT_ID(N'[dbo].[FK_GsmMessageSwitchOnTimes]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[SwitchOnTimes] DROP CONSTRAINT [FK_GsmMessageSwitchOnTimes];
GO
IF OBJECT_ID(N'[dbo].[FK_GsmMessageConfiguration]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GsmMessages] DROP CONSTRAINT [FK_GsmMessageConfiguration];
GO
IF OBJECT_ID(N'[dbo].[FK_CompanyConfigurationV2]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ConfigurationsV2] DROP CONSTRAINT [FK_CompanyConfigurationV2];
GO
IF OBJECT_ID(N'[dbo].[FK_CompanyCurve]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Curves] DROP CONSTRAINT [FK_CompanyCurve];
GO
IF OBJECT_ID(N'[dbo].[FK_CompanyScale]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Scales] DROP CONSTRAINT [FK_CompanyScale];
GO
IF OBJECT_ID(N'[dbo].[FK_ConfigurationV2FlockV2]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[FlockV2] DROP CONSTRAINT [FK_ConfigurationV2FlockV2];
GO
IF OBJECT_ID(N'[dbo].[FK_CurveCurveValue]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[CurveValues] DROP CONSTRAINT [FK_CurveCurveValue];
GO
IF OBJECT_ID(N'[dbo].[FK_CurveFemale]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[FlockV2] DROP CONSTRAINT [FK_CurveFemale];
GO
IF OBJECT_ID(N'[dbo].[FK_CurveMale]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[FlockV2] DROP CONSTRAINT [FK_CurveMale];
GO
IF OBJECT_ID(N'[dbo].[FK_FlockScale]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Flocks] DROP CONSTRAINT [FK_FlockScale];
GO
IF OBJECT_ID(N'[dbo].[FK_StatScale]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Stats] DROP CONSTRAINT [FK_StatScale];
GO
IF OBJECT_ID(N'[dbo].[FK_UserScale_Scale]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[UserScale] DROP CONSTRAINT [FK_UserScale_Scale];
GO
IF OBJECT_ID(N'[dbo].[FK_UserScale_User]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[UserScale] DROP CONSTRAINT [FK_UserScale_User];
GO
IF OBJECT_ID(N'[dbo].[FK_ConfigurationsV2Scale]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ConfigurationsV2] DROP CONSTRAINT [FK_ConfigurationsV2Scale];
GO
IF OBJECT_ID(N'[dbo].[FK_ScaleConfiguration]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Configurations] DROP CONSTRAINT [FK_ScaleConfiguration];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Flocks]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Flocks];
GO
IF OBJECT_ID(N'[dbo].[CurveValues]', 'U') IS NOT NULL
    DROP TABLE [dbo].[CurveValues];
GO
IF OBJECT_ID(N'[dbo].[Stats]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Stats];
GO
IF OBJECT_ID(N'[dbo].[Companies]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Companies];
GO
IF OBJECT_ID(N'[dbo].[Users]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Users];
GO
IF OBJECT_ID(N'[dbo].[Claims]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Claims];
GO
IF OBJECT_ID(N'[dbo].[Logins]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Logins];
GO
IF OBJECT_ID(N'[dbo].[Roles]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Roles];
GO
IF OBJECT_ID(N'[dbo].[Configurations]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Configurations];
GO
IF OBJECT_ID(N'[dbo].[SwitchOnTimes]', 'U') IS NOT NULL
    DROP TABLE [dbo].[SwitchOnTimes];
GO
IF OBJECT_ID(N'[dbo].[GsmMessages]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GsmMessages];
GO
IF OBJECT_ID(N'[dbo].[ConfigurationsV2]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ConfigurationsV2];
GO
IF OBJECT_ID(N'[dbo].[Curves]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Curves];
GO
IF OBJECT_ID(N'[dbo].[FlockV2]', 'U') IS NOT NULL
    DROP TABLE [dbo].[FlockV2];
GO
IF OBJECT_ID(N'[dbo].[Scales]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Scales];
GO
IF OBJECT_ID(N'[dbo].[UserRole]', 'U') IS NOT NULL
    DROP TABLE [dbo].[UserRole];
GO
IF OBJECT_ID(N'[dbo].[UserFlock]', 'U') IS NOT NULL
    DROP TABLE [dbo].[UserFlock];
GO
IF OBJECT_ID(N'[dbo].[UserScale]', 'U') IS NOT NULL
    DROP TABLE [dbo].[UserScale];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Flocks'
CREATE TABLE [dbo].[Flocks] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [StartDate] datetime  NOT NULL,
    [EndDate] datetime  NULL,
    [InitialAge] int  NOT NULL,
    [Note] nvarchar(max)  NULL,
    [ScaleId] int  NOT NULL,
    [Name] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'CurveValues'
CREATE TABLE [dbo].[CurveValues] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [CurveId] int  NOT NULL,
    [Day] int  NOT NULL,
    [Weight] decimal(18,3)  NOT NULL
);
GO

-- Creating table 'Stats'
CREATE TABLE [dbo].[Stats] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Day] int  NOT NULL,
    [Sex] int  NOT NULL,
    [Date] datetime  NOT NULL,
    [Count] int  NOT NULL,
    [Average] decimal(18,3)  NOT NULL,
    [Gain] decimal(18,3)  NOT NULL,
    [Sigma] decimal(18,3)  NOT NULL,
    [Cv] decimal(18,3)  NOT NULL,
    [Uni] decimal(18,3)  NOT NULL,
    [ScaleId] int  NOT NULL,
    [Temperature] decimal(18,3)  NULL,
    [CarbonDioxide] int  NULL,
    [Ammonia] int  NULL,
    [Humidity] decimal(18,3)  NULL
);
GO

-- Creating table 'Companies'
CREATE TABLE [dbo].[Companies] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'Users'
CREATE TABLE [dbo].[Users] (
    [Id] uniqueidentifier  NOT NULL,
    [UserName] nvarchar(256)  NOT NULL,
    [Email] nvarchar(256)  NULL,
    [EmailConfirmed] bit  NOT NULL,
    [PasswordHash] nvarchar(100)  NULL,
    [SecurityStamp] nvarchar(100)  NULL,
    [PhoneNumber] nvarchar(25)  NULL,
    [PhoneNumberConfirmed] bit  NOT NULL,
    [TwoFactorEnabled] bit  NOT NULL,
    [LockoutEndDateUtc] datetime  NULL,
    [LockoutEnabled] bit  NOT NULL,
    [AccessFailedCount] int  NOT NULL,
    [IsAnonymous] bit  NULL,
    [LastLoginDate] datetime  NULL,
    [ApplicationId] uniqueidentifier  NULL,
    [ParentId] uniqueidentifier  NULL,
    [CompanyId] int  NULL,
    [CreationDate] datetime  NOT NULL,
    [Comment] nvarchar(max)  NULL,
    [LastPasswordChangedDate] datetime  NULL
);
GO

-- Creating table 'Claims'
CREATE TABLE [dbo].[Claims] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [ClaimType] nvarchar(max)  NULL,
    [ClaimValue] nvarchar(max)  NULL,
    [UserId] uniqueidentifier  NOT NULL
);
GO

-- Creating table 'Logins'
CREATE TABLE [dbo].[Logins] (
    [LoginProvider] nvarchar(128)  NOT NULL,
    [ProviderKey] nvarchar(128)  NOT NULL,
    [UserId] uniqueidentifier  NOT NULL
);
GO

-- Creating table 'Roles'
CREATE TABLE [dbo].[Roles] (
    [Id] uniqueidentifier  NOT NULL,
    [Name] nvarchar(256)  NOT NULL
);
GO

-- Creating table 'Configurations'
CREATE TABLE [dbo].[Configurations] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Country_CountryCode] tinyint  NOT NULL,
    [Country_Language] int  NOT NULL,
    [Country_CodePage] int  NOT NULL,
    [Country_DateFormat] int  NOT NULL,
    [Country_DateSeparator1] nvarchar(max)  NOT NULL,
    [Country_DateSeparator2] nvarchar(max)  NOT NULL,
    [Country_TimeFormat] int  NOT NULL,
    [Country_TimeSeparator] nvarchar(max)  NOT NULL,
    [Country_DaylightSaving] int  NOT NULL,
    [Display_Contrast] tinyint  NOT NULL,
    [Display_Mode] int  NOT NULL,
    [Display_SavePower] bit  NOT NULL,
    [Display_Backlight_Duration] smallint  NOT NULL,
    [Display_Backlight_Intensity] tinyint  NOT NULL,
    [Display_Backlight_Mode] int  NOT NULL,
    [Password] nvarchar(max)  NOT NULL,
    [WeightUnit_Decimals] tinyint  NOT NULL,
    [WeightUnit_Division] smallint  NOT NULL,
    [WeightUnit_DivisionMax] smallint  NOT NULL,
    [WeightUnit_Range] int  NOT NULL,
    [WeightUnit_Units] int  NOT NULL,
    [WeightUnit_Capacity] int  NOT NULL,
    [DataPublication_SendAt] datetime  NOT NULL,
    [DataPublication_AcceleratePeriod] smallint  NOT NULL,
    [DataPublication_AccelerateFromDay] smallint  NOT NULL,
    [DataPublication_Period] smallint  NOT NULL,
    [DataPublication_StartFromDay] smallint  NOT NULL,
    [DataPublication_Interface] int  NOT NULL,
    [DataPublication_Password] nvarchar(max)  NOT NULL,
    [DataPublication_Url] nvarchar(max)  NOT NULL,
    [DataPublication_Username] nvarchar(max)  NOT NULL,
    [DataPublication_CellularData_Apn] nvarchar(max)  NOT NULL,
    [DataPublication_CellularData_Password] nvarchar(max)  NOT NULL,
    [DataPublication_CellularData_Username] nvarchar(max)  NOT NULL,
    [DataPublication_Ethernet_Dhcp] bit  NOT NULL,
    [DataPublication_Ethernet_Gateway] int  NOT NULL,
    [DataPublication_Ethernet_Ip] int  NOT NULL,
    [DataPublication_Ethernet_PrimaryDns] int  NOT NULL,
    [DataPublication_Ethernet_SecondaryDns] int  NOT NULL,
    [DataPublication_Ethernet_SubnetMask] int  NOT NULL,
    [Rs485Options_DacsOptions_Address] tinyint  NOT NULL,
    [Rs485Options_DacsOptions_DeviceAddress] tinyint  NOT NULL,
    [Rs485Options_DacsOptions_Version] int  NOT NULL,
    [Rs485Options_MegaviOptions_Address] tinyint  NOT NULL,
    [Rs485Options_MegaviOptions_Code] tinyint  NOT NULL,
    [Rs485Options_MegaviOptions_DeviceAddress] tinyint  NOT NULL,
    [Rs485Options_ModbusOptions_Address] tinyint  NOT NULL,
    [Rs485Options_ModbusOptions_BaudRate] int  NOT NULL,
    [Rs485Options_ModbusOptions_DataBits] tinyint  NOT NULL,
    [Rs485Options_ModbusOptions_DeviceAddress] tinyint  NOT NULL,
    [Rs485Options_ModbusOptions_Mode] int  NOT NULL,
    [Rs485Options_ModbusOptions_Parity] int  NOT NULL,
    [Rs485Options_ModbusOptions_ReplyDelay] smallint  NOT NULL,
    [Rs485Options_Enabled] bit  NOT NULL,
    [Rs485Options_Mode] int  NOT NULL,
    [Rs485Options_PhysicalInterfaceAddress] tinyint  NOT NULL,
    [WeighingConfiguration_Name] nvarchar(max)  NOT NULL,
    [WeighingConfiguration_MenuMask] int  NOT NULL,
    [WeighingConfiguration_InitialDay] smallint  NOT NULL,
    [WeighingConfiguration_GrowthCurveIndex] int  NOT NULL,
    [WeighingConfiguration_PredefinedIndex] int  NOT NULL,
    [WeighingConfiguration_DayStart] datetime  NOT NULL,
    [WeighingConfiguration_MaleInitialWeight] int  NOT NULL,
    [WeighingConfiguration_FemaleInitialWeight] int  NOT NULL,
    [WeighingConfiguration_AdjustTargetWeights] int  NOT NULL,
    [WeighingConfiguration_Sex] int  NOT NULL,
    [WeighingConfiguration_SexDifferentiation] int  NOT NULL,
    [WeighingConfiguration_Growth] int  NOT NULL,
    [WeighingConfiguration_Mode] int  NOT NULL,
    [WeighingConfiguration_Filter] tinyint  NOT NULL,
    [WeighingConfiguration_Stabilization_Time] tinyint  NOT NULL,
    [WeighingConfiguration_Stabilization_Range] tinyint  NOT NULL,
    [WeighingConfiguration_Step] int  NOT NULL,
    [WeighingConfiguration_MaleMargin_Above] tinyint  NOT NULL,
    [WeighingConfiguration_MaleMargin_Below] tinyint  NOT NULL,
    [WeighingConfiguration_FemaleMargin_Above] tinyint  NOT NULL,
    [WeighingConfiguration_FemaleMargin_Below] tinyint  NOT NULL,
    [WeighingConfiguration_ShortPeriod] tinyint  NOT NULL,
    [WeighingConfiguration_ShortType] int  NOT NULL,
    [WeighingConfiguration_UniformityRange] tinyint  NOT NULL,
    [WeighingConfiguration_Histogram_Step] smallint  NOT NULL,
    [WeighingConfiguration_Histogram_Range] tinyint  NOT NULL,
    [WeighingConfiguration_Histogram_Mode] int  NOT NULL,
    [WeighingConfiguration_Planning] bit  NOT NULL,
    [WeighingConfiguration_WeighingPlanIndex] int  NOT NULL,
    [ScaleId] int  NULL
);
GO

-- Creating table 'SwitchOnTimes'
CREATE TABLE [dbo].[SwitchOnTimes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [From] datetime  NOT NULL,
    [To] datetime  NOT NULL,
    [GsmMessageId] int  NOT NULL
);
GO

-- Creating table 'GsmMessages'
CREATE TABLE [dbo].[GsmMessages] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Commands_Enabled] bit  NOT NULL,
    [Commands_CheckPhoneNumber] bit  NOT NULL,
    [Commands_Expiration] smallint  NOT NULL,
    [EventMask] int  NOT NULL,
    [Mode] int  NOT NULL,
    [SwitchOnDuration] tinyint  NOT NULL,
    [SwitchOnPeriod] tinyint  NOT NULL,
    [Configuration_Id] int  NOT NULL
);
GO

-- Creating table 'ConfigurationsV2'
CREATE TABLE [dbo].[ConfigurationsV2] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NULL,
    [CompanyId] int  NULL,
    [Backlight] int  NOT NULL,
    [Statistics_HistogramRange] smallint  NOT NULL,
    [Statistics_UniformityRange] smallint  NOT NULL,
    [Gsm_Use] bit  NOT NULL,
    [Gsm_SendMidnight] bit  NOT NULL,
    [Gsm_SendRequest] bit  NOT NULL,
    [Gsm_CheckNumbers] bit  NOT NULL,
    [Gsm_Period1] smallint  NOT NULL,
    [Gsm_Period2] smallint  NOT NULL,
    [Gsm_Day1] smallint  NOT NULL,
    [Gsm_SendHour] smallint  NOT NULL,
    [Gsm_SendMin] smallint  NOT NULL,
    [Gsm_Number0] nvarchar(max)  NULL,
    [Gsm_Number1] nvarchar(max)  NULL,
    [Gsm_Number2] nvarchar(max)  NULL,
    [Gsm_Number3] nvarchar(max)  NULL,
    [Gsm_Number4] nvarchar(max)  NULL,
    [Rs485_Speed] int  NOT NULL,
    [Rs485_Parity] smallint  NOT NULL,
    [Rs485_Protocol] smallint  NOT NULL,
    [Rs485_ReplyDelay] smallint  NOT NULL,
    [Rs485_SilentInterval] smallint  NOT NULL,
    [ScaleConfig_MaleMarginAbove] smallint  NOT NULL,
    [ScaleConfig_MaleMarginBelow] int  NOT NULL,
    [ScaleConfig_FemaleMarginAbove] smallint  NOT NULL,
    [ScaleConfig_FemaleMarginBelow] smallint  NOT NULL,
    [ScaleConfig_Filter] smallint  NOT NULL,
    [ScaleConfig_Stabilization] float  NOT NULL,
    [ScaleConfig_StabilizationTime] smallint  NOT NULL,
    [ScaleConfig_GainAutomaticMode] bit  NOT NULL,
    [ScaleConfig_SaveUnpon] smallint  NOT NULL,
    [ScaleConfig_Units] smallint  NOT NULL,
    [Correction_UseCurve] bit  NOT NULL,
    [Correction_Day1] smallint  NOT NULL,
    [Correction_Day2] smallint  NOT NULL,
    [Correction_UniformityRange] float  NOT NULL,
    [ScaleId] int  NULL,
    [Active] bit  NOT NULL
);
GO

-- Creating table 'Curves'
CREATE TABLE [dbo].[Curves] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [CompanyId] int  NULL
);
GO

-- Creating table 'FlockV2'
CREATE TABLE [dbo].[FlockV2] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [UseCurves] bit  NOT NULL,
    [UseGender] bit  NOT NULL,
    [WeighFrom] int  NULL,
    [WeighTo] int  NULL,
    [ConfigurationV2_Id] int  NOT NULL,
    [CurveFemale_Id] int  NULL,
    [CurveMale_Id] int  NULL,
    [InitialFemale] float  NULL,
    [InitialMale] float  NULL,
    [Index] smallint  NOT NULL
);
GO

-- Creating table 'Scales'
CREATE TABLE [dbo].[Scales] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [SerialNumber] int  NOT NULL,
    [CompanyId] int  NULL,
    [PhoneNumber] nvarchar(25)  NULL,
    [FarmName] nvarchar(256)  NULL,
    [ConfigurationChanged] bit  NOT NULL
);
GO

-- Creating table 'UserRole'
CREATE TABLE [dbo].[UserRole] (
    [Users_Id] uniqueidentifier  NOT NULL,
    [Roles_Id] uniqueidentifier  NOT NULL
);
GO

-- Creating table 'UserFlock'
CREATE TABLE [dbo].[UserFlock] (
    [UserFlock_Flock_Id] uniqueidentifier  NOT NULL,
    [SelectedFlocks_Id] int  NOT NULL
);
GO

-- Creating table 'UserScale'
CREATE TABLE [dbo].[UserScale] (
    [Scales_Id] int  NOT NULL,
    [Users_Id] uniqueidentifier  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'Flocks'
ALTER TABLE [dbo].[Flocks]
ADD CONSTRAINT [PK_Flocks]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'CurveValues'
ALTER TABLE [dbo].[CurveValues]
ADD CONSTRAINT [PK_CurveValues]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Stats'
ALTER TABLE [dbo].[Stats]
ADD CONSTRAINT [PK_Stats]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Companies'
ALTER TABLE [dbo].[Companies]
ADD CONSTRAINT [PK_Companies]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Users'
ALTER TABLE [dbo].[Users]
ADD CONSTRAINT [PK_Users]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Claims'
ALTER TABLE [dbo].[Claims]
ADD CONSTRAINT [PK_Claims]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [LoginProvider], [ProviderKey], [UserId] in table 'Logins'
ALTER TABLE [dbo].[Logins]
ADD CONSTRAINT [PK_Logins]
    PRIMARY KEY CLUSTERED ([LoginProvider], [ProviderKey], [UserId] ASC);
GO

-- Creating primary key on [Id] in table 'Roles'
ALTER TABLE [dbo].[Roles]
ADD CONSTRAINT [PK_Roles]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Configurations'
ALTER TABLE [dbo].[Configurations]
ADD CONSTRAINT [PK_Configurations]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'SwitchOnTimes'
ALTER TABLE [dbo].[SwitchOnTimes]
ADD CONSTRAINT [PK_SwitchOnTimes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'GsmMessages'
ALTER TABLE [dbo].[GsmMessages]
ADD CONSTRAINT [PK_GsmMessages]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ConfigurationsV2'
ALTER TABLE [dbo].[ConfigurationsV2]
ADD CONSTRAINT [PK_ConfigurationsV2]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Curves'
ALTER TABLE [dbo].[Curves]
ADD CONSTRAINT [PK_Curves]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'FlockV2'
ALTER TABLE [dbo].[FlockV2]
ADD CONSTRAINT [PK_FlockV2]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Scales'
ALTER TABLE [dbo].[Scales]
ADD CONSTRAINT [PK_Scales]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Users_Id], [Roles_Id] in table 'UserRole'
ALTER TABLE [dbo].[UserRole]
ADD CONSTRAINT [PK_UserRole]
    PRIMARY KEY CLUSTERED ([Users_Id], [Roles_Id] ASC);
GO

-- Creating primary key on [UserFlock_Flock_Id], [SelectedFlocks_Id] in table 'UserFlock'
ALTER TABLE [dbo].[UserFlock]
ADD CONSTRAINT [PK_UserFlock]
    PRIMARY KEY CLUSTERED ([UserFlock_Flock_Id], [SelectedFlocks_Id] ASC);
GO

-- Creating primary key on [Scales_Id], [Users_Id] in table 'UserScale'
ALTER TABLE [dbo].[UserScale]
ADD CONSTRAINT [PK_UserScale]
    PRIMARY KEY CLUSTERED ([Scales_Id], [Users_Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [UserId] in table 'Claims'
ALTER TABLE [dbo].[Claims]
ADD CONSTRAINT [FK_UserClaim]
    FOREIGN KEY ([UserId])
    REFERENCES [dbo].[Users]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_UserClaim'
CREATE INDEX [IX_FK_UserClaim]
ON [dbo].[Claims]
    ([UserId]);
GO

-- Creating foreign key on [UserId] in table 'Logins'
ALTER TABLE [dbo].[Logins]
ADD CONSTRAINT [FK_UserLogin]
    FOREIGN KEY ([UserId])
    REFERENCES [dbo].[Users]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_UserLogin'
CREATE INDEX [IX_FK_UserLogin]
ON [dbo].[Logins]
    ([UserId]);
GO

-- Creating foreign key on [Users_Id] in table 'UserRole'
ALTER TABLE [dbo].[UserRole]
ADD CONSTRAINT [FK_UserRole_User]
    FOREIGN KEY ([Users_Id])
    REFERENCES [dbo].[Users]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Roles_Id] in table 'UserRole'
ALTER TABLE [dbo].[UserRole]
ADD CONSTRAINT [FK_UserRole_Role]
    FOREIGN KEY ([Roles_Id])
    REFERENCES [dbo].[Roles]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_UserRole_Role'
CREATE INDEX [IX_FK_UserRole_Role]
ON [dbo].[UserRole]
    ([Roles_Id]);
GO

-- Creating foreign key on [ParentId] in table 'Users'
ALTER TABLE [dbo].[Users]
ADD CONSTRAINT [FK_UserUser]
    FOREIGN KEY ([ParentId])
    REFERENCES [dbo].[Users]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_UserUser'
CREATE INDEX [IX_FK_UserUser]
ON [dbo].[Users]
    ([ParentId]);
GO

-- Creating foreign key on [CompanyId] in table 'Users'
ALTER TABLE [dbo].[Users]
ADD CONSTRAINT [FK_CompanyUser]
    FOREIGN KEY ([CompanyId])
    REFERENCES [dbo].[Companies]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_CompanyUser'
CREATE INDEX [IX_FK_CompanyUser]
ON [dbo].[Users]
    ([CompanyId]);
GO

-- Creating foreign key on [UserFlock_Flock_Id] in table 'UserFlock'
ALTER TABLE [dbo].[UserFlock]
ADD CONSTRAINT [FK_UserFlock_User]
    FOREIGN KEY ([UserFlock_Flock_Id])
    REFERENCES [dbo].[Users]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [SelectedFlocks_Id] in table 'UserFlock'
ALTER TABLE [dbo].[UserFlock]
ADD CONSTRAINT [FK_UserFlock_Flock]
    FOREIGN KEY ([SelectedFlocks_Id])
    REFERENCES [dbo].[Flocks]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_UserFlock_Flock'
CREATE INDEX [IX_FK_UserFlock_Flock]
ON [dbo].[UserFlock]
    ([SelectedFlocks_Id]);
GO

-- Creating foreign key on [GsmMessageId] in table 'SwitchOnTimes'
ALTER TABLE [dbo].[SwitchOnTimes]
ADD CONSTRAINT [FK_GsmMessageSwitchOnTimes]
    FOREIGN KEY ([GsmMessageId])
    REFERENCES [dbo].[GsmMessages]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GsmMessageSwitchOnTimes'
CREATE INDEX [IX_FK_GsmMessageSwitchOnTimes]
ON [dbo].[SwitchOnTimes]
    ([GsmMessageId]);
GO

-- Creating foreign key on [Configuration_Id] in table 'GsmMessages'
ALTER TABLE [dbo].[GsmMessages]
ADD CONSTRAINT [FK_GsmMessageConfiguration]
    FOREIGN KEY ([Configuration_Id])
    REFERENCES [dbo].[Configurations]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GsmMessageConfiguration'
CREATE INDEX [IX_FK_GsmMessageConfiguration]
ON [dbo].[GsmMessages]
    ([Configuration_Id]);
GO

-- Creating foreign key on [CompanyId] in table 'ConfigurationsV2'
ALTER TABLE [dbo].[ConfigurationsV2]
ADD CONSTRAINT [FK_CompanyConfigurationV2]
    FOREIGN KEY ([CompanyId])
    REFERENCES [dbo].[Companies]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_CompanyConfigurationV2'
CREATE INDEX [IX_FK_CompanyConfigurationV2]
ON [dbo].[ConfigurationsV2]
    ([CompanyId]);
GO

-- Creating foreign key on [CompanyId] in table 'Curves'
ALTER TABLE [dbo].[Curves]
ADD CONSTRAINT [FK_CompanyCurve]
    FOREIGN KEY ([CompanyId])
    REFERENCES [dbo].[Companies]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_CompanyCurve'
CREATE INDEX [IX_FK_CompanyCurve]
ON [dbo].[Curves]
    ([CompanyId]);
GO

-- Creating foreign key on [CompanyId] in table 'Scales'
ALTER TABLE [dbo].[Scales]
ADD CONSTRAINT [FK_CompanyScale]
    FOREIGN KEY ([CompanyId])
    REFERENCES [dbo].[Companies]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_CompanyScale'
CREATE INDEX [IX_FK_CompanyScale]
ON [dbo].[Scales]
    ([CompanyId]);
GO

-- Creating foreign key on [ConfigurationV2_Id] in table 'FlockV2'
ALTER TABLE [dbo].[FlockV2]
ADD CONSTRAINT [FK_ConfigurationV2FlockV2]
    FOREIGN KEY ([ConfigurationV2_Id])
    REFERENCES [dbo].[ConfigurationsV2]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ConfigurationV2FlockV2'
CREATE INDEX [IX_FK_ConfigurationV2FlockV2]
ON [dbo].[FlockV2]
    ([ConfigurationV2_Id]);
GO

-- Creating foreign key on [CurveId] in table 'CurveValues'
ALTER TABLE [dbo].[CurveValues]
ADD CONSTRAINT [FK_CurveCurveValue]
    FOREIGN KEY ([CurveId])
    REFERENCES [dbo].[Curves]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_CurveCurveValue'
CREATE INDEX [IX_FK_CurveCurveValue]
ON [dbo].[CurveValues]
    ([CurveId]);
GO

-- Creating foreign key on [CurveFemale_Id] in table 'FlockV2'
ALTER TABLE [dbo].[FlockV2]
ADD CONSTRAINT [FK_CurveFemale]
    FOREIGN KEY ([CurveFemale_Id])
    REFERENCES [dbo].[Curves]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_CurveFemale'
CREATE INDEX [IX_FK_CurveFemale]
ON [dbo].[FlockV2]
    ([CurveFemale_Id]);
GO

-- Creating foreign key on [CurveMale_Id] in table 'FlockV2'
ALTER TABLE [dbo].[FlockV2]
ADD CONSTRAINT [FK_CurveMale]
    FOREIGN KEY ([CurveMale_Id])
    REFERENCES [dbo].[Curves]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_CurveMale'
CREATE INDEX [IX_FK_CurveMale]
ON [dbo].[FlockV2]
    ([CurveMale_Id]);
GO

-- Creating foreign key on [ScaleId] in table 'Flocks'
ALTER TABLE [dbo].[Flocks]
ADD CONSTRAINT [FK_FlockScale]
    FOREIGN KEY ([ScaleId])
    REFERENCES [dbo].[Scales]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_FlockScale'
CREATE INDEX [IX_FK_FlockScale]
ON [dbo].[Flocks]
    ([ScaleId]);
GO

-- Creating foreign key on [ScaleId] in table 'Stats'
ALTER TABLE [dbo].[Stats]
ADD CONSTRAINT [FK_StatScale]
    FOREIGN KEY ([ScaleId])
    REFERENCES [dbo].[Scales]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_StatScale'
CREATE INDEX [IX_FK_StatScale]
ON [dbo].[Stats]
    ([ScaleId]);
GO

-- Creating foreign key on [Scales_Id] in table 'UserScale'
ALTER TABLE [dbo].[UserScale]
ADD CONSTRAINT [FK_UserScale_Scale]
    FOREIGN KEY ([Scales_Id])
    REFERENCES [dbo].[Scales]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Users_Id] in table 'UserScale'
ALTER TABLE [dbo].[UserScale]
ADD CONSTRAINT [FK_UserScale_User]
    FOREIGN KEY ([Users_Id])
    REFERENCES [dbo].[Users]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_UserScale_User'
CREATE INDEX [IX_FK_UserScale_User]
ON [dbo].[UserScale]
    ([Users_Id]);
GO

-- Creating foreign key on [ScaleId] in table 'ConfigurationsV2'
ALTER TABLE [dbo].[ConfigurationsV2]
ADD CONSTRAINT [FK_ConfigurationsV2Scale]
    FOREIGN KEY ([ScaleId])
    REFERENCES [dbo].[Scales]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ConfigurationsV2Scale'
CREATE INDEX [IX_FK_ConfigurationsV2Scale]
ON [dbo].[ConfigurationsV2]
    ([ScaleId]);
GO

-- Creating foreign key on [ScaleId] in table 'Configurations'
ALTER TABLE [dbo].[Configurations]
ADD CONSTRAINT [FK_ScaleConfiguration]
    FOREIGN KEY ([ScaleId])
    REFERENCES [dbo].[Scales]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ScaleConfiguration'
CREATE INDEX [IX_FK_ScaleConfiguration]
ON [dbo].[Configurations]
    ([ScaleId]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------