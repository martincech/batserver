﻿USE [BatApp];
GO
MERGE INTO [dbo].[UserRole] AS Target
USING (VALUES
    (N'34175b51-189f-4cbf-a9d9-27adef3c3789', N'7fd171ea-ccea-470d-9422-a83cd827ee22')
) AS Source ([Users_Id], [Roles_Id])
ON Target.[Users_Id] = Source.[Users_Id] AND Target.[Roles_Id] = Source.[Roles_Id]
WHEN NOT MATCHED BY TARGET THEN
-- Insert new rows
INSERT ([Users_Id], [Roles_Id])
VALUES ([Users_Id], [Roles_Id]);
GO