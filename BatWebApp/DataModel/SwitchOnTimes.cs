//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
namespace DataModel
{
    
   
   using System;
   using Newtonsoft.Json;
   
   public partial class SwitchOnTimes
   {
      #region Constructors
   
      
      
      public SwitchOnTimes()
      {
         AditionalConstructor();
      }
   
      partial void AditionalConstructor();
   
      #endregion
   
      #region Primary keys
   
      /// <summary>
      /// This is primary key property!
      /// </summary>
      public virtual int Id { get; set; }
   
      #endregion
      public virtual System.DateTime From { get; set; }
      public virtual System.DateTime To { get; set; }
      public virtual int GsmMessageId { get; set; }
   
      public virtual GsmMessage GsmMessage { get; set; }
   }
}
