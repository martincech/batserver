﻿namespace DataModel
{
   public static class Constants
   {
      /// <summary>
      /// Users names
      /// </summary>
      public const string ADMIN = "Admin";
      public const string COMPANY_ADMIN = "CompanyAdmin";
      public const string USER = "User";

      /// <summary>
      /// Privileges names
      /// </summary>
      //public const string CAN_DELETE_WEIGHINGS = "CanDeleteWeighings";
      //public const string CAN_IMPORT_DATA = "CanImportData";
      public const string FLOCK_MANAGEMENT = "FlockManagement";
   }
}