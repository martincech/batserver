﻿using System.Windows;
using Bat2WebTransfer.ModelViews.Applications;
using Bat2WebTransfer.ModelViews.Presentation;

namespace Bat2WebTransfer
{
   /// <summary>
   /// Interaction logic for App.xaml
   /// </summary>
   public partial class App : Application
   {
      protected override void OnStartup(StartupEventArgs e)
      {
         base.OnStartup(e);
       
         // Show Main Window
         MainWindow = new MainWindow {DataContext = new MainWindowViewModel()};
         MainWindow.Show();
      }
   }
}
