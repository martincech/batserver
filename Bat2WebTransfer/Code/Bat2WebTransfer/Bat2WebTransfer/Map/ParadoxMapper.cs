﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bat2Library.Bat2Old.DB;
using Bat2Library.Bat2Old.DB.Enums;
using Bat2Library.Bat2Old.DB.Helpers;
using ParadoxReader.Db;

namespace Bat2WebTransfer.Map
{
   public class ParadoxMapper
   {   
      private readonly string _path;
      private const int InvalidFlockNumber = 255;  // flock with number 255 is invalid

      private readonly string _females = "_" + Properties.Resources.Females;
      private readonly string _males = "_" + Properties.Resources.Males;

      public ParadoxMapper(string path)
      {        
         _path = path;
      }

      #region Curve

      public IEnumerable<GrowthCurve> CurveMap(string tableName)
      {
         try
         {
            var table = new Table(_path, tableName);
            return table.Enumerate().Select(rec => new GrowthCurve
            {
               Name = Convert<string>(rec.DataValues[(int)GrowthCurveE.NAME]),
               Records = FillCurveData(rec.DataValues, (int)GrowthCurveE.DAY00, (int)GrowthCurveE.DAY29)
            }).ToList();
         }
         catch (Exception)
         {  // file is not paradox db file
            return null;
         }
      }

      /// <summary>
      /// Get curves from flocks.
      /// </summary>
      /// <param name="flocks">flocks</param>
      /// <returns>collection of curves</returns>
      public IEnumerable<GrowthCurve> FlockCurves(IEnumerable<SetupF> flocks)
      {
         var data = flocks.ToList();
         var curves = new List<GrowthCurve>();
         foreach (var f in data)
         {
            if (f.FemaleRecords != null)
            {
               var c = new GrowthCurve();
               var name = SetupF.GetName(f);
               c.Name = f.UseGender ? name + _females : name;
               c.Records = f.FemaleRecords;
               curves.Add(c);
            }
            if (f.MaleRecords != null)
            {
               var c = new GrowthCurve();
               var name = SetupF.GetName(f);
               c.Name = name + _males;
               c.Records = f.MaleRecords;
               curves.Add(c);
            }
         }

         return curves.Count > 0 ? curves : null;
      }

      #endregion

      #region Flock

      public IEnumerable<SetupF> SetupFMap(string tableName)
      {
         try
         {
            var table = new Table(_path, tableName);
            return (from rec in table.Enumerate()
               let number = Convert<short>(rec.DataValues[(int) SetupFE.NUMBER])
               where number != InvalidFlockNumber
               select new SetupF
               {
                  Set = Convert<string>(rec.DataValues[(int)SetupFE.SET]),
                  Number = number,
                  Name = Convert<string>(rec.DataValues[(int)SetupFE.NAME]),
                  UseCurves = Convert<bool>(rec.DataValues[(int)SetupFE.USE_CURVES]),
                  UseGender = Convert<bool>(rec.DataValues[(int)SetupFE.USE_GENDER]),
                  WeighFrom = Convert<short>(rec.DataValues[(int)SetupFE.WEIGH_FROM]),
                  WeighTo = Convert<short>(rec.DataValues[(int)SetupFE.WEIGH_TO]),
                  FemaleRecords = FillCurveData(rec.DataValues, (int)SetupFE.FEMALE_DAY00, (int)SetupFE.FEMALE_DAY29),
                  MaleRecords = FillCurveData(rec.DataValues, (int)SetupFE.MALE_DAY00, (int)SetupFE.MALE_DAY29)
               }).ToList();
         }
         catch (Exception)
         {  // file is not paradox db file
            return null;
         }
      }

      public IEnumerable<SetupF> FlockMap(string tableName)
      {
         try
         {
            var table = new Table(_path, tableName);
            return (from rec in table.Enumerate()
               let number = Convert<short>(rec.DataValues[(int) FlockE.NUMBER])
               where number != InvalidFlockNumber
               select new SetupF
               {
                  Number = number, 
                  Name = Convert<string>(rec.DataValues[(int) FlockE.NAME]), 
                  UseCurves = Convert<bool>(rec.DataValues[(int) FlockE.USE_CURVES]), 
                  UseGender = Convert<bool>(rec.DataValues[(int) FlockE.USE_GENDER]), 
                  WeighFrom = Convert<short>(rec.DataValues[(int) FlockE.WEIGH_FROM]), 
                  WeighTo = Convert<short>(rec.DataValues[(int) FlockE.WEIGH_TO]), 
                  FemaleRecords = FillCurveData(rec.DataValues, (int) FlockE.FEMALE_DAY00, (int) FlockE.FEMALE_DAY29), 
                  MaleRecords = FillCurveData(rec.DataValues, (int) FlockE.MALE_DAY00, (int) FlockE.MALE_DAY29)
               }).ToList();
         }
         catch (Exception)
         {  // file is not paradox db file
            return null;
         }
      }

      /// <summary>
      /// Create flocks from statistic (sms) data.
      /// </summary>
      /// <param name="sms">statistics</param>
      /// <returns>collection of flocks</returns>
      public IEnumerable<DataFlock> DataFlockMap(IEnumerable<Sms> sms)
      {
         var data = sms.OrderBy(x => x.Id).ThenBy(x => x.DayDate).ToList();
         var flocks = new List<DataFlock>();
         if (!data.Any()) return null;

         var scale = data.First().Id;
         var startDate = data.First().DayDate;
         var dayNumber = data.First().DayNumber;

         int i;
         for (i = 1; i < data.Count; i++)
         {
            if (data[i].Id != scale || dayNumber >= data[i].DayNumber)
            {
               //save flock
               flocks.Add(CreateFlock(startDate, data[i - 1].DayDate, scale));
              
               scale = data[i].Id;
               startDate = data[i].DayDate;
               dayNumber = data[i].DayNumber;
            }
         }
         //save last flock
         flocks.Add(CreateFlock(startDate, data[i - 1].DayDate, scale));
         return flocks;
      }

      /// <summary>
      /// Create new flock from data.
      /// </summary>
      /// <param name="startDate"></param>
      /// <param name="lastDate"></param>
      /// <param name="scale"></param>
      /// <returns></returns>
      private DataFlock CreateFlock(DateTime startDate, DateTime lastDate, int scale)
      {
         const int activeFlockDayTolerance = 14;
         DateTime? end = null;
         var diff = DateTime.Now.Subtract(lastDate).TotalDays;
         if ((int) diff > activeFlockDayTolerance)
         {  // close flock if last date is older than 2 weeks
            end = lastDate;
         }

         return new DataFlock
         {
            Name = DataFlock.GetFlockName(startDate, scale),
            StartDate = startDate,
            EndDate = end,
            Scale = scale
         };
      }

      #endregion

      #region Sms

      public IEnumerable<Sms> SmsMap(string tableName)
      {
         try
         {     
            var table = new Table(_path, tableName);
            var list = new List<Sms>();
            foreach (var rec in table.Enumerate())
            {
               var sms = new Sms
               {
                  GsmNumber = Convert<string>(rec.DataValues[(int) SmsE.GSM_NUMBER]),
                  InsertDateTime = Convert<DateTime>(rec.DataValues[(int) SmsE.INSERT_DATE_TIME]),
                  EditDateTime = Convert<DateTime>(rec.DataValues[(int) SmsE.EDIT_DATE_TIME]),
                  Id = Convert<short>(rec.DataValues[(int) SmsE.ID]),
                  DayNumber = Convert<short>(rec.DataValues[(int) SmsE.DAY_NUMBER]),
                  DayDate = Convert<DateTime>(rec.DataValues[(int) SmsE.DAY_DATE]),
                  Note = Convert<string>(rec.DataValues[(int)SmsE.NOTE])
               };
               if (!(rec.DataValues[(int) SmsE.FEMALE_COUNT] is DBNull))
               {
                  sms.FemaleStat = new Statistic
                  {
                     Count = Convert<short>(rec.DataValues[(int) SmsE.FEMALE_COUNT]),
                     Average = Convert<double>(rec.DataValues[(int) SmsE.FEMALE_AVERAGE]),
                     Gain = Convert<double>(rec.DataValues[(int) SmsE.FEMALE_GAIN]),
                     Sigma = Convert<double>(rec.DataValues[(int) SmsE.FEMALE_SIGMA]),
                     Cv = Convert<short>(rec.DataValues[(int) SmsE.FEMALE_CV]),
                     Uni = Convert<short>(rec.DataValues[(int) SmsE.FEMALE_UNI])
                  };
               }
               if (!(rec.DataValues[(int)SmsE.MALE_COUNT] is DBNull))
               {
                  sms.MaleStat = new Statistic
                  {
                     Count = Convert<short>(rec.DataValues[(int)SmsE.MALE_COUNT]),
                     Average = Convert<double>(rec.DataValues[(int)SmsE.MALE_AVERAGE]),
                     Gain = Convert<double>(rec.DataValues[(int)SmsE.MALE_GAIN]),
                     Sigma = Convert<double>(rec.DataValues[(int)SmsE.MALE_SIGMA]),
                     Cv = Convert<short>(rec.DataValues[(int)SmsE.MALE_CV]),
                     Uni = Convert<short>(rec.DataValues[(int)SmsE.MALE_UNI])
                  };
               }
               list.Add(sms);
            }
            return list;
         }
         catch (Exception)
         {  // file is not paradox db file
            return null;
         }
      }

      #endregion

      #region Private helpers

      /// <summary>
      /// Fill curve data.
      /// </summary>
      /// <param name="dataValues">array of data</param>
      /// <param name="indexFrom">start index at dataValues</param>
      /// <param name="indexTo">last index at dataValues</param>
      /// <returns>Dictionary of curve data</returns>
      private IDictionary<int, double> FillCurveData(object[] dataValues, int indexFrom, int indexTo)
      {
         if (dataValues[indexFrom] is DBNull) return null;
         
         var records = new Dictionary<int, double>();
         for (var i = indexFrom; i <= indexTo; i++)
         {
            if (dataValues[i] is DBNull) break; // if any item is null, others are all null

            var key = Convert<int>(dataValues[i]);
            if (records.ContainsKey(key))
            {
               i++;
               continue;
            }

            records.Add(
               key,
               Convert<double>(dataValues[++i])
               );
         }

         if (records.Count == 1 && records.First().Key == 0 && Math.Abs(records.First().Value) < 0.00001)
            return null;

         return records;
      }

      /// <summary>
      /// Convert object to request type.
      /// </summary>
      /// <typeparam name="T">type</typeparam>
      /// <param name="o">object to convert</param>
      /// <returns>converted value</returns>
      private T Convert<T>(object o)
      {
         if (o is DBNull)
         {
            return default(T);
         }
         return (T)o;
      }

      #endregion
   }
}
